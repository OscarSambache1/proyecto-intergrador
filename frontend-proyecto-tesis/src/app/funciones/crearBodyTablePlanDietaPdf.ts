
import { ComidaDieta } from '../interfaces/comidas-dieta.ineterface';
import { auto } from 'async';

export function generarBodyTablePlanDieta(tiposComida, planDietaSeleccionado) {
  const arregloComidasDiaTemporal = tiposComida.map(tipoComida => {
    const arregloComidaDia = planDietaSeleccionado.planesDietaDia.map((dietaDia) => {
      const arregloComidasDieta = dietaDia.dieta.comidasDieta.filter((comidaDieta: ComidaDieta) => {
        return comidaDieta.comida.tipo.nombre === tipoComida.nombre;
      });
      const comidaDieta = arregloComidasDieta[0];
      let comida;
      if (comidaDieta) {
        comida = comidaDieta.comida.descripcion
      } else {
        comida = '';
      }
      const comidaPorDia = {
        comida,
        dia: dietaDia.dia,
      }
      return comidaPorDia;
    });
    return { arregloComidaDia, tipoComida: tipoComida.nombre };
  })

  const arregloComidaSemana = arregloComidasDiaTemporal.map((comidasDia) => {
    const arregloComidasTablaImprimir = [];
    arregloComidasTablaImprimir[0] = { text: comidasDia.tipoComida, fontSize: 12 };
    comidasDia.arregloComidaDia.forEach(comidaDia => {
      arregloComidasTablaImprimir[comidaDia.dia.id] = { text: comidaDia.comida, fontSize: 8 }
    });
    return arregloComidasTablaImprimir.filter(function () { return true })
  });

  const arregloCabcerasTabla = planDietaSeleccionado.planesDietaDia.map(planDieta => {
    const columna = {
      text: planDieta.dia.nombre,
      style: 'tableHeader',
      alignment: 'center',
    }
    return columna
  });
  arregloCabcerasTabla.unshift(
    {
      text: 'Tipo comida',
      style: 'tableHeader',
      alignment: 'center',
    },
  );
  arregloComidaSemana.unshift(arregloCabcerasTabla);

  const arregloWidths = arregloCabcerasTabla.map(elemento => {
    return auto;
  });
  arregloWidths[0]= 75;


  const content = [
    { text: planDietaSeleccionado.nombre, style: 'header', alignment: 'center' },
    {
      table: {
        widths: arregloWidths,
        body: arregloComidaSemana
      }
    },
  ]
  return content;
}
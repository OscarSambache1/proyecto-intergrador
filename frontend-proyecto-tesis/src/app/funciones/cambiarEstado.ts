export function cambiarEstado(registro: {estado: string}): string {
    const estaActivo = registro.estado == 'activo';
    estaActivo ? registro.estado = 'inactivo' : 'activo';
    !estaActivo ? registro.estado = 'activo' : 'inactivo';
    return registro.estado;
  }
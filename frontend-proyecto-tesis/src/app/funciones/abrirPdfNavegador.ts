import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export function abrirPdfNavegador(content, styles, pageOrientation?) {

    var docDefinition = {
      pageOrientation,
      content,
      styles,
    };
    pdfMake.createPdf(docDefinition).open();
  }
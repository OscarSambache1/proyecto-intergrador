import { MenuItem } from "primeng/api";

export function cargarRutas(modulo: string, accion: string, labelListar: string): MenuItem[]{

    return [
        {
            label: labelListar,
            routerLink: ['/app', modulo,'listar']
        },
        {
            label: accion,
        },
    ]
}
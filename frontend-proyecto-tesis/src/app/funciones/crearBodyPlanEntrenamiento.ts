export function crearBodyTablePlanEntrenamiento (dias, planEntrenamientoSeleccionado) {
    const rutinasSemana = dias.map(dia => {
        const rutinas = planEntrenamientoSeleccionado.rutinasPlanEntrenamiento.map(
            rutinaDia => {
                if (dia.nombre === rutinaDia.dia.nombre) {
                    const ejerciciosDia = rutinaDia.rutina.ejerciciosRutina.map(ejercicioDia => {
                        const ejercicioRutina = {
                            series: ejercicioDia.series,
                            repeticiones: ejercicioDia.repeticiones,
                            descanso: ejercicioDia.descanso,
                            peso: ejercicioDia.peso,
                            ejercicio: ejercicioDia.ejercicio.nombre,
                            musculo: rutinaDia.rutina.musculo
                        }
                        return ejercicioRutina;
                    });
                    return ejerciciosDia
                }
            }).filter(rutinaDia => {
                return rutinaDia != undefined
            })
        const rutinaPorDia = {
            dia: dia.nombre,
            rutinas
        }
        return rutinaPorDia
    }).filter(rutinaDia => {
        return rutinaDia.rutinas.length > 0
    });
    
    const rutinasDias: any[] = rutinasSemana.map(rutinaDia => {
        const ejercicios = [];
        ejercicios.push(
            [
                {
                    text: rutinaDia.dia,
                    style: 'tableHeader',
                    colSpan: 6,
                    alignment: 'center'
                },
                {}, {}, {}, {}, {}
            ]
        );
        ejercicios.push(
            [
                {
                    text: 'Musculo',
                    style: 'tableHeader',
                    alignment: 'center'
                },
                { 
                    text: 'Ejercicio', 
                    style: 'tableHeader', 
                    alignment: 'center' 
                },
                { 
                    text: 'Series', 
                    style: 'tableHeader', 
                    alignment: 'center' 
                },
                { 
                    text: 'Repeticiones', 
                    style: 'tableHeader', 
                    alignment: 'center' 
                },
                { 
                    text: 'Peso (kg)', 
                    style: 'tableHeader', 
                    alignment: 'center' 
                },
                { 
                    text: 'Descanso (min)', 
                    style: 'tableHeader', 
                    alignment: 'center' 
                },
            ]
        );
        rutinaDia.rutinas.forEach(rutina => {
            rutina.forEach(ejercicioDia => {
                ejercicios.push(
                    [
                        {
                            text: ejercicioDia.musculo,
                            alignment: 'center',
                        },
                        {
                            text: ejercicioDia.ejercicio,
                            alignment: 'center'
                        },
                        {
                            text: ejercicioDia.series,
                            alignment: 'center'
                        },
                        {
                            text: ejercicioDia.repeticiones,
                            alignment: 'center'
                        },
                        {
                            text: ejercicioDia.peso,
                            alignment: 'center'
                        },
                        {
                            text: ejercicioDia.descanso,
                            alignment: 'center'
                        },
                    ],
                )
            });
        });
    
        const ejerciciosDia = {
            style: 'tableExample',
            table: {
                widths: [60, 190, 50, 70, 50, 70],
                body: ejercicios
            },
    
        }
        return ejerciciosDia
    });
    rutinasDias.unshift({ text: planEntrenamientoSeleccionado.nombre,  alignment: 'center'})
    return rutinasDias
}
export function validarCedula(cedula: string): boolean{
    if(cedula.length!==10){
        return false
    }
    else{
        var ultimoDigito: number;
        var arregloCoeficientes=[2,1,2,1,2,1,2,1,2];
        var arregloResultado = arregloCoeficientes.map((elemento, indice)=>{
            var proudcto = elemento*Number(cedula.substring(indice,indice+1));
            if(proudcto>=10){
                proudcto=proudcto-9;
            }
            return proudcto;
        })
        var sumaDigitos= arregloResultado.reduce((acumulador, valor)=>{
            return acumulador + valor;
        },  0)
        ultimoDigito = (Math.ceil(sumaDigitos/10)*10)-sumaDigitos;
    
        const coincideUltimoDigito = ultimoDigito == Number(cedula.substring(cedula.length-1,cedula.length));
        if(coincideUltimoDigito){
            return true;
        }
        else{
            return false;
        }
    }
    
}
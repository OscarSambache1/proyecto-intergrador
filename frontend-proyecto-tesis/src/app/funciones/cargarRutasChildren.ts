import { MenuItem } from "primeng/api";

export function cargarRutasChildren(idCliente: number, modulo: string, accion: string, titulo: string): MenuItem[] {

    return [
        {
            label: 'Cliente',
            routerLink: ['/app', 'lista-clientes', { modulo, titulo}]
        },
        {
            label: `lista de ${titulo}`,
            routerLink: ['/app', 'cliente', idCliente, modulo]
        },
        {
            label: accion
        }
    ]
}
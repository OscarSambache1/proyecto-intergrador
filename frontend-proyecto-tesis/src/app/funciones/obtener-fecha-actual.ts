import * as moment from 'moment';

export function obtenerFechaHoy(): string{
   return (moment().format('YYYY-MM-DD'))
}
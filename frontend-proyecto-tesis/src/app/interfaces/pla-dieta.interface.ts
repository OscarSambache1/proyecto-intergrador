import { PlanDietaDia } from "./pla-dieta-dia.interface";

export interface PlanDieta {
    id?: number;
    nombre?: string;
    descripcion?: string;
    planesDietaDia?: PlanDietaDia[];
    estado?: number;
    planesDietaCliente?: any[];
}
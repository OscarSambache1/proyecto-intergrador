import { Cliente } from "./cliente.interface";

export interface Medida{
    id?: number;
    torax?: number;
    abdomen?: number;
    muslo?: number;
    pantorrilla?: number;
    biceps?: number;
    peso?: number;
    estatura?: number;
    fechaRegistro?: string;
    cliente?: Cliente;
}
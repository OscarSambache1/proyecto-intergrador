import { Usuario } from "./usuario";
import { ClaseHora } from "./clase-hora.interface";

export interface Instructor{
    id?: number;
    estado?: number;
    rol?: string;
    especializacion?: string;
    planesEntrenamiento?: any[];
    usuario?: Usuario;
    clasesHora?: ClaseHora[];
}
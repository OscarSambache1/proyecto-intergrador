import { CabeceraFactura } from "./cabecera-factura.interface";
import { Producto } from "./producto.interface";

export interface DetalleInterface {
    id?: number
    cantidad?: number; 
    total?: number;
    precioUnitario?: number;
    cabeceraFactura?: CabeceraFactura;
    producto?: Producto;
}
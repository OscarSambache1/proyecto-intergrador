import { Comida } from "./comida.interface";
import { Dieta } from "./dieta.interface";

export interface ComidaDieta {
    id?:number;
    hora?:string;
    comida?: Comida;
    dieta?: Dieta;
    idTipo?: number;
}
import { TipoComida } from "./tipo-comida.interface";
import { ComidaDieta } from "./comidas-dieta.ineterface";

export interface Comida {

    id?:number;
    descripcion?: string;
    tipo?: TipoComida;
    estado?: number;
    comidasDieta?: ComidaDieta[];  
}
import { Rutina } from "./rutina.interface";
import { PlanEntrenamiento } from "./plan-entrenamiento.interface";
import { Dia } from "./dia.interface";

export interface RutinaPlanEntrenamiento {
    id?: number;
    dia?: Dia;
    rutina?: Rutina;
    nombreDia?: any;
    planEntrenamiento?: PlanEntrenamiento;
}
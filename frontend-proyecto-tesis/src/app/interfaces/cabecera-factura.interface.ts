import { Cliente } from "./cliente.interface";
import { Usuario } from "./usuario";
import { DetalleInterface } from "./detalle-factura.interface";

export interface CabeceraFactura {
    id?: number
    fecha?: string; 
    subtotal?: number;
    total?: number;
    usuario?: Usuario;
    estado?: string;
    detallesFactura?: DetalleInterface[];
}
import { Ejercicio } from "./ejercicio.interface";
import { Musculo } from "./musculo.interface"
export interface EjercicioMusculo {
    id?: number;
    musculo?: Musculo;
    ejercicio?: Ejercicio;
}
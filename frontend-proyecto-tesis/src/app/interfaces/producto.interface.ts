export interface Producto {
    id?: number;
    nombre?: string;
    descripcion?: string;
    marca?: string;
    estado?: number;
    precio?: number;
}
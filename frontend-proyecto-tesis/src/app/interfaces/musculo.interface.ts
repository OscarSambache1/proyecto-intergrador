import { EjercicioMusculo } from "./ejercicio-musculo.interface";

export interface Musculo {
    id?: number;
    nombre?: string;
    estado?: number;
    ejerciciosMusculo?: EjercicioMusculo[];
}
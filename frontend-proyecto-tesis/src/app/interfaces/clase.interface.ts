import { ClaseHora } from "./clase-hora.interface";

export interface Clase {
    id?: number;
    nombre?: string;
    descripcion?: string;
    estado?: number;
    clasesHora?: ClaseHora[];
}
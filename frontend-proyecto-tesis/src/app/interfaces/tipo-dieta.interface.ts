import { Dieta } from "./dieta.interface";

export interface TipoDieta{
    id?: number;
    nombre?: string;
    estado?: number;
    dietas?: Dieta[];
}
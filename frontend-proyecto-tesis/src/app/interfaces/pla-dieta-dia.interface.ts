import { Dieta } from "./dieta.interface";
import { PlanDieta } from "./pla-dieta.interface";
import { Dia } from "./dia.interface";

export interface PlanDietaDia {
    id?: number;
    dia?: Dia | any;
    dieta?: Dieta;
    nombreDia?: any;
    planDieta?: PlanDieta;
}
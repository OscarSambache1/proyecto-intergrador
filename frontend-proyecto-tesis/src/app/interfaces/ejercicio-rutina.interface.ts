import { Ejercicio } from "./ejercicio.interface";
import { Rutina } from "./rutina.interface";

export interface EjercicioRutina {
    id?: number;
    series?: number;
    repeticiones?: number;
    descanso?: number;
    ejercicio?: Ejercicio;
    rutina?: Rutina;
    peso?: number;
}
import { EjercicioRutina } from "./ejercicio-rutina.interface";
import { RutinaPlanEntrenamiento } from "./rutina-plan-entrenamiento.interface";

export interface Rutina {
    id?: number;
    nombre?: string;
    descripcion?: string;
    musculo?: string;
    estado?: number;
    ejerciciosRutina?: EjercicioRutina[];
    rutinasPlanEntrenamiento?: RutinaPlanEntrenamiento[];
}
import { EjercicioMusculo } from "./ejercicio-musculo.interface";
import { EjercicioRutina } from "./ejercicio-rutina.interface";

export interface Ejercicio {
    id?: number;
    nombre?: string;
    descripcion?: string;
    urlImagen?: string;
    estado?: number;
    ejerciciosMusculo?: EjercicioMusculo[];
    ejerciciosRutina?: EjercicioRutina[];
}
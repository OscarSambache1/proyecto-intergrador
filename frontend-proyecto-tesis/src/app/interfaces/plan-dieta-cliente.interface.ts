import { Cliente } from "./cliente.interface";
import { PlanDieta } from "./pla-dieta.interface";

export interface PlanDietaCliente {
    id?: number;
    fechaInicio?: string;
    fechaFin?: string;
    cliente?: Cliente;
    planDieta?: PlanDieta;
}
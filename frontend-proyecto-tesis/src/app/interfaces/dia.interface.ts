import { ClaseDiaHora } from "./clase-dia-hora.interface";

export interface Dia {
    id?: number;
    nombre?: string;
    clasesDiaHora?:  ClaseDiaHora[];
}
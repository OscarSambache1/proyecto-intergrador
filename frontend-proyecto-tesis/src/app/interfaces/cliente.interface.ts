import { Usuario } from "src/app/interfaces/usuario";

export interface Cliente {
    id?: number
    codigo?: string;
    fechaRegistro?: string;
    diaPago?: number;
    estado?: number;
    rol?: string;
    usuario?: Usuario;
    planesEntrenamiento?: any[];
    planesDieta?: any[];
    medidas?: any[]
}
import { Dia } from "./dia.interface";
import { ClaseHora } from "./clase-hora.interface";

export interface ClaseDiaHora {
    id?: number;
    claseHora?: ClaseHora;
    dia?: Dia;
}
import { Cliente } from "./cliente.interface";
import { Instructor } from "./instructor.interface";
import { PlanEntrenamiento } from "./plan-entrenamiento.interface";

export interface PlanEntrenamientoClienteInstructor {
    id?: number;
    fechaInicio?:string;
    fechaFin?:string;
    cliente?: Cliente;
    instructor?: Instructor;
    planEntrenamiento?: PlanEntrenamiento;
}
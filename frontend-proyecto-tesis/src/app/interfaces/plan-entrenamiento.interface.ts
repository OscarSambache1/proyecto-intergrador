import { RutinaPlanEntrenamiento } from "./rutina-plan-entrenamiento.interface";
import { PlanEntrenamientoClienteInstructor } from "./plan-entrenamiento-cliente-instructor.interface";

export interface PlanEntrenamiento {
    id?: number;
    nombre?: string;
    descripcion?: string;
    estado?: number;
    rutinasPlanEntrenamiento?: RutinaPlanEntrenamiento[];
    planesEntrenamientoClienteInstructor?: PlanEntrenamientoClienteInstructor[];

}
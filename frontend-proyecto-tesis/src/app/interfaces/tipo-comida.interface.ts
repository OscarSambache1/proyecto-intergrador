import { Comida } from "./comida.interface";

export interface TipoComida {
    id?:number;
    nombre? : string;
    estado?: number;
    comidas?: Comida[];
}
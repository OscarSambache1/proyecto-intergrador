import { Cliente } from "./cliente.interface";
import { Instructor } from "./instructor.interface";

export interface Usuario {
    id?: number
    nombres?: string;
    apellidos?: string;
    cedula?: string;
    fechaNacimiento?: string;
    telefono?: string;
    direccion?: string;
    correoElectronico?: string;
    urlFoto?: string;
    password?: string;
    genero?: string;
    cliente?: Cliente;
    instructor?: Instructor;
}
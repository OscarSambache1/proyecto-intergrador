import { Clase } from "./clase.interface";
import { Instructor } from "./instructor.interface";
import { ClaseDiaHora } from "./clase-dia-hora.interface";

export class ClaseHora {
    id?: number;
    horaInicio?: number;
    horaFin?: number;
    instructor?: Instructor;
    clase?: Clase;
    clasesDiaHora?: ClaseDiaHora[] | any;
    nombresDias?: string;
}
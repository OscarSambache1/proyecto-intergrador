import { ComidaDieta } from "./comidas-dieta.ineterface";
import { TipoDieta } from "./tipo-dieta.interface";

export interface Dieta{

    id?:number;
    nombre?: string;
    descripcion?: string;
    estado?: number;
    comidasDieta?: ComidaDieta[];
    dietasDia?: any[];
    tipo?: TipoDieta;
}
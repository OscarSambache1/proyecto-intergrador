import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { InicioComponent } from './rutas/inicio/inicio.component';
import { AppRoutingModule } from './app.routes.module';
import { AutenticacionModule } from './modulos/autenticacion/autenticacion.module';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './modulos/autenticacion/login/login.component';
import {InputTextModule} from 'primeng/inputtext';
import { ModulosComponent } from './componentes/modulos/modulos.component';
import {MenuModule} from 'primeng/menu';
import {ToolbarModule } from 'primeng/toolbar';
import {SplitButtonModule} from 'primeng/splitbutton'
import {TieredMenuModule} from 'primeng/tieredmenu';
import { PanelMenuModule } from 'primeng/panelmenu'
import {MatMenuModule} from '@angular/material/menu';
import { TabMenuModule } from 'primeng/tabmenu'
import {ButtonModule} from 'primeng/button';
import {MatCardModule} from '@angular/material/card';
import { ModulosService } from './servicios/modulos.service';
import {DataViewModule,} from 'primeng/dataview';
import {DataGridModule,} from 'primeng/datagrid';
import {CardModule} from 'primeng/card';
import { UsuarioService } from './servicios/usuario.service';
import { FormularioUsuarioModule } from './formularios/formulario-usuario/formulario-usuario.module';
import { PerfilUsuarioComponent } from './componentes/perfil-usuario/perfil-usuario.component';
import { FormularioClienteModule } from './modulos/cliente/formularios/formulario-cliente/formulario-cliente.module';
import { ClienteService } from './modulos/cliente/cliente.service';
import { ListaClientesModule } from 'src/app/componentes/lista-clientes/lista-clientes.module';
import { SubirArchivoService } from './servicios/subir-archivo.service';
import { CargandoService } from './servicios/cargando.service';
import {BlockUIModule} from 'primeng/blockui';
import { MigasPanModule } from './componentes/migas-pan/migas-pan.module';
import { BuscarClienteModule } from './componentes/buscar-cliente/buscar-cliente.module';
import { PerfilModule } from './componentes/perfil-usuario/perfil.module';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ModulosComponent  
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    CommonModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AutenticacionModule,
    ToasterModule.forRoot(),
    BrowserAnimationsModule,
    InputTextModule,
    MenuModule, 
    ToolbarModule,
    SplitButtonModule,
    TieredMenuModule,
    PanelMenuModule,
    MatMenuModule,
    TabMenuModule,
    ButtonModule,
    MatCardModule,
    DataViewModule,
    DataGridModule,
    CardModule,
    FormularioUsuarioModule,
    FormularioClienteModule,
    ListaClientesModule,
    BlockUIModule,
    MigasPanModule,
    BuscarClienteModule,
    PerfilModule,
    AutenticacionModule
    ],
  providers: [
    AppService, 
    ModulosService, 
    UsuarioService, 
    ClienteService, 
    SubirArchivoService,
    CargandoService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CONFIGURACIONES_CALENDARIO } from '../../constantes/configuracion-calendario';
import { FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { Usuario } from '../../interfaces/usuario';
import { validarCedula } from 'src/app/funciones/validar-cedula';
import { map, debounceTime } from 'rxjs/operators';
import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { setearMensajes } from '../../shared/validaciones';
import { UsuarioService } from '../../servicios/usuario.service';

@Component({
    selector: 'app-formulario-usuario',
    templateUrl: './formulario-usuario.component.html',
    styleUrls: ['./formulario-usuario.component.css']
})
export class FormularioUsuarioComponent implements OnInit {

    @Output() esValidoFormularioUsuario: EventEmitter<FormGroup | boolean> = new EventEmitter();
    @Input() esFormularioCrear = false;
    @Input() esFormularioEditar = false;
    @Input() esFormularioVer = false;
    @Input() esFormularioPerfil = false;
    @Input() usuarioSeleccionado: Usuario;
    formularioUsuario: FormGroup
    dates: Date[];
    configuracion: any;
    mensajeValidacionCedula: string;
    mensajesErroresNombresUsuario = [];
    mensajesErroresApellidosUsuario = [];
    mensajesErroresCorreoUsuario = [];
    mensajesErroresCedulaUsuario = [];
    mensajesErroresDireccionUsuario = [];
    mensajesErroresTelefonoUsuario = [];
    mensajesErroresFechaNacimientoUsuario = [];
    mensajesErroresGenero = [];

    private mensajesValidacionNombre = {
        required: "Los nombres son obligatorios",
        pattern: 'No se admite espacios en blanco'
    }

    private mensajesValidacionApellido = {
        required: "Los apellidos son obligatorios",
        pattern: 'No se admite espacios en blanco'
    }

    private mensajesValidacionDireccion = {
        required: "La direccion es obligatorio",
    }

    private mensajesValidacionCorreo = {
        pattern: 'El correo es incorrecto'
    }


    private mensajesValidacionCedula = {
        required: "La cédula o pasaporte son obligatoria",
        validacionAsincronaUsuario: 'El usuario ya existe',
        pattern: 'Cedula o pasaportre no válido'
    }

    private mensajesValidacionTelefono = {
        required: "El telefóno es obligatorio",
        pattern: 'El teléfono no es valido'
    }

    private mensajesValidacionFechaNacimiento = {
        required: "La fecha de nacimiento es obligatorios",
    }

    private mensajesValidacionGenero = {
        required: "El genero es obligatorio",
    }



    constructor(
        private readonly _formBuilder: FormBuilder,
        private readonly toasterService: ToasterService,
        private readonly _usuarioService: UsuarioService
    ) { }

    ngOnInit(

    ) {
        this.configuracion = CONFIGURACIONES_CALENDARIO;
        this.crearFormulario();
        this.escucharFormulario();
        this.enviarFormularioValido();
        this.verificarTipoFormulario();

    }

    crearFormulario() {
        this.formularioUsuario = this._formBuilder.group({
            nombres: ['', [
                Validators.required,
                Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/),
            ]],
            apellidos: ['', [
                Validators.required,
                Validators.pattern(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/),
            ]],
            direccion: ['', [
                Validators.required,
            ]],
            cedula: [{ value: '', disabled: true }, [
                Validators.required,
                Validators.pattern(/^[A-Z0-9]+$/),
            ]],
            fechaNacimiento: ['', [Validators.required]],
            telefono: ['', [
                Validators.required,
                Validators.pattern(/^([09]?[89]\d{8})$/),
            ]],
            correoElectronico: ['', [
                Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            genero: ['', [Validators.required]]
        })
    }

    escucharFormulario() {
        this.formularioUsuario.valueChanges
            .pipe(debounceTime(0))
            .subscribe(() => {
                this.mensajesErroresNombresUsuario = setearMensajes(this.formularioUsuario.get('nombres'), this.mensajesValidacionNombre);
                this.mensajesErroresApellidosUsuario = setearMensajes(this.formularioUsuario.get('apellidos'), this.mensajesValidacionApellido);
                this.mensajesErroresCorreoUsuario = setearMensajes(this.formularioUsuario.get('correoElectronico'), this.mensajesValidacionCorreo);
                this.mensajesErroresDireccionUsuario = setearMensajes(this.formularioUsuario.get('direccion'), this.mensajesValidacionDireccion);
                this.mensajesErroresFechaNacimientoUsuario = setearMensajes(this.formularioUsuario.get('fechaNacimiento'), this.mensajesValidacionFechaNacimiento);
                this.mensajesErroresTelefonoUsuario = setearMensajes(this.formularioUsuario.get('telefono'), this.mensajesValidacionTelefono);
                this.mensajesErroresGenero = setearMensajes(this.formularioUsuario.get('genero'), this.mensajesValidacionGenero);
                this.escucharCambiosCedula();
            });
    }


    enviarFormularioValido() {
        this.formularioUsuario.valueChanges.subscribe(async formulario => {
            const esCedulaValida = await this.validarCedulaRepetido();
            if (esCedulaValida) {
              this.mensajesErroresCedulaUsuario = [];
              this.mensajesErroresCedulaUsuario.push(this.mensajesValidacionCedula['validacionAsincronaUsuario']);
            }
            if (!esCedulaValida && this.formularioUsuario.valid) {
                this.esValidoFormularioUsuario.emit(this.formularioUsuario);
              }
              else {
                this.esValidoFormularioUsuario.emit(false);
              }
            
          });
    }

    verificarTipoFormulario() {
        if (this.esFormularioCrear) {
            this.formularioUsuario.get('cedula').enable();
        }
        if (this.esFormularioEditar) {
            this.llenarFormulario();
        }
        if (this.esFormularioVer) {
            this.llenarFormulario();
            this.formularioUsuario.disable();
        }
        if (this.esFormularioPerfil) {
            this.llenarFormulario();
        }
    }

    llenarFormulario() {
        this.formularioUsuario.patchValue(
            {
                nombres: this.usuarioSeleccionado.nombres,
                apellidos: this.usuarioSeleccionado.apellidos,
                cedula: this.usuarioSeleccionado.cedula,
                telefono: this.usuarioSeleccionado.telefono,
                correoElectronico: this.usuarioSeleccionado.correoElectronico,
                direccion: this.usuarioSeleccionado.direccion,
                fechaNacimiento: this.usuarioSeleccionado.fechaNacimiento,
                genero: this.usuarioSeleccionado.genero,
            }
        )
    }

    escucharCambiosCedula() {
        const cedulaControl = this.formularioUsuario.get('cedula')
        cedulaControl.valueChanges
            .subscribe(valor => {
                this.setearMensajesErroresCedula(cedulaControl)
            }
            )
    }

    setearMensajesErroresCedula(valor: AbstractControl) {
        this.mensajesErroresCedulaUsuario = [];
        const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
        if (esValidoCampo) {
            this.mensajesErroresCedulaUsuario = Object.keys(valor.errors).map(atributo => {
                return this.mensajesValidacionCedula[atributo]
            })
        }
    }

    validacionMinLength(longitud: number): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (valorInput.value.length > longitud) {
                return null
            }
            return { 'validacionMinLength': true }
        }
    }

    validacionLongitudCedula(longitud: number): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (valorInput.value.length == longitud) {
                return null
            }
            return { 'validacionLongitudCedula': true }
        }
    }

    validacionCedula(): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (validarCedula(valorInput.value)) {
                return null
            }
            return { 'validacionCedula': true }
        }
    }

    validacionLongitud(longitud: number): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (valorInput.value.length == longitud) {
                return null
            }
            return { 'validacionLongitud': true }
        }
    }


    mostrarToast(tipo: string, titulo: string, mensaje: string) {
        this.toasterService.pop(tipo, titulo, mensaje);
    }

    async validarCedulaRepetido(): Promise<Boolean> {
        const cedula = this.formularioUsuario.get('cedula').value;
        const consulta =
        {
          order: { id: 'DESC' },
          where: {
            cedula
          }
        }
        const promesaCedula: any = this._usuarioService.findAll(consulta).toPromise();
        const respuestaPromesa = await promesaCedula;
        const existeCedula: boolean = respuestaPromesa.length !== 0;
    
        if (this.usuarioSeleccionado) {
            if(existeCedula){
              if(this.usuarioSeleccionado.cedula === this.formularioUsuario.get('cedula').value.trim()){
                return false;
              }
              else {
                return true;
              }
            }
        }
        else {
          return existeCedula;
        }
      }
}

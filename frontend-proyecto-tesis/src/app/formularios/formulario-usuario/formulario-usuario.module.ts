import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { FormularioUsuarioComponent } from "../../formularios/formulario-usuario/formulario-usuario.component";
import { ClienteModule } from "../../modulos/cliente/cliente.module";
import { TableModule } from "primeng/table";
import { CalendarModule } from "primeng/calendar";
import { InputTextModule } from "primeng/inputtext";
import { RadioButtonModule } from "primeng/radiobutton";
import { DropdownModule } from "primeng/dropdown";
import {InputMaskModule} from 'primeng/inputmask';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        TableModule,
        CalendarModule,
        InputTextModule,
        RadioButtonModule,
        DropdownModule,
        InputMaskModule
    ],
    declarations: [
    FormularioUsuarioComponent
    ],
    providers: [
        
    ],
    exports:[FormularioUsuarioComponent]
})
export class FormularioUsuarioModule { }

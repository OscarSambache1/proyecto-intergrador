import { Component, OnInit } from '@angular/core';
import { ModulosService } from '../../servicios/modulos.service';
import { CargandoService } from 'src/app/servicios/cargando.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  arregloModulos=[];
  constructor(
    private readonly _modulosService : ModulosService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this.arregloModulos=this._modulosService.arregloModulos;
    this._cargandoService.deshabiltarCargando();
  }

}

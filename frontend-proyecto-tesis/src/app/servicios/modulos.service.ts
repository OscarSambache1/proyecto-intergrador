
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
@Injectable()
export class ModulosService {
    arregloModulos = [];
    constructor() {
        this.arregloModulos = [
            {
                nombre: 'Clientes',
                urlImagen: environment.url + '/imagenes-modulos/modulo-clientes.png',
                ruta:'cliente',
                parametrosOpcionales:{}
            },

            {
                nombre: 'Intructores',
                urlImagen: environment.url + '/imagenes-modulos/modulo-instructor.png',
                ruta:'instructor',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Ejercicios',
                urlImagen: environment.url + '/imagenes-modulos/modulo-ejercicio.png',
                ruta:'ejercicio',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Rutinas',
                urlImagen: environment.url + '/imagenes-modulos/modulo-rutina.png',
                ruta:'rutina',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Entrenamiento',
                urlImagen: environment.url + '/imagenes-modulos/modulo-plan-entrenamiento.png',
                ruta:'plan-entrenamiento',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Comidas',
                urlImagen: environment.url + '/imagenes-modulos/modulo-comida.png',
                ruta:'comida',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Dietas',
                urlImagen: environment.url + '/imagenes-modulos/modulo-dieta.png',
                ruta:'dieta',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Nutricion',
                urlImagen: environment.url + '/imagenes-modulos/modulo-nutricion.png',
                ruta:'plan-dieta',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Pagos',
                urlImagen: environment.url + '/imagenes-modulos/modulo-pagos.png',
                ruta:'pago',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Productos',
                urlImagen: environment.url + '/imagenes-modulos/modulo-productos.png',
                ruta:'producto',
                parametrosOpcionales:{}

            },
            {
                nombre: 'Clases',
                urlImagen: environment.url + '/imagenes-modulos/modulo-clases.png',
                ruta:'clase',
                parametrosOpcionales:{}
            },
            {
                nombre: 'Medidas',
                urlImagen: environment.url + '/imagenes-modulos/modulo-medidas.png',
                ruta:'lista-clientes',
                parametrosOpcionales:{modulo:'medidas', titulo:'medidas'}
            },
        ];
    }
}
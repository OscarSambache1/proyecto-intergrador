import { Injectable } from "@angular/core";
import { Usuario } from "../interfaces/usuario";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { ModulosComponent } from "../componentes/modulos/modulos.component";
@Injectable()
export class SubirArchivoService {

    constructor(private readonly _httpClient: HttpClient){

    }

    onUpload(id: number, selectedFile, modulo: string): Observable<any> {
        const file2 = new FormData();
        file2.append('file2', selectedFile, selectedFile.name)
        return  this._httpClient.post(environment.url + `/${modulo}/subirArchivo/${id}`, file2)
      }
    
  
}ModulosComponent
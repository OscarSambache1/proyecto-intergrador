import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Dia } from "../interfaces/dia.interface";
@Injectable()
export class DiaService {

    constructor(private readonly _httpClient: HttpClient){

    }

    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/dia?consulta=` + `${JSON.stringify(consulta)}`)
    }

    findOne(id: number | string) : Observable<Dia>{
        return  this._httpClient.get(environment.url+`/dia/${id}`)
    }

}
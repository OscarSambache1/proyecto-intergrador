import { Injectable } from "@angular/core";
import { Usuario } from "../interfaces/usuario";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
@Injectable()
export class UsuarioService {

    constructor(private readonly _httpClient: HttpClient) {

    }

    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/usuario/findAll?consulta=` + `${JSON.stringify(consulta)}`)
    }

    create(usuario: Usuario): Observable<Usuario> {
        return this._httpClient.post(environment.url + '/usuario/', usuario)
    }

    update(id: number, usuario: Usuario): Observable<Usuario> {
        return this._httpClient.put(environment.url + `/usuario/${id}`, usuario)
    }

    findOne(id: number | string): Observable<Usuario> {
        return this._httpClient.get(environment.url + `/usuario/${id}`);
    }

    findWhereOr(parametro: string): Observable<any> {
        return this._httpClient.get(environment.url + `/usuario/findWhereOr?expresion=${parametro}`);
    }

    resetearPassword(id: number): Observable<any>{
        return this._httpClient.post(environment.url + `/usuario/resetear-password`, {id})
    }

    cambiarPassword(id: number, passwordNuevo: string, passwordActual: string): Observable<any>{
        return this._httpClient.post(environment.url + `/usuario/cambiar-password`, {id, passwordNuevo, passwordActual})
    }
}
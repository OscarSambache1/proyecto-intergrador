import { environment } from "../../environments/environment";

export const ICONOS = {
    cliente: environment.url+'/iconos/usuario-icono.png',
    instructor: environment.url+'/iconos/instructor-icono.png',
    ejercicio: environment.url+'/iconos/ejercicio-icono.png',
    rutina: environment.url+'/iconos/rutina-icono.png',
    planEntrenamiento: environment.url+'/iconos/plan-entrenamiento-icono.png',
    comida: environment.url+'/iconos/comida-icono.png',
    dieta: environment.url+'/iconos/dieta-icono.png',
    planDieta: environment.url+'/iconos/plan-dieta-icono.png',
    medidas: environment.url+'/iconos/medidas-icono.png',
    producto: environment.url+'/iconos/producto-icono.png',
    pagos: environment.url+'/iconos/pagos-icono.png',
    clase: environment.url+'/iconos/clase-icono.png',
    asignacion: environment.url+'/iconos/asignacion-icono.png',
    musculo: environment.url+'/iconos/musculo-icono.png',
    tipoComida: environment.url+'/iconos/tipo-comida-icono.png',
    resetearPassword: environment.url+'/iconos/resetear-password-icono.png'
}
// <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

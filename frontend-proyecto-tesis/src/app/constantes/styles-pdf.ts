export const STYLES_PDF = {
    header: {
      fontSize: 12,
      bold: true,
      margin: [0, 0, 0, 10],
      alignment: 'center'
    },
    tableExample: {
      margin: [0, 5, 0, 15]
    },
    tableHeader: {
      bold: true,
      fontSize: 10,
      fillColor: '#eeeeee',
      color: 'black',
    }
  }
export const DIAS_SEMANA = [
    {label:'Seleccione un día', value:null},
    {label:'Lunes', value:'Lunes'},
    {label:'Mártes', value:'Mártes'},
    {label:'Miércoles', value:'Miércoles'},
    {label:'Jueves', value:'Jueves'},
    {label:'Viernes', value: 'Viernes'},
    {label:'Sábado', value:'Sábado'},
    {label:'Domingo', value: 'Domingo'}
]
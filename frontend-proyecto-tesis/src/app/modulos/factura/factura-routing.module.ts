import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarFacturasComponent } from './rutas/listar-facturas/listar-facturas.component';
import { CrearFacturaComponent } from './rutas/crear-factura/crear-factura.component';
import { EditarFacturaComponent } from './rutas/editar-factura/editar-factura.component';
import { VerFacturaComponent } from './rutas/ver-factura/ver-factura.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';
import { ClientesPorCobrarComponent } from './rutas/clientes-por-cobrar/clientes-por-cobrar.component';

const routes: Routes = [
  {
    path:'listar',
    component: ListarFacturasComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearFacturaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component: EditarFacturaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path: 'ver/:id',
    component : VerFacturaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: 'clientes-por-cobrar',
    component : ClientesPorCobrarComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturaRoutingModule { }

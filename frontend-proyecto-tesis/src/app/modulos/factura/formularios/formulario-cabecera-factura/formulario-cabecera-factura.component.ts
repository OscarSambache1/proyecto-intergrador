import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';
import { MatDialog } from '@angular/material';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { CONFIGURACIONES_CALENDARIO } from '../../../../constantes/configuracion-calendario';
import { obtenerFechaHoy } from '../../../../funciones/obtener-fecha-actual';
import { CabeceraFacturaService } from '../../servicios/cabecera-factura.service';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { ModalDetalleComponent } from '../../modales/modal-detalle/modal-detalle.component';
import { ConfirmationService } from 'primeng/api';
import { CargandoService } from '../../../../servicios/cargando.service';
import { UsuarioService } from '../../../../servicios/usuario.service';

@Component({
  selector: 'app-formulario-cabecera-factura',
  templateUrl: './formulario-cabecera-factura.component.html',
  styleUrls: ['./formulario-cabecera-factura.component.css']
})
export class FormularioCabeceraFacturaComponent implements OnInit {
  @Output() esValidoFormularioCabecera: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() cabeceraSeleccionada: CabeceraFactura;
  formularioCabecera: FormGroup;
  mensajesErroresFecha = [];
  usuario: any = {};
  usuarioAutoComplete: any = {};
  edad: number;
  usuariosFiltrados: any[] = [];
  parametro: string;
  dates: Date[];
  configuracion: any;
  arregloDetalle: DetalleInterface[] = [];
  subtotal: number;
  subTotalIVA: number;
  total: number;
  colspam = 3;
  columnas = [
    { field: '#', header: '#', width: '5%' },
    { field: 'descripcion', header: 'Descripción', width: '30%' },
    { field: 'cantida', header: 'Cantidad', width: '15%' },
    { field: 'precio', header: 'Precio', width: '15%' },
    { field: 'total', header: 'Total', width: '15%' },
    { field: 'acciones', header: 'Acciones', width: '20%' },
  ];
  private mensajesValidacioFecha = {
    required: "La fecha es obligatoria",
  };


  constructor(
    private readonly _cabeceraFacturaService: CabeceraFacturaService,
    private readonly _toasterService: ToasterService,
    private readonly _formBuilder: FormBuilder,
    private readonly confirmationService: ConfirmationService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    if (this.esFormularioVer) {
      this.columnas.pop();
    }
    this.configuracion = CONFIGURACIONES_CALENDARIO;
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  crearFormulario() {
    this.formularioCabecera = this._formBuilder.group({
      numero: [],
      fecha: ['', [Validators.required]],
      subtotal: ['', [Validators.required]],
      total: ['', [Validators.required]],
      usuario: ['', [Validators.required]],
      detalles: ['', [Validators.required]]
    });
    this.formularioCabecera.get('numero').disable();
  }

  escucharFormulario() {
    this.formularioCabecera.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresFecha = setearMensajes(this.formularioCabecera.get('fecha'), this.mensajesValidacioFecha);
      });
  }

  enviarFormularioValido() {
    this.formularioCabecera.valueChanges.subscribe(formulario => {
      this.esValidoFormularioCabecera.emit(this.formularioCabecera);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
      this.setearNumeroFactura();
      this.formularioCabecera.patchValue(
        { fecha: obtenerFechaHoy() }
      )
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.colspam = 3;
      this.llenarFormulario();
      this.formularioCabecera.disable();
    }
  }


  llenarFormulario() {
    this.usuario = this.cabeceraSeleccionada.usuario;
    this.usuarioAutoComplete = this.usuario;
    this.usuario.nombresApellidos = this.usuario.nombres + ' ' + this.usuario.apellidos
    this.arregloDetalle = this.cabeceraSeleccionada.detallesFactura;
    this.calcularSubtotalIVA();
    this.formularioCabecera.patchValue({
      usuario: this.usuario,
      numero: this.cabeceraSeleccionada.id,
      total: this.total,
      subtotal: this.subtotal,
      fecha: this.cabeceraSeleccionada.fecha,
      detalles: this.cabeceraSeleccionada.detallesFactura
    });
  }


  filtrarUsuario(event) {
    let query = event.query;
    this.obtenerPorParametro(query);
  }

  obtenerPorParametro(parametro) {
    this._usuarioService.findWhereOr(parametro)
      .subscribe(usuariosEncontrados => {
        this.usuariosFiltrados = usuariosEncontrados.map(usuario => {
          usuario.nombresApellidos = usuario.nombres + ' ' + usuario.apellidos
          return usuario;
        });
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al traer datos');
        });
  }

  seleccionarUsuario(eventoUsuario) {
    this.usuario = eventoUsuario;
    this.usuarioAutoComplete = this.usuario;
    this.formularioCabecera.patchValue({ usuario: this.usuario });
  }

  setearNumeroFactura() {
    const consulta = {
      order: {
        id: 'DESC'
      },
      skip: 0,
      take: 1,
    }
    this._cabeceraFacturaService.findAll(consulta)
      .subscribe(factura => {
        let numeroFactura;
        if (!factura[0]) {
          numeroFactura = 1;
        }
        else {
          numeroFactura = Number(factura[0].id)+1;
        }
        this.formularioCabecera.patchValue(
          {
            numero: numeroFactura
          }
        );
      });
  }

  abrirModalCrearDetalle() {
    const dialogRef = this.dialog.open(ModalDetalleComponent, {
      height: '470px',
      width: '650px',
      data: {
        arregloDetalle: this.arregloDetalle
      }
    });
    dialogRef.afterClosed().subscribe(detalleCreado => {
      if (detalleCreado) {
        this.arregloDetalle.push(detalleCreado);
        this.formularioCabecera.patchValue({ detalles: this.arregloDetalle });
        this.calcularSubtotalIVA();
      }
    }
    );
  }

  abrirModalEditarDetalle(detalleFactura: DetalleInterface) {
    const indice = this.arregloDetalle.indexOf(detalleFactura);
    const dialogRef = this.dialog.open(ModalDetalleComponent, {
      height: '470px',
      width: '650px',
      data: {
        detalleFactura,
        arregloDetalle: this.arregloDetalle
      }
    });
    dialogRef.afterClosed().subscribe(detalleCreado => {
      if (detalleCreado) {
        this.arregloDetalle[indice] = detalleCreado;
        this.formularioCabecera.patchValue({ detalles: this.arregloDetalle });
        this.calcularSubtotalIVA();
      }
    }
    );
  }


  calcularSubtotalIVA() {
    this.total = this.arregloDetalle.reduce((acumulador, valor) => {
      return acumulador + valor.total;
    }, 0)
    this.subtotal = this.total / 1.12;
    this.subTotalIVA = this.total - this.subtotal;
    this.formularioCabecera.patchValue(
      {
        subtotal: this.subtotal,
        total: this.total
      });

  }

  confirmar(detalle: DetalleInterface) {
    this.confirmationService.confirm({
      message: '¿Está seguro que quiere eliminar este registro?',
      header: 'Confirmación de eliminación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.eliminarDetalle(detalle)
      },
      reject: () => {
      }
    });
  }

  eliminarDetalle(detalle: DetalleInterface) {
    this._cargandoService.habilitarCargando();
    this._toasterService.pop('success', 'Éxito', 'El registro se ha eliminado correctamente');
    const indice = this.arregloDetalle.indexOf(detalle);
    this.arregloDetalle.splice(indice, 1);
    this.formularioCabecera.patchValue({ detalles: this.arregloDetalle });
    this._cargandoService.deshabiltarCargando();
  }
}

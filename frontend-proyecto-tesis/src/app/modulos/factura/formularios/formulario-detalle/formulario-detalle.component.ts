import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ProductoService } from '../../../producto/producto.service';
import { TIPO_DETALLE } from '../../constantes/tipos-detalle';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-detalle',
  templateUrl: './formulario-detalle.component.html',
  styleUrls: ['./formulario-detalle.component.css']
})
export class FormularioDetalleComponent implements OnInit {
  @Output() esValidoFormularioDetalle: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() detalleSeleccionado: DetalleInterface;
  @Input() arregloDetalle: DetalleInterface[];
  formularioDetalle: FormGroup;
  mensajesErroresCantidad = [];
  mensajesErroresTipo = [];
  mensajesErroresTotal = [];
  mensajesErroresPrecioUnitario = [];
  arregloProductos: SelectItem[] = [];
  private mensajesValidacionCantidad = {
    required: "La cantidad es obligatoria",
  };

  private mensajesValidacionTotal = {
    required: "El Total es obligatorio",
  }

  private mensajesValidacionPrecioUnitario= {
    required: "El precio es obligatorio",
  }
  constructor(
    private readonly _productoServive: ProductoService,
    private readonly _toasterService: ToasterService,
    private readonly _formBuilder: FormBuilder,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.llenarDropdownProductos();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  crearFormulario() {
    this.formularioDetalle = this._formBuilder.group({
      cantidad: ['', [Validators.required]],
      total: [{value: '', disabled: true}, [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: [{value: '', disabled: true}],
      precioUnitario: ['', [Validators.required]],
      producto: ['', [Validators.required]],
      precio: [{value: '', disabled: true}, [Validators.required]],
    });
  }

  escucharFormulario() {
    this.formularioDetalle.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresCantidad = setearMensajes(this.formularioDetalle.get('cantidad'), this.mensajesValidacionCantidad);
        this.mensajesErroresTotal = setearMensajes(this.formularioDetalle.get('total'), this.mensajesValidacionTotal);
        const precio = this.formularioDetalle.get('precio').value;
        const cantidad = this.formularioDetalle.get('cantidad').value;
        const total = cantidad * precio;
        this.formularioDetalle.patchValue(
          {
            total
          }
        );  
      });
    }




  enviarFormularioValido() {
    this.formularioDetalle.valueChanges.subscribe(formulario => {
      this.esValidoFormularioDetalle.emit(this.formularioDetalle);
    });
  }

  verificarTipoFormulario() {
    if (this.detalleSeleccionado) {
      this.llenarFormulario();
    }
  }


  llenarFormulario() {
    this._productoServive.findOne(this.detalleSeleccionado.producto.id)
    .subscribe( producto =>{
      this.formularioDetalle.patchValue(
        {
          producto,
          nombre: this.detalleSeleccionado.producto.nombre,
          descripcion: this.detalleSeleccionado.producto.descripcion,
          precioUnitario: this.detalleSeleccionado.precioUnitario,
          precio: this.detalleSeleccionado.precioUnitario,
          total: this.detalleSeleccionado.total,
          cantidad: this.detalleSeleccionado.cantidad,
        }
      );
    });
    
  }

  llenarDropdownProductos() {
    this.arregloProductos = [];
    const consulta =
    {
      where: {
        estado: 1,
      },
    };
    this._productoServive.findAll(consulta).subscribe(productos => {
      productos.forEach(producto => {
        const elemento = {
          label: producto.nombre,
          value: producto
        }
        this.arregloProductos.push(elemento);
      });
    });
  }

  escucharEventoNombre(eventoProducto) {
    const seSeleccionoProducto = this.arregloDetalle.find(
      detalle => {
        return (
          detalle.producto.id === eventoProducto.value.id
        )
      }
    );
    if(!seSeleccionoProducto) {
      this.formularioDetalle.patchValue(
        {
          precioUnitario: eventoProducto.value.precio,
          precio: eventoProducto.value.precio,
          producto: eventoProducto.value,
          nombre: eventoProducto.value.nombre,
          descripcion: eventoProducto.value.descripcion,
        }
      );
    } else {
      this._toasterService.pop('warning', 'Advertencia', 'El producto ya se ha seleccionado')
    }

  }
}


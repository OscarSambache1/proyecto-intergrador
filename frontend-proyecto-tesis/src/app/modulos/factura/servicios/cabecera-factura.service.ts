import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CabeceraFactura } from '../../../interfaces/cabecera-factura.interface';

@Injectable({
  providedIn: 'root'
})
export class CabeceraFacturaService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }

findAll(consulta:any):Observable<any>{
  return  this._httpClient.get(environment.url+`/cabecera-factura?consulta=`+`${JSON.stringify(consulta)}`);
}

findWhereOr(parametro:string):Observable<any>{
  return  this._httpClient.get(environment.url+`/cabecera-factura/findWhereOr?expresion=${parametro}`);
}

findOne(id:number): Observable<CabeceraFactura>{
  return  this._httpClient.get(environment.url+`/cabecera-factura/${id}`)
}

create(cabeceraFactura: CabeceraFactura): Observable<CabeceraFactura> {
  return this._httpClient.post(environment.url + '/cabecera-factura', cabeceraFactura)
}

update(id: number, cabeceraFactura: CabeceraFactura): Observable<CabeceraFactura> {
  return this._httpClient.put(environment.url + `/cabecera-factura/${id}`, cabeceraFactura)
}

}

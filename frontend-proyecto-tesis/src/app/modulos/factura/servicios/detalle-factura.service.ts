import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DetalleInterface } from '../../../interfaces/detalle-factura.interface';

@Injectable({
  providedIn: 'root'
})
export class DetalleFacturaService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }

findAll(consulta:any):Observable<any>{
  return  this._httpClient.get(environment.url+`/detalle-factura?consulta=`+`${JSON.stringify(consulta)}`)
}

findOne(id:number): Observable<DetalleInterface>{
  return  this._httpClient.get(environment.url+`/detalle-factura/${id}`)
}

create(detalle: DetalleInterface): Observable<DetalleInterface> {
  return this._httpClient.post(environment.url + '/detalle-factura', detalle)
}

createMany(detalle: DetalleInterface[]): Observable<any>{
  console.log('detalle ser', detalle)
  return this._httpClient.post(environment.url + '/detalle-factura/crear-varios', detalle)
}

update(id: number, cabeceraFactura: DetalleInterface): Observable<DetalleInterface> {
  return this._httpClient.put(environment.url + `/detalle-factura/${id}`, cabeceraFactura)
}

delete(id: number): Observable<any> {
  return this._httpClient.delete(environment.url + `/detalle-factura/${id}`)
}
}

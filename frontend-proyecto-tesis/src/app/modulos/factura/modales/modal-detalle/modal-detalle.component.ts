import { Component, OnInit, Inject } from '@angular/core';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';

@Component({
  selector: 'app-modal-detalle',
  templateUrl: './modal-detalle.component.html',
  styleUrls: ['./modal-detalle.component.css']
})
export class ModalDetalleComponent implements OnInit {
  esFormularioDetalleValido = false;
  detalleACrearEditar: DetalleInterface;
  idDetalleFactura: number;
  cabeceraFactura: CabeceraFactura;
  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if(this.data.detalleFactura){
      this.detalleACrearEditar = this.data.detalleFactura;
      this.idDetalleFactura=this.data.detalleFactura.id;
    }
  }

  escucharFormularioDetalle(eventoFormularioDetalle: FormGroup) {
    this.esFormularioDetalleValido = eventoFormularioDetalle.valid;
    if(this.esFormularioDetalleValido) {
      this.detalleACrearEditar = eventoFormularioDetalle.value;
      this.detalleACrearEditar.precioUnitario = this.detalleACrearEditar.precioUnitario;
      this.detalleACrearEditar.total=(this.detalleACrearEditar.cantidad * this.detalleACrearEditar.precioUnitario);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  guardarDetalle(){
    this._cargandoService.habilitarCargando();
    if(this.idDetalleFactura){
      this.detalleACrearEditar.id=this.idDetalleFactura;
    }
    this.dialogRef.close(this.detalleACrearEditar);
    this._cargandoService.deshabiltarCargando();
  }
}

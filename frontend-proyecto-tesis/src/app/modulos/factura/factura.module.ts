import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacturaRoutingModule } from './factura-routing.module';
import { ListarFacturasComponent } from './rutas/listar-facturas/listar-facturas.component';
import { CrearFacturaComponent } from './rutas/crear-factura/crear-factura.component';
import { EditarFacturaComponent } from './rutas/editar-factura/editar-factura.component';
import { VerFacturaComponent } from './rutas/ver-factura/ver-factura.component';
import { FormularioCabeceraFacturaComponent } from './formularios/formulario-cabecera-factura/formulario-cabecera-factura.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { TableModule } from 'primeng/table';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { PanelModule } from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { InputText, InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { CabeceraFacturaService } from './servicios/cabecera-factura.service';
import { ModalDetalleComponent } from './modales/modal-detalle/modal-detalle.component';
import { FormularioDetalleComponent } from './formularios/formulario-detalle/formulario-detalle.component';
import { DropdownModule } from 'primeng/dropdown';
import {ConfirmationService} from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ClientesPorCobrarComponent } from './rutas/clientes-por-cobrar/clientes-por-cobrar.component';

@NgModule({
  imports: [
    CommonModule,
    FacturaRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AutoCompleteModule,
    TableModule,
    InputTextModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    CalendarModule,
    DropdownModule,
    ConfirmDialogModule
    
  ],
  declarations: [
    ListarFacturasComponent , 
    CrearFacturaComponent, 
    EditarFacturaComponent, 
    VerFacturaComponent, 
    FormularioCabeceraFacturaComponent, 
    ModalDetalleComponent, FormularioDetalleComponent, ClientesPorCobrarComponent
  ],
  providers: [
    CabeceraFacturaService,
    ConfirmationService
  ],
  entryComponents: [
    ModalDetalleComponent
  ]
})
export class FacturaModule { }

import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';
import { FormGroup } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { CabeceraFacturaService } from '../../servicios/cabecera-factura.service';
import { DetalleFacturaService } from '../../servicios/detalle-factura.service';

@Component({
  selector: 'app-crear-factura',
  templateUrl: './crear-factura.component.html',
  styleUrls: ['./crear-factura.component.css']
})
export class CrearFacturaComponent implements OnInit {
  items: MenuItem[];
  icono = ICONOS.pagos;
  esFormularioFacturaValido = false;
  facturaACrear: CabeceraFactura = {};
  arregloDetallesFactura: DetalleInterface[] = [];
  formularioCrearFactura: FormGroup;

  constructor(
    private readonly _cabeceraFacturaService: CabeceraFacturaService,
    private readonly _detalleFacturaService: DetalleFacturaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.items = cargarRutas('pago', 'Registrar', 'Lista de pagos');
  }

  escucharFormularioFactura(eventoFormularioFactura: FormGroup) {
    this.esFormularioFacturaValido = eventoFormularioFactura.valid;
    if (this.esFormularioFacturaValido) {
      this.formularioCrearFactura = eventoFormularioFactura;
    }
  }

  crearFactura() {
    this._cargandoService.habilitarCargando();
    this.setarFactura();
    this._cabeceraFacturaService.create(this.facturaACrear).
      subscribe(cabeceraCreada => {
        const detallesACrear = this.arregloDetallesFactura.map(detalle => {
          detalle.cabeceraFactura = cabeceraCreada;
          return detalle;
        });
        this._detalleFacturaService.createMany(detallesACrear)
          .subscribe(detalles => {
            this.irListarFacturas();
            this._toasterService.pop('success', 'Éxito', 'EL pago se ha registrado');
            this._cargandoService.deshabiltarCargando();
          },
            error => {
              this._toasterService.pop('error', 'Error', 'Error al registrar el pago');
              this._cargandoService.deshabiltarCargando();
            });
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al registrar el pago');
          this._cargandoService.deshabiltarCargando();
        });
  }
  setarFactura() {
    this.facturaACrear.fecha = this.formularioCrearFactura.get('fecha').value;
    this.facturaACrear.subtotal = this.formularioCrearFactura.get('subtotal').value;
    this.facturaACrear.total = this.formularioCrearFactura.get('total').value;
    this.facturaACrear.usuario = this.formularioCrearFactura.get('usuario').value;
    this.arregloDetallesFactura= this.formularioCrearFactura.get('detalles').value;
  }

  irListarFacturas() {
    this._router.navigate(['/app', 'pago', 'listar']);
  }

}

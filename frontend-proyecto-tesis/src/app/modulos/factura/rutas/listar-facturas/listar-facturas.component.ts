import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';
import { CabeceraFacturaService } from '../../servicios/cabecera-factura.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { AutenticacionService } from '../../../autenticacion/autenticacion.service';

@Component({
  selector: 'app-listar-facturas',
  templateUrl: './listar-facturas.component.html',
  styleUrls: ['./listar-facturas.component.css']
})
export class ListarFacturasComponent implements OnInit {
  items: MenuItem[];
  icono = ICONOS.pagos;
  arregloFacturas: CabeceraFactura[] = [];
  parametroBusqueda = '';
  columnas = [
    { field: 'numero', header: 'N°', width: '5%' },
    { field: 'cliente', header: 'Cliente', width: '40%' },
    { field: 'fecha', header: 'Fecha', width: '10%' },
    { field: 'total', header: 'Total', width: '10%' },
    { field: 'estado', header: 'Estado', width: '15%' },
    { field: 'acciones', header: 'Acciones', width: '20%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  constructor(
    private readonly _cabeceraFacturaService: CabeceraFacturaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterServce: ToasterService,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.items = cargarRutas('factura', 'Listar', 'Lista de Pagos');
    this.items.pop();
    this.obtenerPorParametro();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    this._cabeceraFacturaService.findWhereOr(this.parametroBusqueda)
      .subscribe(facturas => {
        this.arregloFacturas = facturas;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterServce.pop('error', 'Error', 'Error al traer datos');
          this._cargandoService.deshabiltarCargando();
        });
  }

  cambiarEstado(factura: CabeceraFactura) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloFacturas.indexOf(factura);
    const estaValida = this.arregloFacturas[indice].estado == 'valida';
    estaValida ? this.arregloFacturas[indice].estado = 'anulada' : 'valida';
    !estaValida ? this.arregloFacturas[indice].estado = 'valida' : 'anulada';
    this._cabeceraFacturaService.update(this.arregloFacturas[indice].id, { estado: this.arregloFacturas[indice].estado })
      .subscribe(() => {
        this._cargandoService.deshabiltarCargando();
      },
      error => {
        this._toasterServce.pop('error', 'Error', 'Error al actualizar el estado');
        this._cargandoService.deshabiltarCargando();
      });
  }
}

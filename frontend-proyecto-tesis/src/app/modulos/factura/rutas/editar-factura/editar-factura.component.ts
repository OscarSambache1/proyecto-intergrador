import { Component, OnInit } from '@angular/core';
import { CabeceraFacturaService } from '../../servicios/cabecera-factura.service';
import { DetalleFacturaService } from '../../servicios/detalle-factura.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';
import { FormGroup } from '@angular/forms';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { cargarRutas } from '../../../../funciones/cargarRutas';

@Component({
  selector: 'app-editar-factura',
  templateUrl: './editar-factura.component.html',
  styleUrls: ['./editar-factura.component.css']
})
export class EditarFacturaComponent implements OnInit {
  facturaAEditar: CabeceraFactura;
  facturaEditada: CabeceraFactura = {};
  esFormularioFacturaValido=false;
  formularioEditarFactura: FormGroup;
  arregloDetallesFactura: DetalleInterface[] = [];
  items: MenuItem[];
  icono= ICONOS.pagos;

  constructor(
    private readonly _cabeceraFacturaService: CabeceraFacturaService,
    private readonly _detalleFacturaService: DetalleFacturaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activateRoute: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.items = cargarRutas('pago', 'editar', 'Lista de pagos');
    this.setearCabeceraAEditar();
  }

  escucharFormularioFactura(eventoFormularioFactura: FormGroup) {
    this.esFormularioFacturaValido = eventoFormularioFactura.valid;
    if (this.esFormularioFacturaValido) {
      this.formularioEditarFactura = eventoFormularioFactura;
    }
  }

  editarFactura(){
   this._cargandoService.habilitarCargando();
    this.setearCabeceraEditada();
    this._cabeceraFacturaService.update(this.facturaAEditar.id,this.facturaEditada)
    .subscribe(cabeceraEditada=>{
      this.actualizarDetalles(cabeceraEditada);
      this._toasterService.pop('success', 'Éxito', 'El pago se ha actualizado');
      this.irListarFacturas();
    }
  ,error=>{
    this._toasterService.pop('error','Error','Error al editar el pago')
    this._cargandoService.deshabiltarCargando();
  });
  }

  actualizarDetalles(cabeceraEditada: CabeceraFactura){
    const detallesGuardados=cabeceraEditada.detallesFactura;
    const detallesSeleccionados = this.arregloDetallesFactura;
    
    detallesGuardados.forEach(detalleGuardado=>{
      const estaDetalleGuardadoEnDetallesSeleccionados = detallesSeleccionados.find( 
        detalleSeleccionado => 
        detalleSeleccionado.id === detalleGuardado.id
      );

      if(!estaDetalleGuardadoEnDetallesSeleccionados){
       this._detalleFacturaService.delete(detalleGuardado.id)
       .subscribe(
         respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error al editar el pago')
          this._cargandoService.deshabiltarCargando();
        }
       );        
      }
    });

    detallesSeleccionados.forEach(detalleSeleccionado=>{
      if(!detalleSeleccionado.id){
        const detalleACrear: DetalleInterface = detalleSeleccionado;
        detalleACrear.cabeceraFactura=cabeceraEditada;
        this._detalleFacturaService.create(detalleACrear)
        .subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
           },
           error=>{
            this._toasterService.pop('error','Error','Error al editar el pago')
            this._cargandoService.deshabiltarCargando();
          }
        );
      }
      else{
      delete (detalleSeleccionado as any).nombre;
       this._detalleFacturaService.update(detalleSeleccionado.id,detalleSeleccionado)
       .subscribe(
        respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error editar el pago')
          this._cargandoService.deshabiltarCargando();
        }
       );
      }
    })
   }


   setearCabeceraEditada(){
    this.facturaEditada.fecha = this.formularioEditarFactura.get('fecha').value;
    this.facturaEditada.subtotal = this.formularioEditarFactura.get('subtotal').value;
    this.facturaEditada.total = this.formularioEditarFactura.get('total').value;
    this.facturaEditada.usuario = this.formularioEditarFactura.get('usuario').value;
    this.arregloDetallesFactura= this.formularioEditarFactura.get('detalles').value;
  }

  setearCabeceraAEditar(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCabeceraFactura = Number(respuestaParametros.get('id'));
      this._cabeceraFacturaService.findOne(idCabeceraFactura).subscribe(
        cabecera => {
          this.facturaAEditar = cabecera;
          this.facturaAEditar.total = Number(cabecera.total);
          this.facturaAEditar.subtotal = Number(cabecera.subtotal);
          this.facturaAEditar.detallesFactura = cabecera.detallesFactura.map(detalle => {
            detalle.precioUnitario = Number(detalle.precioUnitario);
            detalle.total = Number(detalle.total);
            return detalle;
          })
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar pago')
          this._cargandoService.deshabiltarCargando();
        }
      );
    });
   }

   irListarFacturas() {
    this._router.navigate(['/app', 'pago', 'listar']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from 'src/app/constantes/iconos';
import { ClienteService } from 'src/app/modulos/cliente/cliente.service';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutas } from 'src/app/funciones/cargarRutas';

@Component({
  selector: 'app-clientes-por-cobrar',
  templateUrl: './clientes-por-cobrar.component.html',
  styleUrls: ['./clientes-por-cobrar.component.css']
})
export class ClientesPorCobrarComponent implements OnInit {

  clientes: Cliente[] = [];
  diaPago;
  clienteSeleccionado: Cliente
  columnas = [
    { field: 'nombres', header: 'Nombres', width: '30%' },
    { field: 'apellidos', header: 'Apellidos', width: '30%' },
    { field: 'codigo', header: 'Codigo', width: '20%' },
    { field: 'diaPago', header: 'Dia de Pago', width: '20%' },
  ];
  items: MenuItem[];
  icono = ICONOS.cliente;
  habilitarBoton: boolean;
  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items = cargarRutas('pago', 'Listar', 'Lista de pagos');
    this.items.pop();
  }

  obtenerPorParametro() {

    this._cargandoService.habilitarCargando();
    const consulta = {
      order: { id: 'DESC' },
      where: {
        diaPago: this.diaPago ? this.diaPago : new Date().getDate(),
        estado: 1
      },
      relations: ['usuario']
    };
    this._clienteService.findAll(consulta)
      .subscribe(clientesPorParametro => {
        this.clientes = clientesPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al traer datos');
          this._cargandoService.deshabiltarCargando();
        });
  }
}

import { Component, OnInit } from '@angular/core';
import { CabeceraFactura } from '../../../../interfaces/cabecera-factura.interface';
import { DetalleInterface } from '../../../../interfaces/detalle-factura.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { CabeceraFacturaService } from '../../servicios/cabecera-factura.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';

@Component({
  selector: 'app-ver-factura',
  templateUrl: './ver-factura.component.html',
  styleUrls: ['./ver-factura.component.css']
})
export class VerFacturaComponent implements OnInit {

  facturaAVer: CabeceraFactura;
  arregloDetallesFactura: DetalleInterface[] = [];
  items: MenuItem[];
  icono= ICONOS.pagos;

  constructor(
    private readonly _cabeceraFacturaService: CabeceraFacturaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activateRoute: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.items = cargarRutas('pago', 'ver', 'Lista de pagos');
    this.setearCabeceraAVer();
  }

  setearCabeceraAVer(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCabeceraFactura = Number(respuestaParametros.get('id'));
      this._cabeceraFacturaService.findOne(idCabeceraFactura).subscribe(
        cabecera => {
          this.facturaAVer = cabecera;
          this.facturaAVer.total = Number(cabecera.total);
          this.facturaAVer.subtotal = Number(cabecera.subtotal);
          this.facturaAVer.detallesFactura = cabecera.detallesFactura.map(detalle => {
            detalle.total = Number(detalle.total);
            return detalle;
          })
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar pago')
          this._cargandoService.deshabiltarCargando();
        }
      );
    });
   }

   irListarFacturas() {
    this._router.navigate(['/app', 'pago', 'listar']);
  }
}

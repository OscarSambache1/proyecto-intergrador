import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TipoComida } from '../../../../interfaces/tipo-comida.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-tipo-comida',
  templateUrl: './formulario-tipo-comida.component.html',
  styleUrls: ['./formulario-tipo-comida.component.css']
})
export class FormularioTipoComidaComponent implements OnInit {

  @Output() esValidoFormularioTipoComida: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() tipoComidaSeleccionada: TipoComida;
  formularioTipoComida: FormGroup
  mensajesErroresNombre = [];
  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioTipoComida = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
    })
  }

  escucharFormulario() {
    this.formularioTipoComida.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresNombre = setearMensajes(this.formularioTipoComida.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioTipoComida.valueChanges.subscribe(formulario => {
      this.esValidoFormularioTipoComida.emit(this.formularioTipoComida);
    });
  }


  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioTipoComida.disable();
    }

  }

  llenarFormulario() {
    this.formularioTipoComida.patchValue(
      {
        nombre: this.tipoComidaSeleccionada.nombre,
      }
    )
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaTiposComidaComponent } from './rutas/lista-tipos-comida/lista-tipos-comida.component';
import { CrearTipoComidaComponent } from './rutas/crear-tipo-comida/crear-tipo-comida.component';
import { EditarTipoComidaComponent } from './rutas/editar-tipo-comida/editar-tipo-comida.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListaTiposComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'crear',
    component: CrearTipoComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'editar/:id',
    component: EditarTipoComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoComidaRoutingModule { }

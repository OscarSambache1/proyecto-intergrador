import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoComidaRoutingModule } from './tipo-comida-routing.module';
import { ListaTiposComidaComponent } from './rutas/lista-tipos-comida/lista-tipos-comida.component';
import { CrearTipoComidaComponent } from './rutas/crear-tipo-comida/crear-tipo-comida.component';
import { EditarTipoComidaComponent } from './rutas/editar-tipo-comida/editar-tipo-comida.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { PanelModule } from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { FormularioTipoComidaComponent } from './formularios/formulario-tipo-comida/formulario-tipo-comida.component';

@NgModule({
  imports: [
    CommonModule,
    TipoComidaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [ListaTiposComidaComponent, CrearTipoComidaComponent, EditarTipoComidaComponent, FormularioTipoComidaComponent]
})
export class TipoComidaModule { }

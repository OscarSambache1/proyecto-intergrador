import { Component, OnInit } from '@angular/core';
import { TipoComida } from '../../../../interfaces/tipo-comida.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { TipoComidaService } from '../../../comida/servicios/tipo-comida.service';
import { Router } from '@angular/router';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crear-tipo-comida',
  templateUrl: './crear-tipo-comida.component.html',
  styleUrls: ['./crear-tipo-comida.component.css']
})
export class CrearTipoComidaComponent implements OnInit {
  tipoComidaACrear: TipoComida = {};
  esFormularioTipoComidaValido = false;
  items: MenuItem[];
  icono= ICONOS.tipoComida;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _tipoComidaService: TipoComidaService,
    private readonly _router: Router,

  ) { }
  ngOnInit() {
    this.items= cargarRutas('tipo-comida','Registrar', 'Lista de tipos de comida');
  }

  escucharFormularioTipoComida(eventoFormularioTipoComida: FormGroup) {
    this.esFormularioTipoComidaValido = eventoFormularioTipoComida.valid;
    this.tipoComidaACrear = eventoFormularioTipoComida.value;
  }

  crearTipoComida() {
    this._cargandoService.habilitarCargando();
    this._tipoComidaService.create(this.tipoComidaACrear).subscribe(() => {
      this.irListarTiposComida();
      this._toasterService.pop('success', 'Éxito', 'El tipo de comida se ha registrado correctamente');
      this._cargandoService.deshabiltarCargando();
    }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'El error al registra tipo de comida');
        this._cargandoService.deshabiltarCargando();
      })
  }

  irListarTiposComida() {
    this._router.navigate(['/app', 'tipo-comida', 'listar']);
  }
}

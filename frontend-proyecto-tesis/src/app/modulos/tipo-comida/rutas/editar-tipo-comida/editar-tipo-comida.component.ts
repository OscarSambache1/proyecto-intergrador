import { Component, OnInit } from '@angular/core';
import { TipoComida } from '../../../../interfaces/tipo-comida.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { TipoComidaService } from '../../../comida/servicios/tipo-comida.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editar-tipo-comida',
  templateUrl: './editar-tipo-comida.component.html',
  styleUrls: ['./editar-tipo-comida.component.css']
})
export class EditarTipoComidaComponent implements OnInit {

  tipoComidaAEditar: TipoComida;
  tipoComidaEditado: TipoComida = {};
  esFormularioTipoComidaValido = false;
  items: MenuItem[];
  icono= ICONOS.tipoComida;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _tipoComidaService: TipoComidaService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.setearTipoComida();
    this.items= cargarRutas('tipo-comida','Editar', 'Lista de tipos de comida');
  }

  escucharFormularioTipoComida(eventoFormularioTipoComida: FormGroup) {
    this.esFormularioTipoComidaValido = eventoFormularioTipoComida.valid;
    this.tipoComidaEditado = eventoFormularioTipoComida.value;
  }

  editarTipoComida(){
    this._cargandoService.habilitarCargando();
    this._tipoComidaService.update(this.tipoComidaAEditar.id,this.tipoComidaEditado).subscribe(()=>{
      this.irListarTipoComida();
      this._toasterService.pop('success', 'Éxito', 'El tipo de comida se ha editado correctamente'); 
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._toasterService.pop('error', 'Error', 'Error al editar el tipo de comida'); 
      this._cargandoService.deshabiltarCargando();
    }
  )
   }

   setearTipoComida(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idTipoComida = Number(respuestaParametros.get('id'));
      this._tipoComidaService.findOne(idTipoComida).subscribe(
        tipoComida => {
          this.tipoComidaAEditar = tipoComida;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar tipo de comida')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarTipoComida(){
    this._router.navigate(['/app', 'tipo-comida', 'listar']);
   }


}

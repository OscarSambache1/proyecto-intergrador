import { Component, OnInit } from '@angular/core';
import { TipoComida } from '../../../../interfaces/tipo-comida.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { TipoComidaService } from '../../../comida/servicios/tipo-comida.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-lista-tipos-comida',
  templateUrl: './lista-tipos-comida.component.html',
  styleUrls: ['./lista-tipos-comida.component.css']
})
export class ListaTiposComidaComponent implements OnInit {

  arregloTiposComida: TipoComida[] = [];
  parametroBusqueda = '';
  columnas=[
    { field: 'nombre', header: 'Nombre', width:'50%' },
    { field: 'estado', header: 'Estado',width:'20%' },
    { field: 'acciones', header: 'Acciones',width:'30%' },
  ];
  items: MenuItem[];
  icono= ICONOS.tipoComida;
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  constructor(
    private readonly _tipoComidaService: TipoComidaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterServce: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('tipo-comida','', 'Lista de tipos de comida');
    this.items.pop();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
      { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._tipoComidaService.findAll(consulta)
      .subscribe(arreglosPorParametro => {
        this.arregloTiposComida = arreglosPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
    ,
    error => {
      this._toasterServce.pop('error','Error','Error al mostrar tipos de comida')
      this._cargandoService.deshabiltarCargando();
    })
  }

  cambiarEstado(tipoComida: TipoComida) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloTiposComida.indexOf(tipoComida);
    const estaActivo = this.arregloTiposComida[indice].estado == 1;
    estaActivo ? this.arregloTiposComida[indice].estado = 0 : 1;
    !estaActivo ? this.arregloTiposComida[indice].estado = 1 : 0;
    this._tipoComidaService.update(this.arregloTiposComida[indice].id, { estado: this.arregloTiposComida[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterServce.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }
}

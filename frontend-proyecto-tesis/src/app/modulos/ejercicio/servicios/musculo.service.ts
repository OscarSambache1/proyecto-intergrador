import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Musculo } from '../../../interfaces/musculo.interface';

@Injectable({
  providedIn: 'root'
})
export class MusculoService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta:any):Observable<any>{
    return  this._httpClient.get(environment.url+`/musculo?consulta=`+`${JSON.stringify(consulta)}`)
  }

  findOne(id:number): Observable<Musculo>{
    return  this._httpClient.get(environment.url+`/musculo/${id}`)
  }
  
  create(musculo: Musculo): Observable<Musculo> {
    return this._httpClient.post(environment.url + '/musculo', musculo)
  }
  
  update(id: number, musculo: Musculo): Observable<Musculo> {
    return this._httpClient.put(environment.url + `/musculo/${id}`, musculo)
  }
  
}

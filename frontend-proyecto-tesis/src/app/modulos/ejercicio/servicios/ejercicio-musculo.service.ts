import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { EjercicioMusculo } from '../../../interfaces/ejercicio-musculo.interface';

@Injectable({
  providedIn: 'root'
})
export class EjercicioMusculoService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }
  update(id: number, ejercicioMusculo: EjercicioMusculo): Observable<EjercicioMusculo> {
    return this._httpClient.put(environment.url + `/ejercicio-musculo/${id}`, ejercicioMusculo)
  }
  
  findOne(id:number): Observable<EjercicioMusculo>{
    return  this._httpClient.get(environment.url+`/ejercicio-musculo/${id}`)
  }

  findAll(consulta:any):Observable<any>{
    return  this._httpClient.get(environment.url+`/ejercicio-musculo?consulta=`+`${JSON.stringify(consulta)}`)
  }

  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/ejercicio-musculo/${id}`)
  }
  
  create(ejercicioMusculo: EjercicioMusculo): Observable<EjercicioMusculo>{
    return this._httpClient.post(environment.url+'/ejercicio-musculo/', ejercicioMusculo)
}



}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Ejercicio } from '../../../interfaces/ejercicio.interface';

@Injectable({
  providedIn: 'root'
})
export class EjercicioService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta:any):Observable<any>{
    return  this._httpClient.get(environment.url+`/ejercicio?consulta=`+`${JSON.stringify(consulta)}`)
  }

  findNombreYMusculo(consulta:any):Observable<any>{
    return this._httpClient.get(environment.url + `/ejercicio/findByNombreOMusculo?parametro=${consulta.parametro}&musculo=${consulta.musculo}`)
  }

  create(ejercicio: Ejercicio): Observable<Ejercicio>{
    return this._httpClient.post(environment.url+'/ejercicio/', ejercicio)
}
findOne(id:number): Observable<Ejercicio>{
  return  this._httpClient.get(environment.url+`/ejercicio/${id}`)
}
update(id: number, ejercicio: Ejercicio): Observable<Ejercicio> {
  return this._httpClient.put(environment.url + `/ejercicio/${id}`, ejercicio)
}

}

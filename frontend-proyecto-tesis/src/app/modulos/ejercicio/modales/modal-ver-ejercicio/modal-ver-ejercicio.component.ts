import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { EjercicioService } from '../../servicios/ejercicio.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { ICONOS } from '../../../../constantes/iconos';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-modal-ver-ejercicio',
  templateUrl: './modal-ver-ejercicio.component.html',
  styleUrls: ['./modal-ver-ejercicio.component.css']
})
export class ModalVerEjercicioComponent implements OnInit {
  ejercicioAVer: Ejercicio;
  icono = ICONOS.ejercicio; 
  urlImagen = environment.url + '/archivos-ejercicios/';
  constructor(
  private readonly _ejercicioService: EjercicioService,
  private readonly _cargandoService: CargandoService,
  private readonly _toasterService: ToasterService,
  public dialogRef: MatDialogRef<ModalVerEjercicioComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  
  onNoClick(): void {
    this.dialogRef.close(this.ejercicioAVer);
  }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this._ejercicioService.findOne(this.data.id).subscribe(
      ejercicio => {
        this.ejercicioAVer = ejercicio;
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._toasterService.pop('error', 'Error', 'Error mostrar el ejercicio')
        this._cargandoService.deshabiltarCargando();
      }
    )
  }


}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarEjercicioComponent } from './componentes/listar-ejercicio/listar-ejercicio.component';
import { CrearEjercicioComponent } from './componentes/crear-ejercicio/crear-ejercicio.component';
import { EditarEjercicioComponent } from './componentes/editar-ejercicio/editar-ejercicio.component';
import { VerEjercicioComponent } from './componentes/ver-ejercicio/ver-ejercicio.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarEjercicioComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: 'crear',
    component : CrearEjercicioComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'editar/:id',
    component: EditarEjercicioComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component: VerEjercicioComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjercicioRoutingModule { }

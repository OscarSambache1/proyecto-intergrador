import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EjercicioRoutingModule } from './ejercicio-routing.module';
import { CrearEjercicioComponent } from './componentes/crear-ejercicio/crear-ejercicio.component';
import { EditarEjercicioComponent } from './componentes/editar-ejercicio/editar-ejercicio.component';
import { VerEjercicioComponent } from './componentes/ver-ejercicio/ver-ejercicio.component';
import { ListarEjercicioComponent } from './componentes/listar-ejercicio/listar-ejercicio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { FormularioEjercicioComponent } from './formularios/formulario-ejercicio/formulario-ejercicio.component';
import {MultiSelectModule} from 'primeng/multiselect';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { EjercicioService } from './servicios/ejercicio.service';
import { EjercicioMusculoService } from './servicios/ejercicio-musculo.service';
import { MusculoService } from './servicios/musculo.service';
import { ModalVerEjercicioComponent } from './modales/modal-ver-ejercicio/modal-ver-ejercicio.component';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { TablaEjerciciosModule } from '../../componentes/tabla-ejercicios/tabla-ejercicios.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EjercicioRoutingModule,
    TableModule,
    MultiSelectModule,
    InputTextareaModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    TablaEjerciciosModule
  ],
  declarations: [
    ModalVerEjercicioComponent,
    CrearEjercicioComponent,
    EditarEjercicioComponent,
    VerEjercicioComponent,
    ListarEjercicioComponent,
    FormularioEjercicioComponent,
  ],
  providers:[
    EjercicioService,
    EjercicioMusculoService,
    MusculoService
  ],
  entryComponents:[
    ModalVerEjercicioComponent
  ]
})
export class EjercicioModule { }

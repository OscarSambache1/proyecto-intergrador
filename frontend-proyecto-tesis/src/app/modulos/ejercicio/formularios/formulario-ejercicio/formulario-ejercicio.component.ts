import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, AbstractControl, Validators, FormBuilder } from '@angular/forms';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { SelectItem } from 'primeng/api';
import { MusculoService } from '../../servicios/musculo.service';
import { ToasterService } from 'angular2-toaster';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-ejercicio',
  templateUrl: './formulario-ejercicio.component.html',
  styleUrls: ['./formulario-ejercicio.component.css']
})
export class FormularioEjercicioComponent implements OnInit {
  @Output() esValidoFormularioEjercicio: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() esFormularioVerModal = false;
  @Input() ejercicioSeleccionado: Ejercicio;
  formularioEjercicio: FormGroup
  arregloMusculos: SelectItem[] = [];
  musculosSeleccionados: Musculo[];
  etiquetaPorDefectoDropdown: string;

  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresMusculos = [];
  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };

  private mensajesValidacionMusculos = {
    required: 'La selección de músculos es obligatoría'
  };

  constructor(
    private readonly _musculoService: MusculoService,
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService

  ) { }

  ngOnInit() {
    this.llenarDropdownMusculos();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  setearEtiquetaDropdown() {
    this.etiquetaPorDefectoDropdown = "";
    if (this.musculosSeleccionados) {
      this.musculosSeleccionados
        .forEach((musculo, indice) => {
          if (indice == 0) {
            this.etiquetaPorDefectoDropdown = musculo.nombre;
          }
          else {
            this.etiquetaPorDefectoDropdown = this.etiquetaPorDefectoDropdown + ', ' + musculo.nombre;
          }
        })
    }
    else {
      this.etiquetaPorDefectoDropdown = "Seleccione un musculo";
    }
  }

  llenarDropdownMusculos() {
    const consulta = { order: { id: 'DESC'}, where: {estado: 1} };
    this._musculoService.findAll(consulta).subscribe(musculos => {
      musculos.forEach(musculo => {
        const elemento = {
          label: musculo.nombre,
          value: musculo
        }
        this.arregloMusculos.push(elemento)
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos')
      }
    );
  }

  crearFormulario() {
    this.formularioEjercicio = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
      descripcion: ['', [Validators.required]],
      musculos: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioEjercicio.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioEjercicio.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresMusculos = setearMensajes(this.formularioEjercicio.get('musculos'), this.mensajesValidacionMusculos);
        this.mensajesErroresNombre = setearMensajes(this.formularioEjercicio.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioEjercicio.valueChanges.subscribe(formulario => {
      this.musculosSeleccionados=this.formularioEjercicio.get('musculos').value;
      this.esValidoFormularioEjercicio.emit(this.formularioEjercicio);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
      this.setearEtiquetaDropdown();
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioEjercicio.disable();
    }

  }

  llenarFormulario() {
    const musculos = this.ejercicioSeleccionado.ejerciciosMusculo
      .map(ejercicioMusculo => {
        return ejercicioMusculo.musculo;
      })

    this.formularioEjercicio.patchValue(
      {
        nombre: this.ejercicioSeleccionado.nombre,
        descripcion: this.ejercicioSeleccionado.descripcion,
        musculos
      }
    )
    this.musculosSeleccionados = musculos;
    this.setearEtiquetaDropdown();
  }
  
}

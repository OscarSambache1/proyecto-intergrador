import { Component, OnInit } from '@angular/core';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { EjercicioService } from '../../servicios/ejercicio.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ver-ejercicio',
  templateUrl: './ver-ejercicio.component.html',
  styleUrls: ['./ver-ejercicio.component.css']
})
export class VerEjercicioComponent implements OnInit {
  ejercicioAVer: Ejercicio;
  items: MenuItem[];
  icono= ICONOS.ejercicio;
  urlImagen;

  constructor(
    private readonly _ejercicioService : EjercicioService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,

  ) { }

  ngOnInit() {
    this.setearEjercicio();
    this.items= cargarRutas('ejercicio','Ver', 'Listar ejercicios');
  }
  setearEjercicio(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idEjercicio = Number(respuestaParametros.get('id'));
      this._ejercicioService.findOne(idEjercicio).subscribe(
        ejercicio => {
          this.ejercicioAVer = ejercicio;
          this.urlImagen = `${environment.url}/archivos-ejercicios/${this.ejercicioAVer.urlImagen}`;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error', 'Error', 'Error mostrar el ejercicio')
          this._cargandoService.deshabiltarCargando();

        }
      )
    })
   }

   irListarEjercicios(){
    this._router.navigate(['/app', 'ejercicio', 'listar']);
   }
}

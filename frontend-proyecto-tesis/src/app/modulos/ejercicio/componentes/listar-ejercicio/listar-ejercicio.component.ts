import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-listar-ejercicio',
  templateUrl: './listar-ejercicio.component.html',
  styleUrls: ['./listar-ejercicio.component.css']
})
export class ListarEjercicioComponent implements OnInit {
  items: MenuItem[];
  icono= ICONOS.ejercicio;

  constructor(
  ) { }

  ngOnInit() {
    this.items= cargarRutas('ejercicio','Listar', 'Listar ejercicios');
    this.items.pop();
  }
}

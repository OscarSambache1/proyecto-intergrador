import { Component, OnInit } from '@angular/core';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { EjercicioService } from '../../servicios/ejercicio.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { EjercicioMusculoService } from '../../servicios/ejercicio-musculo.service';
import { EjercicioMusculo } from '../../../../interfaces/ejercicio-musculo.interface';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { SubirArchivoService } from '../../../../servicios/subir-archivo.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-editar-ejercicio',
  templateUrl: './editar-ejercicio.component.html',
  styleUrls: ['./editar-ejercicio.component.css']
})
export class EditarEjercicioComponent implements OnInit {
  ejercicioAEditado: Ejercicio = {};
  ejercicioAEditar: Ejercicio;
  esFormularioEjercicioValido = false;
  formularioEditarEjercicio: FormGroup;
  selectedFile;
  items: MenuItem[];
  icono= ICONOS.ejercicio;
  urlImagen;

  constructor(
    private readonly _ejercicioService: EjercicioService,
    private readonly _subirArchivoService: SubirArchivoService,
    private readonly _ejercicioMusculoService: EjercicioMusculoService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService
  ) { }

  ngOnInit() {
    this.setearEjercicio();
    this.items= cargarRutas('ejercicio','Editar', 'Listar ejercicios');
  }

  escucharFormularioEjercicio(eventoFormularioEjercicio: FormGroup) {
    this.formularioEditarEjercicio = eventoFormularioEjercicio;
    this.esFormularioEjercicioValido = this.formularioEditarEjercicio.valid;
  }

  editarEjercicio() {
    this._cargandoService.habilitarCargando();
    this.ejercicioAEditado.nombre = this.formularioEditarEjercicio.get('nombre').value;
    this.ejercicioAEditado.descripcion = this.formularioEditarEjercicio.get('descripcion').value;
    this._ejercicioService.update(this.ejercicioAEditar.id, this.ejercicioAEditado).
      subscribe(ejercicioEditado => {
        this.actualizarMusculos(ejercicioEditado);
        const seSeleccionoFoto = this.selectedFile;
        if (seSeleccionoFoto) {
          this.subirImagenEjercicio(ejercicioEditado.id);
        }
        this.irListarEjercicios();
        this._toasterService.pop('success', 'Éxito', 'La informacin se ha actualizado');
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error editar el ejercicio')
          this._cargandoService.deshabiltarCargando();
        });
  }

  setearEjercicio() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idEjercicio = Number(respuestaParametros.get('id'));
      this._ejercicioService.findOne(idEjercicio).subscribe(
        ejercicio => {
          this.ejercicioAEditar = ejercicio;
          this.urlImagen = `${environment.url}/archivos-ejercicios/${this.ejercicioAEditar.urlImagen}`;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error mostrar el ejercicio')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
  }

  irListarEjercicios() {
    this._router.navigate(['/app', 'ejercicio', 'listar']);
  }

  obtenerMusculosGuardados(ejerciciosMusculo: EjercicioMusculo[]): Musculo[] {
    const musculosGuardados = ejerciciosMusculo.map(ejerciciosMusculo => {
      return ejerciciosMusculo.musculo;
    });
    return musculosGuardados;
  }

  actualizarMusculos(ejercicioEditado: Ejercicio) {
    const musculosGuardados = this.obtenerMusculosGuardados(this.ejercicioAEditar.ejerciciosMusculo);
    const musculosSeleccionados = this.formularioEditarEjercicio.get('musculos').value
    this.ejercicioAEditar.ejerciciosMusculo.forEach(ejercicioMusculoGuardado => {
      const musculoGuardado = ejercicioMusculoGuardado.musculo;
      const estaMusculoEnArregloMusculosSeleccionado = musculosSeleccionados.find(musculosSeleccionado => musculosSeleccionado === musculoGuardado);
      if (!estaMusculoEnArregloMusculosSeleccionado) {
        this._ejercicioMusculoService.delete(ejercicioMusculoGuardado.id).subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            this._toasterService.pop('error', 'Error', 'Error al editar el ejercicio')
            this._cargandoService.deshabiltarCargando();
          }
        );
      }
    });

    musculosSeleccionados.forEach(musculoSeleccionado => {
      this._cargandoService.habilitarCargando();
      const estaMucusloEnArregloMusculosGuardados = musculosGuardados.find(musculoGuardado => musculoGuardado === musculoSeleccionado);
      if (!estaMucusloEnArregloMusculosGuardados) {
        const ejercicioMusculoSeleccionadoACrear: EjercicioMusculo = {
          musculo: musculoSeleccionado,
          ejercicio: this.ejercicioAEditar
        };
        this._ejercicioMusculoService.create(ejercicioMusculoSeleccionadoACrear)
        .subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            this._toasterService.pop('error', 'Error', 'Error al editar el ejercicio')
            this._cargandoService.deshabiltarCargando();
          }
        )
      }
    })
  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0]
  }

  subirImagenEjercicio(id: number) {
    this._subirArchivoService.onUpload(id, this.selectedFile, 'ejercicio').
      subscribe(archivoSubido => {
      })
  }

}



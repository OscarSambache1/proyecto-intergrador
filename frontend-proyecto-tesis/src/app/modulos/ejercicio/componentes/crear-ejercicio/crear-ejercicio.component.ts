import { Component, OnInit } from '@angular/core';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { EjercicioMusculo } from '../../../../interfaces/ejercicio-musculo.interface';
import { EjercicioService } from '../../servicios/ejercicio.service';
import { EjercicioMusculoService } from '../../servicios/ejercicio-musculo.service';
import { SubirArchivoService } from '../../../../servicios/subir-archivo.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-ejercicio',
  templateUrl: './crear-ejercicio.component.html',
  styleUrls: ['./crear-ejercicio.component.css']
})
export class CrearEjercicioComponent implements OnInit {
  ejercicioACrear: Ejercicio = {};
  ejercicioMusculo: EjercicioMusculo = {};
  esFormularioEjercicioValido = false;
  formularioCrearEjercicio: FormGroup;
  selectedFile;
  items: MenuItem[];
  icono= ICONOS.ejercicio;

  constructor(
    private readonly _ejercicioService: EjercicioService,
    private readonly _subirArchivoService: SubirArchivoService,
    private readonly _ejercicioMusculoService: EjercicioMusculoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.items= cargarRutas('ejercicio','Registrar', 'Listar ejercicios');
  }

  escucharFormularioEjercicio(eventoFormularioEjercicio: FormGroup) {
    this.formularioCrearEjercicio = eventoFormularioEjercicio;
    this.esFormularioEjercicioValido = this.formularioCrearEjercicio.valid;
  }

  crearEjercicio() {
    this._cargandoService.habilitarCargando();
    this.ejercicioACrear.nombre = this.formularioCrearEjercicio.get('nombre').value;
    this.ejercicioACrear.descripcion = this.formularioCrearEjercicio.get('descripcion').value;
    this.ejercicioACrear.urlImagen = 'ejercicio.gif';
    this._ejercicioService.create(this.ejercicioACrear).
      subscribe(ejercicioCreado => {
        this.subirImagenEjercicio(ejercicioCreado.id);
        this.formularioCrearEjercicio.get('musculos').value.forEach(musculo => {
          const ejercicioMusculoACrear: EjercicioMusculo = {};
          ejercicioMusculoACrear.ejercicio = ejercicioCreado;
          ejercicioMusculoACrear.musculo = musculo
          this._ejercicioMusculoService.create(ejercicioMusculoACrear)
            .subscribe(ejercicioMusculoCreado => {
              this._toasterService.pop('success', 'Éxito', 'El ejercicio se ha creado');
              this.irListarEjercicios();
              this._cargandoService.deshabiltarCargando();
            }
              ,
              error => {
                this._toasterService.pop('error', 'Error', 'Errro al crear el ejercicio');
                this._cargandoService.deshabiltarCargando();
              })
        })
      }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Errro al crear el ejercicio');
        this._cargandoService.deshabiltarCargando();
      })
  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0]
  }

  subirImagenEjercicio(id: number) {
    this._subirArchivoService.onUpload(id, this.selectedFile, 'ejercicio').
      subscribe(archivoSubido => {
      })
  }

  irListarEjercicios() {
    this._router.navigate(['/app', 'ejercicio', 'listar']);
  }

}

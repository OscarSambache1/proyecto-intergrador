import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../interfaces/usuario';
import { MenuItem, ConfirmationService } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { UsuarioService } from '../../../../servicios/usuario.service';
import { ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { ModalPasswordReseteadoComponent } from '../../modales/modal-password-reseteado/modal-password-reseteado.component';

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.component.html',
  styleUrls: ['./recuperar-password.component.css']
})
export class RecuperarPasswordComponent implements OnInit {
  usuarios: Usuario[] = [];
  parametro: string;
  usuariosFiltrados: Usuario[] = [];
  columnas = [
    { field: 'nombres', header: 'Nombres' },
    { field: 'apellidos', header: 'Apellidos' },
    { field: 'acciones', header: 'Acciones' },
  ];
  items: MenuItem[];
  icono = ICONOS.resetearPassword;

  constructor(
    private readonly _usuarioService: UsuarioService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly confirmationService: ConfirmationService,
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
    this.items = [{ label: 'Cliente' }];
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando()
    this._usuarioService.findWhereOr(this.parametro)
      .subscribe(usuariosEncontrados => {
        this.usuariosFiltrados = usuariosEncontrados;
        this._cargandoService.deshabiltarCargando()
      }
        ,
        error => {
          this._cargandoService.deshabiltarCargando()
          this._toasterService.pop('error', 'Error', 'Error al traer datos');
        });
  }

  confirmar(usuario: Usuario) {
    this.confirmationService.confirm({
      message: '¿Está seguro que quiere resetear el password de este usuario?',
      header: 'Confirmación de reseteo de password',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.resetearPassword(usuario)
      },
      reject: () => {
      }
    });
  }

  resetearPassword(usuario) {
    this._cargandoService.habilitarCargando();
    this._usuarioService.resetearPassword(usuario.id)
      .subscribe(respuesta => {
        this._cargandoService.deshabiltarCargando();
        this.abrirModalPasswordReseteado(respuesta)
      },
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al resetear password');
        })
  }

  abrirModalPasswordReseteado(usuario: Usuario) {
    const dialogRef = this.dialog.open(ModalPasswordReseteadoComponent, {
      height: 'auto',
      width: 'auto',
      data: {
        usuario
      }
    });
  }
}

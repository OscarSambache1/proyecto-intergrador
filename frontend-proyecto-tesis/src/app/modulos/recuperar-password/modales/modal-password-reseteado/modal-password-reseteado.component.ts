import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Usuario } from '../../../../interfaces/usuario';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-password-reseteado',
  templateUrl: './modal-password-reseteado.component.html',
  styleUrls: ['./modal-password-reseteado.component.css']
})
export class ModalPasswordReseteadoComponent implements OnInit {
  usuario: Usuario = {};
  icono = ICONOS.resetearPassword;

  constructor(
    public dialogRef: MatDialogRef<ModalPasswordReseteadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close(this.usuario);
  }

  ngOnInit() {
    this.usuario=this.data.usuario;
  }
}

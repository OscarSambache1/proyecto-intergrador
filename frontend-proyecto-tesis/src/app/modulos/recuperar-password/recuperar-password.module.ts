import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecuperarPasswordRoutingModule } from './recuperar-password-routing.module';
import { RecuperarPasswordComponent } from './rutas/recuperar-password/recuperar-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PanelModule } from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { TableModule } from 'primeng/table';
import { ConfirmationService } from 'primeng/api';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { ModalPasswordReseteadoComponent } from './modales/modal-password-reseteado/modal-password-reseteado.component';

@NgModule({
  imports: [
    CommonModule,
    RecuperarPasswordRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    TableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
  ],
  declarations: [RecuperarPasswordComponent, ModalPasswordReseteadoComponent],
  providers: [ConfirmationService],
  entryComponents: [ModalPasswordReseteadoComponent]
})
export class RecuperarPasswordModule { }

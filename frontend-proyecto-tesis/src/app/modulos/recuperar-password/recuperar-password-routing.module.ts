import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecuperarPasswordComponent } from './rutas/recuperar-password/recuperar-password.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path: '',
    component: RecuperarPasswordComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecuperarPasswordRoutingModule { }

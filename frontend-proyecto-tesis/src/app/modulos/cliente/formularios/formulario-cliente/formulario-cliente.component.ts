import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CONFIGURACIONES_CALENDARIO } from 'src/app/constantes/configuracion-calendario';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { SelectItem } from 'primeng/api';
import { ClienteService } from 'src/app/modulos/cliente/cliente.service';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { ROLES } from '../../../../constantes/roles';
import { obtenerFechaHoy } from '../../../../funciones/obtener-fecha-actual';


@Component({
  selector: 'app-formulario-cliente',
  templateUrl: './formulario-cliente.component.html',
  styleUrls: ['./formulario-cliente.component.css']
})
export class FormularioClienteComponent implements OnInit {
  @Output() esValidoFormularioCliente: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() esFormularioPerfil = false;
  @Input() clienteSeleccionado: Cliente;
  formularioCliente: FormGroup;
  cliente: Cliente = {}
  dates: Date[];
  configuracion: any;
  roles: SelectItem[];
  mensajesErroresCodigoCliente = [];
  mensajesErroresFechaRegistroCliente = [];
  mensajesErroresRolCliente = [];
  codigoCliente: number;

  private mensajesValidacionCodigo = {
    required: "El código es obligatorio",
  };
  private mensajesValidacionFechaRegistro = {
    required: "La Fecha de registro es obligatoria"
  };

  private mensajesValidacionEstado = {
    required: "El estado es obligatorio",
  };

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _cliemteService: ClienteService

  ) { }

  ngOnInit() {
    this.configuracion = CONFIGURACIONES_CALENDARIO;
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
    this.roles = ROLES;
  }

  crearFormulario() {
    this.formularioCliente = this._formBuilder.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      fechaRegistro: ['', [Validators.required]],
      estado: ['', [Validators.required]],
      rol: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioCliente.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresCodigoCliente = setearMensajes(this.formularioCliente.get('codigo'), this.mensajesValidacionCodigo);
        this.mensajesErroresFechaRegistroCliente = setearMensajes(this.formularioCliente.get('fechaRegistro'), this.mensajesValidacionFechaRegistro);
        this.mensajesErroresRolCliente = setearMensajes(this.formularioCliente.get('rol'), this.mensajesValidacionEstado);
      });
  }


  enviarFormularioValido() {
    this.formularioCliente.valueChanges.subscribe(formulario => {
      this.esValidoFormularioCliente.emit(this.formularioCliente);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
      this.setearCodigoCliente();
      this.formularioCliente.patchValue(
        {
          fechaRegistro: obtenerFechaHoy()
        }
      )
      this.formularioCliente.get('rol').clearValidators();
      this.formularioCliente.get('estado').clearValidators();

    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioCliente.disable();
    }
    if (this.esFormularioPerfil) {
      this.llenarFormulario();
      this.formularioCliente.get('rol').clearValidators();
      this.formularioCliente.get('estado').clearValidators();
      this.formularioCliente.get('fechaRegistro').clearValidators();
      this.formularioCliente.get('rol').disable();
      this.formularioCliente.get('estado').disable();
      this.formularioCliente.get('fechaRegistro').disable();
    }
  }

  llenarFormulario() {
    this.formularioCliente.patchValue(
      {
        codigo: this.clienteSeleccionado.codigo,
        fechaRegistro: this.clienteSeleccionado.fechaRegistro,
        estado: this.clienteSeleccionado.estado,
        rol: this.clienteSeleccionado.rol,
      }
    )
  }

  setearCodigoCliente() {
    const consulta = {
      order: {
        id: 'DESC'
      },
      skip: 0,
      take: 1,
      relations: ['usuario', 'planesDieta', 'medidas', 'planesEntrenamiento']
    }
    this._cliemteService.findLast(consulta)
      .subscribe(cliente => {
        if(cliente[0]) {
          this.codigoCliente = Number(cliente[0].codigo)+1;
        }
        else {
          this.codigoCliente = 1000;
        }
        this.formularioCliente.patchValue(
          {
            codigo: (this.codigoCliente)
          }
        );
        this.formularioCliente.get('codigo').disable();
      })
  }
}

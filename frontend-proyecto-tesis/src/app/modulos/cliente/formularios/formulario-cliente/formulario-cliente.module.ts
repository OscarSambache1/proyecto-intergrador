import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TableModule } from "primeng/table";
import { CalendarModule } from "primeng/calendar";
import { InputTextModule } from "primeng/inputtext";
import { RadioButtonModule } from "primeng/radiobutton";
import { DropdownModule } from "primeng/dropdown";
import { FormularioClienteComponent } from "./formulario-cliente.component";

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        TableModule,
        CalendarModule,
        InputTextModule,
        RadioButtonModule,
        DropdownModule,
        
    ],
    declarations: [
    FormularioClienteComponent
    ],
    providers: [
        
    ],
    exports:[FormularioClienteComponent]
})
export class FormularioClienteModule { }

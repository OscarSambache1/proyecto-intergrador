import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { Cliente } from "src/app/interfaces/cliente.interface";

@Injectable()
export class ClienteService{
    constructor(private readonly _httpClient: HttpClient){

    }
    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/cliente?consulta=` + `${JSON.stringify(consulta)}`)
    }

    findOne(id: number | string) : Observable<Cliente>{
        return  this._httpClient.get(environment.url+`/cliente/${id}`)
    }
    
    findWhereOr(parametro:string, verActivos: boolean):Observable<any>{
        return  this._httpClient.get(environment.url+`/cliente/findWhereOr?expresion=${parametro}&verActivos=${verActivos}`)
    }

    create(cliente: Cliente): Observable<Cliente>{
        return this._httpClient.post(environment.url+'/cliente', cliente)
    }

    update(id: number, cliente:Cliente){
        return this._httpClient.put(environment.url+`/cliente/${id}`, cliente)
    }

    findLast(consulta: any):Observable<any>{
        return this._httpClient.get(environment.url + `/cliente/findLast?consulta=` + `${JSON.stringify(consulta)}`)
        // return  this._httpClient.get(environment.url+'/cliente/findLast')
    }


    
  
     
}

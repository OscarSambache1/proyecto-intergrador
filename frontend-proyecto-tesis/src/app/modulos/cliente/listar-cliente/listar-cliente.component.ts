import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente.service';
import { Cliente } from '../../../interfaces/cliente.interface';
import { CargandoService } from '../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutas } from '../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../constantes/iconos';
import { AutenticacionService } from '../../autenticacion/autenticacion.service';

@Component({
  selector: 'app-listar-cliente',
  templateUrl: './listar-cliente.component.html',
  styleUrls: ['./listar-cliente.component.css']
})
export class ListarClienteComponent implements OnInit {

  clientes: Cliente[] = [];
  parametroBusqueda = '';
  clienteSeleccionado: Cliente
  columnas=[
    { field: 'nombres', header: 'Nombres', width:'25%' },
    { field: 'apellidos', header: 'Apellidos', width:'25%' },
    { field: 'codigo', header: 'Codigo',  width:'15%' },
    { field: 'diaPago', header: 'Dia de Pago',  width:'15%' },
    { field: 'estado', header: 'Estado',  width:'15%' },
    { field: 'acciones', header: 'Acciones',  width:'25%'},
  ];
  items: MenuItem[];
  icono= ICONOS.cliente;
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('cliente','Listar', 'Lista de clientes');
    this.items.pop();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    this._clienteService.findWhereOr(this.parametroBusqueda, false)
      .subscribe(clientesPorParametro => {
        this.clientes = clientesPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al traer datos');
          this._cargandoService.deshabiltarCargando();
        });
  }

  cambiarEstado(cliente: Cliente) {
    this._cargandoService.habilitarCargando();
    const indice = this.clientes.indexOf(cliente);
    const estaActivo = this.clientes[indice].estado == 1;
    estaActivo ? this.clientes[indice].estado = 0 : 1;
    !estaActivo ? this.clientes[indice].estado = 1 : 0;
    this._clienteService.update(this.clientes[indice].id, { estado: this.clientes[indice].estado })
      .subscribe(tiendaEditada => {
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos');
        this._cargandoService.deshabiltarCargando();
      });
  }

}

import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import {RadioButtonModule} from 'primeng/radiobutton';
import { CrearClienteComponent } from "src/app/modulos/cliente/crear-cliente/crear-cliente.component";
import { ClienteRoutingModule } from "src/app/modulos/cliente/cliente-routing.module";
import { ListarClienteComponent } from "src/app/modulos/cliente/listar-cliente/listar-cliente.component";
import { ClienteService } from "src/app/modulos/cliente/cliente.service";
import {DropdownModule} from 'primeng/dropdown';
import { EditarClienteComponent } from "./editar-cliente/editar-cliente.component";
import { VerDetalleClienteComponent } from "./ver-detalle-cliente/ver-detalle-cliente.component";
import { FormularioUsuarioModule } from "../../formularios/formulario-usuario/formulario-usuario.module";
import { FormularioClienteModule } from "./formularios/formulario-cliente/formulario-cliente.module";
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from "../../componentes/migas-pan/migas-pan.module";
import { BarraTituloModule } from "../../componentes/barra-titulo/barra-titulo.module";
import { RolesGuard } from "../autenticacion/guards/roles.guard";


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        ClienteRoutingModule,
        TableModule,
        CalendarModule,
        InputTextModule,
        RadioButtonModule,
        DropdownModule,
        FormularioUsuarioModule,
        FormularioClienteModule,
        PanelModule,
        MigasPanModule,
        BarraTituloModule,
    ],
    declarations: [
        ListarClienteComponent,
        CrearClienteComponent,
        EditarClienteComponent,
        VerDetalleClienteComponent,
    ],
    providers: [
        ClienteService,
        RolesGuard
    ],
    exports:[
    ]
})
export class ClienteModule { }

import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/interfaces/usuario';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { UsuarioService } from '../../../servicios/usuario.service';
import { ClienteService } from 'src/app/modulos/cliente/cliente.service';
import { ToasterService } from 'angular2-toaster';
import { SubirArchivoService } from '../../../servicios/subir-archivo.service';
import { CargandoService } from '../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../funciones/cargarRutas';
import { ICONOS } from '../../../constantes/iconos';

@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styleUrls: ['./crear-cliente.component.css']
})
export class CrearClienteComponent implements OnInit {
  usuario: Usuario = {};
  cliente: Cliente = {};
  esFormularioUsuarioValido = false;
  esFormularioClienteValido = false;
  formularioCrearUsuario: FormGroup;
  formularioCrearCliente: FormGroup;
  selectedFile;
  items: MenuItem[];
  icono= ICONOS.cliente;
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _router: Router,
    private readonly _clienteServiice: ClienteService,
    private readonly _usuarioService: UsuarioService,
    private readonly _toasterService: ToasterService,
    private readonly _subirArchivoService: SubirArchivoService,
    private readonly _cargardoService: CargandoService
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.items= cargarRutas('cliente','Registrar', 'Lista de clientes');
  }

  crearFormulario() {
    this.formularioCrearUsuario = this._formBuilder.group({});
    this.formularioCrearCliente = this._formBuilder.group({});
  }

  escucharFormularioUsuario(eventoFormularioUsuario) {
    this.formularioCrearUsuario = eventoFormularioUsuario;
    if(eventoFormularioUsuario === false){
      this.esFormularioUsuarioValido=eventoFormularioUsuario;
    }
    else {
      this.esFormularioUsuarioValido=eventoFormularioUsuario.valid;
    }
  }

  escucharFormularioCliente(eventoFormularioCliente: FormGroup) {
    this.formularioCrearCliente = eventoFormularioCliente;
    this.esFormularioClienteValido = this.formularioCrearCliente.valid;
  }

  crearCliente() {
    this._cargardoService.habilitarCargando();
    this.setearUsuario();
    this.setearCliente();
    this._usuarioService.create(this.usuario)
    .subscribe(usuario =>{
      this.cliente.usuario=usuario;
      this._clienteServiice.create(this.cliente)
      .subscribe(cliente=>{
        if(this.selectedFile){
          this.subirImagen(usuario.id);
        }
        this._toasterService.pop('success', 'Éxito', 'El cliente se ha creado');
        this.irListar();
        this._cargardoService.deshabiltarCargando();
      }
      , error =>{
        this._toasterService.pop('error', 'Error', 'Falló al añadir el cliente')
        this._cargardoService.deshabiltarCargando();
      })
    })

  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0]
  }


  subirImagen(id: number){
    this._subirArchivoService.onUpload(id, this.selectedFile, 'usuario' ).
    subscribe();
  }

  irListar() {
    this._router.navigate(['/app', 'cliente', 'listar']);
  }

  setearCliente(){
    Object.keys(this.formularioCrearUsuario.controls).map(atributo => {
      this.usuario[atributo] = this.formularioCrearUsuario.controls[atributo].value;
    })
  }

  setearUsuario(){
    Object.keys(this.formularioCrearCliente.controls).map(atributo => {
      this.cliente[atributo] = this.formularioCrearCliente.controls[atributo].value;
    })
  }
}

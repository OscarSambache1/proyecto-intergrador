import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { Usuario } from 'src/app/interfaces/usuario';
import { ClienteService } from '../cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../funciones/cargarRutas';
import { ICONOS } from '../../../constantes/iconos';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ver-detalle-cliente',
  templateUrl: './ver-detalle-cliente.component.html',
  styleUrls: ['./ver-detalle-cliente.component.css']
})
export class VerDetalleClienteComponent implements OnInit {
  id: string;
  clienteSeleccionado: Cliente;
  usuarioSeleccionado: Usuario;
  esFormularioUsuarioValido = false;
  esFormularioClienteValido = false;
  items: MenuItem[];
  icono= ICONOS.cliente;
  urlImagenUsuario;

  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly _cargardoService: CargandoService,
    private readonly _toasterService: ToasterService

  ) { 
    
  }

  ngOnInit() {
    this._cargardoService.habilitarCargando();
    this.setearCliente();
    this.items= cargarRutas('cliente','Ver', 'Lista de clientes');
  }


  setearCliente() {
    this._cargardoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
        this.id = respuestaParametros.get('id')
    })
    this._clienteService.findOne(this.id).subscribe(
        cliente => {
            this.clienteSeleccionado = cliente;
            this.usuarioSeleccionado = cliente.usuario;
            this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${this.usuarioSeleccionado.urlFoto}`;
            this._cargardoService.deshabiltarCargando();
        }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Falló al mostrar el cliente');
          this._cargardoService.deshabiltarCargando();
        })
}

irListar() {
  this.router.navigate(['/app', 'cliente', 'listar']);
}
}

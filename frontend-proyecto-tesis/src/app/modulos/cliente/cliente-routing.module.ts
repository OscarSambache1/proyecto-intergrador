import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarClienteComponent } from './listar-cliente/listar-cliente.component';
import { CrearClienteComponent } from './crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
import { VerDetalleClienteComponent } from './ver-detalle-cliente/ver-detalle-cliente.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component:ListarClienteComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearClienteComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component: EditarClienteComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'ver/:id',
    component: VerDetalleClienteComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:':id/medidas',
    loadChildren: 'src/app/modulos/medida/medida.module#MedidaModule',
  },
  {
    path:':id/planes-entrenamiento',
    loadChildren: 'src/app/modulos/asignacion-plan-entrenamiento/asignacion-plan-entrenamiento.module#AsignacionPlanEntrenamientoModule',
  },
  {
    path:':id/planes-dieta',
    loadChildren: 'src/app/modulos/asignacion-plan-dieta/asignacion-plan-dieta.module#AsignacionPlanDietaModule',
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../servicios/usuario.service';
import { ClienteService } from '../cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../../../interfaces/cliente.interface';
import { Usuario } from '../../../interfaces/usuario';
import { environment } from 'src/environments/environment';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../servicios/cargando.service';
import { SubirArchivoService } from '../../../servicios/subir-archivo.service';
import { cargarRutas } from '../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../constantes/iconos';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  id: string;
  clienteAEditar: Cliente;
  clienteEditado: Cliente;
  usuarioAEditar: Usuario;
  usuarioEditado: Usuario;
  esFormularioUsuarioValido = false;
  esFormularioClienteValido = false;
  selectedFile;
  items: MenuItem[];
  icono= ICONOS.cliente;
  urlImagenUsuario;

  constructor(
    private readonly _usuarioService: UsuarioService,
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _subirArchivoService: SubirArchivoService,
    private readonly _cargardoService: CargandoService,
    private readonly _toasterService: ToasterService,

  ) { }

  ngOnInit() {
    this._cargardoService.habilitarCargando();
    this.setearCliente();
    this.items= cargarRutas('cliente','Editar', 'Lista de clientes');
  }


  escucharFormularioUsuario(eventoFormularioUsuario) {
    if(eventoFormularioUsuario === false){
      this.esFormularioUsuarioValido=eventoFormularioUsuario;
    }
    else {
      this.usuarioEditado = eventoFormularioUsuario.value;
      this.esFormularioUsuarioValido=eventoFormularioUsuario.valid;
    }
  }

  escucharFormularioCliente(eventoFormularioCliente){
      this.clienteEditado = eventoFormularioCliente.value;
      this.esFormularioClienteValido = eventoFormularioCliente.valid;  
  }

  editarCliente() {
    this._cargardoService.habilitarCargando();
    this._usuarioService.update(this.usuarioAEditar.id, this.usuarioEditado)
      .subscribe(usuario => {
        this._clienteService.update(Number(this.id), this.clienteEditado)
          .subscribe(cliente => {
            if (this.selectedFile) {
              this.subirImagen(usuario.id);
            }
            this._cargardoService.deshabiltarCargando();
            this._toasterService.pop('success', 'Éxito', 'La informacin se ha actualizado');
            this.irListar();
          }
            ,
            error => {
              this._cargardoService.deshabiltarCargando();
              this._toasterService.pop('error', 'Error', 'Falló al actualizar la información cliente')
            })
      }
      ,
      error => {
        this._cargardoService.deshabiltarCargando();
        this._toasterService.pop('error', 'Error', 'Falló al actualizar la información usuario')
      }
    )

  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0];
  }

  subirImagen(id: number) {
    this._subirArchivoService.onUpload(id, this.selectedFile, 'usuario').
      subscribe();
  }

  setearCliente() {
    this._cargardoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      this.id = respuestaParametros.get('id')
    })
    this._clienteService.findOne(this.id).subscribe(
      cliente => {
        this.clienteAEditar = cliente;
        this.usuarioAEditar = cliente.usuario;
        this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${this.usuarioAEditar.urlFoto}`;
        this._cargardoService.deshabiltarCargando();
      }
      , error => {
        this._toasterService.pop('error', 'Error', 'Falló al mostrar el cliente');
        this._cargardoService.deshabiltarCargando();
      })
  }

  irListar() {
    this._router.navigate(['/app', 'cliente', 'listar']);
  }

}

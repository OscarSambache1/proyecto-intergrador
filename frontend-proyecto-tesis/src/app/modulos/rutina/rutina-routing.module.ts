import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarRutinaComponent } from './componentes/listar-rutina/listar-rutina.component';
import { CrearRutinaComponent } from './componentes/crear-rutina/crear-rutina.component';
import { EditarRutinaComponent } from './componentes/editar-rutina/editar-rutina.component';
import { VerRutinaComponent } from './componentes/ver-rutina/ver-rutina.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path: 'listar',
    component:ListarRutinaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: 'crear',
    component: CrearRutinaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'editar/:id',
    component: EditarRutinaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component: VerRutinaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RutinaRoutingModule { }

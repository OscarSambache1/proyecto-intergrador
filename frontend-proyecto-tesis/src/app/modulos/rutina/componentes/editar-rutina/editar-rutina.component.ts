import { Component, OnInit } from '@angular/core';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { FormGroup } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { RutinaService } from '../../servicios/rutina.service';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { EjercicioRutinaService } from '../../servicios/ejercicio-rutina.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-rutina',
  templateUrl: './editar-rutina.component.html',
  styleUrls: ['./editar-rutina.component.css']
})
export class EditarRutinaComponent implements OnInit {
  rutinaAEditar: Rutina;
  rutinaEditada: Rutina = {};
  esFormularioRutinaValido=false;
  formularioEditarRutina: FormGroup;  
  items: MenuItem[];
  icono= ICONOS.rutina;

  constructor( 
    private readonly _rutinaService: RutinaService,
    private readonly _ejercicioRutinaService: EjercicioRutinaService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  
  ) { }

  ngOnInit() {
    this.setearRutinaAEditar();
    this.items= cargarRutas('rutina','Editar', 'Listar rutinas');
  }
  escucharFormularioRutina(eventoFormularioRutina: FormGroup){
    this.formularioEditarRutina = eventoFormularioRutina;
    this.esFormularioRutinaValido = this.formularioEditarRutina.valid;  
  }

  editarRutina(){
    this._cargandoService.habilitarCargando();
    this.setearRutinaEditada();
    this._rutinaService.update(this.rutinaAEditar.id,this.rutinaEditada)
    .subscribe(rutinaEditada=>{
      this.actualizarEjerciciosRutina(rutinaEditada);
      this._toasterService.pop('success', 'Éxito', 'La rutina se ha actualizado');
      this.irListarRutinas();
      this._cargandoService.deshabiltarCargando();
    }
    ,
    error=>{
      this._toasterService.pop('error','Error','Error al editar rutina')
      this._cargandoService.deshabiltarCargando();
    }
  )
  }

  actualizarEjerciciosRutina(rutinaEditada:Rutina){
    const ejerciciosRutinasGuardados=rutinaEditada.ejerciciosRutina;
    const ejerciciosRutinaSeleccionados = this.formularioEditarRutina.get('ejerciciosRutina').value;
    
    ejerciciosRutinasGuardados.forEach(ejercicioRutinaGuardado=>{
      const estaEjercicioGuardadoEnEjerciciosSeleccionados = ejerciciosRutinaSeleccionados.find( 
        ejercicioRutinaSeleccionado => 
        ejercicioRutinaSeleccionado.id === ejercicioRutinaGuardado.id
      );

      if(!estaEjercicioGuardadoEnEjerciciosSeleccionados){
       this._ejercicioRutinaService.delete(ejercicioRutinaGuardado.id).subscribe(
        respuesta=>{
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al editar rutina');
          this._cargandoService.deshabiltarCargando();
       }
       );        
      }
    });

    ejerciciosRutinaSeleccionados.forEach(ejercicioRutinaSeleccionado=>{
      if(!ejercicioRutinaSeleccionado.id){
        const ejercicioRutinaACrear: EjercicioRutina = ejercicioRutinaSeleccionado;
        ejercicioRutinaACrear.rutina=rutinaEditada;
        this._ejercicioRutinaService.create(ejercicioRutinaACrear).subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
          },
          error=>{
            this._toasterService.pop('error','Error','Error al editar rutina');
            this._cargandoService.deshabiltarCargando();
         }
        );
      }
      else{
        this._ejercicioRutinaService.update(ejercicioRutinaSeleccionado.id,ejercicioRutinaSeleccionado).subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
          },
          error=>{
            this._toasterService.pop('error','Error','Error al editar rutina');
            this._cargandoService.deshabiltarCargando();
         }
        );
      }
    })
   }

  setearRutinaEditada(){
    this.rutinaEditada.nombre = this.formularioEditarRutina.get('nombre').value;
    this.rutinaEditada.descripcion = this.formularioEditarRutina.get('descripcion').value;
    this.rutinaEditada.musculo = this.formularioEditarRutina.get('musculo').value.nombre;
  }

  setearRutinaAEditar(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idRutina = Number(respuestaParametros.get('id'));
      this._rutinaService.findOne(idRutina).subscribe(
        rutina => {
          this.rutinaAEditar = rutina;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar rutina')
          this._cargandoService.deshabiltarCargando();
        }
      )
    }
    ,
    error=>{
      this._toasterService.pop('error','Error','Error al mostrar rutina')
      this._cargandoService.deshabiltarCargando();
    })
   }

   irListarRutinas(){
    this._router.navigate(['/app', 'rutina', 'listar']);
   }
}

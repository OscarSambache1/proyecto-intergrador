import { Component, OnInit } from '@angular/core';
import { RutinaService } from '../../servicios/rutina.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-rutina',
  templateUrl: './ver-rutina.component.html',
  styleUrls: ['./ver-rutina.component.css']
})
export class VerRutinaComponent implements OnInit {
  rutinaAVer: Rutina;
  items: MenuItem[];
  icono= ICONOS.rutina;

  constructor(
    private readonly _rutinaService: RutinaService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,


  ) { }

  ngOnInit() {
    this.setearRutinaAVer();
    this.items= cargarRutas('rutina','Ver', 'Listar rutinas');
  }

  setearRutinaAVer() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idRutina = Number(respuestaParametros.get('id'));
      this._rutinaService.findOne(idRutina).subscribe(
        rutina => {
          this.rutinaAVer = rutina;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar rutina')
          this._cargandoService.deshabiltarCargando();
        }
      )
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al mostrar rutina')
        this._cargandoService.deshabiltarCargando();
      }
    )
  }

  irListarRutinas() {
    this._router.navigate(['/app', 'rutina', 'listar']);
  }
}

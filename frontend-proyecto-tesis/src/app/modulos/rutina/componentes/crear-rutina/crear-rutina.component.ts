import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { ToasterService } from 'angular2-toaster';
import { RutinaService } from '../../servicios/rutina.service';
import { EjercicioRutinaService } from '../../servicios/ejercicio-rutina.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-rutina',
  templateUrl: './crear-rutina.component.html',
  styleUrls: ['./crear-rutina.component.css']
})
export class CrearRutinaComponent implements OnInit {
  rutinaACrear: Rutina = {};
  arregloEjerciciosRutina: EjercicioRutina[] = [];
  esFormularioRutinaValido = false;
  formularioCrearRutina: FormGroup;
  items: MenuItem[];
  icono= ICONOS.rutina;

  constructor(
    private readonly _rutinaService: RutinaService,
    private readonly _ejercicioRutinaService: EjercicioRutinaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.items= cargarRutas('rutina','Registrar', 'Listar rutinas');

  }

  escucharFormularioRutina(eventoFormularioRutina: FormGroup) {
    this.formularioCrearRutina = eventoFormularioRutina;
    this.esFormularioRutinaValido = this.formularioCrearRutina.valid;
  }

  crearRutina() {
    this._cargandoService.habilitarCargando()
    this.setearRutina();
    this._rutinaService.create(this.rutinaACrear)
      .subscribe(rutinaCreada => {
        this.arregloEjerciciosRutina.forEach(ejercicioRutina => {
          ejercicioRutina.rutina = rutinaCreada;
          this._ejercicioRutinaService.create(ejercicioRutina)
            .subscribe(ejercicioRutinaCreada => {
              this._toasterService.pop('success', 'Éxito', 'La rutina se ha creado');
              this._cargandoService.deshabiltarCargando();
              this.irListarRutinas();
            })
        })
      }, error => {
        this._toasterService.pop('error', 'Error', 'Error al crear la rutina');
        this._cargandoService.deshabiltarCargando();
      });
  }

  setearRutina() {
    this.rutinaACrear.nombre = this.formularioCrearRutina.get('nombre').value;
    this.rutinaACrear.descripcion = this.formularioCrearRutina.get('descripcion').value;
    this.rutinaACrear.musculo = this.formularioCrearRutina.get('musculo').value.nombre;
    this.arregloEjerciciosRutina = this.formularioCrearRutina.get('ejerciciosRutina').value;
  }

  irListarRutinas() {
    this._router.navigate(['/app', 'rutina', 'listar']);
  }


}

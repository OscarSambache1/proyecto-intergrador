import { Component, OnInit } from '@angular/core';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { RutinaService } from '../../servicios/rutina.service';
import { MusculoService } from '../../../ejercicio/servicios/musculo.service';
import { SelectItem, MenuItem } from 'primeng/api';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-listar-rutina',
  templateUrl: './listar-rutina.component.html',
  styleUrls: ['./listar-rutina.component.css']
})
export class ListarRutinaComponent implements OnInit {
  arregloRutina: Rutina[]=[];
  parametroBusqueda='';
  arregloMusculos: SelectItem[]=[];
  musculoSeleccionado: Musculo;
  items: MenuItem[];
  icono= ICONOS.rutina;

  constructor(
    private readonly _rutinaService: RutinaService,
    private readonly _musculoService: MusculoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService : ToasterService 
  ) { }

  ngOnInit() {
    this.llenarDropdownMusculos();
    this.obtenerPorParametro();
    this.items= cargarRutas('rutina','Listar', 'Listar rutinas');
    this.items.pop();

  }

  obtenerPorParametro(){
    this._cargandoService.habilitarCargando();
    const consulta =
    { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } };
    this._rutinaService.findAll(consulta)
    .subscribe(rutinasPorParametro=>{
      this.arregloRutina=rutinasPorParametro;
      this._cargandoService.deshabiltarCargando();
    },
  error=>{
    this._cargandoService.deshabiltarCargando();
    this._toasterService.pop('error','Error','Error al mostrar rutinas');
  })
  }

  llenarDropdownMusculos(){
    this._cargandoService.habilitarCargando();
    const consulta ={ order: { id: 'DESC' } };
    this._musculoService.findAll(consulta).subscribe(musculos =>{
      this.arregloMusculos.push(
        {
          label: 'todos',
          value: null
        }
      )
      musculos.forEach(musculo => {
        const elemento ={
          label: musculo.nombre,
          value: musculo
        }
        this.arregloMusculos.push(elemento)
      });
    },
    error=>{
      this._toasterService.pop('error','Error','Error al traer datos');

    });
  }

  buscarPorMusculo(evento){
    this._cargandoService.habilitarCargando();
    const verTodosMusculos = evento.value == null
    if(verTodosMusculos){
      this.obtenerPorParametro();
    }
    else{
      const consulta =
      { order: { id: 'DESC' }, where: { musculo: `${evento.value.nombre}` } }
   
      this._rutinaService.findAll(consulta)
      .subscribe(rutinasPorParametro=>{
        this.arregloRutina=rutinasPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._toasterService.pop('error','Error','Error al mostrar rutinas');
        this._cargandoService.deshabiltarCargando();
      }
    )
    }
  }
}

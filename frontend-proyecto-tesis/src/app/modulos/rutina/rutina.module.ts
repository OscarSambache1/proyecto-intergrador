import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RutinaRoutingModule } from './rutina-routing.module';
import { CrearRutinaComponent } from './componentes/crear-rutina/crear-rutina.component';
import { ListarRutinaComponent } from './componentes/listar-rutina/listar-rutina.component';
import { EditarRutinaComponent } from './componentes/editar-rutina/editar-rutina.component';
import { VerRutinaComponent } from './componentes/ver-rutina/ver-rutina.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RutinaService } from './servicios/rutina.service';
import { EjercicioModule } from '../ejercicio/ejercicio.module';
import {DropdownModule} from 'primeng/dropdown';
import { FormularioRutinaComponent } from './formularios/formulario-rutina/formulario-rutina.component';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { ModalCrearEjercicioRutinaComponent } from './modales/modal-crear-ejercicio-rutina/modal-crear-ejercicio-rutina.component';
import { FormularioEjercicioRutinaComponent } from './formularios/formulario-ejercicio-rutina/formulario-ejercicio-rutina.component';
import { EjercicioRutinaService } from './servicios/ejercicio-rutina.service';
import { ModalEditarEjercicioRutinaComponent } from './modales/modal-editar-ejercicio-rutina/modal-editar-ejercicio-rutina.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { ModalVerRutinaComponent } from './modales/modal-ver-rutina/modal-ver-rutina.component';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { TablaEjerciciosModule } from '../../componentes/tabla-ejercicios/tabla-ejercicios.module';
import { TablaRutinasModule } from '../../componentes/tabla-rutinas/tabla-rutinas.module';


@NgModule({
  imports: [
    CommonModule,
    RutinaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TablaRutinasModule,
    TableModule,
    MultiSelectModule,
    DropdownModule,
    InputTextareaModule,
    EjercicioModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    ConfirmDialogModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    TablaEjerciciosModule,
  ],
  declarations: [
    FormularioRutinaComponent,
    FormularioEjercicioRutinaComponent,
    CrearRutinaComponent,
    ListarRutinaComponent,
    EditarRutinaComponent,
    VerRutinaComponent,
    ModalCrearEjercicioRutinaComponent,
    ModalEditarEjercicioRutinaComponent,
    ModalVerRutinaComponent,

  ],
  providers:[
    RutinaService,
    EjercicioRutinaService,
    ConfirmationService
  ],
  entryComponents:[
    ModalCrearEjercicioRutinaComponent,
    ModalEditarEjercicioRutinaComponent,
    ModalVerRutinaComponent
  ],
  exports: [
    ModalCrearEjercicioRutinaComponent,
  ]
})
export class RutinaModule { }

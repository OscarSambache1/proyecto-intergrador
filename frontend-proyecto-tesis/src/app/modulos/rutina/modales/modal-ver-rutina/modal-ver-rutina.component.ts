import { Component, OnInit, Inject } from '@angular/core';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RutinaService } from '../../servicios/rutina.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-ver-rutina',
  templateUrl: './modal-ver-rutina.component.html',
  styleUrls: ['./modal-ver-rutina.component.css']
})
export class ModalVerRutinaComponent implements OnInit {
  rutinaAVer: Rutina;
  icono = ICONOS.rutina;

  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _ToasterService: ToasterService,
    private readonly _rutinaService: RutinaService,
    public dialogRef: MatDialogRef<ModalVerRutinaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close(this.rutinaAVer);
  }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this._rutinaService.findOne(this.data.id)
      .subscribe(rutina => {
        this.rutinaAVer = rutina;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._ToasterService.pop('error','Error','Error al mostar rutina')
          this._cargandoService.deshabiltarCargando();
        })
  }


}

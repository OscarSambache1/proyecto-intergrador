import { Component, OnInit, Inject } from '@angular/core';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-crear-ejercicio-rutina',
  templateUrl: './modal-crear-ejercicio-rutina.component.html',
  styleUrls: ['./modal-crear-ejercicio-rutina.component.css']
})
export class ModalCrearEjercicioRutinaComponent implements OnInit {
  ejercicioRutinaACrear: EjercicioRutina={}
  ejercicio: Ejercicio;
  esFormularioEjercicioRutinaValido = false;
  icono = ICONOS.ejercicio;
  
  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalCrearEjercicioRutinaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.ejercicio= this.data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  escucharFormularioEjercicioRutina(eventoFormularioEjercicioRutina: FormGroup) {
    this.esFormularioEjercicioRutinaValido = eventoFormularioEjercicioRutina.valid;
    this.ejercicioRutinaACrear=eventoFormularioEjercicioRutina.value;
  }

  seleccionarEjercicioRutina(){
    this._cargandoService.habilitarCargando();
    this.ejercicioRutinaACrear.ejercicio=this.ejercicio;
    this.dialogRef.close(this.ejercicioRutinaACrear);
    this._cargandoService.deshabiltarCargando();
  }

}

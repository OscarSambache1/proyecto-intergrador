import { Component, OnInit, Inject } from '@angular/core';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-editar-ejercicio-rutina',
  templateUrl: './modal-editar-ejercicio-rutina.component.html',
  styleUrls: ['./modal-editar-ejercicio-rutina.component.css']
})
export class ModalEditarEjercicioRutinaComponent implements OnInit {
  ejercicioRutinaAEditar: EjercicioRutina = {};
  ejercicioRutinaAEditado : EjercicioRutina = {};
  esFormularioEjercicioRutinaValido = false;
  icono = ICONOS.ejercicio;

  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalEditarEjercicioRutinaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.ejercicioRutinaAEditar=this.data;
  }

  escucharFormularioEjercicioRutina(eventoFormulariEjercicioRutina: FormGroup) {
    this.esFormularioEjercicioRutinaValido = eventoFormulariEjercicioRutina.valid;
    this.ejercicioRutinaAEditado = eventoFormulariEjercicioRutina.value;
  }

  editarEjercicioRutina(){
    this._cargandoService.habilitarCargando();
    if(this.ejercicioRutinaAEditar.id){
      this.ejercicioRutinaAEditado.id=this.ejercicioRutinaAEditar.id;
    }
    this.ejercicioRutinaAEditado.ejercicio=this.ejercicioRutinaAEditar.ejercicio;
    this.dialogRef.close(this.ejercicioRutinaAEditado);
    this._cargandoService.deshabiltarCargando();

   }

}

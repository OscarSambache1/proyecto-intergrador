import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Rutina } from '../../../interfaces/rutina.interface';

@Injectable({
  providedIn: 'root'
})
export class RutinaService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/rutina?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(rutina: Rutina): Observable<Rutina> {
    return this._httpClient.post(environment.url + '/rutina/', rutina)
  }
  findOne(id: number): Observable<Rutina> {
    return this._httpClient.get(environment.url + `/rutina/${id}`)
  }
  update(id: number, rutina: Rutina): Observable<Rutina> {
    return this._httpClient.put(environment.url + `/rutina/${id}`, rutina)
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { EjercicioRutina } from '../../../interfaces/ejercicio-rutina.interface';

@Injectable({
  providedIn: 'root'
})
export class EjercicioRutinaService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }
  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/ejercicio-rutina?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(ejercicioRutina: EjercicioRutina): Observable<EjercicioRutina> {
    return this._httpClient.post(environment.url + '/ejercicio-rutina/', ejercicioRutina)
  }

  createMany(ejerciciosRutina: EjercicioRutina[]): Observable<any>{
    return this._httpClient.post(environment.url + '/ejercicio-rutina/crear-varios', ejerciciosRutina)
  }

  findOne(id: number): Observable<EjercicioRutina> {
    return this._httpClient.get(environment.url + `/ejercicio-rutina/${id}`)
  }
  update(id: number, ejercicioRutina: EjercicioRutina): Observable<EjercicioRutina> {
    return this._httpClient.put(environment.url + `/ejercicio-rutina/${id}`, ejercicioRutina)
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/ejercicio-rutina/${id}`)
  }
  
}

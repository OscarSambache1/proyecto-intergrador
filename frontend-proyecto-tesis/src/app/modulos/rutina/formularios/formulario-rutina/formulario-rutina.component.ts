import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { MusculoService } from '../../../ejercicio/servicios/musculo.service';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { EjercicioMusculoService } from '../../../ejercicio/servicios/ejercicio-musculo.service';
import { ModalVerEjercicioComponent } from '../../../ejercicio/modales/modal-ver-ejercicio/modal-ver-ejercicio.component';
import { MatDialog } from '@angular/material';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { ToasterService } from 'angular2-toaster';
import { ModalEditarEjercicioRutinaComponent } from '../../modales/modal-editar-ejercicio-rutina/modal-editar-ejercicio-rutina.component';
import { CargandoService } from '../../../../servicios/cargando.service';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-rutina',
  templateUrl: './formulario-rutina.component.html',
  styleUrls: ['./formulario-rutina.component.css']
})
export class FormularioRutinaComponent implements OnInit {

  @Output() esValidoFormularioRutina: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() rutinaSeleccionada: Rutina;
  formularioRutina: FormGroup;
  arregloEjercicios: Ejercicio[];
  arregloEjerciciosRutina: EjercicioRutina[] = [];
  arregloMusculos: SelectItem[] = [];
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresMusculo = [];

  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };

  private mensajesValidacionMusculo = {
    required: 'EL músculo es obligatorío'
  };

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _ejercicioMusculoService: EjercicioMusculoService,
    private readonly _musculoService: MusculoService,
    private readonly _formBuilder: FormBuilder,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.llenarDropdownMusculos();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  llenarDropdownMusculos() {
    const consulta = { order: { id: 'DESC' } };
    this._musculoService.findAll(consulta).subscribe(musculos => {
      this.arregloMusculos.push(
        {
          label: "seleccione un músculo",
          value: null
        }
      );
      musculos.forEach(musculo => {
        const elemento = {
          label: musculo.nombre,
          value: musculo
        }
        this.arregloMusculos.push(elemento);
      });
    });
  }

  crearFormulario() {
    this.formularioRutina = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
      descripcion: ['', []],
      musculo: ['', [Validators.required]],
      ejerciciosRutina:[[],[Validators.required]]
    });
  }

  escucharFormulario() {
    this.formularioRutina.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioRutina.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresMusculo = setearMensajes(this.formularioRutina.get('musculo'), this.mensajesValidacionMusculo);
        this.mensajesErroresNombre = setearMensajes(this.formularioRutina.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioRutina.valueChanges.subscribe(formulario => {
      this.esValidoFormularioRutina.emit(this.formularioRutina);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioRutina.disable();
    }

  }


  llenarFormulario() {
    const consulta =
      {
        order: { id: 'DESC' },
        where: { nombre: `${this.rutinaSeleccionada.musculo}` },
      }
      this._musculoService.findAll(consulta)
      .subscribe(musculos=>{
        const musculo= musculos[0];
        this.arregloEjerciciosRutina= this.rutinaSeleccionada.ejerciciosRutina;
        this.formularioRutina.patchValue(
          {
            nombre: this.rutinaSeleccionada.nombre,
            descripcion: this.rutinaSeleccionada.descripcion,
            musculo,
            ejerciciosRutina: this.arregloEjerciciosRutina
          }
        );
        this.llenarTablaEjercicios(musculo);
      },
      error=>{
        this._toasterService.pop('error','Error','Error traer Datos');
     }
    )  
  }

  llenarTablaEjercicios(musculoSeleccionado: Musculo) {
    this._cargandoService.habilitarCargando();
    const seSeleccionoMusculo = musculoSeleccionado != null
    if (seSeleccionoMusculo) {
      const consulta =
      {
        order: { id: 'DESC' },
        where: { musculo: `${musculoSeleccionado.id}` },
        relations: ['ejercicio','musculo','ejercicio.ejerciciosMusculo']
      }
      this.arregloEjercicios = [];
      this._ejercicioMusculoService.findAll(consulta)
      .subscribe(ejerciciosPorMusculo => {
        ejerciciosPorMusculo.forEach(ejercicioMusculo => {
          this.arregloEjercicios.push(ejercicioMusculo.ejercicio);
          this._cargandoService.deshabiltarCargando();
        })
      },
      error=>{
        this._cargandoService.deshabiltarCargando();
        this._toasterService.pop('error','Error','Error mostrar ejercicios');
     }
    )
    }
    else {
      this.arregloEjercicios = undefined;
      this._cargandoService.deshabiltarCargando();
    }
  }

  abrirModalVerEjercicio(ejercicioAVer: Ejercicio) {
    const dialogRef = this.dialog.open(ModalVerEjercicioComponent, {
      height: 'auto',
      width: '700px',
      data: ejercicioAVer
    });
  }

  abrirModalEditarEjercicioRutina(ejercicioRutina: EjercicioRutina) {
    const indice = this.arregloEjerciciosRutina.indexOf(ejercicioRutina);
    const dialogRef = this.dialog.open(ModalEditarEjercicioRutinaComponent, {
      height: 'auto',
      width: '600px',
      data: ejercicioRutina
    });
    dialogRef.afterClosed().subscribe(ejercicioRutinaEditado => {
      if (ejercicioRutinaEditado) {
        this.arregloEjerciciosRutina[indice] = ejercicioRutinaEditado;
        this.formularioRutina.patchValue({ejerciciosRutina:this.arregloEjerciciosRutina}); 
      }
    });
  }

  quitarEjercicio(ejercicioRutina: EjercicioRutina) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloEjerciciosRutina.indexOf(ejercicioRutina);
    this.arregloEjerciciosRutina.splice(indice, 1);
    this.formularioRutina.patchValue({ejerciciosRutina:this.arregloEjerciciosRutina}); 
    this._cargandoService.deshabiltarCargando();
  }


  confirmar(ejercicioRutina: EjercicioRutina) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar esta rutina?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarEjercicio(ejercicioRutina)
      },
      reject: () => {
      }
    });
  }
  
  escucharEventoMusculo(eventoMusculo){
    let musculo;
    eventoMusculo === null ? musculo= undefined: musculo=eventoMusculo; 
    this.formularioRutina.patchValue(
      {
        musculo
      }
    );
  }

  escucharEventoEjercicioRutina(eventoEjercicioRutina){
    this.arregloEjerciciosRutina.push(eventoEjercicioRutina);
    this.formularioRutina.patchValue({ejerciciosRutina:this.arregloEjerciciosRutina});     
  }
}

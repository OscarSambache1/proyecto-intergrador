import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-ejercicio-rutina',
  templateUrl: './formulario-ejercicio-rutina.component.html',
  styleUrls: ['./formulario-ejercicio-rutina.component.css']
})
export class FormularioEjercicioRutinaComponent implements OnInit {

  @Output() esValidoFormularioEjercicioRutina: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() ejercicioRutinaSeleccionada: EjercicioRutina;
  formularioEjercicioRutina: FormGroup;
  mensajesErroresSeries = [];
  mensajesErroresRepeticiones = [];
  mensajesErroresDescanso = [];
  mensajesErroresPeso = [];

  private mensajesValidacionSeries = {
    required: "El número de series es obligatorio",
    pattern: 'Ingrese solo número enteros'
  };

  private mensajesValidacionRepeticiones = {
    required: "El número de repeticiones es obligatorio",
    pattern: 'Ingrese solo número enteros'
  };

  private mensajesValidacionDescanso = {
    required: 'El tiempo de descanso es obligatorio',
    pattern: 'Ingrese solo número enteros'
  };

  private mensajesValidacionPeso = {
    required: 'El peso es obligatorio',
    pattern: 'Ingrese solo número enteros'
  };

  constructor(
    private readonly _formBuilder: FormBuilder,

  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioEjercicioRutina = this._formBuilder.group({
      series: ['', [Validators.required,  Validators.pattern(/^\d+$/)]],
      repeticiones: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      descanso: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      peso: ['', [Validators.required, Validators.pattern(/^\d+$/)]]
    })
  }

  escucharFormulario() {
    this.formularioEjercicioRutina.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescanso = setearMensajes(this.formularioEjercicioRutina.get('descanso'), this.mensajesValidacionDescanso);
        this.mensajesErroresRepeticiones = setearMensajes(this.formularioEjercicioRutina.get('repeticiones'), this.mensajesValidacionRepeticiones);
        this.mensajesErroresSeries = setearMensajes(this.formularioEjercicioRutina.get('series'), this.mensajesValidacionSeries);
        this.mensajesErroresPeso = setearMensajes(this.formularioEjercicioRutina.get('peso'), this.mensajesValidacionPeso);
      });
  }

  enviarFormularioValido() {
    this.formularioEjercicioRutina.valueChanges.subscribe(formulario => {
      this.esValidoFormularioEjercicioRutina.emit(this.formularioEjercicioRutina);
    });
  }
  
  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioEjercicioRutina.disable();
    }
  }

  llenarFormulario() {
    this.formularioEjercicioRutina.patchValue(
      {
        series: this.ejercicioRutinaSeleccionada.series,
        repeticiones: this.ejercicioRutinaSeleccionada.repeticiones,
        descanso: this.ejercicioRutinaSeleccionada.descanso,
        peso: this.ejercicioRutinaSeleccionada.peso
      }
    )
  }

}

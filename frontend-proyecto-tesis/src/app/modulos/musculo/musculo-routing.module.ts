import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaMusculosComponent } from './rutas/lista-musculos/lista-musculos.component';
import { CrearMusculosComponent } from './rutas/crear-musculos/crear-musculos.component';
import { EditarMusculosComponent } from './rutas/editar-musculos/editar-musculos.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListaMusculosComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'crear',
    component: CrearMusculosComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'editar/:id',
    component: EditarMusculosComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusculoRoutingModule { }

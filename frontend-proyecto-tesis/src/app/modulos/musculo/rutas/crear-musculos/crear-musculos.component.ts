import { Component, OnInit } from '@angular/core';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MusculoService } from '../../../ejercicio/servicios/musculo.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crear-musculos',
  templateUrl: './crear-musculos.component.html',
  styleUrls: ['./crear-musculos.component.css']
})
export class CrearMusculosComponent implements OnInit {

  musculoACrear: Musculo = {};
  esFormularioMusculoValido = false;
  items: MenuItem[];
  icono= ICONOS.musculo;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _musculoService: MusculoService,
    private readonly _router: Router,

  ) { }
  ngOnInit() {
    this.items= cargarRutas('musculo','Registrar', 'Lista de musculo');
  }

  escucharFormularioMusculo(eventoFormularioMusculo: FormGroup) {
    this.esFormularioMusculoValido = eventoFormularioMusculo.valid;
    this.musculoACrear = eventoFormularioMusculo.value;
  }

  crearMusculo() {
    this._cargandoService.habilitarCargando();
    this._musculoService.create(this.musculoACrear).subscribe(musculoCreado => {
      this.irListarMusculos();
      this._toasterService.pop('success', 'Éxito', 'El musculo se ha regiistrado correctamente');
      this._cargandoService.deshabiltarCargando();
    }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'El error al registra musculo');
        this._cargandoService.deshabiltarCargando();
      })
  }

  irListarMusculos() {
    this._router.navigate(['/app', 'musculo', 'listar']);
  }
}

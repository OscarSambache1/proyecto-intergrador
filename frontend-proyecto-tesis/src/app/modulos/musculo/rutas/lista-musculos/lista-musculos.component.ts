import { Component, OnInit } from '@angular/core';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MusculoService } from '../../../ejercicio/servicios/musculo.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-lista-musculos',
  templateUrl: './lista-musculos.component.html',
  styleUrls: ['./lista-musculos.component.css']
})
export class ListaMusculosComponent implements OnInit {

  arregloMusculos: Musculo[] = [];
  parametroBusqueda = '';
  columnas=[
    { field: 'nombre', header: 'Nombre', width:'33%' },
    { field: 'estado', header: 'estado', width:'33%' },
    { field: 'acciones', header: 'Acciones',width:'34%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  items: MenuItem[];
  icono= ICONOS.musculo;

  constructor(
    private readonly _musculoService: MusculoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterServce: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('musculo','', 'Lista de musculos');
    this.items.pop();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }


  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
      { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._musculoService.findAll(consulta)
      .subscribe(arreglosPorParametro => {
        this.arregloMusculos = arreglosPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
    ,
    error => {
      this._toasterServce.pop('error','Error','Error al mostrar musculos')
      this._cargandoService.deshabiltarCargando();
    })
  }

  cambiarEstado(musculo: Musculo) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloMusculos.indexOf(musculo);
    const estaActivo = this.arregloMusculos[indice].estado == 1;
    estaActivo ? this.arregloMusculos[indice].estado = 0 : 1;
    !estaActivo ? this.arregloMusculos[indice].estado = 1 : 0;
    this._musculoService.update(this.arregloMusculos[indice].id, { estado: this.arregloMusculos[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterServce.pop('error', 'Error', 'Error al cambiar estado');
        this._cargandoService.deshabiltarCargando();
      });
  }
}

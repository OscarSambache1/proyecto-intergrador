import { Component, OnInit } from '@angular/core';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { MusculoService } from '../../../ejercicio/servicios/musculo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editar-musculos',
  templateUrl: './editar-musculos.component.html',
  styleUrls: ['./editar-musculos.component.css']
})
export class EditarMusculosComponent implements OnInit {

  musculoAEditar: Musculo;
  musculoEditado: Musculo = {};
  esFormularioMusculoValido = false;
  items: MenuItem[];
  icono= ICONOS.musculo;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _musculoService: MusculoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.setearMusculo();
    this.items= cargarRutas('musculo','Editar', 'Lista de musculos');
  }

  escucharFormularioMusculo(eventoFormularioMusculo: FormGroup) {
    this.esFormularioMusculoValido = eventoFormularioMusculo.valid;
    this.musculoEditado = eventoFormularioMusculo.value;
  }

  editarMusculo(){
    this._cargandoService.habilitarCargando();
    this._musculoService.update(this.musculoAEditar.id,this.musculoEditado).subscribe(musculoEditado=>{
      this.irListarMusculos();
      this._toasterService.pop('success', 'Éxito', 'El musculo se ha editado correctamente'); 
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._toasterService.pop('error', 'Error', 'Error al editar el musculo'); 
      this._cargandoService.deshabiltarCargando();
    }
  )
   }

   setearMusculo(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idMusculo = Number(respuestaParametros.get('id'));
      this._musculoService.findOne(idMusculo).subscribe(
        musculo => {
          this.musculoAEditar = musculo;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar musculo')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarMusculos(){
    this._router.navigate(['/app', 'musculo', 'listar']);
   }

}

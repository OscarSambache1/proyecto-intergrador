import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusculoRoutingModule } from './musculo-routing.module';
import { ListaMusculosComponent } from './rutas/lista-musculos/lista-musculos.component';
import { CrearMusculosComponent } from './rutas/crear-musculos/crear-musculos.component';
import { EditarMusculosComponent } from './rutas/editar-musculos/editar-musculos.component';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { PanelModule } from 'primeng/panel';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormularioMusculoComponent } from './formularios/formulario-musculo/formulario-musculo.component';

@NgModule({
  imports: [
    CommonModule,
    MusculoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [ListaMusculosComponent, CrearMusculosComponent, EditarMusculosComponent, FormularioMusculoComponent]
})
export class MusculoModule { }

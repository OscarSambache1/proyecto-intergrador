import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-musculo',
  templateUrl: './formulario-musculo.component.html',
  styleUrls: ['./formulario-musculo.component.css']
})
export class FormularioMusculoComponent implements OnInit {

  @Output() esValidoFormularioMusculo: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() musculoSeleccionado: Musculo;
  formularioMusculo: FormGroup
  mensajesErroresNombre = [];
  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioMusculo = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
    })
  }

  escucharFormulario() {
    this.formularioMusculo.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresNombre = setearMensajes(this.formularioMusculo.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioMusculo.valueChanges.subscribe(formulario => {
      this.esValidoFormularioMusculo.emit(this.formularioMusculo);
    });
  }


  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioMusculo.disable();
    }

  }

  llenarFormulario() {
    this.formularioMusculo.patchValue(
      {
        nombre: this.musculoSeleccionado.nombre,
      }
    )
  }
}

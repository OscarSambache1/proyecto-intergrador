import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { PlanDietaCliente } from '../../../interfaces/plan-dieta-cliente.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanDietaClienteService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/plan-dieta-cliente?consulta=` + `${JSON.stringify(consulta)}`);
  }

  create(planDietaCliente: PlanDietaCliente): Observable<PlanDietaCliente> {
    return this._httpClient.post(environment.url + '/plan-dieta-cliente/', planDietaCliente);
  }
  findOne(id: number): Observable<PlanDietaCliente> {
    return this._httpClient.get(environment.url + `/plan-dieta-cliente/${id}`);
  }
  update(id: number, planDietaCliente: PlanDietaCliente): Observable<PlanDietaCliente> {
    return this._httpClient.put(environment.url + `/plan-dieta-cliente/${id}`, planDietaCliente);
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/plan-dieta-cliente/${id}`)
  }
}

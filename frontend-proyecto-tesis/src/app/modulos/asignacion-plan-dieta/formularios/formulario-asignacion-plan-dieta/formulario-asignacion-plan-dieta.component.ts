import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanDietaCliente } from '../../../../interfaces/plan-dieta-cliente.interface';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { MatDialog } from '@angular/material';
import { PlanDietaService } from '../../../plan-dieta/servicios/plan-dieta.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { CONFIGURACIONES_CALENDARIO } from '../../../../constantes/configuracion-calendario';
import { Router } from '@angular/router';
import { ModalPlanDietaComponent } from '../../../plan-dieta/modales/modal-plan-dieta/modal-plan-dieta.component';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { ConfirmationService } from 'primeng/api';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { TipoComidaService } from '../../../comida/servicios/tipo-comida.service';
import { auto } from 'async';
import { generarBodyTablePlanDieta } from '../../../../funciones/crearBodyTablePlanDietaPdf';
import { STYLES_PDF } from '../../../../constantes/styles-pdf';
import { abrirPdfNavegador } from '../../../../funciones/abrirPdfNavegador';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-formulario-asignacion-plan-dieta',
  templateUrl: './formulario-asignacion-plan-dieta.component.html',
  styleUrls: ['./formulario-asignacion-plan-dieta.component.css']
})
export class FormularioAsignacionPlanDietaComponent implements OnInit {
  @Output() esValidoFormularioAsignacionPlanDieta: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() cliente: Cliente;
  @Input() planDietaClienteSeleccionado: PlanDietaCliente;
  formularioPlanDietaCliente: FormGroup
  dates: Date[];
  configuracion: any;
  arregloPlanesDieta: PlanDieta[];
  arregloPlanesDietaSeleccionado: PlanDieta[]=[];
  planDietaSeleccionado: PlanDieta = {};
  mensajesErroresFechaInicio = [];
  mensajesErroresFechaFin = [];
  mostrarBotonAniadir= true;
  mostrarPanelListaPlanesDieta=true;
  
  columnas=[
    { field: 'plan-dieta', header: 'Plan de dieta',width:'60%' },
    { field: 'acciones', header: 'Acciones',width:'40%' },
  ];
  private mensajesValidacionFechaInicio = {
    required: "La fecha de inicio es obligatoria",
  }
  private mensajesValidacionFechaFin = {
    required: "La fecha de fin es obligatoria",
  }
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _planDietaService: PlanDietaService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private confirmationService: ConfirmationService,
    private readonly _tiposComidaService: TipoComidaService,

  ) { }

  ngOnInit() {
    this.configuracion = CONFIGURACIONES_CALENDARIO;
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioPlanDietaCliente = this._formBuilder.group({
      fechaFin: ['', [Validators.required]],
      fechaInicio: ['', [Validators.required]],
      planDieta: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioPlanDietaCliente.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresFechaFin = setearMensajes(this.formularioPlanDietaCliente.get('fechaFin'), this.mensajesValidacionFechaFin);
        this.mensajesErroresFechaInicio = setearMensajes(this.formularioPlanDietaCliente.get('fechaInicio'), this.mensajesValidacionFechaInicio);
      });
  }

  enviarFormularioValido() {
    this.formularioPlanDietaCliente.valueChanges.subscribe(formulario => {
      this.esValidoFormularioAsignacionPlanDieta.emit(this.formularioPlanDietaCliente);
    });
  }


  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
    }
    if (this.esFormularioEditar) {
      this.mostrarBotonAniadir=false;
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.formularioPlanDietaCliente.disable();
      this.llenarFormulario();
    }
  }

  llenarFormulario() {
    this._cargandoService.habilitarCargando();
    this.planDietaSeleccionado = this.planDietaClienteSeleccionado.planDieta;
    this._planDietaService.findOne(this.planDietaSeleccionado.id)
      .subscribe(
        planDieta => {
          this.formularioPlanDietaCliente.patchValue(
            {
              fechaInicio: this.planDietaClienteSeleccionado.fechaInicio,
              fechaFin: this.planDietaClienteSeleccionado.fechaFin,
              planDieta
            }
          );
          this.arregloPlanesDietaSeleccionado.push(planDieta);
          this.planDietaSeleccionado = planDieta;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al mostrar los planes de dieta');
        }
      );
  }

  abrirModalVerPlanDieta(planDieta: PlanDieta) {
     const dialogRef = this.dialog.open(ModalPlanDietaComponent, {
      height: '650px',
      width: 'auto',
      data: planDieta
     });
   }

   escucharPlanDieta(planDietaSeleccionado){
    this.arregloPlanesDietaSeleccionado.push(planDietaSeleccionado);
    this.formularioPlanDietaCliente.patchValue({
      planDieta:planDietaSeleccionado
    })
  }

  confirmar(planDieta) {
    this.confirmationService.confirm({
      message: '¿Está seguro que quiere eliminar este registro?',
      header: 'Confirmación de eliminación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarPLanDieta(planDieta)
      },
      reject: () => {
      }
    });
  }

  quitarPLanDieta(planDieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloPlanesDietaSeleccionado.indexOf(planDieta);
    this.arregloPlanesDietaSeleccionado.splice(indice, 1);
    this.formularioPlanDietaCliente.patchValue({
      planDieta:this.arregloPlanesDietaSeleccionado[0]
    });
    this._cargandoService.deshabiltarCargando();
  }

 
  async imprimir(planDietaSeleccionado) {
    this._planDietaService.findOne(planDietaSeleccionado.id)
    .subscribe( async planDieta => {
      const promesaTiposComidaService = this._tiposComidaService.findAll({}).toPromise();
      const tiposComida = await promesaTiposComidaService;
      const styles = STYLES_PDF;
      abrirPdfNavegador(
        generarBodyTablePlanDieta(tiposComida, planDieta),
        styles, 'landscape')
    },
    error =>{
      this._toasterService.pop('error', 'Error', 'Error al imprimir el plan de dieta');
    }
    );
  }
}

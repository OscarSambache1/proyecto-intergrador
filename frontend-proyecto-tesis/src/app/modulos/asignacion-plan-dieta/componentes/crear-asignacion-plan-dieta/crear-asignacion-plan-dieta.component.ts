import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanDietaClienteService } from '../../servicios/plan-dieta-cliente';
import { ClienteService } from '../../../cliente/cliente.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanDietaCliente } from '../../../../interfaces/plan-dieta-cliente.interface';
import { MenuItem } from 'primeng/api';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-asignacion-plan-dieta',
  templateUrl: './crear-asignacion-plan-dieta.component.html',
  styleUrls: ['./crear-asignacion-plan-dieta.component.css']
})
export class CrearAsignacionPlanDietaComponent implements OnInit {
  planDietaClienteACrear: PlanDietaCliente = {};
  esFormularioPlanDietaClienteValido = false;
  formularioCrearPlanDietaCliente: FormGroup;
  cliente: Cliente;
  items : MenuItem[];
  icono= ICONOS.asignacion;

  constructor(
    private readonly _planDietaClienteService: PlanDietaClienteService,
    private readonly _clienteService: ClienteService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,   
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
  ) { }

  ngOnInit() {
    this.setearPlanDietaCliente();
  }

  escucharFormularioAsignacionPlanDieta(eventoFormulario: FormGroup) {
    this.formularioCrearPlanDietaCliente = eventoFormulario;
    this.esFormularioPlanDietaClienteValido = this.formularioCrearPlanDietaCliente.valid;
  }

  setearPlanDietaCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCliente = Number(respuestaParametros.get('id'));
      this._clienteService.findOne(idCliente).subscribe(
        cliente => {
          this.cliente = cliente;
          this.items=cargarRutasChildren(this.cliente.id,'planes-dieta','Asignar', 'planes de dieta');
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar cliente');
        }
      );
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al mostrar cliente');
    })
  }

  crearPlanDietaCliente(){
    this._cargandoService.habilitarCargando();
    this.setearPlanDietaClienteACrear();
    this._planDietaClienteService.create(this.planDietaClienteACrear)
    .subscribe(planDietaClienteCreado=>{
      this._toasterService.pop('success', 'Éxito', 'El plan de dieta se ha asignado');
      this.irListarPlanesDietaCliente();
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al crear el  plane de dieta');
    }
  );
  }
  
  setearPlanDietaClienteACrear() {
    this.planDietaClienteACrear.fechaFin = this.formularioCrearPlanDietaCliente.get('fechaFin').value;
    this.planDietaClienteACrear.fechaInicio = this.formularioCrearPlanDietaCliente.get('fechaInicio').value;
    this.planDietaClienteACrear.cliente = this.cliente;
    this.planDietaClienteACrear.planDieta = this.formularioCrearPlanDietaCliente.get('planDieta').value;
  }

  irListarPlanesDietaCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-dieta']);
  }

}

import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanDietaCliente } from '../../../../interfaces/plan-dieta-cliente.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../../../cliente/cliente.service';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { PlanDietaClienteService } from '../../servicios/plan-dieta-cliente';
import { ICONOS } from '../../../../constantes/iconos';
import { AutenticacionService } from '../../../autenticacion/autenticacion.service';

@Component({
  selector: 'app-listar-asignaciones-plan-dieta',
  templateUrl: './listar-asignaciones-plan-dieta.component.html',
  styleUrls: ['./listar-asignaciones-plan-dieta.component.css']
})
export class ListarAsignacionesPlanDietaComponent implements OnInit {
  arregloPlanesDietaCliente: PlanDietaCliente[]=[];
  cliente:Cliente;
  parametroBusqueda='';
  columnas=[
    { field: 'inicio', header: 'Inicio', width:'15%' },
    { field: 'fin', header: 'Fin', width:'15%' },
    { field: 'nombre', header: 'Nombre',width:'35%' },
    { field: 'acciones', header: 'Acciones',width:'35%' },
  ];
  items: MenuItem[]=[];
  icono= ICONOS.asignacion;
  habilitarBoton: boolean;
  mostrarBoton: boolean
  constructor(
    private readonly _planDietaClienteService : PlanDietaClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _clienteService: ClienteService,
    private readonly _router: Router,
    private readonly _confirmationService: ConfirmationService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.obtenerPlanDietaPorCliente();
    this.items=[
      {
        label: 'Cliente',
        routerLink: ['/app', 'lista-clientes',{modulo:'planes-dieta', titulo:'planes de dieta'}]
      },
      {
        label: 'Lista de planes de dieta',
      }
    ];
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPlanDietaPorCliente(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCliente = Number(respuestaParametros.get('id'));
      this.obtenerCliente(idCliente);
      const consulta ={ 
        order: { id: 'DESC' }, 
        where: { cliente: idCliente },
        relations: ['cliente','cliente.usuario','planDieta']
      }

      this._planDietaClienteService.findAll(consulta).subscribe(
        planDietaClienteEncontrado => {
          this.arregloPlanesDietaCliente = planDietaClienteEncontrado;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar planes de dieta');
        }
      );
    });
   }

   obtenerCliente(id: number){
    this._cargandoService.habilitarCargando();
    this._clienteService.findOne(id).subscribe(
      cliente => {
        this.cliente = cliente;
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._cargandoService.deshabiltarCargando();
        this._toasterService.pop('error','Error','Error al mostrar cliente');
      }
    );
   }

   quitarPlanDietaCliente(planDietaCliente: PlanDietaCliente) {
    this._cargandoService.habilitarCargando();
    this._planDietaClienteService.delete(planDietaCliente.id)
    .subscribe(respuesta=>{
      const indice = this.arregloPlanesDietaCliente.indexOf(planDietaCliente);
      this.arregloPlanesDietaCliente.splice(indice, 1);
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('success','Exito','Registro eliminado')
    }
    ,error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al quitar el plan de dieta');
    }
  );
  }


  confirmar(planDietaCliente: PlanDietaCliente) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar este plan de dieta?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarPlanDietaCliente(planDietaCliente)
      },
      reject: () => {
      }
    });
  }

   irListaClientes(){
    this._router.navigate(['/app', 'lista-clientes',{modulo:'planes-dieta', titulo:'planes-dieta'}]);
  }

}

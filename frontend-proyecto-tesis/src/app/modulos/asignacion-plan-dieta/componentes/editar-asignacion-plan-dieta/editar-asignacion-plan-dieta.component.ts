import { Component, OnInit } from '@angular/core';
import { PlanDietaCliente } from '../../../../interfaces/plan-dieta-cliente.interface';
import { FormGroup } from '@angular/forms';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanDietaClienteService } from '../../servicios/plan-dieta-cliente';
import { ClienteService } from '../../../cliente/cliente.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-asignacion-plan-dieta',
  templateUrl: './editar-asignacion-plan-dieta.component.html',
  styleUrls: ['./editar-asignacion-plan-dieta.component.css']
})
export class EditarAsignacionPlanDietaComponent implements OnInit {

  planDietaClienteAEditar: PlanDietaCliente;
  planDietaClienteEditado: PlanDietaCliente={};
  esFormularioPlanDietaClienteValido = false;
  formularioEditarPlanDietaCliente: FormGroup;
  cliente: Cliente;
  items : MenuItem[];
  icono= ICONOS.asignacion;

  constructor(
    private readonly _planDietaClienteService: PlanDietaClienteService,
    private readonly _clienteService: ClienteService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.setearPlanDietaCliente();
  }

  escucharFormularioAsignacionPlanDieta(eventoFormulario: FormGroup) {
    this.formularioEditarPlanDietaCliente = eventoFormulario;
    this.esFormularioPlanDietaClienteValido = this.formularioEditarPlanDietaCliente.valid;
  }

  editarPlanDietaCliente(){
    this._cargandoService.habilitarCargando();
    this.setearPlanDietaClienteEditado();
    this._planDietaClienteService.update(this.planDietaClienteAEditar.id,this.planDietaClienteEditado)
    .subscribe(planDietaClienteEditado=>{
      this._toasterService.pop('success', 'Éxito', 'La asignnación se ha actualizado');
      this.irListarPlanesDietaCliente();
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al editar la asignnación del plan de dieta');
    })
  }

  setearPlanDietaClienteEditado(){
    this.planDietaClienteEditado.fechaInicio = this.formularioEditarPlanDietaCliente.get('fechaInicio').value;
    this.planDietaClienteEditado.fechaFin = this.formularioEditarPlanDietaCliente.get('fechaFin').value;
    this.planDietaClienteEditado.planDieta = this.formularioEditarPlanDietaCliente.get('planDieta').value;
    }

  setearPlanDietaCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPLanDietaCliente = Number(respuestaParametros.get('id'));
      this._planDietaClienteService.findOne(idPLanDietaCliente).subscribe(
        planEntenamientoCliente => {
          this.planDietaClienteAEditar = planEntenamientoCliente;
          const idCliente = this.planDietaClienteAEditar.cliente.id;
          this._clienteService.findOne(idCliente).subscribe(
            cliente => {
              this.cliente = cliente;
              this.items=cargarRutasChildren(this.cliente.id,'planes-dieta','Editar', 'planes de dieta');
              this._cargandoService.deshabiltarCargando();
            },
            error=>{
              this._cargandoService.deshabiltarCargando();
              this._toasterService.pop('error','Error','Error al mostrar el cliente');
            }
          );
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar el cliente');
        });
    })
  }

  irListarPlanesDietaCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-dieta']);
  }

}

import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanDietaCliente } from '../../../../interfaces/plan-dieta-cliente.interface';
import { PlanDietaClienteService } from '../../servicios/plan-dieta-cliente';
import { ClienteService } from '../../../cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-asignacion-plan-dieta',
  templateUrl: './ver-asignacion-plan-dieta.component.html',
  styleUrls: ['./ver-asignacion-plan-dieta.component.css']
})
export class VerAsignacionPlanDietaComponent implements OnInit {

  planDietaClienteAVer: PlanDietaCliente;
  cliente: Cliente;
  items : MenuItem[];
  icono= ICONOS.asignacion;

  constructor(
    private readonly _planDietaClienteService: PlanDietaClienteService,
    private readonly _clienteService: ClienteService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.setearPlanDietaCliente();
  }


  setearPlanDietaCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPLanDietaCliente = Number(respuestaParametros.get('id'));
      this._planDietaClienteService.findOne(idPLanDietaCliente).subscribe(
        planEntenamientoCliente => {
          this.planDietaClienteAVer = planEntenamientoCliente;
          const idCliente = this.planDietaClienteAVer.cliente.id;
          this._clienteService.findOne(idCliente).subscribe(
            cliente => {
              this.cliente = cliente;
              this.items=cargarRutasChildren(this.cliente.id,'planes-dieta','Ver', 'planes de dieta');
              this._cargandoService.deshabiltarCargando();
            },
            error=>{
              this._cargandoService.deshabiltarCargando();
              this._toasterService.pop('error','Error','Error al mostrar el cliente');
            }
          );
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar el cliente');
        });
    })
  }

  irListarPlanesDietaCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-dieta']);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsignacionPlanDietaRoutingModule } from './asignacion-plan-dieta-routing.module';
import { ListarAsignacionesPlanDietaComponent } from './componentes/listar-asignaciones-plan-dieta/listar-asignaciones-plan-dieta.component';
import { CrearAsignacionPlanDietaComponent } from './componentes/crear-asignacion-plan-dieta/crear-asignacion-plan-dieta.component';
import { EditarAsignacionPlanDietaComponent } from './componentes/editar-asignacion-plan-dieta/editar-asignacion-plan-dieta.component';
import { VerAsignacionPlanDietaComponent } from './componentes/ver-asignacion-plan-dieta/ver-asignacion-plan-dieta.component';
import { PlanDietaDiaService } from '../plan-dieta/servicios/plan-dieta-dia.service';
import { DietaModule } from '../dieta/dieta.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MatFormFieldModule, MatDialogModule } from '@angular/material';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CalendarModule } from 'primeng/calendar';
import { PlanDietaModule } from '../plan-dieta/plan-dieta.module';
import { FormularioAsignacionPlanDietaComponent } from './formularios/formulario-asignacion-plan-dieta/formulario-asignacion-plan-dieta.component';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { InformacionClienteModule } from '../../componentes/informacion-cliente/informacion-cliente.module';
import { TablaPlanesDietaModule } from '../../componentes/tabla-planes-dieta/tabla-planes-dieta.module';

@NgModule({
  imports: [
    CommonModule,
    AsignacionPlanDietaRoutingModule,
    DietaModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DropdownModule,
    MatDialogModule,
    MatFormFieldModule,
    ConfirmDialogModule,
    CalendarModule,
    PlanDietaModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    InformacionClienteModule,
    TablaPlanesDietaModule
  ],
  declarations: [ListarAsignacionesPlanDietaComponent, CrearAsignacionPlanDietaComponent, EditarAsignacionPlanDietaComponent, VerAsignacionPlanDietaComponent, FormularioAsignacionPlanDietaComponent],
  providers:[
    PlanDietaDiaService
  ]
})
export class AsignacionPlanDietaModule { }

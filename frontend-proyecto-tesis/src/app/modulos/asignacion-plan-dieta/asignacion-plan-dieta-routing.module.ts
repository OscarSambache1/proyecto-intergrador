import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarAsignacionesPlanDietaComponent } from './componentes/listar-asignaciones-plan-dieta/listar-asignaciones-plan-dieta.component';
import { CrearAsignacionPlanDietaComponent } from './componentes/crear-asignacion-plan-dieta/crear-asignacion-plan-dieta.component';
import { EditarAsignacionPlanDietaComponent } from './componentes/editar-asignacion-plan-dieta/editar-asignacion-plan-dieta.component';
import { VerAsignacionPlanDietaComponent } from './componentes/ver-asignacion-plan-dieta/ver-asignacion-plan-dieta.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'',
    component : ListarAsignacionesPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component : CrearAsignacionPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component : EditarAsignacionPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'ver/:id',
    component : VerAsignacionPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsignacionPlanDietaRoutingModule { }

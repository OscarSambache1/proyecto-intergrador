import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoRoutingModule } from './producto-routing.module';
import { ListarProductoComponent } from './componentes/listar-producto/listar-producto.component';
import { ProductoService } from './producto.service';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ModalCrearProductoComponent } from './modales/modal-crear-producto/modal-crear-producto.component';
import { ModalEditarProductoComponent } from './modales/modal-editar-producto/modal-editar-producto.component';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { FormularioProductoComponent } from './formularios/formulario-producto/formulario-producto.component';
import { ModalVerProductoComponent } from './modales/modal-ver-producto/modal-ver-producto.component';
import { CrearProductoComponent } from './componentes/crear-producto/crear-producto.component';
import { EditarProductoComponent } from './componentes/editar-producto/editar-producto.component';
import { VerProductoComponent } from './componentes/ver-producto/ver-producto.component';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProductoRoutingModule,
    TableModule,
    ToolbarModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [
    ListarProductoComponent,
    ModalCrearProductoComponent,
    ModalEditarProductoComponent,
    ModalVerProductoComponent,
    FormularioProductoComponent,
    CrearProductoComponent,
    EditarProductoComponent,
    VerProductoComponent
  ],
  providers:[
    ProductoService
  ],
  entryComponents:[
    ModalCrearProductoComponent,
    ModalEditarProductoComponent,
    ModalVerProductoComponent
  ]

})
export class ProductoModule { }

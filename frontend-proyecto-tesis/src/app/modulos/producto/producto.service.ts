import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Producto } from '../../interfaces/producto.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }

findAll(consulta:any):Observable<any>{
  return  this._httpClient.get(environment.url+`/producto?consulta=`+`${JSON.stringify(consulta)}`)
}

findOne(id:number): Observable<Producto>{
  return  this._httpClient.get(environment.url+`/producto/${id}`)
}

create(producto: Producto): Observable<Producto> {
  return this._httpClient.post(environment.url + '/producto', producto)
}

update(id: number, producto: Producto): Observable<Producto> {
  return this._httpClient.put(environment.url + `/producto/${id}`, producto)
}

}

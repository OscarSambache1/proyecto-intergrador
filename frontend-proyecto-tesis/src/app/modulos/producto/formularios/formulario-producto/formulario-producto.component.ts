import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Producto } from '../../../../interfaces/producto.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-producto',
  templateUrl: './formulario-producto.component.html',
  styleUrls: ['./formulario-producto.component.css']
})
export class FormularioProductoComponent implements OnInit {
  @Output() esValidoFormularioProducto: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() productoSeleccionado: Producto;
  formularioProducto: FormGroup
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresMarca = [];
  mensajesErroresPrecio = [];

  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  }

  private mensajesValidacionDescripcion = {
    pattern: 'La descripción no es válida'
  }

  private mensajesValidacionMarca = {
    pattern: 'La marca no es válida'
  }

  private mensajesValidacionPrecio = {
    required: "El precio es obligatorio",
    pattern: 'El precio no es válido'
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioProducto = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
      descripcion: ['', [Validators.pattern(/^\S.*\S$/)]],
      precio: ['', [Validators.required, Validators.pattern(/^\s*-?(\d+(\.\d{1,2})?|\.\d{1,2})\s*$/)]],
      marca: ['', [Validators.pattern(/^\S.*\S$/)]],
    })
  }

  escucharFormulario() {
    this.formularioProducto.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioProducto.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresNombre = setearMensajes(this.formularioProducto.get('nombre'), this.mensajesValidacionNombre);
        this.mensajesErroresPrecio = setearMensajes(this.formularioProducto.get('precio'), this.mensajesValidacionPrecio);
        this.mensajesErroresMarca = setearMensajes(this.formularioProducto.get('marca'), this.mensajesValidacionMarca);
      });
  }

  enviarFormularioValido() {
    this.formularioProducto.valueChanges.subscribe(formulario => {
      this.esValidoFormularioProducto.emit(this.formularioProducto);
    });
  }


  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioProducto.disable();
    }

  }

  llenarFormulario() {
    this.formularioProducto.patchValue(
      {
        nombre: this.productoSeleccionado.nombre,
        descripcion: this.productoSeleccionado.descripcion,
        marca: this.productoSeleccionado.marca,
        precio: this.productoSeleccionado.precio,
      }
    )
  }
}
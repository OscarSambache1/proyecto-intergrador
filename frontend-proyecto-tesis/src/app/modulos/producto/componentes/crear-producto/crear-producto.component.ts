import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../../interfaces/producto.interface';
import { ToasterService } from 'angular2-toaster';
import { ProductoService } from '../../producto.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {
  productoACrear: Producto = {}
  esFormularioProductoValido = false;
  items: MenuItem[];
  icono= ICONOS.producto;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _productoService: ProductoService,
    private readonly _router: Router,

  ) { }
  ngOnInit() {
    this.items= cargarRutas('producto','Registrar', 'Lista de productos');
  }

  escucharFormularioProducto(eventoFormularioProducto: FormGroup) {
    this.esFormularioProductoValido = eventoFormularioProducto.valid;
    this.productoACrear = eventoFormularioProducto.value;
  }

  crearProducto() {
    this._cargandoService.habilitarCargando();
    this._productoService.create(this.productoACrear).subscribe(produtoCreado => {
      this.irListarProductos();
      this._toasterService.pop('success', 'Éxito', 'El producto se ha regiistrado correctamente');
      this._cargandoService.deshabiltarCargando();
    }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'El error al registra producto');
        this._cargandoService.deshabiltarCargando();
      })
  }

  irListarProductos() {
    this._router.navigate(['/app', 'producto', 'listar']);
  }
}

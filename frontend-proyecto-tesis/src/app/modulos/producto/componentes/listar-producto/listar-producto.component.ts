import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../producto.service';
import { Producto } from '../../../../interfaces/producto.interface'
import { MatDialog } from '@angular/material';
import { ModalCrearProductoComponent } from '../../modales/modal-crear-producto/modal-crear-producto.component';
import { ModalEditarProductoComponent } from '../../modales/modal-editar-producto/modal-editar-producto.component';
import { ModalVerProductoComponent } from '../../modales/modal-ver-producto/modal-ver-producto.component';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';
@Component({
  selector: 'app-listar-producto',
  templateUrl: './listar-producto.component.html',
  styleUrls: ['./listar-producto.component.css']
})
export class ListarProductoComponent implements OnInit {
  arregloProductos: Producto[] = [];
  parametroBusqueda = '';
  columnas = [
    { field: 'nombre', header: 'Nombre', width: '30%' },
    { field: 'precio', header: 'Precio', width: '20%' },
    { field: 'estado', header: 'Estado', width: '30%' },
    { field: 'acciones', header: 'Acciones', width: '30%' },
  ];
  items: MenuItem[];
  icono = ICONOS.producto;
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  constructor(
    private readonly _productoService: ProductoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterServce: ToasterService,
    private readonly _autenticacionService: AutenticacionService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items = cargarRutas('producto', '', 'Lista de productos');
    this.items.pop();
    this.comprobarPermisos();
}

async comprobarPermisos(){
  this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
  this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
}
  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
    {
      order: { id: 'DESC' }, 
      where: {
        nombre: `${this.parametroBusqueda}`,
      }
    }

    this._productoService.findAll(consulta)
      .subscribe(arreglosPorParametro => {
        this.arregloProductos = arreglosPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterServce.pop('error', 'Error', 'Error al mostrar productos')
          this._cargandoService.deshabiltarCargando();
        })
  }

  abrirModalCrearProducto() {
    const dialogRef = this.dialog.open(ModalCrearProductoComponent, {
      height: 'auto',
      width: 'auto',
    });
    dialogRef.afterClosed().subscribe(modalProducto => {
      if (modalProducto) {
        this.arregloProductos.unshift(modalProducto);
      }
    });
  }

  abrirModalEditarProducto(productoAEditar: Producto) {
    const indice = this.arregloProductos.indexOf(productoAEditar);
    const dialogRef = this.dialog.open(ModalEditarProductoComponent, {
      height: 'auto',
      width: 'auto',
      data: productoAEditar
    });
    dialogRef.afterClosed().subscribe(modalProducto => {
      if (modalProducto) {
        this.arregloProductos[indice].nombre = modalProducto.nombre;
        this.arregloProductos[indice].descripcion = modalProducto.descripcion;
        this.arregloProductos[indice].marca = modalProducto.marca;
        this.arregloProductos[indice].precio = modalProducto.precio;
      }
    });
  }

  abrirModalVerProducto(productoAVer: Producto) {
    const dialogRef = this.dialog.open(ModalVerProductoComponent, {
      height: 'auto',
      width: 'auto',
      data: productoAVer
    });
    dialogRef.afterClosed().subscribe(modalProducto => {
    });
  }

  cambiarEstado(producto: Producto) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloProductos.indexOf(producto);
    const estaActivo = this.arregloProductos[indice].estado == 1;
    estaActivo ? this.arregloProductos[indice].estado = 0 : 1;
    !estaActivo ? this.arregloProductos[indice].estado = 1 : 0;
    this._productoService.update(this.arregloProductos[indice].id, { estado: this.arregloProductos[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterServce.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }

}

import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../../interfaces/producto.interface';
import { ToasterService } from 'angular2-toaster';
import { ProductoService } from '../../producto.service';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-producto',
  templateUrl: './editar-producto.component.html',
  styleUrls: ['./editar-producto.component.css']
})
export class EditarProductoComponent implements OnInit {
  productoAEditar: Producto;
  productoEditado: Producto = {};
  esFormularioProductoValido = false;
  items: MenuItem[];
  icono= ICONOS.producto;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _productoService: ProductoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.setearProducto();
    this.items= cargarRutas('producto','Editar', 'Lista de productos');
  }

  escucharFormularioProducto(eventoFormulariProducto: FormGroup) {
    this.esFormularioProductoValido = eventoFormulariProducto.valid;
    this.productoEditado=eventoFormulariProducto.value;
  }

  editarProducto(){
    this._cargandoService.habilitarCargando();
    this._productoService.update(this.productoAEditar.id,this.productoEditado).subscribe(productoEditado=>{
      this.irListarProductos();
      this._toasterService.pop('success', 'Éxito', 'El producto se ha editado correctamente'); 
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._toasterService.pop('error', 'Error', 'Error al editar el producto'); 
      this._cargandoService.deshabiltarCargando();
    }
  )
   }

   setearProducto(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idProducto = Number(respuestaParametros.get('id'));
      this._productoService.findOne(idProducto).subscribe(
        producto => {
          this.productoAEditar = producto;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar producto')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarProductos(){
    this._router.navigate(['/app', 'producto', 'listar']);
   }

}

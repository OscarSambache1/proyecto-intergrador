import { Component, OnInit } from '@angular/core';
import { Producto } from '../../../../interfaces/producto.interface';
import { ProductoService } from '../../producto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-producto',
  templateUrl: './ver-producto.component.html',
  styleUrls: ['./ver-producto.component.css']
})
export class VerProductoComponent implements OnInit {
  productoAVer: Producto;
  items: MenuItem[];
  icono= ICONOS.producto;

  constructor(
    private readonly _productoService: ProductoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.setearProducto();
    this.items= cargarRutas('producto','Ver', 'Lista de productos');

  }

   setearProducto(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idProducto = Number(respuestaParametros.get('id'));
      this._productoService.findOne(idProducto)
      .subscribe(
        producto => {
          this.productoAVer = producto;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar producto')
          this._cargandoService.deshabiltarCargando();
        }
      )
    }
    ,
    error=>{
      this._toasterService.pop('error','Error','Error al mostrar producto')
      this._cargandoService.deshabiltarCargando();
    })
   }

   irListarProductos(){
    this._router.navigate(['/app', 'producto', 'listar']);
   }

}

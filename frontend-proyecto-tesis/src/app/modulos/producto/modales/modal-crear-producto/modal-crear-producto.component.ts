import { Component, OnInit, Inject } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ProductoService } from '../../producto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalCrearMedidaComponent } from '../../../medida/modales/modal-crear-medida/modal-crear-medida.component';
import { Producto } from '../../../../interfaces/producto.interface';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-modal-crear-producto',
  templateUrl: './modal-crear-producto.component.html',
  styleUrls: ['./modal-crear-producto.component.css']
})
export class ModalCrearProductoComponent implements OnInit {
  productoACrear: Producto={}
  esFormularioProductoValido = false;

  constructor(
    private readonly toasterService: ToasterService,
    private readonly _productoService: ProductoService,
    public dialogRef: MatDialogRef<ModalCrearMedidaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
  }

  escucharFormularioProducto(eventoFormularioProducto: FormGroup) {
    this.esFormularioProductoValido = eventoFormularioProducto.valid;
    this.productoACrear=eventoFormularioProducto.value;
  }

  crearProducto(){
    this._productoService.create(this.productoACrear).subscribe(produtoCreado => {
      this.toasterService.pop('success', 'Éxito', 'El producto se ha creado correctamente');
      this.dialogRef.close(produtoCreado);
    })
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Producto } from '../../../../interfaces/producto.interface';

@Component({
  selector: 'app-modal-ver-producto',
  templateUrl: './modal-ver-producto.component.html',
  styleUrls: ['./modal-ver-producto.component.css']
})
export class ModalVerProductoComponent implements OnInit {
  productoAVer: Producto = {};
  esFormularioProductoValido = false;

  constructor(
    public dialogRef: MatDialogRef<ModalVerProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close(this.productoAVer);
  }

  ngOnInit() {
    this.productoAVer=this.data;
  }


  
}

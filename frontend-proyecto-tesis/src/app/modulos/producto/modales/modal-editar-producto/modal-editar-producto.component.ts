import { Component, OnInit, Inject } from '@angular/core';
import { Producto } from '../../../../interfaces/producto.interface';
import { ToasterService } from 'angular2-toaster';
import { ProductoService } from '../../producto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-modal-editar-producto',
  templateUrl: './modal-editar-producto.component.html',
  styleUrls: ['./modal-editar-producto.component.css']
})
export class ModalEditarProductoComponent implements OnInit {
  productoAEditar: Producto = {};
  productoEditado: Producto = {};
  esFormularioProductoValido = false;
  constructor(
    private readonly toasterService: ToasterService,
    private readonly _productoService: ProductoService,
    public dialogRef: MatDialogRef<ModalEditarProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.productoAEditar=this.data;
  }

  escucharFormularioProducto(eventoFormulariProducto: FormGroup) {
    this.esFormularioProductoValido = eventoFormulariProducto.valid;
    this.productoEditado=eventoFormulariProducto.value;
  }

  editarProducto(){
    
    this._productoService.update(this.productoAEditar.id,this.productoEditado).subscribe(productoEditado=>{
     this.toasterService.pop('success', 'Éxito', 'El producto se ha editado correctamente'); 
     this.dialogRef.close(productoEditado);
    })
 
   }
}

import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Instructor } from '../../../../interfaces/instructor.interface';
import { SelectItem } from 'primeng/api';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-formulario-instructor',
  templateUrl: './formulario-instructor.component.html',
  styleUrls: ['./formulario-instructor.component.css']
})
export class FormularioInstructorComponent implements OnInit {
  @Output() esValidoFormularioInstructor: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() esFormularioPerfil = false;
  @Input() instructorSeleccionado: Instructor;
  formularioInstructor: FormGroup;
  instructor: Instructor = {}
  roles: SelectItem[];
  mensajesErroresEspecializacionInstructor = [];
  mensajesErroresRolInstructor = [];
  habilitarRoles = false;
  private mensajesValidacionEspecializacioon = {
    required: "La especializacion obligatoria"
  };
  private mensajesValidacionRol = {
    required: "El rol es obligatorio",
  };

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _autenticacionService: AutenticacionService,
  ) { }

  async ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
    this.roles = [
      { label: 'Selecciones un rol', value: null },
      { label: 'instructor', value: 'instructor' },
      { label: 'empleado', value: 'empleado' },
    ];
    const usuario = await this._autenticacionService.obtenerUsuarioLogueado();
    if(usuario.instructor.rol === 'administrador') {
      this.roles.push({ label: 'administrador', value: 'administrador' });
    }

    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarRoles = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
  }

  crearFormulario() {
    this.formularioInstructor = this._formBuilder.group({
      especializacion: ['', [Validators.required]],
      rol: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioInstructor.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresEspecializacionInstructor = setearMensajes(this.formularioInstructor.get('especializacion'), this.mensajesValidacionEspecializacioon);
        this.mensajesErroresRolInstructor = setearMensajes(this.formularioInstructor.get('rol'), this.mensajesValidacionRol);
      });
  }

  enviarFormularioValido() {
    this.formularioInstructor.valueChanges.subscribe(formulario => {
      this.esValidoFormularioInstructor.emit(this.formularioInstructor);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioInstructor.disable();
    }
    if (this.esFormularioPerfil) {
      this.llenarFormulario();
      this.formularioInstructor.get('rol').clearValidators();
      this.formularioInstructor.get('rol').disable();
    }
  }

  llenarFormulario() {
    this.formularioInstructor.patchValue(
      {
        especializacion: this.instructorSeleccionado.especializacion,
        estado: this.instructorSeleccionado.estado,
        rol: this.instructorSeleccionado.rol,
      }
    )
  }

 
}

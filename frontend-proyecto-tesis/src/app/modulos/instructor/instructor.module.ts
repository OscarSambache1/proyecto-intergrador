import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorRoutingModule } from './instructor-routing.module';
import { ListarInstructoresComponent } from './componentes/listar-instructores/listar-instructores.component';
import { CrearInstructorComponent } from './componentes/crear-instructor/crear-instructor.component';
import { EditarInstructorComponent } from './componentes/editar-instructor/editar-instructor.component';
import { InstructorService } from './instructor.service';
import { VerInstructorComponent } from './componentes/ver-instructor/ver-instructor.component';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormularioInstructorComponent } from './formularios/formulario-instructor/formulario-instructor.component';
import { FormularioUsuarioModule } from '../../formularios/formulario-usuario/formulario-usuario.module';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    InstructorRoutingModule,
    TableModule,
    ToolbarModule,
    RadioButtonModule,
    DropdownModule,
    FormularioUsuarioModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [
    ListarInstructoresComponent,
    CrearInstructorComponent,
    EditarInstructorComponent,
    VerInstructorComponent,
    FormularioInstructorComponent,
  ],
  providers:[
    InstructorService
  ]
})
export class InstructorModule { }

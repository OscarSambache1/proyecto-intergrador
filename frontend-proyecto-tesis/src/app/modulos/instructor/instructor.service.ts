import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Instructor } from '../../interfaces/instructor.interface';

@Injectable({
  providedIn: 'root'
})
export class InstructorService {

  constructor(private readonly _httpClient: HttpClient) {

  }

  findAll(consulta:any):Observable<any>{
    return  this._httpClient.get(environment.url+`/instructor?consulta=`+`${JSON.stringify(consulta)}`)
  }

  findWhereOr(parametro: string,verActivos: boolean ): Observable<any> {
    return this._httpClient.get(environment.url + `/instructor/findWhereOr?expresion=${parametro}&verActivos=${verActivos}`)
  }

  findOne(id: number | string) : Observable<Instructor>{
    return  this._httpClient.get(environment.url+`/instructor/${id}`)
}

  create(instructor: Instructor): Observable<Instructor> {
    return this._httpClient.post(environment.url + '/instructor', instructor)
  }

  update(id: number, instructor: Instructor) : Observable<Instructor>{
    return this._httpClient.put(environment.url + `/instructor/${id}`, instructor)
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarInstructoresComponent } from './componentes/listar-instructores/listar-instructores.component';
import { CrearInstructorComponent } from './componentes/crear-instructor/crear-instructor.component';
import { EditarInstructorComponent } from './componentes/editar-instructor/editar-instructor.component';
import { VerInstructorComponent } from './componentes/ver-instructor/ver-instructor.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';
import { EditarAdministradorGuard } from '../autenticacion/guards/editar-administrador.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarInstructoresComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearInstructorComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component: EditarInstructorComponent,
    canActivate: [EditarAdministradorGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'ver/:id',
    component: VerInstructorComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstructorRoutingModule { }

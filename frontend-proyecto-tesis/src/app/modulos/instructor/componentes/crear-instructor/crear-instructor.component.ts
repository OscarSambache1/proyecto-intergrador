import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../interfaces/usuario';
import { Instructor } from '../../../../interfaces/instructor.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { InstructorService } from '../../instructor.service';
import { UsuarioService } from '../../../../servicios/usuario.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { SubirArchivoService } from '../../../../servicios/subir-archivo.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';


@Component({
  selector: 'app-crear-instructor',
  templateUrl: './crear-instructor.component.html',
  styleUrls: ['./crear-instructor.component.css']
})
export class CrearInstructorComponent implements OnInit {
  usuario: Usuario = {};
  instructor: Instructor = {};
  esFormularioUsuarioValido = false;
  esFormularioInstructorValido = false;
  formularioCrearUsuario: FormGroup;
  formularioCrearInstructor: FormGroup;
  selectedFile;
  items: MenuItem[];
  icono = ICONOS.instructor;
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _router: Router,
    private readonly _instructorService: InstructorService,
    private readonly _usuarioService: UsuarioService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _subirArchivoService: SubirArchivoService,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.items= cargarRutas('instructor','Registrar', 'Listar instructores');
  }


  crearFormulario() {
    this.formularioCrearUsuario = this._formBuilder.group({});
    this.formularioCrearInstructor = this._formBuilder.group({});
  }

  escucharFormularioUsuario(eventoFormularioUsuario) {
    this.formularioCrearUsuario = eventoFormularioUsuario;
    if (eventoFormularioUsuario === false) {
      this.esFormularioUsuarioValido = eventoFormularioUsuario;
    }
    else {
      this.esFormularioUsuarioValido = eventoFormularioUsuario.valid;
    }
  }

  escucharFormularioInstructor(eventoFormularioInstructor: FormGroup) {
    this.formularioCrearInstructor = eventoFormularioInstructor;
    this.esFormularioInstructorValido = this.formularioCrearInstructor.valid
  }

  crearInstructor() {
    this._cargandoService.habilitarCargando();
    this.setearUsuario();
    this.setearInstructor();
    this._usuarioService.create(this.usuario)
      .subscribe(usuario => {
        this.instructor.usuario = usuario;
        this._instructorService.create(this.instructor)
          .subscribe(instructor => {
            if (this.selectedFile) {
              this.subirImagen(usuario.id)
            }
            this._toasterService.pop('success', 'Éxito', 'EL instructor se ha creado correctamente');
            this.irListar();
            this._cargandoService.deshabiltarCargando();
          }
            , error => {
              this._toasterService.pop('warning', 'Advetencia', 'Falló al registrar instructor')
              this._cargandoService.deshabiltarCargando();
            })
      })

  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0]
  }

  subirImagen(id: number) {
    this._subirArchivoService.onUpload(id, this.selectedFile, 'usuario').
      subscribe();
  }

  irListar() {
    this._router.navigate(['/app', 'instructor', 'listar']);
  }

  setearUsuario() {
    Object.keys(this.formularioCrearUsuario.controls).map(atributo => {
      this.usuario[atributo] = this.formularioCrearUsuario.controls[atributo].value;
    })
  }

  setearInstructor() {
    Object.keys(this.formularioCrearInstructor.controls).map(atributo => {
      this.instructor[atributo] = this.formularioCrearInstructor.controls[atributo].value;
    })
  }
}

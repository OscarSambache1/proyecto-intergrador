import { Component, OnInit } from '@angular/core';
import { InstructorService } from '../../instructor.service';
import { Instructor } from 'src/app/interfaces/instructor.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import {MenuItem} from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';
import { AutenticacionService } from '../../../autenticacion/autenticacion.service';

@Component({
  selector: 'app-listar-instructores',
  templateUrl: './listar-instructores.component.html',
  styleUrls: ['./listar-instructores.component.css']
})
export class ListarInstructoresComponent implements OnInit {
  instructores: Instructor[] = [];
  parametroBusqueda = '';
  columnas=[
    { field: 'nombres', header: 'Nombres' },
    { field: 'apellidos', header: 'Apellidos' },
    { field: 'rol', header: 'Rol' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Acciones' },
  ];
  items: MenuItem[];
  icono = ICONOS.instructor;
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  esEmpleado: boolean;
  esAdministrador: boolean;
  esInstructor: boolean;
  constructor(
    private readonly _instructorService: InstructorService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService
  ) { }


  ngOnInit() {
    this.comprobarPermisos();
    this.obtenerPorParametro();
    this.items= cargarRutas('instructor','Listar', 'Listar instructores');
    this.items.pop();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
    this.esEmpleado = await this._autenticacionService.habilitarBotonSegunRol([ 'empleado'])
    this.esAdministrador = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.esInstructor = await this._autenticacionService.habilitarBotonSegunRol(['instructor'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    this._instructorService.findWhereOr(this.parametroBusqueda, false)
      .subscribe(instructoresPorParametro => {
        this.instructores = instructoresPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
        , error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar instructores');
          this._cargandoService.deshabiltarCargando();
        })
  }


  cambiarEstado(instructor: Instructor) {
    this._cargandoService.habilitarCargando();
    const indice = this.instructores.indexOf(instructor);
    const estaActivo = this.instructores[indice].estado == 1
    estaActivo ? this.instructores[indice].estado = 0 : 1;
    !estaActivo ? this.instructores[indice].estado = 1 : 0;
    this._instructorService.update(this.instructores[indice].id, { estado: this.instructores[indice].estado })
      .subscribe(tiendaEditada => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error=>{
          this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        })
  }
}



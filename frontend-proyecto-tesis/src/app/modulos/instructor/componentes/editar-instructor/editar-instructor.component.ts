import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../interfaces/usuario';
import { Instructor } from '../../../../interfaces/instructor.interface';
import { UsuarioService } from '../../../../servicios/usuario.service';
import { InstructorService } from '../../instructor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { SubirArchivoService } from '../../../../servicios/subir-archivo.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-editar-instructor',
  templateUrl: './editar-instructor.component.html',
  styleUrls: ['./editar-instructor.component.css']
})
export class EditarInstructorComponent implements OnInit {

  id: string;
  instructorAEditar: Instructor;
  instructorEditado: Instructor;
  usuarioAEditar: Usuario;
  usuarioEditado: Usuario;
  esFormularioUsuarioValido = false;
  esFormularioInstructorValido = false;
  selectedFile;
  items: MenuItem[];
  icono = ICONOS.instructor;
  urlImagenUsuario;

  constructor(
    private readonly _usuarioService: UsuarioService,
    private readonly _instructorService: InstructorService,
    private readonly _activateRoute: ActivatedRoute,
    private _router: Router,
    private readonly _subirArchivoService: SubirArchivoService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this.setearInstructor();
    this.items= cargarRutas('instructor','Editar', 'Listar instructores');
  }

  escucharFormularioUsuario(eventoFormularioUsuario) {
    if(eventoFormularioUsuario === false){
      this.esFormularioUsuarioValido=eventoFormularioUsuario;
    }
    else {
      this.usuarioEditado = eventoFormularioUsuario.value;
      this.esFormularioUsuarioValido=eventoFormularioUsuario.valid;
    }
  }

  escucharFormularioInstructor(eventoFormularioInstructor) {
    this.instructorEditado = eventoFormularioInstructor.value;
    this.esFormularioInstructorValido = eventoFormularioInstructor.valid;
  }

  editarInstructor() {
    this._cargandoService.habilitarCargando();
    this._usuarioService.update(this.usuarioAEditar.id, this.usuarioEditado)
      .subscribe(usuario => {
        this._instructorService.update(Number(this.id), this.instructorEditado)
          .subscribe(instructor => {
            if (this.selectedFile) {
              this.subirImagen(usuario.id);
            }
            this._cargandoService.deshabiltarCargando();
            this._toasterService.pop('success', 'Éxito', 'La informacin se ha actualizado');
            this.irListar();
          }
            , error => {
              this._cargandoService.deshabiltarCargando();
              this._toasterService.pop('error', 'Error', 'Falló al actualizar la información')
            })
      })

  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0];
  }

  subirImagen(id: number) {
    this._subirArchivoService.onUpload(id, this.selectedFile, 'usuario').
      subscribe();
  }

  setearInstructor() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      this.id = respuestaParametros.get('id')
    })
    this._instructorService.findOne(this.id).subscribe(
      instructor => {
        this.instructorAEditar = instructor;
        this.usuarioAEditar = instructor.usuario;
        this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${this.usuarioAEditar.urlFoto}`;
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterService.pop('error', 'Advetencia', 'Falló al mostrar la información');
        this._cargandoService.deshabiltarCargando();
      }
    )
  }

  irListar() {
    this._router.navigate(['/app', 'instructor', 'listar']);
  }

}

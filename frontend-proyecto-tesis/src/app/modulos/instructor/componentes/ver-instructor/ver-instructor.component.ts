import { Component, OnInit } from '@angular/core';
import { Instructor } from '../../../../interfaces/instructor.interface';
import { Usuario } from '../../../../interfaces/usuario';
import { InstructorService } from '../../instructor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ver-instructor',
  templateUrl: './ver-instructor.component.html',
  styleUrls: ['./ver-instructor.component.css']
})
export class VerInstructorComponent implements OnInit {
  id: string;
  instructorSeleccionado: Instructor;
  usuarioSeleccionado: Usuario;
  esFormularioUsuarioValido = false;
  esFormularioInstructorValido = false;
  items: MenuItem[];
  icono = ICONOS.instructor;
  urlImagenUsuario;

  constructor(
    private readonly _instructorServide: InstructorService,
    private readonly _activateRoute: ActivatedRoute,
    private _router: Router,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this.setearInstructor();
    this.items= cargarRutas('instructor','Ver', 'Listar instructores');
  }


  setearInstructor() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
        this.id = respuestaParametros.get('id')
    })
    this._instructorServide.findOne(this.id).subscribe(
        cliente => {
            this.instructorSeleccionado = cliente;
            this.usuarioSeleccionado = cliente.usuario;
            this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${this.usuarioSeleccionado.urlFoto}`;
            this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error', 'Error', 'Falló al mostrar la información');
          this._cargandoService.deshabiltarCargando();
        }
    )
}

irListar() {
  this._router.navigate(['/app', 'instructor', 'listar']);
}

}

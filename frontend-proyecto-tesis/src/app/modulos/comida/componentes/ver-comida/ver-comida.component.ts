import { Component, OnInit } from '@angular/core';
import { Comida } from '../../../../interfaces/comida.interface';
import { ComidaService } from '../../servicios/comida.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-comida',
  templateUrl: './ver-comida.component.html',
  styleUrls: ['./ver-comida.component.css']
})
export class VerComidaComponent implements OnInit {

  comidaAVer: Comida;
  items: MenuItem[];
  icono= ICONOS.comida;

  constructor(
    private readonly _comidaService: ComidaService,
    private readonly _router: Router,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService

  ) { }

  ngOnInit() {
    this.setearComidaAVer();
    this.items= cargarRutas('comida','Ver', 'Listar comidas');

  }

  setearComidaAVer(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idComida = Number(respuestaParametros.get('id'));
      this._comidaService.findOne(idComida).subscribe(
        comida => {
          this.comidaAVer = comida;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar la comida')
          this._cargandoService.deshabiltarCargando();
        });      
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al buscar la comida')
      this._cargandoService.deshabiltarCargando();
    }); 
   }

   irListarComidas(){
    this._router.navigate(['/app', 'comida','listar']);
  }
}

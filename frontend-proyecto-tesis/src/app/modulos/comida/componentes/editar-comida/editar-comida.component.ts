import { Component, OnInit } from '@angular/core';
import { Comida } from '../../../../interfaces/comida.interface';
import { FormGroup } from '@angular/forms';
import { ComidaService } from '../../servicios/comida.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-comida',
  templateUrl: './editar-comida.component.html',
  styleUrls: ['./editar-comida.component.css']
})
export class EditarComidaComponent implements OnInit {
  comidaEditada: Comida={};
  comidaAEditar: Comida;
  esFormularioComidaValido=false;
  formularioEditarComida: FormGroup;
  items: MenuItem[];
  icono= ICONOS.comida;

  constructor(
    private readonly _comidaService: ComidaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _cargandoService:  CargandoService,

  ) { }

  ngOnInit() {
    this.setearComidaAEditar();
    this.items= cargarRutas('comida','Editar', 'Listar de comidas');
  }

  escucharFormularioComida(eventoFormularioComida: FormGroup) {
    this.formularioEditarComida = eventoFormularioComida;
    this.esFormularioComidaValido = this.formularioEditarComida.valid;  
  }

  editarComida(){
    this._cargandoService.habilitarCargando();
    this.setearComidaEditada();
    this._comidaService.update(this.comidaAEditar.id, this.comidaEditada)
    .subscribe(comidaEditada=>{
      this._toasterService.pop('success', 'Éxito', 'La comida se ha editado');
      this.irListarComidas();
      this._cargandoService.deshabiltarCargando();
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al editar las comidas')
      this._cargandoService.deshabiltarCargando();
    });
  }

  setearComidaAEditar(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idComida = Number(respuestaParametros.get('id'));
      this._comidaService.findOne(idComida).subscribe(
        comida => {
          this.comidaAEditar = comida;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al buscar la comida')
          this._cargandoService.deshabiltarCargando();
        });
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al buscar la comida')
      this._cargandoService.deshabiltarCargando();
    });
   }

   setearComidaEditada(){
     this.comidaEditada.descripcion=this.formularioEditarComida.get('descripcion').value;
     this.comidaEditada.tipo=this.formularioEditarComida.get('tipo').value;
   }

   irListarComidas(){
    this._router.navigate(['/app', 'comida','listar']);
  }

  }


import { Component, OnInit } from '@angular/core';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-listar-comida',
  templateUrl: './listar-comida.component.html',
  styleUrls: ['./listar-comida.component.css']
})
export class ListarComidaComponent implements OnInit {
  items: MenuItem[];
  icono= ICONOS.comida;

  constructor(
  ) { }

  ngOnInit() {
    this.items= cargarRutas('comida','', 'Lista de comidas');
    this.items.pop();
  }
}

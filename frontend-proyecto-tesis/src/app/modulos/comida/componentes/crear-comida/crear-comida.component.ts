import { Component, OnInit } from '@angular/core';
import { Comida } from '../../../../interfaces/comida.interface';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { ComidaService } from '../../servicios/comida.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-comida',
  templateUrl: './crear-comida.component.html',
  styleUrls: ['./crear-comida.component.css']
})
export class CrearComidaComponent implements OnInit {
 
  comidaACrear: Comida={};
  esFormularioComidaValido = false;
  formularioCrearComida: FormGroup;
  items: MenuItem[];
  icono= ICONOS.comida;

  constructor(
    private readonly _comidaService: ComidaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,  
    private readonly _router: Router,
  ) { }

  ngOnInit() {
    this.items= cargarRutas('comida','Registrar', 'Lista de comidas');
  }

  escucharFormularioComida(eventoFormularioComida: FormGroup) {
    this.formularioCrearComida = eventoFormularioComida;
    this.esFormularioComidaValido = this.formularioCrearComida.valid;  
  }

  crearComida(){
    this._cargandoService.habilitarCargando();
    this.setearComidaACrear();
    this._comidaService.create(this.comidaACrear)
    .subscribe(comidaCreada=>{
      this._toasterService.pop('success', 'Éxito', 'La comida se ha creado');
      this.irListarComidas();
      this._cargandoService.deshabiltarCargando();
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al crearc la comida')
      this._cargandoService.deshabiltarCargando();
    });
  }

  setearComidaACrear(){
    this.comidaACrear.descripcion=this.formularioCrearComida.get('descripcion').value;
    this.comidaACrear.tipo=this.formularioCrearComida.get('tipo').value;
  }

  irListarComidas(){
    this._router.navigate(['/app', 'comida','listar']);
  }

}

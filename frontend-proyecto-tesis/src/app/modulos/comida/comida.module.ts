import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComidaRoutingModule } from './comida-routing.module';
import { ListarComidaComponent } from './componentes/listar-comida/listar-comida.component';
import { CrearComidaComponent } from './componentes/crear-comida/crear-comida.component';
import { EditarComidaComponent } from './componentes/editar-comida/editar-comida.component';
import { VerComidaComponent } from './componentes/ver-comida/ver-comida.component';
import { ComidaService } from './servicios/comida.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatFormFieldModule, MatDialogModule, MatButtonModule } from '@angular/material';
import { FormularioComidaComponent } from './formularios/formulario-comida/formulario-comida.component';
import { DropdownModule } from 'primeng/dropdown';
import { TipoComidaService } from './servicios/tipo-comida.service';
import { ModalVerComidaComponent } from './modales/modal-ver-comida/modal-ver-comida.component';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TablaComidasModule } from '../../componentes/tabla-comidas/tabla-comidas.module';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TablaComidasModule,
    ReactiveFormsModule,
    ComidaRoutingModule,
    TableModule,
    DropdownModule,
    InputTextareaModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    InputTextModule,
    PanelModule,
    NgbModule.forRoot(),
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [
    ListarComidaComponent, 
    CrearComidaComponent, 
    EditarComidaComponent, 
    VerComidaComponent, FormularioComidaComponent, ModalVerComidaComponent
  ],
  providers:[
    ComidaService,
    TipoComidaService
  ]
,
entryComponents:[
  ModalVerComidaComponent
]

})
export class ComidaModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Comida } from '../../../interfaces/comida.interface';

@Injectable({
    providedIn: 'root'
})
export class ComidaService {

    constructor(
        private readonly _httpClient: HttpClient
    ) { }

    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/comida?consulta=` + `${JSON.stringify(consulta)}`)
    }

    create(comida: Comida): Observable<Comida> {
        return this._httpClient.post(environment.url + '/comida/', comida)
    }
    findOne(id: number): Observable<Comida> {
        return this._httpClient.get(environment.url + `/comida/${id}`)
    }
    update(id: number, comida: Comida): Observable<Comida> {
        return this._httpClient.put(environment.url + `/comida/${id}`, comida)
    }

}

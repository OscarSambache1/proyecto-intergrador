import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { TipoComida } from '../../../interfaces/tipo-comida.interface';

@Injectable({
    providedIn: 'root'
})
export class TipoComidaService {

    constructor(
        private readonly _httpClient: HttpClient
    ) { }

    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/tipo-comida?consulta=` + `${JSON.stringify(consulta)}`)
    }

    create(tipoComida: TipoComida): Observable<TipoComida> {
        return this._httpClient.post(environment.url + '/tipo-comida/', tipoComida)
    }

    findOne(id: number): Observable<TipoComida> {
        return this._httpClient.get(environment.url + `/tipo-comida/${id}`)
    }

    update(id: number, tipoComida: TipoComida): Observable<TipoComida> {
        return this._httpClient.put(environment.url + `/tipo-comida/${id}`, tipoComida)
    }

}

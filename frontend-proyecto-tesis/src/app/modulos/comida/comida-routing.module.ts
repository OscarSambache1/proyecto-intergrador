import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComidaComponent } from './componentes/listar-comida/listar-comida.component';
import { CrearComidaComponent } from './componentes/crear-comida/crear-comida.component';
import { EditarComidaComponent } from './componentes/editar-comida/editar-comida.component';
import { VerComidaComponent } from './componentes/ver-comida/ver-comida.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: 'crear',
    component : CrearComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'editar/:id',
    component: EditarComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component: VerComidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComidaRoutingModule { }

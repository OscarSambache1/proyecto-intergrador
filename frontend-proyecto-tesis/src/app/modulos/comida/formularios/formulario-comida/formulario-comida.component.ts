import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Comida } from '../../../../interfaces/comida.interface';
import { SelectItem } from 'primeng/api';
import { TipoComidaService } from '../../servicios/tipo-comida.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-comida',
  templateUrl: './formulario-comida.component.html',
  styleUrls: ['./formulario-comida.component.css']
})
export class FormularioComidaComponent implements OnInit {
  @Output() esValidoFormularioComida: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() comidaSeleccionada: Comida;
  formularioComida: FormGroup
  arregloTiposComida: SelectItem[] = [];
  tipoComidaSeleccionado: Comida;

  mensajesErroresDescripcion = [];
  mensajesErroresTipo = [];

  private mensajesValidacionTipo = {
    required: 'El tipo de comida es obligatoría'
  };

  private mensajesValidacionDescripcion= {
    required: "La descripción es obligatoria",
  };

  constructor(
    private readonly _tipoComidaService: TipoComidaService,
    private readonly _formBuilder: FormBuilder,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.llenarDropdownTiposComida();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  llenarDropdownTiposComida() {
    this._cargandoService.habilitarCargando();
    const consulta ={where: {estado: 1}};
    this._tipoComidaService.findAll(consulta).subscribe(musculos =>{
      this.arregloTiposComida.push(
        {
          label: "seleccione un tipo",
          value: null
        }
      );
      musculos.forEach(tipo => {
        const elemento ={
          label: tipo.nombre,
          value: tipo
        }
        this.arregloTiposComida.push(elemento);
        this._cargandoService.deshabiltarCargando();
      });
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al mostrar los tipos')
      this._cargandoService.deshabiltarCargando();
    }
  );
  }

  crearFormulario() {
    this.formularioComida = this._formBuilder.group({
      descripcion: ['', [Validators.required]],
      tipo: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioComida.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioComida.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresTipo = setearMensajes(this.formularioComida.get('tipo'), this.mensajesValidacionTipo);
      });
  }

  enviarFormularioValido() {
    this.formularioComida.valueChanges.subscribe(formulario => {
      this.esValidoFormularioComida.emit(this.formularioComida);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioComida.disable();
    }

  }

  llenarFormulario() {
    this._cargandoService.habilitarCargando();
    this._tipoComidaService.findOne(this.comidaSeleccionada.tipo.id)
    .subscribe(
      tipo=>{
        this.formularioComida.patchValue(
          {
            descripcion: this.comidaSeleccionada.descripcion,
            tipo
          }
        )
        this.tipoComidaSeleccionado = tipo;
        this._cargandoService.deshabiltarCargando();
      },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al buscar la dieta')
        this._cargandoService.deshabiltarCargando();
      }
    )   
  }

}

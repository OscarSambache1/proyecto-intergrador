import { Component, OnInit, Inject } from '@angular/core';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Comida } from '../../../../interfaces/comida.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ComidaService } from '../../servicios/comida.service';

@Component({
  selector: 'app-modal-ver-comida',
  templateUrl: './modal-ver-comida.component.html',
  styleUrls: ['./modal-ver-comida.component.css']
})
export class ModalVerComidaComponent implements OnInit {
  comidaAVer: Comida;

  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _comidaService: ComidaService,
    public dialogRef: MatDialogRef<ModalVerComidaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close(this.comidaAVer);
  }

  ngOnInit() {
    this.setearComidaAVer();
  }

  setearComidaAVer(){
    this._cargandoService.habilitarCargando();
      this._comidaService.findOne(this.data.id).subscribe(
        comida => {
          this._cargandoService.deshabiltarCargando();
          this.comidaAVer = comida;
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar la comida')
          this._cargandoService.deshabiltarCargando();
        }); 
   }
}

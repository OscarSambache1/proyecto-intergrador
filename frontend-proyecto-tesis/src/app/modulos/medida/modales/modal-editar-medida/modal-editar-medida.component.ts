import { Component, OnInit, Inject } from '@angular/core';
import { Medida } from 'src/app/interfaces/medidas.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MedidaService } from 'src/app/modulos/medida/medida.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-modal-editar-medida',
  templateUrl: './modal-editar-medida.component.html',
  styleUrls: ['./modal-editar-medida.component.css']
})
export class ModalEditarMedidaComponent implements OnInit {
  medidaAEditar: Medida = {};
  medidaAEditada: Medida = {};
  formularioMedida: FormGroup;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly toasterService: ToasterService,
    private readonly _medidaService: MedidaService,
    public dialogRef: MatDialogRef<ModalEditarMedidaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.medidaAEditar=this.data;
  }

  crearFormulario() {
    this.formularioMedida = this._formBuilder.group({
    })
}

  escucharFormularioMedida(eventoFormularioMedida: FormGroup) {
    this.formularioMedida=eventoFormularioMedida
    this.medidaAEditada=eventoFormularioMedida.value;
  }

  editarMedida(){
   this._medidaService.update(this.medidaAEditar.id,this.medidaAEditada).subscribe(medidaEditada=>{
    this.toasterService.pop('success', 'Éxito', 'El registro se ha editado correctamente'); 
    this.medidaAEditada= this.medidaAEditada;
   })

  }
}

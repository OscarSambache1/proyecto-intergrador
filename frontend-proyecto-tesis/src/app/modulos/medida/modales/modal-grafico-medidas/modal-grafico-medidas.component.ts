import { Component, OnInit, Inject } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Medida } from '../../../../interfaces/medidas.interface';
import { ClienteService } from '../../../cliente/cliente.service';
import { MedidaService } from '../../medida.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-grafico-medidas',
  templateUrl: './modal-grafico-medidas.component.html',
  styleUrls: ['./modal-grafico-medidas.component.css']
})
export class ModalGraficoMedidasComponent implements OnInit {
  datos = {
    labels: [],
    datasets: []
  };
  nombreMedidas: SelectItem[] = [];
  medida: Medida;
  constructor(
    private readonly _medidaService: MedidaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    public dialogRef: MatDialogRef<ModalGraficoMedidasComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.nombreMedidas = [
      { label: 'Seleccione medida', value: null },
      { label: 'peso', value: 'peso' },
      { label: 'estatura', value: 'estatura' },
      { label: 'abdomen', value: 'abdomen' },
      { label: 'torax', value: 'torax' },
      { label: 'muslo', value: 'muslo' },
      { label: 'pantorrilla', value: 'pantorrilla' },
      { label: 'biceps', value: 'biceps' }
    ];
  }

  mostrarGrafico(eventoMedida) {
    if (eventoMedida.value !== null) {
      this.obtenerMedidas(eventoMedida.value);
    }
  }

  obtenerMedidas(medidaAMostrar) {

    this._cargandoService.habilitarCargando();
    const consulta =
    {
      select: ['fechaRegistro', medidaAMostrar],
      where: { cliente: this.data.cliente.id },
    }
    this._medidaService.findAll(consulta)
      .subscribe(medidas => {
        const arregloFecha = medidas.map(medidaFecha => {
          return medidaFecha['fechaRegistro'];
        });

        const arregloMedidas = medidas.map(medida => {
          return medida[medidaAMostrar];
        });

        this.datos = {
          labels: arregloFecha,
          datasets: [
            {
              label: medidaAMostrar,
              data: arregloMedidas,
              fill: false,
              borderColor: '#4bc0c0'
            }
          ]
        }
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al mostrar medidas')
        });
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Medida } from 'src/app/interfaces/medidas.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MedidaService } from 'src/app/modulos/medida/medida.service';
import { ToasterService } from 'angular2-toaster';
import { Cliente } from '../../../../interfaces/cliente.interface';

@Component({
  selector: 'app-modal-crear-medida',
  templateUrl: './modal-crear-medida.component.html',
  styleUrls: ['./modal-crear-medida.component.css']
})
export class ModalCrearMedidaComponent implements OnInit {
  medidaACrear: Medida={};
  cliente: Cliente={};
  formularioMedida: FormGroup;


  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly toasterService: ToasterService,
    private readonly _medidaService: MedidaService,
    public dialogRef: MatDialogRef<ModalCrearMedidaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.crearFormulario();
    this.cliente=this.data;
  }

  crearFormulario() {
    this.formularioMedida = this._formBuilder.group({
    })
}

  escucharFormularioMedida(eventoFormularioMedida: FormGroup) {
    this.formularioMedida=eventoFormularioMedida
    this.medidaACrear=eventoFormularioMedida.value;
    this.medidaACrear.cliente= this.data;
  }

  crearMedida(){
    this._medidaService.create(this.medidaACrear).subscribe(medidaCreada => {
      this.toasterService.pop('success', 'Éxito', 'El registro se ha creado correctamente');
      this.dialogRef.close(medidaCreada);
    })
  }


}

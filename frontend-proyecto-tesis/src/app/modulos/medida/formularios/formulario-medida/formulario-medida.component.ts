import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CONFIGURACIONES_CALENDARIO } from 'src/app/constantes/configuracion-calendario';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Medida } from 'src/app/interfaces/medidas.interface';
import { obtenerFechaHoy } from 'src/app/funciones/obtener-fecha-actual';
import { map, debounceTime } from 'rxjs/operators';
import { MedidaService } from '../../medida.service';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { setearMensajes } from '../../../../shared/validaciones';

@Component({
  selector: 'app-formulario-medida',
  templateUrl: './formulario-medida.component.html',
  styleUrls: ['./formulario-medida.component.css']
})
export class FormularioMedidaComponent implements OnInit {

  configuracionCalendario: any
  fechaDefault: string
  @Output() esValidoFormularioMedida: EventEmitter<FormGroup | Boolean> = new EventEmitter();
  @Input() esFormularioEditar = false;
  @Input() medidaSeleccionada: Medida;
  @Input() cliente: Cliente;
  formularioMedida: FormGroup;
  mensajeValidacionFecha: string;


  mensajesErroresPeso = [];
  mensajesErroresEstatura = [];
  mensajesErroresTorax = [];
  mensajesErroresAbdomen = [];
  mensajesErroresMuslo = [];
  mensajesErroresPantorrilla = [];
  mensajesErroresBiceps = [];
  mensajesErroresFecha = [];

  private mensajesValidacionPeso = {
    required: "El peso es obligatorio",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionEstatura = {
    required: "La estatura es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionTorax = {
    required: "La medida del torax es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionMuslo = {
    required: "La medida del muslo es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionPantorrilla = {
    required: "La medida del pantorrilla es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionAbdomen = {
    required: "La medida del abdomen es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionBiceps = {
    required: "La medida del biceps es obligatoria",
    pattern: 'Solo puede ingresar hasta dos números decimales'
  }

  private mensajesValidacionFecha = {
    required: "La fecha es obligatoria",
    validacionFecha: "No se puede agregar mas medidas para esta fecha"
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _medidaService: MedidaService,
    private readonly toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.configuracionCalendario = CONFIGURACIONES_CALENDARIO;
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioMedida = this._formBuilder.group({
      peso: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      estatura: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      torax: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      abdomen: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      pantorrilla: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      muslo: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      biceps: ['', [Validators.required, Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      fechaRegistro: [{ value: '' }, [Validators.required],
      [
      ]
      ],
    })
  }

  escucharFormulario() {
    this.formularioMedida.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresAbdomen = setearMensajes(this.formularioMedida.get('abdomen'), this.mensajesValidacionAbdomen);
        this.mensajesErroresBiceps = setearMensajes(this.formularioMedida.get('biceps'), this.mensajesValidacionBiceps);
        this.mensajesErroresEstatura = setearMensajes(this.formularioMedida.get('estatura'), this.mensajesValidacionEstatura);
        this.mensajesErroresMuslo = setearMensajes(this.formularioMedida.get('muslo'), this.mensajesValidacionMuslo);
        this.mensajesErroresPantorrilla = setearMensajes(this.formularioMedida.get('pantorrilla'), this.mensajesValidacionPantorrilla);
        this.mensajesErroresPeso = setearMensajes(this.formularioMedida.get('peso'), this.mensajesValidacionPeso);
        this.mensajesErroresTorax = setearMensajes(this.formularioMedida.get('torax'), this.mensajesValidacionTorax);
        this.escucharCambiosFecha();
      });
  }

  async enviarFormularioValido() {
    this.formularioMedida.valueChanges.subscribe(async formulario => {
      const existeFecha = await this.validarFecha();
      if (existeFecha && !this.esFormularioEditar) {
        this.mensajesErroresFecha = [];
        this.mensajesErroresFecha.push(this.mensajesValidacionFecha['validacionFecha']);
      }
      if (!this.esFormularioEditar) {
        if (!existeFecha && this.formularioMedida.valid) {
          this.esValidoFormularioMedida.emit(this.formularioMedida);
        }
        else {
          this.esValidoFormularioMedida.emit(false);
        }
      }
      else{
        if (this.formularioMedida.valid) {
          this.esValidoFormularioMedida.emit(this.formularioMedida);
        }
        else {
          this.esValidoFormularioMedida.emit(false);
        }
      }

    });
  }

  async  validarFecha(): Promise<Boolean> {
    const fechaFormulario = this.formularioMedida.get('fechaRegistro').value;
    const consulta =
    {
      order: { id: 'DESC' },
      where: {
        fechaRegistro: fechaFormulario,
        cliente: this.cliente.id
      }
    }
    const promesaFecha: any = this._medidaService.findAll(consulta).toPromise();
    const respuestaPromesa = await promesaFecha;
    const existeFecha: boolean = respuestaPromesa.length !== 0
    return existeFecha
  }

  llenarFormulario() {
    this.formularioMedida.patchValue(
      {
        peso: this.medidaSeleccionada.peso,
        estatura: this.medidaSeleccionada.estatura,
        torax: this.medidaSeleccionada.torax,
        abdomen: this.medidaSeleccionada.abdomen,
        pantorrilla: this.medidaSeleccionada.pantorrilla,
        biceps: this.medidaSeleccionada.biceps,
        muslo: this.medidaSeleccionada.muslo,
        fechaRegistro: this.medidaSeleccionada.fechaRegistro,
      }
    )
  }

  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
      this.formularioMedida.get('fechaRegistro').disable();
    }
    else {
      // this.formularioMedida.get('fechaRegistro').setAsyncValidators(this.validacionAsincronaFecha.bind(this));
      this.formularioMedida.patchValue(
        {
          fechaRegistro: obtenerFechaHoy()
        }
      )
    }
  }

  escucharCambiosFecha() {
    const fechaControl = this.formularioMedida.get('fechaRegistro')
    fechaControl.valueChanges
      .subscribe(valor => {
        this.setearMensajesErroresFecha(fechaControl)
      }
      )
  }

  setearMensajesErroresFecha(valor: AbstractControl) {
    this.mensajesErroresFecha = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresFecha = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionFecha[atributo]
      })
    }
  }
}
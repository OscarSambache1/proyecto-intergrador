import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedidaRoutingModule } from './medida-routing.module';
import { ListarMedidaComponent } from './componentes/listar-medida/listar-medida.component';
import { MedidaService } from './medida.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import {ToolbarModule} from 'primeng/toolbar';
import { FormularioMedidaComponent } from './formularios/formulario-medida/formulario-medida.component';
import { ModalCrearMedidaComponent } from './modales/modal-crear-medida/modal-crear-medida.component';
import { ModalEditarMedidaComponent } from './modales/modal-editar-medida/modal-editar-medida.component';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import {CalendarModule} from 'primeng/calendar';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { CrearMedidaComponent } from './componentes/crear-medida/crear-medida.component';
import { EditarMedidaComponent } from './componentes/editar-medida/editar-medida.component';
import {PanelModule} from 'primeng/panel';
import {ChartModule} from 'primeng/chart';
import { ModalGraficoMedidasComponent } from './modales/modal-grafico-medidas/modal-grafico-medidas.component';
import {DropdownModule} from 'primeng/dropdown';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { InformacionClienteModule } from '../../componentes/informacion-cliente/informacion-cliente.module';

@NgModule({
  imports: [
    CommonModule,
    MedidaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    PanelModule,
    ChartModule,
    DropdownModule,
    MigasPanModule,
    BarraTituloModule,
    InformacionClienteModule
  ],
  declarations: [
    ListarMedidaComponent,
    FormularioMedidaComponent,
    ModalCrearMedidaComponent,
    ModalEditarMedidaComponent,
    CrearMedidaComponent,
    EditarMedidaComponent,
    ModalGraficoMedidasComponent,
  ],
  providers: [
    MedidaService,
    ConfirmationService
  ],
  entryComponents:[
    ModalCrearMedidaComponent,
    ModalEditarMedidaComponent,
    ModalGraficoMedidasComponent
  ]
})
export class MedidaModule { }

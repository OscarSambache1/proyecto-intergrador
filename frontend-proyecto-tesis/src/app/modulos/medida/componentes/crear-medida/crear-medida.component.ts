import { Component, OnInit } from '@angular/core';
import { Medida } from '../../../../interfaces/medidas.interface';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { MedidaService } from '../../medida.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../../../cliente/cliente.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-medida',
  templateUrl: './crear-medida.component.html',
  styleUrls: ['./crear-medida.component.css']
})
export class CrearMedidaComponent implements OnInit {
  medidaACrear: Medida = {};
  cliente: Cliente;
  formularioMedida: FormGroup;
  esFormularioMedidasValido = false;
  items : MenuItem[];
  icono= ICONOS.medidas;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _medidaService: MedidaService,
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService
  ) { }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this.obtenerIdCliente();
    this.crearFormulario();
    this._cargandoService.deshabiltarCargando();
  }

  crearFormulario() {
    this.formularioMedida = this._formBuilder.group({
    })
  }

  escucharFormularioMedida(eventoFormularioMedida) {
    this.formularioMedida = eventoFormularioMedida;
    if(eventoFormularioMedida === false){
      this.esFormularioMedidasValido=eventoFormularioMedida;
    }
    else {
      this.esFormularioMedidasValido=eventoFormularioMedida.valid;
      this.medidaACrear = eventoFormularioMedida.value;
    }
  }

  crearMedida() {
    this._cargandoService.habilitarCargando();
    this.medidaACrear.cliente = this.cliente;
    this._medidaService.create(this.medidaACrear).subscribe(medidaCreada => {
      this.irListarMedidas();
      this._toasterService.pop('success', 'Éxito', 'El registro se ha creado correctamente');
      this._cargandoService.deshabiltarCargando();
    }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Error al registrar medida');
        this._cargandoService.deshabiltarCargando();
      })
  }

  obtenerIdCliente() {
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCliente = Number(respuestaParametros.get('id'));
      this._clienteService.findOne(idCliente).subscribe(
        cliente => {
          this.cliente = cliente;
          this.items=cargarRutasChildren(this.cliente.id,'medidas','Registrar', 'medidas');
        }
      )
      ,
      errror=>{
        this._toasterService.pop('error', 'Error', 'Error al obtener cliente');
      }
    }
    ,
    errror=>{
      this._toasterService.pop('error', 'Error', 'Error al obtener id cliente');
    })
  }

  irListarMedidas() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'medidas']);
  }
}

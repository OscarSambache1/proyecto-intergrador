import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/modulos/cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Medida } from 'src/app/interfaces/medidas.interface';
import { MatDialog } from '@angular/material';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { MedidaService } from '../../medida.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ModalGraficoMedidasComponent } from '../../modales/modal-grafico-medidas/modal-grafico-medidas.component';
import { ICONOS } from '../../../../constantes/iconos';
import { AutenticacionService } from '../../../autenticacion/autenticacion.service';


@Component({
  selector: 'app-listar-medida',
  templateUrl: './listar-medida.component.html',
  styleUrls: ['./listar-medida.component.css']
})
export class ListarMedidaComponent implements OnInit {

  arregloMedidas: Medida[] = [];
  cliente: Cliente;
  medidaCreada: Medida = {}
  columnas=[
    { field: 'fecha', header: 'Fecha', width:'16%' },
    { field: 'peso', header: 'Peso (kg)', width:'7%'},
    { field: 'estatura', header: 'Estatura (cm)', width:'10%'},
    { field: 'torax', header: 'Tórax (cm)', width:'7%' },
    { field: 'abdomen', header: 'Abdomen (cm)', width:'10%' },
    { field: 'muslo', header: 'Muslo (cm)', width:'7%' },
    { field: 'pantorilla', header: 'Pantorilla (cm)', width:'10%' },
    { field: 'biceps', header: 'Bíceps (cm)',width:'7%' },
    { field: 'acciones', header: 'Acciones', width:'26%' },
  ];
  items: MenuItem[]=[];
  icono= ICONOS.medidas;
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _medidaService: MedidaService,
    private confirmationService: ConfirmationService,
    public dialog: MatDialog,
    private readonly toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.llenarTabla();
    this.items=[
      {
        label: 'Cliente',
        routerLink: ['/app', 'lista-clientes',{modulo:'medidas', titulo:'medidas'}]
      },
      {
        label: 'Lista de Medidas',
      }
    ];
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
    if (!this.mostrarBoton){
      this.columnas.splice(8,1);
    }
  }

  llenarTabla() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      this._clienteService.findOne(Number(respuestaParametros.get('id')))
        .subscribe(cliente => {
          this.cliente = cliente;
          const consulta =
            { order: { id: 'DESC' }, where: { cliente: this.cliente }, relations: ['cliente'] }
          this._medidaService.findAll(consulta)
            .subscribe(medidas => {
              this.arregloMedidas = medidas;
              this._cargandoService.deshabiltarCargando();
            }
              ,
              error => {
                this._cargandoService.deshabiltarCargando();
                this._toasterService.pop('error', 'Error', 'Error al traer medidas')
              })
        }
          ,
          error => {
            this._cargandoService.deshabiltarCargando();
            this._toasterService.pop('error', 'Error', 'Error al traer medidas')
          })
    })
  }

  abrirModalGraficos(){
    const dialogRef = this.dialog.open(ModalGraficoMedidasComponent, {
      height: 'auto',
      width: '1000px',
      data: {cliente:this.cliente}
    });

  }

  confirmar(medidaAEliminar: Medida) {
    this.confirmationService.confirm({
      message: '¿Está seguro que quiere eliminar este registro?',
      header: 'Confirmación de eliminación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.eliminarMedida(medidaAEliminar)
      },
      reject: () => {
      }
    });
  }

  eliminarMedida(medidaAEliminar: Medida) {
    this._cargandoService.habilitarCargando();
    this._medidaService.delete(medidaAEliminar.id)
      .subscribe(medidaEliminada => {
        this.toasterService.pop('success', 'Éxito', 'El registro se ha eliminado correctamente');
        const indice = this.arregloMedidas.indexOf(medidaAEliminar);
        this.arregloMedidas.splice(indice, 1);
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this.toasterService.pop('error', 'Error', 'Error al eliminar el registro');
          this._cargandoService.deshabiltarCargando();
        })
  }

  irListaClientes() {
    this._router.navigate(['/app', 'lista-clientes', { modulo: 'medidas', titulo: 'medidas' }]);
  }
}

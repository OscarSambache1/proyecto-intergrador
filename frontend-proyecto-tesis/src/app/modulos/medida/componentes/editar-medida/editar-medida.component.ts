import { Component, OnInit } from '@angular/core';
import { Medida } from '../../../../interfaces/medidas.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { MedidaService } from '../../medida.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { ClienteService } from '../../../cliente/cliente.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-medida',
  templateUrl: './editar-medida.component.html',
  styleUrls: ['./editar-medida.component.css']
})
export class EditarMedidaComponent implements OnInit {
  medidaAEditar: Medida;
  medidaAEditada: Medida = {};
  formularioMedida: FormGroup;
  cliente: Cliente;
  esFormularioMedidasValido = false;
  items : MenuItem[];
  icono= ICONOS.medidas;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _medidaService: MedidaService,
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService
  ) { }

  ngOnInit() {
    this.setearMedida();
    this.crearFormulario();
  }

  crearFormulario() {
    this.formularioMedida = this._formBuilder.group({
    })
  }

  escucharFormularioMedida(eventoFormularioMedida) {
    this.formularioMedida = eventoFormularioMedida;
    if(eventoFormularioMedida === false){
      this.esFormularioMedidasValido=eventoFormularioMedida;
    }
    else {
      this.esFormularioMedidasValido=eventoFormularioMedida.valid;
      this.seetarMedidaAEditar();
    }
  }

  seetarMedidaAEditar () {
    this.medidaAEditada.peso = +this.formularioMedida.get('peso').value;
    this.medidaAEditada.estatura = +this.formularioMedida.get('peso').value;
    this.medidaAEditada.torax = +this.formularioMedida.get('torax').value;
    this.medidaAEditada.abdomen = +this.formularioMedida.get('abdomen').value;
    this.medidaAEditada.pantorrilla = +this.formularioMedida.get('pantorrilla').value;
    this.medidaAEditada.muslo = +this.formularioMedida.get('muslo').value;
    this.medidaAEditada.biceps = +this.formularioMedida.get('biceps').value;
    this.medidaAEditada.fechaRegistro = this.formularioMedida.get('fechaRegistro').value;
  }

  editarMedida() {
    this._cargandoService.habilitarCargando();
    this._medidaService.update(this.medidaAEditar.id, this.medidaAEditada)
      .subscribe(medidaEditada => {
        this.irListarMedidas();
        this._toasterService.pop('success', 'Éxito', 'El registro se ha editado correctamente');
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al editar la medida');
          this._cargandoService.deshabiltarCargando();
        }
      )

  }

  setearMedida() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idMedida = Number(respuestaParametros.get('id'));
      this._medidaService.findOne(idMedida).subscribe(
        medida => {
          this.medidaAEditar = medida;
          this.setearCliente(this.medidaAEditar.cliente.id);
          this.items=cargarRutasChildren(this.medidaAEditar.cliente.id,'medidas','Crear', 'medidas');
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar medida');
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
  }

  setearCliente(idCliente: number) {
    this._clienteService.findOne(idCliente)
      .subscribe(cliente => {
        this.cliente = cliente;
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al setaer cliente');
        });
  }

  irListarMedidas() {
    this._router.navigate(['/app', 'cliente', this.medidaAEditar.cliente.id, 'medidas']);
  }
}

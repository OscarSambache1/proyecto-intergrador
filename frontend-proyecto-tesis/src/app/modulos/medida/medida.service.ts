import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Medida } from '../../interfaces/medidas.interface';
import { Observable } from 'rxjs';

@Injectable()
export class MedidaService {

    constructor(private readonly _httpClient: HttpClient) { }


    findAll(consulta: any):Observable<any>{
        return  this._httpClient.get(environment.url+`/medida?consulta=`+`${JSON.stringify(consulta)}`)
      }

    findOne(id:number): Observable<Medida>{
        return  this._httpClient.get(environment.url+`/medida/${id}`)
    }
    create(medida: Medida): Observable<Medida> {
        return this._httpClient.post(environment.url + '/medida', medida)
    }

    update(id: number, medida: Medida): Observable<Medida> {
        return this._httpClient.put(environment.url + `/medida/${id}`, medida)
    }

    delete(id: number): Observable<any> {
        return this._httpClient.delete(environment.url + `/medida/${id}`)
    }
}

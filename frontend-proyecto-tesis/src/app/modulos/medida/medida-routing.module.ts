import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarMedidaComponent } from './componentes/listar-medida/listar-medida.component';
import { CrearMedidaComponent } from './componentes/crear-medida/crear-medida.component';
import { EditarMedidaComponent } from './componentes/editar-medida/editar-medida.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'',
    component : ListarMedidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component : CrearMedidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component : EditarMedidaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedidaRoutingModule { }

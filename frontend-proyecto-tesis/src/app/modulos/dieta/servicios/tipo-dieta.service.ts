import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { TipoDieta } from '../../../interfaces/tipo-dieta.interface';

@Injectable({
    providedIn: 'root'
})
export class TipoDietaService {

    constructor(
        private readonly _httpClient: HttpClient
    ) { }

    findAll(consulta: any): Observable<any> {
        return this._httpClient.get(environment.url + `/tipo-dieta?consulta=` + `${JSON.stringify(consulta)}`)
    }

    create(tipoDieta: TipoDieta): Observable<TipoDieta> {
        return this._httpClient.post(environment.url + '/tipo-dieta/', tipoDieta)
    }

    findOne(id: number): Observable<TipoDieta> {
        return this._httpClient.get(environment.url + `/tipo-dieta/${id}`)
    }

    update(id: number, tipoDieta: TipoDieta): Observable<TipoDieta> {
        return this._httpClient.put(environment.url + `/tipo-dieta/${id}`, tipoDieta)
    }

}

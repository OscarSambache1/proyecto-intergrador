import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ComidaDieta } from '../../../interfaces/comidas-dieta.ineterface';

@Injectable({
  providedIn: 'root'
})
export class ComidaDietaService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }
  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/comida-dieta?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(comidaDieta: ComidaDieta): Observable<ComidaDieta> {
    return this._httpClient.post(environment.url + '/comida-dieta/', comidaDieta)
  }

  createMany(comidasDietas: ComidaDieta[]): Observable<any>{
    return this._httpClient.post(environment.url + '/comida-dieta/crear-varios', comidasDietas)
  }

  findOne(id: number): Observable<ComidaDieta> {
    return this._httpClient.get(environment.url + `/comida-dieta-rutina/${id}`)
  }

  update(id: number, comidaDieta: ComidaDieta): Observable<ComidaDieta> {
    return this._httpClient.put(environment.url + `/comida-dieta/${id}`, comidaDieta)
  }
  
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/comida-dieta/${id}`)
  }
  
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Dieta } from '../../../interfaces/dieta.interface';

@Injectable({
  providedIn: 'root'
})
export class DietaService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/dieta?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(dieta: Dieta): Observable<Dieta> {
    return this._httpClient.post(environment.url + '/dieta/', dieta)
  }
  findOne(id: number): Observable<Dieta> {
    return this._httpClient.get(environment.url + `/dieta/${id}`)
  }
  update(id: number, dieta: Dieta): Observable<Dieta> {
    return this._httpClient.put(environment.url + `/dieta/${id}`, dieta)
  }
}

import { Component, OnInit } from '@angular/core';
import { Dieta } from '../../../../interfaces/dieta.interface';
import { FormGroup } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { DietaService } from '../../servicios/dieta.service';
import { ComidaDietaService } from '../../servicios/comida-dieta.service';
import { ComidaDieta } from '../../../../interfaces/comidas-dieta.ineterface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-dieta',
  templateUrl: './editar-dieta.component.html',
  styleUrls: ['./editar-dieta.component.css']
})
export class EditarDietaComponent implements OnInit {
  dietaAEditar: Dieta;
  dietaEditada: Dieta = {};
  arregloComidasDietasAGuardadas: ComidaDieta[];
  arregloComidasDietasAEditar: ComidaDieta[];
  esFormularioDietaValido = false;
  formularioEditarDieta: FormGroup;
  items: MenuItem[];
  icono= ICONOS.dieta;

  constructor(
    private readonly _dietaService: DietaService,
    private readonly _comidaDietaService: ComidaDietaService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.setearDietaAEditar();
    this.items= cargarRutas('dieta','Editar', 'Lista de dietas');
  }

  escucharFormularioDieta(eventoFormularioDieta) {
    this.formularioEditarDieta = eventoFormularioDieta;
    if(eventoFormularioDieta === false){
      this.esFormularioDietaValido=eventoFormularioDieta;
    }
    else {
      this.esFormularioDietaValido=eventoFormularioDieta.valid;
      this.dietaEditada = eventoFormularioDieta.value;
    }
  }

  editarDieta() {
    this._cargandoService.habilitarCargando();
    this.setearDietaEditada();
    delete this.dietaEditada.comidasDieta;
    this._dietaService.update(this.dietaAEditar.id, this.dietaEditada)
      .subscribe(dietaEditada => {
        this.actualizarComidasDieta(dietaEditada);
        this._toasterService.pop('success', 'Éxito', 'La dieta se ha actualizado');
        this.irListarDietas();
        this._cargandoService.deshabiltarCargando();
      },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al editar la dieta')
        this._cargandoService.deshabiltarCargando();
      });
  }

  actualizarComidasDieta(dietaEditada: Dieta) {
    this._dietaService.findOne(dietaEditada.id)
      .subscribe(dieta => {
        this.arregloComidasDietasAGuardadas = dieta.comidasDieta;
        this.arregloComidasDietasAGuardadas.forEach(comidaGuardada => {
          const estaComidaGuardadaEnComidasSeleccionadas = this.arregloComidasDietasAEditar.find(
            comidaDietaAEditar => {
              return comidaDietaAEditar.id === comidaGuardada.id;
            }
          );

          if (!estaComidaGuardadaEnComidasSeleccionadas) {
             this._comidaDietaService.delete(comidaGuardada.id).subscribe(
              respuesta=>{
                this._cargandoService.deshabiltarCargando();
              },
              error => {
                this._toasterService.pop('error', 'Error', 'Error al editar la dieta')
                this._cargandoService.deshabiltarCargando();
              }
             );        
          }
        });

        this.arregloComidasDietasAEditar.forEach(comidaDietaAEditar => {
          if (!comidaDietaAEditar.id) {
            const comidaDietaACrear: ComidaDieta = comidaDietaAEditar;
            comidaDietaACrear.dieta = dietaEditada;
            this._comidaDietaService.create(comidaDietaACrear).subscribe(
              respuesta=>{
                this._cargandoService.deshabiltarCargando();
              },
              error => {
                this._toasterService.pop('error', 'Error', 'Error al editar la dieta')
                this._cargandoService.deshabiltarCargando();
              }
            );
          }
          else {
            delete comidaDietaAEditar.idTipo;
            this._comidaDietaService.update(comidaDietaAEditar.id,comidaDietaAEditar).subscribe(
              respuesta=>{
                this._cargandoService.deshabiltarCargando();
              },
              error=>{
                this._toasterService.pop('error','Error','Error al editar rutina');
                this._cargandoService.deshabiltarCargando();
             }
            );
          }
        })

      },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al buscar la dieta')
        this._cargandoService.deshabiltarCargando();
      });

  }

  setearDietaEditada() {
    this.dietaEditada.nombre = this.formularioEditarDieta.get('nombre').value;
    this.dietaEditada.descripcion = this.formularioEditarDieta.get('descripcion').value;
    this.dietaEditada.tipo = this.formularioEditarDieta.get('tipo').value;
    this.arregloComidasDietasAEditar = this.formularioEditarDieta.get('comidasDieta').value

  }

  setearDietaAEditar() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idDieta = Number(respuestaParametros.get('id'));
      this._dietaService.findOne(idDieta).subscribe(
        dieta => {
          this.dietaAEditar = dieta;
          this.arregloComidasDietasAGuardadas = this.dietaAEditar.comidasDieta;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar la dieta')
          this._cargandoService.deshabiltarCargando();
        });
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al buscar la dieta')
      this._cargandoService.deshabiltarCargando();
    });
  }

  irListarDietas() {
    this._router.navigate(['/app', 'dieta', 'listar']);
  }

}
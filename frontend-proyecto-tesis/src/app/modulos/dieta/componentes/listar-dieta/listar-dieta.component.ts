import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
    selector: 'app-listar-dieta',
    templateUrl: './listar-dieta.component.html',
    styleUrls: ['./listar-dieta.component.css'],
    animations: [
        trigger('rowExpansionTrigger', [
            state('void', style({
                transform: 'translateX(-10%)',
                opacity: 0
            })),
            state('active', style({
                transform: 'translateX(0)',
                opacity: 1
            })),
            transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class ListarDietaComponent implements OnInit {
    items: MenuItem[];
    icono= ICONOS.dieta;

    constructor() { }

    ngOnInit() {
        this.items= cargarRutas('dieta','', 'Lista de dietas');
        this.items.pop();

    }
}

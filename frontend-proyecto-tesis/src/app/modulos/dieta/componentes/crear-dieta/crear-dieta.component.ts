import { Component, OnInit } from '@angular/core';
import { Dieta } from '../../../../interfaces/dieta.interface';
import { FormGroup } from '@angular/forms';
import { DietaService } from '../../servicios/dieta.service';
import { ComidaDietaService } from '../../servicios/comida-dieta.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { ComidaDieta } from '../../../../interfaces/comidas-dieta.ineterface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-dieta',
  templateUrl: './crear-dieta.component.html',
  styleUrls: ['./crear-dieta.component.css']
})
export class CrearDietaComponent implements OnInit {
  dietaACrear: Dieta = {};
  arregloComidasDietaSeleccionadas: ComidaDieta[];
  esFormularioDietaValido = false;
  existeNombre=false;
  formularioCrearDieta: FormGroup;
  items: MenuItem[];
  icono= ICONOS.dieta;

  constructor(
    private readonly _dietaService: DietaService,
    private readonly _comidaDietaService: ComidaDietaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.items= cargarRutas('dieta','Registrar', 'Lista de dietas');
  }

  escucharFormularioDieta(eventoFormularioDieta) {
    this.formularioCrearDieta = eventoFormularioDieta;
    if(eventoFormularioDieta === false){
      this.esFormularioDietaValido=eventoFormularioDieta;
    }
    else {
      this.esFormularioDietaValido=eventoFormularioDieta.valid;
      this.dietaACrear = eventoFormularioDieta.value;
    }
  }

  crearDieta() {
    this._cargandoService.habilitarCargando();
    this.setearDietaACrear();
    this._dietaService.create(this.dietaACrear)
    .subscribe(dietaCreada=>{
      const arregloComidasDieta: ComidaDieta[]= this.arregloComidasDietaSeleccionadas
      .map(comidaSeleccionada=>{
        comidaSeleccionada.dieta=dietaCreada;
        return comidaSeleccionada
      });
      this._comidaDietaService.createMany(arregloComidasDieta)
      .subscribe(comidasDietaCreadas=>{
        this._toasterService.pop('success', 'Éxito', 'La dieta se ha creado');
        this.irListarDietas();
        this._cargandoService.deshabiltarCargando();
      },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al crear la dieta')
        this._cargandoService.deshabiltarCargando();
      }
    );
    });
  }

  setearDietaACrear() {
    this.dietaACrear.nombre=this.formularioCrearDieta.get('nombre').value;
    this.dietaACrear.descripcion=this.formularioCrearDieta.get('descripcion').value;
    this.dietaACrear.tipo=this.formularioCrearDieta.get('tipo').value;
    this.arregloComidasDietaSeleccionadas=this.formularioCrearDieta.get('comidasDieta').value;

   }

  irListarDietas() {
    this._router.navigate(['/app', 'dieta', 'listar']);
  }

}
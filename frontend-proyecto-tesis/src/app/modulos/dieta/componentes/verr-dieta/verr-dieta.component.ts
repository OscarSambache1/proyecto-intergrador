import { Component, OnInit } from '@angular/core';
import { Dieta } from '../../../../interfaces/dieta.interface';
import { DietaService } from '../../servicios/dieta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-verr-dieta',
  templateUrl: './verr-dieta.component.html',
  styleUrls: ['./verr-dieta.component.css']
})
export class VerrDietaComponent implements OnInit {
  dietaAVer: Dieta;
  items: MenuItem[];
  icono= ICONOS.dieta;

  constructor(
    private readonly _dietaService: DietaService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.setearDietaAVer();
    this.items= cargarRutas('dieta','Ver', 'Lista de dietas');

  }

  setearDietaAVer() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idDieta = Number(respuestaParametros.get('id'));
      this._dietaService.findOne(idDieta).subscribe(
        dieta => {
          this.dietaAVer = dieta;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar la dieta')
          this._cargandoService.deshabiltarCargando();
        });
    },
    error => {
      this._toasterService.pop('error', 'Error', 'Error al buscar la dieta')
      this._cargandoService.deshabiltarCargando();
    });
  }

  irListarDietas() {
    this._router.navigate(['/app', 'dieta', 'listar']);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DietaRoutingModule } from './dieta-routing.module';
import { ListarDietaComponent } from './componentes/listar-dieta/listar-dieta.component';
import { CrearDietaComponent } from './componentes/crear-dieta/crear-dieta.component';
import { EditarDietaComponent } from './componentes/editar-dieta/editar-dieta.component';
import { VerrDietaComponent } from './componentes/verr-dieta/verr-dieta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { DietaService } from './servicios/dieta.service';
import { TipoDietaService } from './servicios/tipo-dieta.service';
import { ComidaDietaService } from './servicios/comida-dieta.service';
import { FormularioDietaComponent } from './formularios/formulario-dieta/formulario-dieta.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ComidaModule } from '../comida/comida.module';
import { ConfirmationService } from 'primeng/api';
import { ModalVerDietaComponent } from './modales/modal-ver-dieta/modal-ver-dieta.component';
import {InputMaskModule} from 'primeng/inputmask';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TablaDietasModule } from '../../componentes/tabla-dietas/tabla-dietas.module';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import { TablaComidasModule } from '../../componentes/tabla-comidas/tabla-comidas.module';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TablaDietasModule,
    TableModule,
    DropdownModule,
    InputTextareaModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,    
    DietaRoutingModule,
    ConfirmDialogModule,
    ComidaModule,
    InputMaskModule,
    NgbModule.forRoot(),
    PanelModule,
    InputTextModule,
    TablaComidasModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [
    ListarDietaComponent, 
    CrearDietaComponent, 
    EditarDietaComponent, 
    VerrDietaComponent, 
    FormularioDietaComponent, 
    ModalVerDietaComponent, 
  ],
  providers:[
    DietaService,
    TipoDietaService,
    ComidaDietaService,
    ConfirmationService
  ],
  entryComponents:[
    ModalVerDietaComponent,
  ]
})
export class DietaModule { }

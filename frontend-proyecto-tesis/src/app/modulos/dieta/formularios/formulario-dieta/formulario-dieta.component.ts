import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Dieta } from '../../../../interfaces/dieta.interface';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { TipoDietaService } from '../../servicios/tipo-dieta.service';
import { MatDialog } from '@angular/material';
import { TipoDieta } from '../../../../interfaces/tipo-dieta.interface';
import { ToasterService } from 'angular2-toaster';
import { ComidaDieta } from '../../../../interfaces/comidas-dieta.ineterface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { debounceTime, map } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { ModalComidaDietaComponent } from '../../../../componentes/tabla-comidas/modales/modal-comida-dieta/modal-comida-dieta.component';
import { DietaService } from '../../servicios/dieta.service';

@Component({
  selector: 'app-formulario-dieta',
  templateUrl: './formulario-dieta.component.html',
  styleUrls: ['./formulario-dieta.component.css']
})
export class FormularioDietaComponent implements OnInit {

  @Output() esValidoFormularioDieta: EventEmitter<FormGroup | boolean> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() dietaSeleccionada: Dieta;
  @Input() arregloComidasSeleccionadas: ComidaDieta[] = [];

  formularioDieta: FormGroup;
  arregloTipoDietas: SelectItem[] = [];
  tipoDietaSeleccionado: TipoDieta = {};
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresTipo = [];

  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido',
    validacionAsincronaDieta: 'El nombre ya exite'
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };

  private mensajesValidacionTipo = {
    required: 'EL tipo es obligatorío'
  };


  constructor(
    private readonly _dietaService: DietaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _tipoDietaService: TipoDietaService,
    private readonly _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private readonly _confirmationService: ConfirmationService,

  ) { }

  ngOnInit() {
    this.llenarDropdownTiposDieta();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  llenarDropdownTiposDieta() {
    this._cargandoService.habilitarCargando();
    const consulta = { order: { id: 'DESC' } };
    this.arregloTipoDietas.push(
      {
        label: "seleccione un tipo",
        value: null
      }
    );
    this._tipoDietaService.findAll(consulta).subscribe(tiposDietas => {
      tiposDietas.forEach(tipo => {
        const elemento = {
          label: tipo.nombre,
          value: tipo
        }
        this.arregloTipoDietas.push(elemento);
        this._cargandoService.deshabiltarCargando();
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al mostrar los tipos')
        this._cargandoService.deshabiltarCargando();
      });
  }

  crearFormulario() {
    this.formularioDieta = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)],
      ],
      descripcion: ['', []],
      tipo: ['', [Validators.required], []],
      comidasDieta: [[], [Validators.required]]
    });
  }

  escucharFormulario() {
    this.formularioDieta.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresNombre = setearMensajes(this.formularioDieta.get('nombre'), this.mensajesValidacionNombre);
        this.mensajesErroresTipo = setearMensajes(this.formularioDieta.get('tipo'), this.mensajesValidacionTipo);
      });
  }

  enviarFormularioValido() {
    this.formularioDieta.valueChanges.subscribe(async formulario => {
      const esNombreIncorrecto = await this.validarNombreRepetido();
      if (esNombreIncorrecto) {
        this.mensajesErroresNombre = [];
        this.mensajesErroresNombre.push(this.mensajesValidacionNombre['validacionAsincronaDieta']);
      }
      if (!esNombreIncorrecto && this.formularioDieta.valid) {
          this.esValidoFormularioDieta.emit(this.formularioDieta);
        }
        else {
          this.esValidoFormularioDieta.emit(false);
        }
      
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioDieta.disable();
    }
  }

  llenarFormulario() {
    this._tipoDietaService.findOne(this.dietaSeleccionada.tipo.id)
      .subscribe(tipo => {
        const comidasDieta = this.dietaSeleccionada.comidasDieta;
        this.arregloComidasSeleccionadas = comidasDieta.map(comidaDieta => {
          comidaDieta.idTipo = comidaDieta.comida.tipo.id;
          return comidaDieta;
        });
        this.formularioDieta.patchValue(
          {
            nombre: this.dietaSeleccionada.nombre,
            descripcion: this.dietaSeleccionada.descripcion,
            tipo,
            comidasDieta
          }
        );
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar la dieta')
          this._cargandoService.deshabiltarCargando();
        });
  }

  quitarComida(comidaDieta: ComidaDieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloComidasSeleccionadas.indexOf(comidaDieta);
    this.arregloComidasSeleccionadas.splice(indice, 1);
    this.formularioDieta.patchValue({ comidasDieta: this.arregloComidasSeleccionadas });
    this._cargandoService.deshabiltarCargando();
  }

  confirmar(comidaDieta: ComidaDieta) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar esta comida?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarComida(comidaDieta)
      },
      reject: () => {
      }
    });
  }

  abrirModalEditarComidaDieta(comidaDieta: ComidaDieta) {
    const indice = this.arregloComidasSeleccionadas.indexOf(comidaDieta);
    const dialogRef = this.dialog.open(ModalComidaDietaComponent, {
      height: '400px',
      width: '500px',
      data: { comidaDieta }
    });
    dialogRef.afterClosed().subscribe(comidaDietaEditada => {
      if (comidaDietaEditada) {
        this.arregloComidasSeleccionadas[indice] = comidaDietaEditada;
        this.arregloComidasSeleccionadas = this.arregloComidasSeleccionadas.map(comidaDieta => {
          comidaDieta.idTipo = comidaDieta.comida.tipo.id;
          return comidaDieta;
        });
        this.formularioDieta.patchValue({ comidasDieta: this.arregloComidasSeleccionadas });
      }
    });
  }

  escucharComidaDietaSeleccionado(eventoComidaDieta) {
    if (eventoComidaDieta) {
      this.arregloComidasSeleccionadas.push(eventoComidaDieta);
        this.arregloComidasSeleccionadas = this.arregloComidasSeleccionadas.map(comidaDieta => {
          comidaDieta.idTipo = comidaDieta.comida.tipo.id;
          return comidaDieta;
        });
      this.formularioDieta.patchValue({ comidasDieta: this.arregloComidasSeleccionadas });
    }
  }

  async  validarNombreRepetido(): Promise<Boolean> {
    const nombre = this.formularioDieta.get('nombre').value;
    const consulta =
    {
      order: { id: 'DESC' },
      where: {
        nombre
      }
    }
    const promesaFechaNombres: any = this._dietaService.findAll(consulta).toPromise();
    const respuestaPromesa = await promesaFechaNombres;
    const existeNombre: boolean = respuestaPromesa.length !== 0;

    if (this.dietaSeleccionada) {
        if(existeNombre){
          if(this.dietaSeleccionada.nombre === this.formularioDieta.get('nombre').value.trim()){
            return false;
          }
          else {
            return true;
          }
        }
    }
    else {
      return existeNombre;
    }
  }
}
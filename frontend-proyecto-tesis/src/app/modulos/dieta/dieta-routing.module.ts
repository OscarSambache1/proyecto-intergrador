import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarDietaComponent } from './componentes/listar-dieta/listar-dieta.component';
import { CrearDietaComponent } from './componentes/crear-dieta/crear-dieta.component';
import { EditarDietaComponent } from './componentes/editar-dieta/editar-dieta.component';
import { VerrDietaComponent } from './componentes/verr-dieta/verr-dieta.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: 'crear',
    component : CrearDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'editar/:id',
    component: EditarDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component: VerrDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DietaRoutingModule { }

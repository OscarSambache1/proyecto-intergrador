import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Dia } from '../../../../interfaces/dia.interface';
import { InstructorService } from '../../../instructor/instructor.service';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { DiaService } from '../../../../servicios/dia.service';
import { ClaseHora } from '../../../../interfaces/clase-hora.interface';

@Component({
  selector: 'app-formulario-clase-hora-dia',
  templateUrl: './formulario-clase-hora-dia.component.html',
  styleUrls: ['./formulario-clase-hora-dia.component.css']
})
export class FormularioClaseHoraDiaComponent implements OnInit {

  @Output() esValidoFormularioClaseHoraDia: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() claseHoraDiaSeleccionada: ClaseHora;
  formularioClaseHoraDia: FormGroup;
  arregloDias: SelectItem[] = [];
  diasSeleccionados: Dia[];
  etiquetaPorDefectoDropdown: string;
  arregloInstructores: SelectItem[] = [];
  mensajesErroresHoraInicio = [];
  mensajesErroresHoraFin= [];
  mensajesErroresDias= [];
  mensajeErroresInstructor = [];

  private mensajesValidacionHoraInicio = {
    required: "La hora de inicio es obligatoria",
  };

  private mensajesValidacionHoraFin = {
    required: "La hora de fin es obligatoria",
  };

  private mensajesValidacionDias = {
    required: 'Seleccion al menos un día'
  };
  
  private mensajesValidacionInstructor = {
    required: "El instructor es obligatorio",
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _instructorService: InstructorService,
    private readonly _diaService: DiaService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.llenarDropdownDias();
    this.llenarDropdownInstructores();
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  llenarDropdownDias() {
    const consulta = { order: { id: 'ASC' }};
    this._diaService.findAll(consulta).subscribe(dias => {
      dias.forEach(dia => {
        const elemento = {
          label: dia.nombre,
          value: dia
        }
        this.arregloDias.push(elemento)
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos')
      }
    );
  }

  llenarDropdownInstructores() {
    const consulta =
    {
      order: { id: 'DESC' }, 
      where: {
        estado: 1,
      },
      relations: ['usuario']
    };
    this._instructorService.findAll(consulta).subscribe(instructores => {
      this.arregloInstructores.push(
        {
          label: "seleccione un instructor",
          value: null
        }
      );
      instructores.forEach(instructor => {
        const elemento = {
          label: instructor.usuario.nombres  +' '+ instructor.usuario.apellidos,
          value: instructor
        }
        this.arregloInstructores.push(elemento);
      });
    },
    error=>{
      this._toasterService.pop('error','Error','Error al mostrar los instructores');
    }
  );
  }

  crearFormulario() {
    this.formularioClaseHoraDia = this._formBuilder.group({
      horaFin: ['', [Validators.required]],
      horaInicio: ['', [Validators.required]],
      dias: ['', [Validators.required]],
      instructor: ['', [Validators.required]],
    })
  }

  escucharFormulario() {
    this.formularioClaseHoraDia.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresHoraInicio = setearMensajes(this.formularioClaseHoraDia.get('horaInicio'), this.mensajesValidacionHoraInicio);
        this.mensajesErroresHoraFin = setearMensajes(this.formularioClaseHoraDia.get('horaFin'), this.mensajesValidacionHoraFin);
        this.mensajesErroresDias = setearMensajes(this.formularioClaseHoraDia.get('dias'), this.mensajesValidacionDias);
        this.mensajeErroresInstructor = setearMensajes(this.formularioClaseHoraDia.get('instructor'), this.mensajesValidacionInstructor);
      });
  }

  enviarFormularioValido() {
    this.formularioClaseHoraDia.valueChanges.subscribe(formulario => {
      this.diasSeleccionados=this.formularioClaseHoraDia.get('dias').value;
      this.esValidoFormularioClaseHoraDia.emit(this.formularioClaseHoraDia);
    });
  }

  verificarTipoFormulario() {
    if (this.claseHoraDiaSeleccionada) {
      this.llenarFormulario();
    }
    else {
      this.setearEtiquetaDropdown();
    }
  }

  llenarFormulario() {
    this._cargandoService.habilitarCargando();
      this._instructorService.findOne(this.claseHoraDiaSeleccionada.instructor.id)
      .subscribe( instructor => {
        const dias = this.claseHoraDiaSeleccionada.clasesDiaHora
        .map(claseDiaHora => {
          return claseDiaHora.dia;
        });
        this.formularioClaseHoraDia.patchValue(
          {
            horaFin: this.claseHoraDiaSeleccionada.horaFin,
            horaInicio: this.claseHoraDiaSeleccionada.horaInicio,
            instructor,
            dias
          }
        )
        this.diasSeleccionados = dias;
        this.setearEtiquetaDropdown();
        this._cargandoService.deshabiltarCargando();
      },
    error => {
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error', 'Error al mostrar datos');
    })
  }

  setearEtiquetaDropdown() {
    this.etiquetaPorDefectoDropdown = "";
    if (this.diasSeleccionados) {
      this.diasSeleccionados
        .forEach((musculo, indice) => {
          if (indice == 0) {
            this.etiquetaPorDefectoDropdown = musculo.nombre;
          }
          else {
            this.etiquetaPorDefectoDropdown = this.etiquetaPorDefectoDropdown + ', ' + musculo.nombre;
          }
        });
    }
    else {
      this.etiquetaPorDefectoDropdown = "Seleccione un dia";
    }
  }
}

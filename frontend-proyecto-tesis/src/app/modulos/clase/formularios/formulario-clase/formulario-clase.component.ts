import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Clase } from '../../../../interfaces/clase.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { ClaseHora } from '../../../../interfaces/clase-hora.interface';
import { ModalClaseHoraDiaComponent } from '../../modales/modal-clase-hora-dia/modal-clase-hora-dia.component';

@Component({
  selector: 'app-formulario-clase',
  templateUrl: './formulario-clase.component.html',
  styleUrls: ['./formulario-clase.component.css']
})
export class FormularioClaseComponent implements OnInit {
  @Output() esValidoFormularioClase: EventEmitter<FormGroup | boolean> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() claseSeleccionada: Clase;
  @Input() arregloClasesHoraSeleccionadas: ClaseHora[] = [];
  formularioClase: FormGroup;
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresEstado = [];
  estados: SelectItem[] = [];

  columnas = [
    { field: 'dias', header: 'dias', width: '20%' },
    { field: 'horaFin', header: 'Hora Fin', width: '10%' },
    { field: 'horaInicio', header: 'Hora Inicio', width: '10%' },
    { field: 'instructor', header: 'Instructor', width: '40%' },
    { field: 'acciones', header: 'Acciones', width: '20%' },
  ];

  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido',
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };

  private mensajesValidacionEstado = {
    required: "El estadp es obligatorio",
  };

  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private readonly _confirmationService: ConfirmationService,
  ) { }

  ngOnInit() {
    console.log(this.esFormularioCrear)
    this.llenarDropdownTiposComida();
    if (this.esFormularioVer) {
      this.columnas.pop();
    }
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
  }

  crearFormulario() {
    this.formularioClase = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)],
      ],
      descripcion: ['', []],
      estado: ['', [Validators.required]],
      clasesHora: [[], [Validators.required]]
    });
  }

  escucharFormulario() {
    this.formularioClase.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresNombre = setearMensajes(this.formularioClase.get('nombre'), this.mensajesValidacionNombre);
        this.mensajesErroresDescripcion = setearMensajes(this.formularioClase.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresEstado = setearMensajes(this.formularioClase.get('estado'), this.mensajesValidacionEstado);

      });
  }

  enviarFormularioValido() {
    this.formularioClase.valueChanges.subscribe(async formulario => {
      this.esValidoFormularioClase.emit(this.formularioClase);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
      this.formularioClase.patchValue({ estado: 1 });
    }
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioClase.disable();
    }
  }

  llenarFormulario() {
    this.arregloClasesHoraSeleccionadas = this.claseSeleccionada.clasesHora;
    this.formularioClase.patchValue({
      nombre: this.claseSeleccionada.nombre,
      descripcion: this.claseSeleccionada.descripcion,
      estado: this.claseSeleccionada.estado,
      clasesHora: this.claseSeleccionada.clasesHora
    });
    this.setearNombresDiasSeleccionados();
  }

  llenarDropdownTiposComida() {
      this.estados = [
        {
          label: "seleccione un tipo",
          value: null
        },
        {
          label: 'activo',
          value: 1
        },
        {
          label: 'inactivo',
          value: 0
        }
      ]
  }


  setearNombresDiasSeleccionados(){
    this.arregloClasesHoraSeleccionadas.map(claseHora => {
      const claseDiasSeleccionados = claseHora.clasesDiaHora;
      if (claseDiasSeleccionados) {
        claseDiasSeleccionados
          .forEach((claseDiaHora, indice) => {
            if (indice == 0) {
              claseHora.nombresDias = claseDiaHora.dia.nombre;
            }
            else {
              claseHora.nombresDias = claseHora.nombresDias + ', ' + claseDiaHora.dia.nombre;
            }
          });
      }
      else {
        claseHora.nombresDias = '';
      }
      return claseHora;
    });
  }

  abrirModalClaseHoraDia() {
    const dialogRef = this.dialog.open(ModalClaseHoraDiaComponent, {
      height: 'auto',
      width: 'auto',
      data: {}
    });
    dialogRef.afterClosed().subscribe(claseHora => {
      if (claseHora) {
        this.arregloClasesHoraSeleccionadas.push(claseHora);
        this.setearNombresDiasSeleccionados();
        this.formularioClase.patchValue({ clasesHora: this.arregloClasesHoraSeleccionadas });
      }
    });
  }

  abrirModalEditarClaseHoraDia(claseHora: ClaseHora) {
    const indice = this.arregloClasesHoraSeleccionadas.indexOf(claseHora);
    const dialogRef = this.dialog.open(ModalClaseHoraDiaComponent, {
      height: 'auto',
      width: 'auto',
      data: { claseHora }
    });
    dialogRef.afterClosed().subscribe(claseHora => {
      if (claseHora) {
        this.arregloClasesHoraSeleccionadas[indice] = claseHora;
        this.setearNombresDiasSeleccionados();
        this.formularioClase.patchValue({ clasesHora: this.arregloClasesHoraSeleccionadas });
      }
    });
  }

  confirmar(claseHora: ClaseHora) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere eliminar este registro?',
      header: 'Confirmación de eliminación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.eliminarClaseHora(claseHora)
      },
      reject: () => {
      }
    });
  }

  eliminarClaseHora(claseHora: ClaseHora) {
    this._cargandoService.habilitarCargando();
    this._toasterService.pop('success', 'Éxito', 'El registro se ha eliminado correctamente');
    const indice = this.arregloClasesHoraSeleccionadas.indexOf(claseHora);
    this.arregloClasesHoraSeleccionadas.splice(indice, 1);
    this.formularioClase.patchValue({ clasesHora: this.arregloClasesHoraSeleccionadas });
    this._cargandoService.deshabiltarCargando();
  }
}

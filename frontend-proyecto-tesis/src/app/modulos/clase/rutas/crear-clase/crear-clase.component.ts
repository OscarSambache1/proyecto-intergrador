import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { FormGroup } from '@angular/forms';
import { ClaseHora } from '../../../../interfaces/clase-hora.interface';
import { ClaseDiaHora } from '../../../../interfaces/clase-dia-hora.interface';
import { Clase } from '../../../../interfaces/clase.interface';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ClaseService } from '../../servicios/clase.service';
import { ClaseHoraService } from '../../servicios/clase-hora.service';
import { ClaseHoraDiaService } from '../../servicios/clase-hora-dia.service';

@Component({
  selector: 'app-crear-clase',
  templateUrl: './crear-clase.component.html',
  styleUrls: ['./crear-clase.component.css']
})
export class CrearClaseComponent implements OnInit {

  items: MenuItem[];
  icono = ICONOS.clase;
  esFormularioClaseValido = false;
  claseACrear: Clase = {};
  arregloClasesHoras: ClaseHora[] = [];
  formularioCrearClase: FormGroup;
  constructor(
    private readonly _claseService: ClaseService,
    private readonly _claseHoraService: ClaseHoraService,
    private readonly _claseHoraDiaService: ClaseHoraDiaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.items = cargarRutas('clase', 'Registrar', 'Lista de clases');
  }

  escucharFormularioClase(eventoFormularioClase: FormGroup) {
    this.esFormularioClaseValido = eventoFormularioClase.valid;
    if (this.esFormularioClaseValido) {
      this.formularioCrearClase = eventoFormularioClase;
    }
  }

  crearClase() {
    this._cargandoService.habilitarCargando();
    this.setarClase();
    this._claseService.create(this.claseACrear)
      .subscribe(
        claseCreada => {
          this.arregloClasesHoras.forEach(
            (claseHora) => {
              const claseHoraACrear: ClaseHora={};
              claseHoraACrear.horaInicio = claseHora.horaInicio;
              claseHoraACrear.horaFin = claseHora.horaFin;
              claseHoraACrear.instructor = claseHora.instructor;
              claseHoraACrear.clase = claseCreada;
              this._claseHoraService.create(claseHoraACrear)
                .subscribe(claseHoraCreada => {
                  const clasesHoraDiaACrear = claseHora.clasesDiaHora.map(claseHoraDia => {
                    claseHoraDia.claseHora = claseHoraCreada;
                    return claseHoraDia;
                  }
                  );
                  this._claseHoraDiaService.createMany(clasesHoraDiaACrear)
                  .subscribe(clasesHoraDiaCreadas => {
                    this.irListarClases();
                    this._cargandoService.deshabiltarCargando();
                  },
                    error => {
                      this._cargandoService.deshabiltarCargando();
                      this._toasterService.pop('error', 'Error', 'Error al crear la claseHoraDia');
                    });
                },
                  error => {
                    this._cargandoService.deshabiltarCargando();
                    this._toasterService.pop('error', 'Error', 'Error al crear la claseHora');
                  }
                );
            }
          )
          this._toasterService.pop('success', 'Éxito', 'La clase se ha creado correctamente');
        },
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al crear la clase');
        }
      );
  }

  setarClase() {
    this.claseACrear.nombre = this.formularioCrearClase.get('nombre').value;
    this.claseACrear.descripcion = this.formularioCrearClase.get('descripcion').value;
    this.arregloClasesHoras = this.formularioCrearClase.get('clasesHora').value;
  }

  irListarClases() {
    this._router.navigate(['/app', 'clase', 'listar']);
  }
}

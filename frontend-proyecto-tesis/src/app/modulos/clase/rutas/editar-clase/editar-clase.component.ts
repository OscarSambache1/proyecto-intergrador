import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ClaseService } from '../../servicios/clase.service';
import { Clase } from '../../../../interfaces/clase.interface';
import { ClaseHora } from '../../../../interfaces/clase-hora.interface';
import { FormGroup } from '@angular/forms';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ClaseHoraService } from '../../servicios/clase-hora.service';
import { ClaseHoraDiaService } from '../../servicios/clase-hora-dia.service';
import { ClaseDiaHora } from 'src/app/interfaces/clase-dia-hora.interface';

@Component({
  selector: 'app-editar-clase',
  templateUrl: './editar-clase.component.html',
  styleUrls: ['./editar-clase.component.css']
})
export class EditarClaseComponent implements OnInit {

  claseAEditar: Clase;
  claseEditada: Clase = {};
  esFormularioClaseValido = false;
  formularioEditarClase: FormGroup;
  items: MenuItem[];
  icono = ICONOS.clase;
  arregloClasesHora: ClaseHora[] = [];

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _claseService: ClaseService,
    private readonly _claseHoraService: ClaseHoraService,
    private readonly _claseHoraDiaService: ClaseHoraDiaService
  ) { }

  ngOnInit() {
    this.items = cargarRutas('clase', 'Editar', 'Lista de clases');
    this.setearClaseAEditar();
  }


  setearClaseEditada() {
    this.claseEditada.nombre = this.formularioEditarClase.get('nombre').value;
    this.claseEditada.descripcion = this.formularioEditarClase.get('descripcion').value;
    this.claseEditada.estado = this.formularioEditarClase.get('estado').value;
    this.arregloClasesHora = this.formularioEditarClase.get('clasesHora').value;
  }

  escucharFormularioClase(eventoFormularioClase: FormGroup) {
    this.esFormularioClaseValido = eventoFormularioClase.valid;
    if (this.esFormularioClaseValido) {
      this.formularioEditarClase = eventoFormularioClase;
    }
  }

  editarClase() {
    this._cargandoService.habilitarCargando();
    this.setearClaseEditada();
    this._claseService.update(this.claseAEditar.id, this.claseEditada)
      .subscribe(claseEditada => {
        this.actualizarClasesHora(claseEditada);
        this._toasterService.pop('success', 'Éxito', 'La clase se ha actualizado');
        this.irListarClases();
      }
        , error => {
          this._toasterService.pop('error', 'Error', 'Error al editar la clase')
          this._cargandoService.deshabiltarCargando();
        });
  }

  actualizarClasesHora(claseEditada: Clase) {
    const clasesHoraGuardadas: ClaseHora[] = claseEditada.clasesHora;
    const clasesHoraSeleccionadas = this.arregloClasesHora;

    clasesHoraGuardadas.forEach(claseHoraGuardada => {
      const estaClaseHoraEnClasesHoraSeleccionadas = clasesHoraSeleccionadas.find(
        claseHoraSeleccionada => {
          return claseHoraSeleccionada.id === claseHoraGuardada.id
        }
      );

      if (!estaClaseHoraEnClasesHoraSeleccionadas) {
        if (claseHoraGuardada.clasesDiaHora) {
          claseHoraGuardada.clasesDiaHora.forEach(claseHoraDia => {
            this._claseHoraDiaService.delete(claseHoraDia.id)
              .subscribe(respuesta => { }
              ),
              error => {
                this._toasterService.pop('error', 'Error', 'Error al eliminar la claseHora')
                this._cargandoService.deshabiltarCargando();
              }
          });
        }

        this._claseHoraService.delete(claseHoraGuardada.id)
          .subscribe(
            respuesta => {
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error al eliminar la claseHoraDia')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
    });

    clasesHoraSeleccionadas.forEach(claseHoraSeleccionada => {
      if (!claseHoraSeleccionada.id) {
        const claseHoraACrear: ClaseHora = {};
        claseHoraACrear.horaInicio = claseHoraSeleccionada.horaInicio;
        claseHoraACrear.horaFin = claseHoraSeleccionada.horaFin;
        claseHoraACrear.instructor = claseHoraSeleccionada.instructor;
        claseHoraACrear.clase = claseHoraSeleccionada;
        claseHoraACrear.clase = claseEditada;
        this._claseHoraService.create(claseHoraACrear)
          .subscribe(
            claseHoraCreada => {
              const clasesHoraDiaACrear = claseHoraSeleccionada.clasesDiaHora.map(claseHoraDia => {
                claseHoraDia.claseHora = claseHoraCreada;
                return claseHoraDia;
              }
              );
              this._claseHoraDiaService.createMany(clasesHoraDiaACrear)
                .subscribe(clasesHoraDiaCreadas => {
                  this._cargandoService.deshabiltarCargando();
                },
                  error => {
                    this._cargandoService.deshabiltarCargando();
                    this._toasterService.pop('error', 'Error', 'Error al crear la claseHoraDia');
                  });
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error al crear la claseHora')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
      else {
        const clasesHoraDiaSeleccionadas = claseHoraSeleccionada.clasesDiaHora;
        delete claseHoraSeleccionada.clasesDiaHora
        delete claseHoraSeleccionada.nombresDias
        this._claseHoraService.update(claseHoraSeleccionada.id, claseHoraSeleccionada)
          .subscribe(
            claseHoraEditada => {
              this.actualizarClasesHoraDia(claseHoraEditada, clasesHoraDiaSeleccionadas);
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error editar la claseHora')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
    })
  }

  actualizarClasesHoraDia(claseHoraEditada: ClaseHora, clasesHoraDiaSeleccionadas:ClaseDiaHora[]) {
    const clasesHoraDiaGuardadas: ClaseDiaHora[] = claseHoraEditada.clasesDiaHora;
    clasesHoraDiaGuardadas.forEach(claseHoraDiaGuardada => {
      const estaClaseHoraDiaEnClasesHoraDiaSeleccionadas = clasesHoraDiaSeleccionadas.find(
        claseHoraDiaSeleccionada => {
          return claseHoraDiaSeleccionada.id === claseHoraDiaGuardada.id
        }
      );

      if (!estaClaseHoraDiaEnClasesHoraDiaSeleccionadas) {
        // console.log('delete caseDiaHora', claseHoraDiaGuardada)
        this._claseHoraDiaService.delete(claseHoraDiaGuardada.id)
          .subscribe(
            respuesta => {
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error al eliminar la claseHoraDia')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
    });

    clasesHoraDiaSeleccionadas.forEach(claseHoraDiaSeleccionada => {
      if (!claseHoraDiaSeleccionada.id) {
        const claseHoraDiaACrear: ClaseDiaHora = {};
        claseHoraDiaACrear.claseHora = claseHoraDiaSeleccionada.claseHora;
        claseHoraDiaACrear.dia = claseHoraDiaSeleccionada.dia;
        // console.log('crear caseDiaHora', claseHoraDiaACrear)
        this._claseHoraDiaService.create(claseHoraDiaACrear)
          .subscribe(
            claseHoraDiaCreada => {
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error al crear la claseHoraDia')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
      else {
        // console.log('update caseDiaHora', claseHoraDiaSeleccionada)
        this._claseHoraDiaService.update(claseHoraDiaSeleccionada.id, claseHoraDiaSeleccionada)
          .subscribe(
            claseHoraEditada => {
              this._cargandoService.deshabiltarCargando();
            },
            error => {
              this._toasterService.pop('error', 'Error', 'Error editar la claseHoraDia')
              this._cargandoService.deshabiltarCargando();
            }
          );
      }
    })
  }

  setearClaseAEditar() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idClase = Number(respuestaParametros.get('id'));
      this._claseService.findOne(idClase).subscribe(
        clase => {
          this.claseAEditar = clase;
          this.arregloClasesHora = clase.clasesHora;
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostar clase')
          this._cargandoService.deshabiltarCargando();
        }
      );
    });
  }

  irListarClases() {
    this._router.navigate(['/app', 'clase', 'listar']);
  }
}

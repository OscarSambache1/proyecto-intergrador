import { Component, OnInit } from '@angular/core';
import { Clase } from '../../../../interfaces/clase.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ClaseService } from '../../servicios/clase.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';

@Component({
  selector: 'app-ver-clase',
  templateUrl: './ver-clase.component.html',
  styleUrls: ['./ver-clase.component.css']
})
export class VerClaseComponent implements OnInit {

  claseAVer: Clase;
  items: MenuItem[];
  icono = ICONOS.clase;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _claseService: ClaseService,
  ) { }

  ngOnInit() {
    this.items = cargarRutas('clase', 'Ver', 'Lista de clases');
    this.setearClaseAVer();
  }

  setearClaseAVer() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idClase = Number(respuestaParametros.get('id'));
      this._claseService.findOne(idClase).subscribe(
        clase  => {
          this.claseAVer = clase;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar clase')
          this._cargandoService.deshabiltarCargando();
        }
      );
    });
   }

   irListarClases() {
    this._router.navigate(['/app', 'clase', 'listar']);
  }
}

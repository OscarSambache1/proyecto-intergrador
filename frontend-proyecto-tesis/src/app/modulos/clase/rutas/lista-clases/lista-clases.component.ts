import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { Clase } from '../../../../interfaces/clase.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { ClaseHoraDiaService } from '../../servicios/clase-hora-dia.service';
import { ClaseService } from '../../servicios/clase.service';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-lista-clases',
  templateUrl: './lista-clases.component.html',
  styleUrls: ['./lista-clases.component.css']
})
export class ListaClasesComponent implements OnInit {
  items: MenuItem[];
  icono = ICONOS.clase;
  arregloClases: Clase[] = []
  arregloClasesHora: any[] = [];
  rowGroupMetadata: any;
  parametroBusqueda='';
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  verHorario = false;
  columnas2 = [
    { field: 'dia', header: 'Dia', width: '10%' },
    { field: 'clase', header: 'Clase', width: '35%' },
    { field: 'instructor', header: 'Instructor', width: '35%' },
    { field: 'Acciones', header: 'Acciones', width: '20%' },

  ];
  columnasClases = [
    { field: 'nombre', header: 'Nombre', width: '50%' },
    { field: 'estado', header: 'Estado', width: '20%' },
    { field: 'Acciones', header: 'Acciones', width: '30%' },
  ];
  
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _claseHoraDiaService: ClaseHoraDiaService,
    private readonly _claseService: ClaseService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.items = cargarRutas('clase', 'Listar', 'Lista de clases');
    this.items.pop();
    this.mostrarClases();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }


  buscarClases() {
    if (this.verHorario) {
      this.mostrarCLasePorDia();
    } else {
      this.mostrarClases();
    }
  }

  mostrarClases() {
    this._cargandoService.habilitarCargando();
    const consulta =
    { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._claseService.findAll(consulta)
    .subscribe(clases => {
      this.arregloClases =clases;
      this._cargandoService.deshabiltarCargando();
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos');
        this._cargandoService.deshabiltarCargando();
      }); 
  }

  mostrarCLasePorDia() {
    this._cargandoService.habilitarCargando();
    this._claseHoraDiaService.findWhereOr(this.parametroBusqueda)
    .subscribe(clases => {
      this.arregloClasesHora = clases.map(claseHora => {
        claseHora.nombreDia = claseHora.dia.id;
        return claseHora;
      })
      this.actualizarTabla();
      this._cargandoService.deshabiltarCargando();
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos');
        this._cargandoService.deshabiltarCargando();
      });
  }

  onSort() {
    this.actualizarTabla();
  }

  actualizarTabla() {
    this.rowGroupMetadata = {};
    if (this.arregloClasesHora) {
      for (let i = 0; i < this.arregloClasesHora.length; i++) {
        let rowData = this.arregloClasesHora[i];
        let nombreDia = rowData.nombreDia;
        if (i == 0) {
          this.rowGroupMetadata[nombreDia] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.arregloClasesHora[i - 1];
          let previousRowGroup = previousRowData.nombreDia;
          if (nombreDia === previousRowGroup)
            this.rowGroupMetadata[nombreDia].size++;
          else
            this.rowGroupMetadata[nombreDia] = { index: i, size: 1 };
        }
      }
    }

  }

  cambiarEstado(clase: Clase) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloClases.indexOf(clase);
    const estaActivo = this.arregloClases[indice].estado == 1;
    estaActivo ? this.arregloClases[indice].estado = 0 : 1;
    !estaActivo ? this.arregloClases[indice].estado = 1 : 0;
    this._claseService.update(this.arregloClases[indice].id, { estado: this.arregloClases[indice].estado })
      .subscribe(() => {
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos');
        this._cargandoService.deshabiltarCargando();
      });
  }

  verHorarioOListaClases() {
    this.parametroBusqueda = '';
    this.verHorario = !this.verHorario;
    if (this.verHorario) {
      this.mostrarCLasePorDia();
    } else {
      this.mostrarClases();
    }
  }
}

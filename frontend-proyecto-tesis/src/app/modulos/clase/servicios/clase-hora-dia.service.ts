import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ClaseDiaHora } from '../../../interfaces/clase-dia-hora.interface';

@Injectable({
  providedIn: 'root'
})
export class ClaseHoraDiaService {

  constructor(
    private readonly _httpClient: HttpClient

  ) { }

findAll(consulta:any):Observable<any>{
  return  this._httpClient.get(environment.url+`/clase-dia-hora?consulta=`+`${JSON.stringify(consulta)}`)
}

findOne(id:number): Observable<ClaseDiaHora>{
  return  this._httpClient.get(environment.url+`/clase-dia-hora/${id}`)
}

create(ClaseHoraDia: ClaseDiaHora): Observable<ClaseDiaHora> {
  return this._httpClient.post(environment.url + '/clase-dia-hora', ClaseHoraDia)
}

createMany(ClaseHoraDia: ClaseDiaHora[]): Observable<any>{
  return this._httpClient.post(environment.url + '/clase-dia-hora/crear-varios', ClaseHoraDia)
}

update(id: number, ClaseHoraDia: ClaseDiaHora): Observable<ClaseDiaHora> {
  return this._httpClient.put(environment.url + `/clase-dia-hora/${id}`, ClaseHoraDia)
}

delete(id: number): Observable<any> {
  return this._httpClient.delete(environment.url + `/clase-dia-hora/${id}`)
}

findWhereOr(parametro:string):Observable<any>{
  return  this._httpClient.get(environment.url+`/clase-dia-hora/findWhereOr?expresion=${parametro}`);
}

}


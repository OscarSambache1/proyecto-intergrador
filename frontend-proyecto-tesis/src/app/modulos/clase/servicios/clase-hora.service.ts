import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ClaseHora } from '../../../interfaces/clase-hora.interface';

@Injectable({
  providedIn: 'root'
})
export class ClaseHoraService {
  constructor(
    private readonly _httpClient: HttpClient

  ) { }
  
  findAll(consulta:any):Observable<any>{
    return  this._httpClient.get(environment.url+`/clase-hora?consulta=`+`${JSON.stringify(consulta)}`)
  }
  
  findOne(id:number): Observable<ClaseHora>{
    return  this._httpClient.get(environment.url+`/clase-hora/${id}`)
  }
  
  create(claseHora: ClaseHora): Observable<ClaseHora> {
    return this._httpClient.post(environment.url + '/clase-hora', claseHora)
  }
  
  createMany(claseHora: ClaseHora[]): Observable<any>{
    return this._httpClient.post(environment.url + '/clase-hora/crear-varios', claseHora)
  }
  
  update(id: number, claseHora: ClaseHora): Observable<ClaseHora> {
    return this._httpClient.put(environment.url + `/clase-hora/${id}`, claseHora)
  }
  
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/clase-hora/${id}`)
  }}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Clase } from '../../../interfaces/clase.interface';

@Injectable({
  providedIn: 'root'
})
export class ClaseService {
  constructor(
    private readonly _httpClient: HttpClient

  ) { }

findAll(consulta:any):Observable<any>{
  return  this._httpClient.get(environment.url+`/clase?consulta=`+`${JSON.stringify(consulta)}`)
}

findOne(id:number): Observable<Clase>{
  return  this._httpClient.get(environment.url+`/clase/${id}`)
}

create(clase: Clase): Observable<Clase> {
  return this._httpClient.post(environment.url + '/clase', clase)
}

createMany(clase: Clase[]): Observable<any>{
  return this._httpClient.post(environment.url + '/clase/crear-varios', clase)
}

update(id: number, clase: Clase): Observable<Clase> {
  return this._httpClient.put(environment.url + `/clase/${id}`, clase)
}

delete(id: number): Observable<any> {
  return this._httpClient.delete(environment.url + `/clase/${id}`)
}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaClasesComponent } from './rutas/lista-clases/lista-clases.component';
import { CrearClaseComponent } from './rutas/crear-clase/crear-clase.component';
import { EditarClaseComponent } from './rutas/editar-clase/editar-clase.component';
import { VerClaseComponent } from './rutas/ver-clase/ver-clase.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListaClasesComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearClaseComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'editar/:id',
    component: EditarClaseComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path: 'ver/:id',
    component : VerClaseComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaseRoutingModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ClaseDiaHora } from '../../../../interfaces/clase-dia-hora.interface';
import { ClaseHora } from '../../../../interfaces/clase-hora.interface';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICONOS } from 'src/app/constantes/iconos';

@Component({
  selector: 'app-modal-clase-hora-dia',
  templateUrl: './modal-clase-hora-dia.component.html',
  styleUrls: ['./modal-clase-hora-dia.component.css']
})
export class ModalClaseHoraDiaComponent implements OnInit {
  claseHoraACrearEditar: ClaseHora;
  clasesDiaHora: ClaseDiaHora[] = [];
  esFormularioClaseHoraValido = false;
  formularioCrearClaseHora: FormGroup;
  idClaseHora: number;
  icono;
  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalClaseHoraDiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.icono = ICONOS.clase;
    if(this.data.claseHora){
      this.claseHoraACrearEditar = this.data.claseHora;
      this.idClaseHora=this.data.claseHora.id;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  escucharFormularioClaseHora(eventoFormularioClaseHora: FormGroup) {
    this.formularioCrearClaseHora = eventoFormularioClaseHora;
    this.esFormularioClaseHoraValido = this.formularioCrearClaseHora.valid;
  }

  guardarClaseHoraDia() {
    this.claseHoraACrearEditar = {};
    if(this.idClaseHora){
      this.claseHoraACrearEditar.id=this.idClaseHora;
    }
    this._cargandoService.habilitarCargando();
    this.claseHoraACrearEditar.horaInicio = this.formularioCrearClaseHora.get('horaInicio').value;
    this.claseHoraACrearEditar.horaFin = this.formularioCrearClaseHora.get('horaFin').value;
    this.claseHoraACrearEditar.instructor = this.formularioCrearClaseHora.get('instructor').value;
    this.clasesDiaHora= this.formularioCrearClaseHora.get('dias').value.map(dia => {
      const claseDiaHora: ClaseDiaHora = {};
      claseDiaHora.dia = dia;
      claseDiaHora.claseHora = this.claseHoraACrearEditar;
      return claseDiaHora;
    });
    this.claseHoraACrearEditar.clasesDiaHora = this.clasesDiaHora;
    this.dialogRef.close(this.claseHoraACrearEditar);
    this._cargandoService.deshabiltarCargando();
  }


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaseRoutingModule } from './clase-routing.module';
import { CrearClaseComponent } from './rutas/crear-clase/crear-clase.component';
import { EditarClaseComponent } from './rutas/editar-clase/editar-clase.component';
import { VerClaseComponent } from './rutas/ver-clase/ver-clase.component';
import { ListaClasesComponent } from './rutas/lista-clases/lista-clases.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { PanelModule } from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormularioClaseComponent } from './formularios/formulario-clase/formulario-clase.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { ConfirmationService } from 'primeng/api';
import { ModalClaseHoraDiaComponent } from './modales/modal-clase-hora-dia/modal-clase-hora-dia.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormularioClaseHoraDiaComponent } from './formularios/formulario-clase-hora-dia/formulario-clase-hora-dia.component';
import { DiaService } from '../../servicios/dia.service';

@NgModule({
  imports: [
    CommonModule,
    ClaseRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    InputTextModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    DropdownModule,
    ConfirmDialogModule,
    MultiSelectModule,
    NgbModule.forRoot(),

  ],
  declarations: [
    CrearClaseComponent, 
    EditarClaseComponent, 
    VerClaseComponent, 
    ListaClasesComponent, 
    FormularioClaseComponent, ModalClaseHoraDiaComponent, FormularioClaseHoraDiaComponent
  ],
  providers: [
  ConfirmationService,
  DiaService
  ],
  entryComponents: [
    ModalClaseHoraDiaComponent
  ]
})
export class ClaseModule { }

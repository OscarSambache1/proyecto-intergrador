import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarTipoDietaComponent } from './rutas/listar-tipo-dieta/listar-tipo-dieta.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';
import { CrearTipoDietaComponent } from './rutas/crear-tipo-dieta/crear-tipo-dieta.component';
import { EditarTipoDietaComponent } from './rutas/editar-tipo-dieta/editar-tipo-dieta.component';

const routes: Routes = [
  {
    path:'listar',
    component: ListarTipoDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'crear',
    component: CrearTipoDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path:'editar/:id',
    component: EditarTipoDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoDietaRoutingModule { }

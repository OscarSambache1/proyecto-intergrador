import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TipoDieta } from 'src/app/interfaces/tipo-dieta.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from 'src/app/shared/validaciones';

@Component({
  selector: 'app-formulario-tipo-dieta',
  templateUrl: './formulario-tipo-dieta.component.html',
  styleUrls: ['./formulario-tipo-dieta.component.css']
})
export class FormularioTipoDietaComponent implements OnInit {
  @Output() esValidoFormularioTipoDieta: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() tipoDietaSeleccionada: TipoDieta;
  formularioTipoDieta: FormGroup
  mensajesErroresNombre = [];
  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioTipoDieta = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
    })
  }

  escucharFormulario() {
    this.formularioTipoDieta.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresNombre = setearMensajes(this.formularioTipoDieta.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioTipoDieta.valueChanges.subscribe(formulario => {
      this.esValidoFormularioTipoDieta.emit(this.formularioTipoDieta);
    });
  }


  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioTipoDieta.disable();
    }

  }

  llenarFormulario() {
    this.formularioTipoDieta.patchValue(
      {
        nombre: this.tipoDietaSeleccionada.nombre,
      }
    )
  }
}

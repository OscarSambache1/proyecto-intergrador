import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoDietaRoutingModule } from './tipo-dieta-routing.module';
import { CrearTipoDietaComponent } from './rutas/crear-tipo-dieta/crear-tipo-dieta.component';
import { EditarTipoDietaComponent } from './rutas/editar-tipo-dieta/editar-tipo-dieta.component';
import { ListarTipoDietaComponent } from './rutas/listar-tipo-dieta/listar-tipo-dieta.component';
import { FormularioTipoDietaComponent } from './componentes/formulario-tipo-dieta/formulario-tipo-dieta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { PanelModule } from 'primeng/panel';
import { MigasPanModule } from 'src/app/componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from 'src/app/componentes/barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    TipoDietaRoutingModule
  ],
  declarations: [CrearTipoDietaComponent, EditarTipoDietaComponent, ListarTipoDietaComponent, FormularioTipoDietaComponent]
})
export class TipoDietaModule { }

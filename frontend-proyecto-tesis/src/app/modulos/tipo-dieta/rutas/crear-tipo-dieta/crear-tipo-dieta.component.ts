import { Component, OnInit } from '@angular/core';
import { TipoDieta } from 'src/app/interfaces/tipo-dieta.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from 'src/app/constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { TipoDietaService } from 'src/app/modulos/dieta/servicios/tipo-dieta.service';
import { Router } from '@angular/router';
import { cargarRutas } from 'src/app/funciones/cargarRutas';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crear-tipo-dieta',
  templateUrl: './crear-tipo-dieta.component.html',
  styleUrls: ['./crear-tipo-dieta.component.css']
})
export class CrearTipoDietaComponent implements OnInit {

  tipoDietaACrear: TipoDieta = {};
  esFormularioValido = false;
  items: MenuItem[];
  icono= ICONOS.dieta;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _tipoDietaService: TipoDietaService,
    private readonly _router: Router,

  ) { }
  ngOnInit() {
    this.items= cargarRutas('tipo-dieta','Registrar', 'Lista de tipos de dieta');
  }

  escucharFormularioTipoDieta(eventoFormularioTipoDieta: FormGroup) {
    this.esFormularioValido = eventoFormularioTipoDieta.valid;
    this.tipoDietaACrear = eventoFormularioTipoDieta.value;
  }

  crearTipoDieta() {
    this._cargandoService.habilitarCargando();
    this._tipoDietaService.create(this.tipoDietaACrear).subscribe(() => {
      this.irListarTiposDieta();
      this._toasterService.pop('success', 'Éxito', 'El tipo de dieta se ha registrado correctamente');
      this._cargandoService.deshabiltarCargando();
    }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'El error al registra tipo de dieta');
        this._cargandoService.deshabiltarCargando();
      })
  }

  irListarTiposDieta() {
    this._router.navigate(['/app', 'tipo-dieta', 'listar']);
  }
}

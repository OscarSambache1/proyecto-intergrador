import { Component, OnInit } from '@angular/core';
import { TipoDieta } from 'src/app/interfaces/tipo-dieta.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from 'src/app/constantes/iconos';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { TipoDietaService } from 'src/app/modulos/dieta/servicios/tipo-dieta.service';
import { cargarRutas } from 'src/app/funciones/cargarRutas';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editar-tipo-dieta',
  templateUrl: './editar-tipo-dieta.component.html',
  styleUrls: ['./editar-tipo-dieta.component.css']
})
export class EditarTipoDietaComponent implements OnInit {

  tipoDietaAEditar: TipoDieta;
  tipoDietaEditado: TipoDieta = {};
  esFormularioValido = false;
  items: MenuItem[];
  icono= ICONOS.dieta;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _tipoDietaService: TipoDietaService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit() {
    this.setearTipoDieta();
    this.items= cargarRutas('tipo-dieta','Editar', 'Lista de tipos de dieta');
  }

  escucharFormularioTipoDieta(eventoFormularioTipoDieta: FormGroup) {
    this.esFormularioValido = eventoFormularioTipoDieta.valid;
    this.tipoDietaEditado = eventoFormularioTipoDieta.value;
  }

  editarTipoDieta(){
    this._cargandoService.habilitarCargando();
    this._tipoDietaService.update(this.tipoDietaAEditar.id,this.tipoDietaEditado).subscribe(()=>{
      this.irListarTipoDieta();
      this._toasterService.pop('success', 'Éxito', 'El tipo de dieta se ha editado correctamente'); 
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._toasterService.pop('error', 'Error', 'Error al editar el tipo de dieta'); 
      this._cargandoService.deshabiltarCargando();
    }
  )
   }

   setearTipoDieta(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idTipoDieta = Number(respuestaParametros.get('id'));
      this._tipoDietaService.findOne(idTipoDieta).subscribe(
        tipoDieta => {
          this.tipoDietaAEditar = tipoDieta;
          this._cargandoService.deshabiltarCargando();
        }
        ,
        error=>{
          this._toasterService.pop('error','Error','Error al mostrar tipo de dieta')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarTipoDieta(){
    this._router.navigate(['/app', 'tipo-dieta', 'listar']);
   }

}

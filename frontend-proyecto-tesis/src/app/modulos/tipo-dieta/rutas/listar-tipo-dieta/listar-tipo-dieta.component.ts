import { Component, OnInit } from '@angular/core';
import { TipoDieta } from 'src/app/interfaces/tipo-dieta.interface';
import { MenuItem } from 'primeng/api';
import { ICONOS } from 'src/app/constantes/iconos';
import { TipoDietaService } from 'src/app/modulos/dieta/servicios/tipo-dieta.service';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { cargarRutas } from 'src/app/funciones/cargarRutas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-listar-tipo-dieta',
  templateUrl: './listar-tipo-dieta.component.html',
  styleUrls: ['./listar-tipo-dieta.component.css']
})
export class ListarTipoDietaComponent implements OnInit {
  arregloTiposDieta: TipoDieta[] = [];
  parametroBusqueda = '';
  columnas=[
    { field: 'nombre', header: 'Nombre', width:'50%' },
    { field: 'estado', header: 'Estado',width:'20%' },
    { field: 'acciones', header: 'Acciones',width:'30%' },
  ];
  items: MenuItem[];
  icono= ICONOS.dieta;
  habilitarBoton: boolean;
  mostrarBoton: boolean

  constructor(
    private readonly _tipoDietaService: TipoDietaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterServce: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('tipo-dieta','', 'Lista de tipos de dieta');
    this.items.pop();
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
      { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._tipoDietaService.findAll(consulta)
      .subscribe(arreglosPorParametro => {
        this.arregloTiposDieta = arreglosPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
    ,
    error => {
      this._toasterServce.pop('error','Error','Error al mostrar tipos de dieta')
      this._cargandoService.deshabiltarCargando();
    })
  }

  cambiarEstado(tipoDieta: TipoDieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloTiposDieta.indexOf(tipoDieta);
    const estaActivo = this.arregloTiposDieta[indice].estado == 1;
    estaActivo ? this.arregloTiposDieta[indice].estado = 0 : 1;
    !estaActivo ? this.arregloTiposDieta[indice].estado = 1 : 0;
    this._tipoDietaService.update(this.arregloTiposDieta[indice].id, { estado: this.arregloTiposDieta[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterServce.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }
}

import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http"
import { ToasterService } from "angular2-toaster";
import { AutenticacionService } from "./autenticacion.service";
import { IngresoModulosGuard } from "./guards/ingreso-modulos.guard";
import { IngresoLoginGuard } from "./guards/ingreso-login.guard";
import { RolesGuard } from "./guards/roles.guard";
import { ConfirmationService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { LoginComponent } from "./login/login.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CardModule } from "primeng/card";


@NgModule({
    declarations: [
        LoginComponent,
    ],
    providers: [
        AutenticacionService, 
        ToasterService,
        IngresoModulosGuard,
        IngresoLoginGuard,
        RolesGuard,
        ConfirmationService
    ],
    imports: [
        HttpClientModule,
        ConfirmDialogModule,
        NgbModule.forRoot(),
        CommonModule,
        HttpClientModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        CardModule,
    ],
    exports: []
})

export class AutenticacionModule { }

import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { AutenticacionService } from "../autenticacion.service";

@Injectable()
export class IngresoLoginGuard implements CanActivate {
    
    constructor(private readonly _route:Router,
        private readonly _autenticacionService: AutenticacionService){
    }

    async canActivate(){
        const existeUsuarioLogueado = await this._autenticacionService.obtenerUsuarioLogueado();
        const estaLogueado = this._autenticacionService.estaLogueado || existeUsuarioLogueado;
        if(!estaLogueado){
            return true     
        }   
        else{
            this._route.navigate(['/app','inicio'])
            return false
        }

    }
}

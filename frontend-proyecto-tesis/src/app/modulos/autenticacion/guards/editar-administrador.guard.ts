import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { AutenticacionService } from '../autenticacion.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { InstructorService } from '../../instructor/instructor.service';

@Injectable({
    providedIn: 'root'
})
export class EditarAdministradorGuard implements CanActivate {
    constructor(
        private readonly _toasterService: ToasterService,
        private readonly _autenticacionService: AutenticacionService,
        private readonly _instructorService: InstructorService
    ) {

    }
    async canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        const proemsUsuarioAEditar = this._instructorService.findOne(next.params.id).toPromise();
        const instructor = await proemsUsuarioAEditar;
        const esEmpleado = await this._autenticacionService.habilitarBotonSegunRol(['empleado'])
        const esAdministrador = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
        const habiliatrVista = 
        (esAdministrador) || 
        (esEmpleado && (instructor.rol === 'empleado' ||  instructor.rol === 'instructor' ))
        ;
        if (habiliatrVista) {
            return true;
        }
        else {
            this._toasterService.pop('warning', 'Advertencia', 'El usuario no tiene permiso');
            return false;
        }
    }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { AutenticacionService } from '../autenticacion.service';

@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements CanActivate {
  constructor( 
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService
  ) {

  }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
      const usuario = await this._autenticacionService.obtenerUsuarioLogueado();
      const rolesPermitidos = next.data.rolesPermitidos;
      const rolesUsuario=[];
        if(usuario.cliente) {
            rolesUsuario.push(usuario.cliente.rol);
        }
        if(usuario.instructor) {
            rolesUsuario.push(usuario.instructor.rol);
        }
        const hasRole = () => rolesUsuario.some((role) => !!rolesPermitidos.find((item) => item === role));
        if (usuario && rolesUsuario && hasRole()) {
            return true;
        }
        else {
          this._toasterService.pop('warning','Advertencia','El usuario no tiene permiso');
          return false;
        }
    }
}

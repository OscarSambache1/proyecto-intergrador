import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { AutenticacionService } from '../autenticacion.service';
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { ToasterService } from 'angular2-toaster';
import { validarCedula } from '../../../funciones/validar-cedula';
import { environment } from '../../../../environments/environment';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { ConfirmationService } from 'primeng/api';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    urlLogo = environment.url + '/logo/logo-gym.PNG';
    urlFondoLogin = environment.url + '/logo/fondo-login.jpg'
    formularioLogin: FormGroup
    mensajesErroresCedulaUsuario = []
    mensajesErroresPasswordUsuario = []

    private mensajesValidacionCedula = {
        required: "La cédula es obligatoria",
        validacionCedula: 'La cédula no es válida',
        validacionLongitud:'La cédula no es válida'
    }

    private mensajesValidacionPassword = {
        required: "La contraseña es obligatoria",
    }

    constructor(
        private readonly _formBuilder: FormBuilder,
        private readonly _autenticacionService: AutenticacionService,
        private readonly _router: Router,
        private readonly toasterService: ToasterService,
        private readonly _cargandoService: CargandoService,
        private confirmationService: ConfirmationService,

    ) {

    }
    ngOnInit() {
        this.crearFormulario()
        // this.llenarFormulario()
        this.escucharCambiosCedula()
        this.escucharCambiosPassword()
    }

    login() {
        this._cargandoService.habilitarCargando();
        const cedula = this.formularioLogin.get('cedula').value;
        const password = this.formularioLogin.get('password').value;
        this._autenticacionService.login(cedula, password).
        subscribe( usuario => {
            this._cargandoService.deshabiltarCargando();
            delete usuario.password;
            if (usuario.cliente){
                this.mostrarToast('warning', 'Advetencia', 'Acceso denegado al sistema')
            } else {
                if(usuario.instructor.estado === 0) {
                    this.mostrarToast('warning', 'Advetencia', 'Acceso denegaod, usuario inactivo')
                }
                else {
                    this._autenticacionService.setearUsuarioLogueado(usuario);
                    this.mostrarToast('success', 'Éxito', 'Credenciales correctas');
                    this._router.navigate(['app','inicio']);
                    this._autenticacionService.estaLogueado = true;
                }
            }
        },
        error => {
            this._cargandoService.deshabiltarCargando();
            this.mostrarToast('warning', 'Advetencia', 'Falló login');
            this.formularioLogin.get('password').reset();
            this.formularioLogin.get('cedula').reset();
            this._autenticacionService.estaLogueado = false;
        })
    }

    crearFormulario() {
        this.formularioLogin = this._formBuilder.group({
            cedula: [
                '', 
                [
                    Validators.required,
                ], 
                    [
                        // this.validacionAsincronaUsuario.bind(this)
                    ]
                ],
            password: ['', [Validators.required]],
        })
    }

    llenarFormulario() {
        this.formularioLogin.patchValue(
            {
                cedula: "1724155914",
                password: "1724155914",
            }
        )
    }


    escucharCambiosCedula() {
        const cedulaControl = this.formularioLogin.get('cedula')
        cedulaControl.valueChanges
            .subscribe(valor => {
                this.setearMensajesErroresCedula(cedulaControl)
            }
            )
    }

    escucharCambiosPassword() {
        const passwordControl = this.formularioLogin.get('password')
        passwordControl.valueChanges
            .subscribe(valor => {
                this.setearMensajesErroresPassword(passwordControl)
            }
            )
    }

    setearMensajesErroresCedula(valor: AbstractControl) {
        this.mensajesErroresCedulaUsuario = [];
        const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
        if (esValidoCampo) {
            this.mensajesErroresCedulaUsuario = Object.keys(valor.errors).map(atributo => {
                return this.mensajesValidacionCedula[atributo]
            })
        }
    }

    setearMensajesErroresPassword(valor: AbstractControl) {
        this.mensajesErroresPasswordUsuario = [];
        const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
        if (esValidoCampo) {
            this.mensajesErroresPasswordUsuario = Object.keys(valor.errors).map(atributo => {
                return this.mensajesValidacionPassword[atributo]
            })
        }
    }


    validacionAsincronaUsuario(valorInput: AbstractControl) {
        return this._autenticacionService.buscarUsuarioPorCedula(valorInput.value)
            .pipe(
                map(
                    usuario => {
                        if (usuario) {
                            return null
                        }
                        else {
                            return { miErrorAsync: true }
                        }
                    }
                ))
    }

    validacionCedula(): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (validarCedula(valorInput.value)) {
                return null
            }
            return { 'validacionCedula': true }
        }
    }

    validacionLongitud(longitud: number): ValidatorFn {
        return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
            if (valorInput.value.length == longitud) {
                return null
            }
            return { 'validacionLongitudCedula': true }
        }
    }


    mostrarToast(tipo: string, titulo: string, mensaje: string) {
        this.toasterService.pop(tipo, titulo, mensaje);
    }

    modalRecuperarPassword() {
        this.confirmationService.confirm({
          message: 'Para resetear su contraseña, porfavor comunicarse con el administrador del gimnasio',
          header: 'Resetear contraseña',
          icon: 'pi pi-info-circle',
          accept: () => {
          },
          reject: () => {
          }
        });
      }
}

import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpRequest } from "@angular/common/http";
import * as _moment from 'moment';
import { Usuario } from "../../interfaces/usuario";
import { UsuarioService } from "src/app/servicios/usuario.service";


@Injectable()
export class AutenticacionService{
    estaLogueado: boolean=false;
    usuarioLogueado :Usuario;
    constructor(
        private readonly _httpClient: HttpClient,
        private readonly _usuarioService: UsuarioService
        ){

    }

    login(cedula: string, password:string): Observable<any>{
        const credenciales= {
            cedula, 
            password
        };
        return this._httpClient.request("POST",environment.url+'/autenticacion/login',{responseType:"json", body: credenciales})
    }

    buscarUsuarioPorCedula(cedula: string): Observable<Usuario>{
        const credenciales={
            cedula
        };
        return this._httpClient.post(environment.url+'/autenticacion/buscarUsuarioPorCedula', credenciales)
    }

    setearUsuarioLogueado(usuario: Usuario){
        this.estaLogueado = true;
        this.usuarioLogueado = usuario;
        localStorage.setItem("usuario", JSON.stringify(usuario));
    }

    cerrarSesion(){
        this.estaLogueado=false;
        this.usuarioLogueado = undefined;
        localStorage.removeItem("usuario");
        return this._httpClient.get(environment.url+'/autenticacion/logout')
    }

    async obtenerUsuarioLogueado() {
        const usuario = JSON.parse(localStorage.getItem("usuario"));
        if (usuario) {
            const promesaUsuarioLogueado = this._usuarioService.findOne(usuario.id).toPromise();
            return await promesaUsuarioLogueado ? await promesaUsuarioLogueado : usuario;
        } else {
            return undefined;
        }
    }

    async habilitarBotonSegunRol (rolesPermitidos: string[]) {
        const usuario = await this.obtenerUsuarioLogueado();
        const rolesUsuario=[];
        if(usuario.cliente) {
            rolesUsuario.push(usuario.cliente.rol);
        }
        if(usuario.instructor) {
            rolesUsuario.push(usuario.instructor.rol);
        }
        const hasRole = () => rolesUsuario.some((role) => !!rolesPermitidos.find((item) => item === role));
        return hasRole();
    }
    
}

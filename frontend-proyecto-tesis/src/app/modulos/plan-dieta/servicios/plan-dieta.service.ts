import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { PlanDieta } from '../../../interfaces/pla-dieta.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanDietaService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/plan-dieta?consulta=` + `${JSON.stringify(consulta)}`);
  }

  findLike(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/plan-dieta/like?consulta=` + `${JSON.stringify(consulta)}`);
  }


  create(planDieta: PlanDieta): Observable<PlanDieta> {
    return this._httpClient.post(environment.url + '/plan-dieta/', planDieta);
  }
  findOne(id: number): Observable<PlanDieta> {
    return this._httpClient.get(environment.url + `/plan-dieta/${id}`);
  }
  update(id: number, planDieta: PlanDieta): Observable<PlanDieta> {
    return this._httpClient.put(environment.url + `/plan-dieta/${id}`, planDieta);
  }
}

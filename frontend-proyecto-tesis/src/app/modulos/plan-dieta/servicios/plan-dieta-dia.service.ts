import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { PlanDietaDia } from '../../../interfaces/pla-dieta-dia.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanDietaDiaService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/dieta-dia?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(dietaDia: PlanDietaDia): Observable<PlanDietaDia> {
    return this._httpClient.post(environment.url + '/dieta-dia/', dietaDia)
  }

  createMany(dietaDia: PlanDietaDia[]): Observable<any>{
    return this._httpClient.post(environment.url + '/dieta-dia/crear-varios', dietaDia)
  }

  findOne(id: number): Observable<PlanDietaDia> {
    return this._httpClient.get(environment.url + `/dieta-dia/${id}`)
  }
  update(id: number, dietaDia: PlanDietaDia): Observable<PlanDietaDia> {
    return this._httpClient.put(environment.url + `/dieta-dia/${id}`, dietaDia)
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/dieta-dia/${id}`)
  }
}

import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { PlanDietaDia } from '../../../../interfaces/pla-dieta-dia.interface';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { TipoDieta } from '../../../../interfaces/tipo-dieta.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MatDialog } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ModalPlanDietaDiaComponent } from '../../../../componentes/tabla-dietas/modales/modal-plan-dieta-dia/modal-plan-dieta-dia.component';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { ToasterService } from 'angular2-toaster';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { TipoComidaService } from '../../../comida/servicios/tipo-comida.service';
import { auto } from 'async';
import { generarBodyTablePlanDieta } from '../../../../funciones/crearBodyTablePlanDietaPdf';
import { abrirPdfNavegador } from '../../../../funciones/abrirPdfNavegador';
import {STYLES_PDF} from '../../../../constantes/styles-pdf'
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-formulario-plan-dieta',
  templateUrl: './formulario-plan-dieta.component.html',
  styleUrls: ['./formulario-plan-dieta.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))

    ])
  ]
})
export class FormularioPlanDietaComponent implements OnInit {
  @Output() esValidoFormularioPlanDieta: EventEmitter<FormGroup | boolean> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() planDietaSeleccionado: PlanDieta;
  formularioPlanDieta: FormGroup;
  @Input() arregloPlanDietaDia: PlanDietaDia[] = [];
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  mensajesErroresTiposDieta = [];
  rowGroupMetadata: any;
  arregloTiposDieta: SelectItem[] = [];
  tipoDietaSeleccionado: TipoDieta;
  parametro: string = "";
  cols: any[];
  cols2: any[];
  docDefinition: any;

  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido',
    validacionAsincronaPlanDieta: 'El nombre ya existe'
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _planDietaService: PlanDietaService,
    private readonly _confirmationService: ConfirmationService,
    private readonly _cargandoService: CargandoService,
    private readonly _formBuilder: FormBuilder,
    private readonly _tiposComidaService: TipoComidaService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.setarCabecerasTablas();
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.actualizarTabla();
    this.enviarFormularioValido();
  }

  setarCabecerasTablas() {
    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'tipo', header: 'Tipos' },
      { field: 'acciones', header: 'Acciones' },
    ];

    if (this.esFormularioVer) {
      this.cols2 = [
        { field: '', header: '', width: '10%' },
        { field: 'dia', header: 'Dia', width: '20%' },
        { field: 'dieta', header: 'Dieta', width: '40%' },
        { field: 'tipo', header: 'Tipos', width: '30%' },
      ]
    }
    else {
      this.cols2 = [
        { field: '', header: '', width: '5%' },
        { field: 'dia', header: 'Dia', width: '10%' },
        { field: 'dieta', header: 'Dieta', width: '30%' },
        { field: 'tipo', header: 'Tipos', width: '20%' },
        { field: 'acciones', header: 'Acciones', width: '35%' },
      ];
    }
  }

  crearFormulario() {
    this.formularioPlanDieta = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
      descripcion: ['', []],
      planesDietaDia: [[], [Validators.required]]
    });
  }

  escucharFormulario() {
    this.formularioPlanDieta.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioPlanDieta.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresNombre = setearMensajes(this.formularioPlanDieta.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioPlanDieta.valueChanges.subscribe(async formulario => {
      const esNombreIncorrecto = await this.validarNombreRepetido();
      if (esNombreIncorrecto) {
        this.mensajesErroresNombre = [];
        this.mensajesErroresNombre.push(this.mensajesValidacionNombre['validacionAsincronaPlanDieta']);
      }
      if (!esNombreIncorrecto && this.formularioPlanDieta.valid) {
        this.esValidoFormularioPlanDieta.emit(this.formularioPlanDieta);
      }
      else {
        this.esValidoFormularioPlanDieta.emit(false);
      }

    });
  }
  escucharPlanDietaDiaSeleccionado(eventoPlanDietaDia) {
    if (eventoPlanDietaDia) {
      const seSeleccionoMasDeUnaDieta = this.arregloPlanDietaDia.find(planDietaDia => planDietaDia.dia.nombre === eventoPlanDietaDia.dia.nombre);
      if (seSeleccionoMasDeUnaDieta) {
        this._toasterService.pop('warning', 'Advertencia', `Ya se ha seleccionado dieta para el dia ${eventoPlanDietaDia.dia.nombre}`);
      }
      else {
        this.arregloPlanDietaDia.push(eventoPlanDietaDia);
        this.arregloPlanDietaDia = this.arregloPlanDietaDia.map(planDietaDia => {
          planDietaDia.nombreDia = planDietaDia.dia.id;
          return planDietaDia;
        });
        this.formularioPlanDieta.patchValue({ planesDietaDia: this.arregloPlanDietaDia });
        this.actualizarTabla();
      }
    }
  }

  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioPlanDieta.disable();
    }
  }

  llenarFormulario() {
    this.arregloPlanDietaDia = this.planDietaSeleccionado.planesDietaDia.map(planDietaDia => {
      planDietaDia.nombreDia = planDietaDia.dia.id;
      return planDietaDia;
    });
    this.actualizarTabla();
    this.formularioPlanDieta.patchValue(
      {
        nombre: this.planDietaSeleccionado.nombre,
        descripcion: this.planDietaSeleccionado.descripcion,
        planesDietaDia: this.arregloPlanDietaDia
      }
    );
  }

  onSort() {
    this.actualizarTabla();
  }

  actualizarTabla() {
    this.rowGroupMetadata = {};
    if (this.arregloPlanDietaDia) {
      for (let i = 0; i < this.arregloPlanDietaDia.length; i++) {
        let rowData = this.arregloPlanDietaDia[i];
        let nombreDia = rowData.nombreDia;
        if (i == 0) {
          this.rowGroupMetadata[nombreDia] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.arregloPlanDietaDia[i - 1];
          let previousRowGroup = previousRowData.nombreDia;
          if (nombreDia === previousRowGroup)
            this.rowGroupMetadata[nombreDia].size++;
          else
            this.rowGroupMetadata[nombreDia] = { index: i, size: 1 };
        }
      }
    }

  }

  abrirModalEditarPlanDietaDia(planDietaDia: PlanDietaDia) {
    const indice = this.arregloPlanDietaDia.indexOf(planDietaDia);
    const dialogRef = this.dialog.open(ModalPlanDietaDiaComponent, {
      height: '325px',
      width: '450px',
      data: { planDietaDia }
    });
    dialogRef.afterClosed().subscribe(planDietaDiaEditado => {
      if (planDietaDiaEditado) {
        const seSeleccionoMasDeUnaDieta = this.arregloPlanDietaDia.find(planDietaDia => planDietaDia.dia === planDietaDiaEditado.dia);
        if (seSeleccionoMasDeUnaDieta) {
          this._toasterService.pop('warning', 'Advertencia', `Ya se ha seleccionado dieta para el dia ${planDietaDiaEditado.dia}`);
        }
        else {
          this.arregloPlanDietaDia[indice] = planDietaDiaEditado;
          this.arregloPlanDietaDia = this.arregloPlanDietaDia.map(planDietaDia => {
            planDietaDia.nombreDia = planDietaDia.dia.id;
            return planDietaDia;
          });
          this.formularioPlanDieta.patchValue({ planesDietaDia: this.arregloPlanDietaDia });
          this.actualizarTabla();
        }
      }
    });
  }

  quitarDieta(planDietaDia: PlanDieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloPlanDietaDia.indexOf(planDietaDia);
    this.arregloPlanDietaDia.splice(indice, 1);
    this.actualizarTabla();
    this.formularioPlanDieta.patchValue({ planesDietaDia: this.arregloPlanDietaDia });
    this._cargandoService.deshabiltarCargando();
  }

  confirmar(planDietaDia: PlanDietaDia) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar esta dieta?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarDieta(planDietaDia)
      },
      reject: () => {
      }
    });
  }

  async  validarNombreRepetido(): Promise<Boolean> {
    const nombre = this.formularioPlanDieta.get('nombre').value;
    const consulta =
    {
      order: { id: 'DESC' },
      where: {
        nombre
      }
    }
    const promesaNombres: any = this._planDietaService.findAll(consulta).toPromise();
    const respuestaPromesa = await promesaNombres;
    const existeNombre: boolean = respuestaPromesa.length !== 0;

    if (this.planDietaSeleccionado) {
      if (existeNombre) {
        if (this.planDietaSeleccionado.nombre === this.formularioPlanDieta.get('nombre').value.trim()) {
          return false;
        }
        else {
          return true;
        }
      }
    }
    else {
      return existeNombre;
    }
  }

  async imprimir() {
    const promesaTiposComidaService = this._tiposComidaService.findAll({}).toPromise();
    const tiposComida = await promesaTiposComidaService;
    const styles = STYLES_PDF;
    abrirPdfNavegador(
      generarBodyTablePlanDieta(tiposComida, this.planDietaSeleccionado),
      styles, 'landscape')
  }
}

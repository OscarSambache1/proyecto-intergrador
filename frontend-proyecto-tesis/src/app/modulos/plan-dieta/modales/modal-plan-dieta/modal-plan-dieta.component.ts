import { Component, OnInit, Inject } from '@angular/core';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-plan-dieta',
  templateUrl: './modal-plan-dieta.component.html',
  styleUrls: ['./modal-plan-dieta.component.css']
})
export class ModalPlanDietaComponent implements OnInit {

  planDietaAVer: PlanDieta;
  icono = ICONOS.planDieta;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _planDietaService: PlanDietaService,
    public dialogRef: MatDialogRef<ModalPlanDietaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this._cargandoService.habilitarCargando();
    this._planDietaService.findOne(this.data.id)
    .subscribe(planDieta=>{
      this.planDietaAVer=planDieta;
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._toasterService.pop('error','Error','Error al mostrar el plan de dieta')
      this._cargandoService.deshabiltarCargando();
    });
  }

}

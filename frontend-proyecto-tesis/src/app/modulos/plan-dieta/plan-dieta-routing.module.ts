import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarPlanDietaComponent } from './componentes/listar-plan-dieta/listar-plan-dieta.component';
import { CrearPlanDietaComponent } from './componentes/crear-plan-dieta/crear-plan-dieta.component';
import { EditarPlanDietaComponent } from './componentes/editar-plan-dieta/editar-plan-dieta.component';
import { VerPlanDietaComponent } from './componentes/ver-plan-dieta/ver-plan-dieta.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component: EditarPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component : VerPlanDietaComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path: '',
    redirectTo: 'listar',
    pathMatch: 'full',
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanDietaRoutingModule { }

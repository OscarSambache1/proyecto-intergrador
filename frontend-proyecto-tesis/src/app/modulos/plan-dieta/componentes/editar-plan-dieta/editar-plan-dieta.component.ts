import { Component, OnInit } from '@angular/core';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { FormGroup } from '@angular/forms';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { PlanDietaDiaService } from '../../servicios/plan-dieta-dia.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { PlanDietaDia } from '../../../../interfaces/pla-dieta-dia.interface';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-plan-dieta',
  templateUrl: './editar-plan-dieta.component.html',
  styleUrls: ['./editar-plan-dieta.component.css']
})
export class EditarPlanDietaComponent implements OnInit {
  planDietaAEditar: PlanDieta;
  planDietaEditado: PlanDieta = {};
  esFormularioPlanDietaValido=false;
  formularioEditarPlanDieta: FormGroup;  
  items: MenuItem[];
  icono= ICONOS.planDieta;
  planesDietaDiaPlanDietaEditado: PlanDietaDia[];
  constructor(
    private readonly _planDietaService: PlanDietaService,
    private readonly _planDietaDiaService: PlanDietaDiaService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService

  ) { }

  ngOnInit() {
    this.setearPlanDietaAEditar();
    this.items= cargarRutas('plan-dieta','Editar', 'Lista de planes dieta');
  }

  escucharFormularioPlanDieta(eventoFormularioPlanDieta) {
    this.formularioEditarPlanDieta = eventoFormularioPlanDieta;
    if(eventoFormularioPlanDieta === false){
      this.esFormularioPlanDietaValido=eventoFormularioPlanDieta;
    }
    else {
      this.esFormularioPlanDietaValido=eventoFormularioPlanDieta.valid;
      this.planDietaEditado = eventoFormularioPlanDieta.value;
    }
  }


  editarPlanDieta(){
    this._cargandoService.habilitarCargando();
    this.setearPLanDietaEditado();
    this.planesDietaDiaPlanDietaEditado = this.planDietaEditado.planesDietaDia;
    delete this.planDietaEditado.planesDietaDia;
    this._planDietaService.update(this.planDietaAEditar.id,this.planDietaEditado)
    .subscribe(plaDietaEditado=>{
      this.actualizarPlanDietaDia(plaDietaEditado);
      this._toasterService.pop('success', 'Éxito', 'El plan de dieta se ha actualizado');
      this.irListarPlanesDieta();
    }
  ,error=>{
    this._toasterService.pop('error','Error','Error al editar el plan de dieta')
    this._cargandoService.deshabiltarCargando();
  });
  }

  actualizarPlanDietaDia(planDietaEditado:PlanDieta){
    const planesDietaDiaGuardados = planDietaEditado.planesDietaDia;
    const planesDietaDiaSeleccionados = this.formularioEditarPlanDieta.get('planesDietaDia').value;
    planesDietaDiaGuardados.forEach(planDietaDiaGuardado=>{
      const estaPlanDietaGuardadoEnPlaneSeleccionados = planesDietaDiaSeleccionados.find( 
        planDietaDiaSeleccionado => {
          return  planDietaDiaSeleccionado.id === planDietaDiaGuardado.id
        }
      );

      if(!estaPlanDietaGuardadoEnPlaneSeleccionados){
       this._planDietaDiaService.delete(planDietaDiaGuardado.id)
       .subscribe(
         respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error al editar el plan de dieta')
          this._cargandoService.deshabiltarCargando();
        }
       );        
      }
    });

    planesDietaDiaSeleccionados.forEach(planDietaDiaSeleccionado=>{
      if(!planDietaDiaSeleccionado.id){
        delete planDietaDiaSeleccionado.nombreDia;
        const planDietaDiaACrear: PlanDietaDia = planDietaDiaSeleccionado;
        planDietaDiaACrear.planDieta=planDietaEditado;
        this._planDietaDiaService.create(planDietaDiaACrear)
        .subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
           },
           error=>{
            this._toasterService.pop('error','Error','Error al editar el plan de dieta')
            this._cargandoService.deshabiltarCargando();
          }
        );
      }
      else{
        delete planDietaDiaSeleccionado.nombreDia;
       this._planDietaDiaService.update(planDietaDiaSeleccionado.id,planDietaDiaSeleccionado)
       .subscribe(
        respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error editar el plan de dieta')
          this._cargandoService.deshabiltarCargando();
        }
       );
      }
    })
   }


   setearPLanDietaEditado(){
    this.planDietaEditado.nombre = this.formularioEditarPlanDieta.get('nombre').value;
    this.planDietaEditado.descripcion = this.formularioEditarPlanDieta.get('descripcion').value;
  }

  setearPlanDietaAEditar(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPlanDieta = Number(respuestaParametros.get('id'));
      this._planDietaService.findOne(idPlanDieta).subscribe(
        planDieta => {
          this.planDietaAEditar = planDieta;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar plan de dieta')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarPlanesDieta(){
    this._router.navigate(['/app', 'plan-dieta', 'listar']);
   }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { PlanDietaDia } from '../../../../interfaces/pla-dieta-dia.interface';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { PlanDietaDiaService } from '../../servicios/plan-dieta-dia.service';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { MenuItem } from 'primeng/api';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-plan-dieta',
  templateUrl: './crear-plan-dieta.component.html',
  styleUrls: ['./crear-plan-dieta.component.css']
})
export class CrearPlanDietaComponent implements OnInit {
  planDietaACrear: PlanDieta = {};
  arregloPlanesDietaDia: PlanDietaDia[] = [];
  esFormularioPlanDietaValido = false;
  formularioCrearPlanDieta: FormGroup;
  idCliente:string;
  items: MenuItem[];
  icono= ICONOS.planDieta;

  constructor(
    private readonly _planDietaService: PlanDietaService,
    private readonly _planDietaDiaService: PlanDietaDiaService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,

  ) { }


  ngOnInit() {
    this.items= cargarRutas('plan-dieta','Registrar', 'Lista de planes dieta');
    const parametrosOpcionales$ = this._activatedRoute.paramMap;
    parametrosOpcionales$.subscribe(parametros => {
      this.idCliente = parametros.get('idCliente');
    });

  }

  escucharFormularioPlanDieta(eventoFormularioPlanDieta) {
    this.formularioCrearPlanDieta = eventoFormularioPlanDieta;
    if(eventoFormularioPlanDieta === false){
      this.esFormularioPlanDietaValido=eventoFormularioPlanDieta;
    }
    else {
      this.esFormularioPlanDietaValido=eventoFormularioPlanDieta.valid;
      this.planDietaACrear = eventoFormularioPlanDieta.value;
    }
  }

  crearPlanDieta() {
    this._cargandoService.habilitarCargando();
    this.setearPlanDieta();
    this._planDietaService.create(this.planDietaACrear)
      .subscribe(planDietaCreado => {
        this.arregloPlanesDietaDia = this.formularioCrearPlanDieta.get('planesDietaDia').value
        .map(planDietaDia=>{
          planDietaDia.planDieta=planDietaCreado;
          return planDietaDia
        });
        this._planDietaDiaService.createMany(this.arregloPlanesDietaDia)
        .subscribe(planesDietaDia=>{
          this.verificarRuta();          
          this._toasterService.pop('success', 'Éxito', 'El plan de dieta se ha creado');
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error crear el plan de dieta')
          this._cargandoService.deshabiltarCargando();
        });
      });
  }

  setearPlanDieta() {
    this.planDietaACrear.nombre = this.formularioCrearPlanDieta.get('nombre').value;
    this.planDietaACrear.descripcion = this.formularioCrearPlanDieta.get('descripcion').value;
   }

   verificarRuta(){
    if(this.idCliente !== null){
      this.irAsignacionPLanDietaCliente();
    }
    else {
      this.irListarPlanesDieta();
    }
   }

  irAsignacionPLanDietaCliente(){
    this._router.navigate(['/app','cliente',this.idCliente ,'planes-dieta', 'crear']);
   }

  irListarPlanesDieta() {
      this._router.navigate(['/app', 'plan-dieta', 'listar']);
  }

}
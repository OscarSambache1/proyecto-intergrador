import { Component, OnInit } from '@angular/core';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-listar-plan-dieta',
  templateUrl: './listar-plan-dieta.component.html',
  styleUrls: ['./listar-plan-dieta.component.css']
})
export class ListarPlanDietaComponent implements OnInit {

  arregloPlanesDieta: PlanDieta[] = [];
  parametroBusqueda = '';
  columnas=[
      { field: 'nombre', header: 'Nombre' },
      { field: 'acciones', header: 'Acciones' },
  ];
  items: MenuItem[];
  icono= ICONOS.planDieta;

  constructor(
    private readonly _planDietaService: PlanDietaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('plan-dieta','', 'Lista de planes dieta');
    this.items.pop();
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
      { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._planDietaService.findLike(consulta)
      .subscribe(planesDietaPorParametro => {
        this.arregloPlanesDieta = planesDietaPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar planes de dieta')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }


}

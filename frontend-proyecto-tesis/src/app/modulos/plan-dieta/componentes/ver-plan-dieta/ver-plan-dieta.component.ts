import { Component, OnInit } from '@angular/core';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { PlanDietaService } from '../../servicios/plan-dieta.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-plan-dieta',
  templateUrl: './ver-plan-dieta.component.html',
  styleUrls: ['./ver-plan-dieta.component.css']
})
export class VerPlanDietaComponent implements OnInit {
  items: MenuItem[];
  icono= ICONOS.planDieta;

  planDietaAVer: PlanDieta;
  constructor(
    private readonly _planDietaService: PlanDietaService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService

  ) { 
  }

  ngOnInit() {
    this.items= cargarRutas('plan-dieta','Ver', 'Lista de planes dieta');
    this.setearPlanDietaAVer();
  }
  setearPlanDietaAVer(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPlanDieta = Number(respuestaParametros.get('id'));
      this._planDietaService.findOne(idPlanDieta).subscribe(
        planDieta => {
          this.planDietaAVer = planDieta;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar plan de dieta')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarPlanesDieta(){
    this._router.navigate(['/app', 'plan-dieta', 'listar']);
   }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanDietaRoutingModule } from './plan-dieta-routing.module';
import { ListarPlanDietaComponent } from './componentes/listar-plan-dieta/listar-plan-dieta.component';
import { CrearPlanDietaComponent } from './componentes/crear-plan-dieta/crear-plan-dieta.component';
import { EditarPlanDietaComponent } from './componentes/editar-plan-dieta/editar-plan-dieta.component';
import { VerPlanDietaComponent } from './componentes/ver-plan-dieta/ver-plan-dieta.component';
import { DietaModule } from '../dieta/dieta.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDialogModule, MatFormFieldModule } from '@angular/material';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PlanDietaDiaService } from './servicios/plan-dieta-dia.service';
import { PlanDietaService } from './servicios/plan-dieta.service';
import { FormularioPlanDietaComponent } from './formularios/formulario-plan-dieta/formulario-plan-dieta.component';
import { TablaDietasModule } from '../../componentes/tabla-dietas/tabla-dietas.module';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import { ModalPlanDietaComponent } from './modales/modal-plan-dieta/modal-plan-dieta.component';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { TablaPlanesDietaModule } from '../../componentes/tabla-planes-dieta/tabla-planes-dieta.module';
import { DiaService } from '../../servicios/dia.service';

@NgModule({
  imports: [
    CommonModule,
    PlanDietaRoutingModule,
    DietaModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    MultiSelectModule,
    DropdownModule,
    InputTextareaModule,
    MatDialogModule,
    MatFormFieldModule,
    ConfirmDialogModule,
    TablaDietasModule,
    PanelModule,
    InputTextModule,
    MigasPanModule,
    BarraTituloModule,
    TablaPlanesDietaModule
  ],
  declarations: [
    ListarPlanDietaComponent, 
    CrearPlanDietaComponent, 
    EditarPlanDietaComponent, 
    VerPlanDietaComponent, 
    FormularioPlanDietaComponent, 
    ModalPlanDietaComponent
  ],
  providers:[
    PlanDietaService,
    PlanDietaDiaService,
    DiaService
  ],
  entryComponents:[
    ModalPlanDietaComponent
  ]
})
export class PlanDietaModule { }

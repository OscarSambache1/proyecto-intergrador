import { Component, OnInit } from '@angular/core';
import { PlanEntrenamientoClienteInstructorService } from '../../servicios/plan-entrenamiento-cliente-instructor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanEntrenamientoClienteInstructor } from '../../../../interfaces/plan-entrenamiento-cliente-instructor.interface'
import { Cliente } from '../../../../interfaces/cliente.interface';
import { ClienteService } from '../../../cliente/cliente.service';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { ICONOS } from '../../../../constantes/iconos';
import { AutenticacionService } from '../../../autenticacion/autenticacion.service';
@Component({
  selector: 'app-listar-asignaciones-plan-entrenamiento',
  templateUrl: './listar-asignaciones-plan-entrenamiento.component.html',
  styleUrls: ['./listar-asignaciones-plan-entrenamiento.component.css']
})
export class ListarAsignacionesPlanEntrenamientoComponent implements OnInit {
  arregloPlanesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructor[]=[];
  cliente:Cliente;
  parametroBusqueda='';
  columnas=[
    { field: 'inicio', header: 'Inicio', width:'15%' },
    { field: 'fin', header: 'Fin', width:'15%' },
    { field: 'nombre', header: 'Nombre',width:'35%' },
    { field: 'acciones', header: 'Acciones',width:'35%' },
  ];
  items: MenuItem[]=[];
  icono= ICONOS.asignacion;
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  constructor(
    private readonly _planEntenamientoClienteInrtructor : PlanEntrenamientoClienteInstructorService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _clienteService: ClienteService,
    private readonly _router: Router,
    private readonly _confirmationService: ConfirmationService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.obtenerPlanEntrenamientoPorCliente();
    this.items=[
      {
        label: 'Cliente',
        routerLink: ['/app', 'lista-clientes',{modulo:'planes-entrenamiento', titulo:'planes de entrenamiento'}]
      },
      {
        label: 'Lista de planes de entrenamiento',
      }
    ];
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }


  obtenerPlanEntrenamientoPorCliente(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCliente = Number(respuestaParametros.get('id'));
      this.obtenerCliente(idCliente);
      const consulta ={ 
        order: { id: 'DESC' }, 
        where: { cliente: idCliente },
        relations: ['cliente','instructor','cliente.usuario','planEntrenamiento']
      }

      this._planEntenamientoClienteInrtructor.findAll(consulta).subscribe(
        planesEntrenamientoClienteInstructor => {
          this.arregloPlanesEntrenamientoClienteInstructor = planesEntrenamientoClienteInstructor;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar planes de entrenamiento');
        }
      );
    });
   }

   obtenerCliente(id: number){
    this._cargandoService.habilitarCargando();
    this._clienteService.findOne(id).subscribe(
      cliente => {
        this.cliente = cliente;
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._cargandoService.deshabiltarCargando();
        this._toasterService.pop('error','Error','Error al mostrar cliente');
      }
    );
   }

   quitarPlanEntrenamientoCliente(planEntrenamientoCliente: PlanEntrenamientoClienteInstructor) {
    this._cargandoService.habilitarCargando();
    this._planEntenamientoClienteInrtructor.delete(planEntrenamientoCliente.id)
    .subscribe(respuesta=>{
      const indice = this.arregloPlanesEntrenamientoClienteInstructor.indexOf(planEntrenamientoCliente);
      this.arregloPlanesEntrenamientoClienteInstructor.splice(indice, 1);
      this._cargandoService.deshabiltarCargando();
    }
    ,error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al quitar el plan de entrenamiento');
    }
  );
  }


  confirmar(planEntrenamientoCliente: PlanEntrenamientoClienteInstructor) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar este plan de entrenamiento?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarPlanEntrenamientoCliente(planEntrenamientoCliente)
      },
      reject: () => {
      }
    });
  }

   irListaClientes(){
    this._router.navigate(['/app', 'lista-clientes',{modulo:'planes-entrenamiento', titulo:'planes-entrenamiento'}]);
  }
}

import { Component, OnInit } from '@angular/core';
import { PlanEntrenamientoClienteInstructor } from '../../../../interfaces/plan-entrenamiento-cliente-instructor.interface';
import { FormGroup } from '@angular/forms';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanEntrenamientoClienteInstructorService } from '../../servicios/plan-entrenamiento-cliente-instructor.service';
import { ClienteService } from '../../../cliente/cliente.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-asignacion-plan-entrenamiento',
  templateUrl: './editar-asignacion-plan-entrenamiento.component.html',
  styleUrls: ['./editar-asignacion-plan-entrenamiento.component.css']
})
export class EditarAsignacionPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoClienteInstructorAEditar: PlanEntrenamientoClienteInstructor;
  planEntrenamientoClienteInstructorEditado: PlanEntrenamientoClienteInstructor={};
  esFormularioPlanEntrenamientoClienteInstructorValido = false;
  formularioEditarPlanEntrenamientoClienteInstructor: FormGroup;
  cliente: Cliente;
  items: MenuItem[]=[];
  icono= ICONOS.asignacion;

  constructor(
    private readonly _planEntrenamientoClienteInstructorService: PlanEntrenamientoClienteInstructorService,
    private readonly _clienteService: ClienteService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,

  ) { }

  ngOnInit() {
    this.setearPlanEntenamientoCliente();
  }

  escucharFormularioAsignacionPlanEntrenamiento(eventoFormulario: FormGroup) {
    this.formularioEditarPlanEntrenamientoClienteInstructor = eventoFormulario;
    this.esFormularioPlanEntrenamientoClienteInstructorValido = this.formularioEditarPlanEntrenamientoClienteInstructor.valid;
  }

  editarPlanEntrenamientoClienteInstructor(){
    this._cargandoService.habilitarCargando();
    this.setearPlanEntrenamientoClienteInstructorEditado();
    this._planEntrenamientoClienteInstructorService.update(this.planEntrenamientoClienteInstructorAEditar.id,this.planEntrenamientoClienteInstructorEditado)
    .subscribe(planEntrenamientoClienteInstructorEditado=>{
      this._toasterService.pop('success', 'Éxito', 'La asignnación se ha actualizado');
      this.irListarPlanesEntrenamientoCliente();
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al editar el plan de entrenamiento');
    })
  }

  setearPlanEntrenamientoClienteInstructorEditado(){
    this.planEntrenamientoClienteInstructorEditado.fechaInicio = this.formularioEditarPlanEntrenamientoClienteInstructor.get('fechaInicio').value;
    this.planEntrenamientoClienteInstructorEditado.fechaFin = this.formularioEditarPlanEntrenamientoClienteInstructor.get('fechaFin').value;
    this.planEntrenamientoClienteInstructorEditado.instructor = this.formularioEditarPlanEntrenamientoClienteInstructor.get('instructor').value;
    this.planEntrenamientoClienteInstructorEditado.planEntrenamiento = this.formularioEditarPlanEntrenamientoClienteInstructor.get('planEntrenamiento').value;
    }

  setearPlanEntenamientoCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPLanEntrenamientoCliente = Number(respuestaParametros.get('id'));
      this._planEntrenamientoClienteInstructorService.findOne(idPLanEntrenamientoCliente).subscribe(
        planEntenamientoCliente => {
          this.planEntrenamientoClienteInstructorAEditar = planEntenamientoCliente;
          const idCliente = this.planEntrenamientoClienteInstructorAEditar.cliente.id;
          this._clienteService.findOne(idCliente).subscribe(
            cliente => {
              this.cliente = cliente;
              this.items=cargarRutasChildren(this.cliente.id,'planes-entrenamiento','Editar', 'planes de entrenamiento');
              this._cargandoService.deshabiltarCargando();
            },
            error=>{
              this._cargandoService.deshabiltarCargando();
              this._toasterService.pop('error','Error','Error al mostrar el cliente');
            }
          );
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar el cliente');
        });
    })
  }

  irListarPlanesEntrenamientoCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-entrenamiento']);
  }
}

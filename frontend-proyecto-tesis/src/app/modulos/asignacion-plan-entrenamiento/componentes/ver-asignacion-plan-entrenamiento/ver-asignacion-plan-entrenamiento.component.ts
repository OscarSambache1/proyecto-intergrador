import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { PlanEntrenamientoClienteInstructor } from '../../../../interfaces/plan-entrenamiento-cliente-instructor.interface';
import { PlanEntrenamientoClienteInstructorService } from '../../servicios/plan-entrenamiento-cliente-instructor.service';
import { ClienteService } from '../../../cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-asignacion-plan-entrenamiento',
  templateUrl: './ver-asignacion-plan-entrenamiento.component.html',
  styleUrls: ['./ver-asignacion-plan-entrenamiento.component.css']
})
export class VerAsignacionPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoClienteInstructorAVer: PlanEntrenamientoClienteInstructor;
  cliente: Cliente;
  items: MenuItem[]=[];
  icono= ICONOS.asignacion;
  constructor(
    private readonly _planEntrenamientoClienteInstructorService: PlanEntrenamientoClienteInstructorService,
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.setearPlanEntenamientoCliente();
  }

  setearPlanEntenamientoCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPLanEntrenamientoCliente = Number(respuestaParametros.get('id'));
      this._planEntrenamientoClienteInstructorService.findOne(idPLanEntrenamientoCliente).subscribe(
        planEntenamientoCliente => {
          this.planEntrenamientoClienteInstructorAVer = planEntenamientoCliente;
          const idCliente = this.planEntrenamientoClienteInstructorAVer.cliente.id;
          this._clienteService.findOne(idCliente).subscribe(
            cliente => {
              this.cliente = cliente;
              this.items=cargarRutasChildren(this.cliente.id,'planes-entrenamiento','Ver', 'planes de entrenamiento');
              this._cargandoService.deshabiltarCargando();
            }
          ),
          error=>{
            this._cargandoService.deshabiltarCargando();
            this._toasterService.pop('error','Error','Error al mostrar el cliente');
          }
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al mostrar el cliente');
        });
    });
  }

  irListarPlanesEntrenamientoCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-entrenamiento']);
  }
}

import { Component, OnInit } from '@angular/core';
import { PlanEntrenamientoClienteInstructor } from '../../../../interfaces/plan-entrenamiento-cliente-instructor.interface';
import { FormGroup } from '@angular/forms';
import { PlanEntrenamientoClienteInstructorService } from '../../servicios/plan-entrenamiento-cliente-instructor.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { ClienteService } from '../../../cliente/cliente.service';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutasChildren } from '../../../../funciones/cargarRutasChildren';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-asignacion-plan-entrenamiento',
  templateUrl: './crear-asignacion-plan-entrenamiento.component.html',
  styleUrls: ['./crear-asignacion-plan-entrenamiento.component.css']
})
export class CrearAsignacionPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoClienteInstructorACrear: PlanEntrenamientoClienteInstructor = {};
  esFormularioPlanEntrenamientoClienteInstructorValido = false;
  formularioCrearPlanEntrenamientoClienteInstructor: FormGroup;
  cliente: Cliente;
  items: MenuItem[]=[];
  icono= ICONOS.asignacion;

  constructor(
    private readonly _planEntrenamientoClienteInstructorService: PlanEntrenamientoClienteInstructorService,
    private readonly _clienteService: ClienteService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,   
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
  ) { }

  ngOnInit() {
    this.setearPlanEnternamientoCliente();
  }

  escucharFormularioAsignacionPlanEntrenamiento(eventoFormulario: FormGroup) {
    this.formularioCrearPlanEntrenamientoClienteInstructor = eventoFormulario;
    this.esFormularioPlanEntrenamientoClienteInstructorValido = this.formularioCrearPlanEntrenamientoClienteInstructor.valid;
  }

  setearPlanEnternamientoCliente() {
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idCliente = Number(respuestaParametros.get('id'));
      this._clienteService.findOne(idCliente).subscribe(
        cliente => {
          this.cliente = cliente;
          this.items=cargarRutasChildren(this.cliente.id,'planes-entrenamiento','Asignar', 'planes de entrenamiento');
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error','Error','Error al crear mostrar cliente');
        }
      );
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al crear mostrar cliente');
    })
  }

  crearPlanEntrenamientoClienteInstructor(){
    this._cargandoService.habilitarCargando();
    this.setearPlanEntrenamientoClienteInstructorACrear();
    this._planEntrenamientoClienteInstructorService.create(this.planEntrenamientoClienteInstructorACrear)
    .subscribe(planEntrenamientoClienteInstructorCreado=>{
      this._toasterService.pop('success', 'Éxito', 'El plan de entrenamiento se ha asignado');
      this.irListarPlanesEntrenamientoCliente();
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al crear el  plane de entrenamiento');
    }
  );
  }
  
  setearPlanEntrenamientoClienteInstructorACrear() {
    this.planEntrenamientoClienteInstructorACrear.fechaFin = this.formularioCrearPlanEntrenamientoClienteInstructor.get('fechaFin').value;
    this.planEntrenamientoClienteInstructorACrear.fechaInicio = this.formularioCrearPlanEntrenamientoClienteInstructor.get('fechaInicio').value;
    this.planEntrenamientoClienteInstructorACrear.cliente = this.cliente;
    this.planEntrenamientoClienteInstructorACrear.instructor = this.formularioCrearPlanEntrenamientoClienteInstructor.get('instructor').value;
    this.planEntrenamientoClienteInstructorACrear.planEntrenamiento = this.formularioCrearPlanEntrenamientoClienteInstructor.get('planEntrenamiento').value;
  }

  irListarPlanesEntrenamientoCliente() {
    this._router.navigate(['/app', 'cliente', this.cliente.id, 'planes-entrenamiento']);
  }
}

import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { PlanEntrenamientoClienteInstructor } from '../../../../interfaces/plan-entrenamiento-cliente-instructor.interface';
import { ToasterService } from 'angular2-toaster';
import { CONFIGURACIONES_CALENDARIO } from '../../../../constantes/configuracion-calendario';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { InstructorService } from '../../../instructor/instructor.service';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { PlanEntrenamientoService } from '../../../plan-entrenamiento/servicios/plan-entrenamiento.service';
import { ModalVerPlanEntrenamientoComponent } from '../../../plan-entrenamiento/modales/modal-ver-plan-entrenamiento/modal-ver-plan-entrenamiento.component';
import { MatDialog } from '@angular/material';
import { CargandoService } from '../../../../servicios/cargando.service';
import { setearMensajes } from '../../../../shared/validaciones';
import { debounceTime } from 'rxjs/operators';
import { DiaService } from '../../../../servicios/dia.service';
import { abrirPdfNavegador } from '../../../../funciones/abrirPdfNavegador';
import { crearBodyTablePlanEntrenamiento } from '../../../../funciones/crearBodyPlanEntrenamiento';
import { STYLES_PDF } from '../../../../constantes/styles-pdf';

@Component({
  selector: 'app-formulario-asignacion-plan-entrenamiento',
  templateUrl: './formulario-asignacion-plan-entrenamiento.component.html',
  styleUrls: ['./formulario-asignacion-plan-entrenamiento.component.css']
})
export class FormularioAsignacionPlanEntrenamientoComponent implements OnInit {
  @Output() esValidoFormularioAsignacionPlanEntrenamiento: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() cliente: Cliente;
  @Input() planEntrenamientoClienteInstructorSeleccionado: PlanEntrenamientoClienteInstructor;
  formularioPlanEntrenamientoClienteInstructor: FormGroup
  dates: Date[];
  configuracion: any;
  arregloInstructores: SelectItem[] = [];
  arregloPlanesEntrenamientoSeleccionado: PlanEntrenamiento[] = [];
  planEntrenamientoSeleccionado: PlanEntrenamiento = {};
  mensajesErroresFechaInicio = [];
  mensajesErroresFechaFin = [];
  mensajeErroresInstructor = [];
  mostrarBotonAniadir = true;

  columnas = [
    { field: 'plan Entrenamiento', header: 'Plan de entrenamiento', width: '50%' },
    { field: 'acciones', header: 'Acciones', width: '50%' },
  ];
  private mensajesValidacionFechaInicio = {
    required: "La fecha de inicio es obligatoria",
  }
  private mensajesValidacionFechaFin = {
    required: "La fecha de fin es obligatoria",
  }

  private mensajesValidacionInstructor = {
    required: "El instructor es obligatorio",
  }
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _instructorService: InstructorService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private confirmationService: ConfirmationService,
    private readonly _diaService: DiaService,
  ) { }

  ngOnInit() {
    this.llenarDropdownInstructores();
    this.configuracion = CONFIGURACIONES_CALENDARIO;
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  llenarDropdownInstructores() {
    this._cargandoService.habilitarCargando();
    const consulta =
    {
      order: { id: 'DESC' },
      where: [
        {
          estado: 1,
          rol: 'empleado'
        },
        {
          estado: 1,
          rol: 'administrador'
        }
      ],
      relations: ['usuario']
    };
    this._instructorService.findAll(consulta).subscribe(instructores => {
      this.arregloInstructores.push(
        {
          label: "seleccione un instructor",
          value: null
        }
      );
      instructores.forEach(instructor => {
        const elemento = {
          label: instructor.usuario.nombres + ' ' + instructor.usuario.apellidos,
          value: instructor
        }
        this.arregloInstructores.push(elemento);
      });
    },
      error => {
        this._cargandoService.deshabiltarCargando();
        this._toasterService.pop('error', 'Error', 'Error al mostrar los instructores');
      }
    );
  }

  crearFormulario() {
    this.formularioPlanEntrenamientoClienteInstructor = this._formBuilder.group({
      fechaFin: ['', [Validators.required]],
      fechaInicio: ['', [Validators.required]],
      instructor: ['', [Validators.required]],
      planEntrenamiento: ['', [Validators.required]]
    })
  }

  escucharFormulario() {
    this.formularioPlanEntrenamientoClienteInstructor.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresFechaFin = setearMensajes(this.formularioPlanEntrenamientoClienteInstructor.get('fechaFin'), this.mensajesValidacionFechaFin);
        this.mensajesErroresFechaInicio = setearMensajes(this.formularioPlanEntrenamientoClienteInstructor.get('fechaInicio'), this.mensajesValidacionFechaInicio);
        this.mensajeErroresInstructor = setearMensajes(this.formularioPlanEntrenamientoClienteInstructor.get('instructor'), this.mensajesValidacionInstructor);
      });
  }

  enviarFormularioValido() {
    this.formularioPlanEntrenamientoClienteInstructor.valueChanges.subscribe(formulario => {
      this.esValidoFormularioAsignacionPlanEntrenamiento.emit(this.formularioPlanEntrenamientoClienteInstructor);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioCrear) {
      this.mostrarBotonAniadir = true;

    }
    if (this.esFormularioEditar) {
      this.mostrarBotonAniadir = false;
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.formularioPlanEntrenamientoClienteInstructor.disable();
      this.llenarFormulario();
    }
  }

  llenarFormulario() {
    this._cargandoService.habilitarCargando();
    this.planEntrenamientoSeleccionado = this.planEntrenamientoClienteInstructorSeleccionado.planEntrenamiento;
    this._instructorService.findOne(this.planEntrenamientoClienteInstructorSeleccionado.instructor.id)
      .subscribe(
        instructor => {
          this._planEntrenamientoService.findOne(this.planEntrenamientoSeleccionado.id)
            .subscribe(
              planEntrenamiento => {
                this.formularioPlanEntrenamientoClienteInstructor.patchValue(
                  {
                    fechaInicio: this.planEntrenamientoClienteInstructorSeleccionado.fechaInicio,
                    fechaFin: this.planEntrenamientoClienteInstructorSeleccionado.fechaFin,
                    instructor,
                    planEntrenamiento
                  }
                )
                this.arregloPlanesEntrenamientoSeleccionado.push(planEntrenamiento);
                this.planEntrenamientoSeleccionado = planEntrenamiento;
                this._cargandoService.deshabiltarCargando();
              },
              error => {
                this._cargandoService.deshabiltarCargando();
                this._toasterService.pop('error', 'Error', 'Error al mostrar los planes de entrenamiento');
              }
            );
        },
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al mostrar el instructor');
        }
      );

  }

  abrirModalVerPlanEntrenamiento(planEntrenamiento: PlanEntrenamiento) {
    const dialogRef = this.dialog.open(ModalVerPlanEntrenamientoComponent, {
      height: '650px',
      width: '800px',
      data: planEntrenamiento
    });
  }

  escucharPlanEntrenamiento(planEntrenamientoSeleccionado) {
    this.arregloPlanesEntrenamientoSeleccionado.push(planEntrenamientoSeleccionado);
    this.formularioPlanEntrenamientoClienteInstructor.patchValue({
      planEntrenamiento: planEntrenamientoSeleccionado
    })
  }

  confirmar(planEntrenamiento) {
    this.confirmationService.confirm({
      message: '¿Está seguro que quiere eliminar este registro?',
      header: 'Confirmación de eliminación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarPLanEntrenamiento(planEntrenamiento)
      },
      reject: () => {
      }
    });
  }

  quitarPLanEntrenamiento(planEntrenamiento) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloPlanesEntrenamientoSeleccionado.indexOf(planEntrenamiento);
    this.arregloPlanesEntrenamientoSeleccionado.splice(indice, 1);
    this.formularioPlanEntrenamientoClienteInstructor.patchValue({
      planEntrenamiento: this.arregloPlanesEntrenamientoSeleccionado[0]
    });
    this._cargandoService.deshabiltarCargando();
  }

  async imprimir(planEntrenamientoSeleccionado) {
    this._planEntrenamientoService.findOne(planEntrenamientoSeleccionado.id)
      .subscribe(async planEntrenamiento => {
        const promesaDias = this._diaService.findAll({}).toPromise();
        const dias = await promesaDias;
        const styles = STYLES_PDF
        abrirPdfNavegador(
          crearBodyTablePlanEntrenamiento(dias, planEntrenamiento), styles, '')
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al imprimir el plan de entrenamiento');
        })
  }
}

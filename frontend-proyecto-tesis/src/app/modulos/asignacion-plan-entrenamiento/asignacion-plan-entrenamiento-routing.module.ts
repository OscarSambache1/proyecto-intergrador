import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarAsignacionesPlanEntrenamientoComponent } from './componentes/listar-asignaciones-plan-entrenamiento/listar-asignaciones-plan-entrenamiento.component';
import { CrearAsignacionPlanEntrenamientoComponent } from './componentes/crear-asignacion-plan-entrenamiento/crear-asignacion-plan-entrenamiento.component';
import { EditarAsignacionPlanEntrenamientoComponent } from './componentes/editar-asignacion-plan-entrenamiento/editar-asignacion-plan-entrenamiento.component';
import { VerAsignacionPlanEntrenamientoComponent } from './componentes/ver-asignacion-plan-entrenamiento/ver-asignacion-plan-entrenamiento.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'',
    component : ListarAsignacionesPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component : CrearAsignacionPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component : EditarAsignacionPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'ver/:id',
    component : VerAsignacionPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsignacionPlanEntrenamientoRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { PlanEntrenamientoClienteInstructor } from '../../../interfaces/plan-entrenamiento-cliente-instructor.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanEntrenamientoClienteInstructorService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/plan-entrenamiento-cliente-instructor?consulta=` + `${JSON.stringify(consulta)}`);
  }

  create(planEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructor): Observable<PlanEntrenamientoClienteInstructor> {
    return this._httpClient.post(environment.url + '/plan-entrenamiento-cliente-instructor/', planEntrenamientoClienteInstructor);
  }
  findOne(id: number): Observable<PlanEntrenamientoClienteInstructor> {
    return this._httpClient.get(environment.url + `/plan-entrenamiento-cliente-instructor/${id}`);
  }
  update(id: number, planEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructor): Observable<PlanEntrenamientoClienteInstructor> {
    return this._httpClient.put(environment.url + `/plan-entrenamiento-cliente-instructor/${id}`, planEntrenamientoClienteInstructor);
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/plan-entrenamiento-cliente-instructor/${id}`)
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsignacionPlanEntrenamientoRoutingModule } from './asignacion-plan-entrenamiento-routing.module';
import { ListarAsignacionesPlanEntrenamientoComponent } from './componentes/listar-asignaciones-plan-entrenamiento/listar-asignaciones-plan-entrenamiento.component';
import { CrearAsignacionPlanEntrenamientoComponent } from './componentes/crear-asignacion-plan-entrenamiento/crear-asignacion-plan-entrenamiento.component';
import { EditarAsignacionPlanEntrenamientoComponent } from './componentes/editar-asignacion-plan-entrenamiento/editar-asignacion-plan-entrenamiento.component';
import { VerAsignacionPlanEntrenamientoComponent } from './componentes/ver-asignacion-plan-entrenamiento/ver-asignacion-plan-entrenamiento.component';
import { PlanEntrenamientoClienteInstructorService } from './servicios/plan-entrenamiento-cliente-instructor.service';
import { RutinaModule } from '../rutina/rutina.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MatDialogModule, MatFormFieldModule } from '@angular/material';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormularioAsignacionPlanEntrenamientoComponent } from './formularios/formulario-asignacion-plan-entrenamiento/formulario-asignacion-plan-entrenamiento.component';
import { CalendarModule } from "primeng/calendar";
import { InstructorModule } from '../instructor/instructor.module';
import { PlanEntrenamientoModule } from '../plan-entrenamiento/plan-entrenamiento.module';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { InformacionClienteModule } from '../../componentes/informacion-cliente/informacion-cliente.module';
import { TablaPlanesEntrenamientoModule } from '../../componentes/tabla-planes-entrenamiento/tabla-planes-entrenamiento.module';

@NgModule({
  imports: [
    CommonModule,
    AsignacionPlanEntrenamientoRoutingModule,
    RutinaModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DropdownModule,
    MatDialogModule,
    MatFormFieldModule,
    ConfirmDialogModule,
    CalendarModule,
    InstructorModule,
    PlanEntrenamientoModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    InformacionClienteModule,
    TablaPlanesEntrenamientoModule,
    ConfirmDialogModule,

  ],
  declarations: [
    ListarAsignacionesPlanEntrenamientoComponent, 
    CrearAsignacionPlanEntrenamientoComponent, 
    EditarAsignacionPlanEntrenamientoComponent, 
    VerAsignacionPlanEntrenamientoComponent, 
    FormularioAsignacionPlanEntrenamientoComponent
  ],
  providers:[
    PlanEntrenamientoClienteInstructorService
  ]
})
export class AsignacionPlanEntrenamientoModule { }

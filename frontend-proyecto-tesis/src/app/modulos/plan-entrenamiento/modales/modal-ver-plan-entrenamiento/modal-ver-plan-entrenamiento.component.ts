import { Component, OnInit, Inject } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { PlanEntrenamientoService } from '../../servicios/plan-entrenamiento.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-ver-plan-entrenamiento',
  templateUrl: './modal-ver-plan-entrenamiento.component.html',
  styleUrls: ['./modal-ver-plan-entrenamiento.component.css']
})
export class ModalVerPlanEntrenamientoComponent implements OnInit {

  planEntrenamientoAVer: PlanEntrenamiento;
  icono = ICONOS.planEntrenamiento;

  constructor(
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    public dialogRef: MatDialogRef<ModalVerPlanEntrenamientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this._planEntrenamientoService.findOne(this.data.id)
    .subscribe(planEntrenamiento=>{
      this.planEntrenamientoAVer=planEntrenamiento
    },
    error=>{
      this._toasterService.pop('error','Error','Error al mostrar el plan de entrenamiento')
      this._cargandoService.deshabiltarCargando();
    });
  }
}

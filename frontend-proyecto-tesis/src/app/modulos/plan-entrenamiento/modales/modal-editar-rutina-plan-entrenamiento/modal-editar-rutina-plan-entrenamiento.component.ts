import { Component, OnInit, Inject } from '@angular/core';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-editar-rutina-plan-entrenamiento',
  templateUrl: './modal-editar-rutina-plan-entrenamiento.component.html',
  styleUrls: ['./modal-editar-rutina-plan-entrenamiento.component.css']
})
export class ModalEditarRutinaPlanEntrenamientoComponent implements OnInit {
  rutinaPlanEntrenamientoAEditar: RutinaPlanEntrenamiento = {};
  rutinaPlanEntrenamientoEditado : RutinaPlanEntrenamiento;
  esFormularioRutinaPlanEntrenamientoValido = false;
  icono = ICONOS.rutina;

  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalEditarRutinaPlanEntrenamientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.rutinaPlanEntrenamientoAEditar= this.data;
  }

  escucharFormularioRutinaPlanEntrenamiento(eventoFormulariRutinaPlanEntrenamiento: FormGroup) {
    this.esFormularioRutinaPlanEntrenamientoValido = eventoFormulariRutinaPlanEntrenamiento.valid;
    this.rutinaPlanEntrenamientoEditado = eventoFormulariRutinaPlanEntrenamiento.value;
  }

  editarRutinaPlanEntrenamiento() {
    this._cargandoService.habilitarCargando();
    if(this.rutinaPlanEntrenamientoAEditar.id) {
      this.rutinaPlanEntrenamientoEditado.id=this.rutinaPlanEntrenamientoAEditar.id;
    }
    this.rutinaPlanEntrenamientoEditado.rutina=this.rutinaPlanEntrenamientoAEditar.rutina;
    this.dialogRef.close(this.rutinaPlanEntrenamientoEditado);
    this._cargandoService.deshabiltarCargando();
   }
}

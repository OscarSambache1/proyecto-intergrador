import { Component, OnInit, Inject } from '@angular/core';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-crear-rutina-plan-entrenamiento',
  templateUrl: './modal-crear-rutina-plan-entrenamiento.component.html',
  styleUrls: ['./modal-crear-rutina-plan-entrenamiento.component.css']
})
export class ModalCrearRutinaPlanEntrenamientoComponent implements OnInit {

  rutinaPlanEntrenamientoACrear: RutinaPlanEntrenamiento={};
  rutina: Rutina;
  esFormularioRutinaPlanEntrenamientoValido = false;
  icono = ICONOS.rutina;

  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalCrearRutinaPlanEntrenamientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.rutina= this.data;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  escucharFormularioRutinaPlanEntrenamiento(eventoFormularioRutinaPlanEntrenamiento: FormGroup) {
    this.esFormularioRutinaPlanEntrenamientoValido = eventoFormularioRutinaPlanEntrenamiento.valid;
    this.rutinaPlanEntrenamientoACrear=eventoFormularioRutinaPlanEntrenamiento.value;
  }

  seleccionarRutinaPlanEntrenamiento(){
    this._cargandoService.habilitarCargando();
    this.rutinaPlanEntrenamientoACrear.rutina=this.rutina;
    this.dialogRef.close(this.rutinaPlanEntrenamientoACrear);
    this._cargandoService.deshabiltarCargando();
  }
}

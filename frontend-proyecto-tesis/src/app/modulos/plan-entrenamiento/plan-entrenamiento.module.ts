import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanEntrenamientoRoutingModule } from './plan-entrenamiento-routing.module';
import { ListarPlanEntrenamientoComponent } from './componentes/listar-plan-entrenamiento/listar-plan-entrenamiento.component';
import { EditarPlanEntrenamientoComponent } from './componentes/editar-plan-entrenamiento/editar-plan-entrenamiento.component';
import { VerPlanEntrenamientoComponent } from './componentes/ver-plan-entrenamiento/ver-plan-entrenamiento.component';
import { CrearPlanEntrenamientoComponent } from './componentes/crear-plan-entrenamiento/crear-plan-entrenamiento.component';
import { RutinaModule } from '../rutina/rutina.module';
import { PlanEntrenamientoService } from './servicios/plan-entrenamiento.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDialogModule, MatFormFieldModule } from '@angular/material';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormularioPlanEntrenamientoComponent } from './formularios/formulario-plan-entrenamiento/formulario-plan-entrenamiento.component';
import { FormularioRutinaPlanEntrenamientoComponent } from './formularios/formulario-rutina-plan-entrenamiento/formulario-rutina-plan-entrenamiento.component';
import { ModalCrearRutinaPlanEntrenamientoComponent } from './modales/modal-crear-rutina-plan-entrenamiento/modal-crear-rutina-plan-entrenamiento.component';
import { ModalEditarRutinaPlanEntrenamientoComponent } from './modales/modal-editar-rutina-plan-entrenamiento/modal-editar-rutina-plan-entrenamiento.component';
import { RutinaPlanEntrenamientoService } from './servicios/rutina-plan.entrenamiento.service';
import { ModalVerPlanEntrenamientoComponent } from './modales/modal-ver-plan-entrenamiento/modal-ver-plan-entrenamiento.component';
import {PanelModule} from 'primeng/panel';
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from '../../componentes/barra-titulo/barra-titulo.module';
import { TablaRutinasModule } from '../../componentes/tabla-rutinas/tabla-rutinas.module';
import { TablaPlanesEntrenamientoModule } from '../../componentes/tabla-planes-entrenamiento/tabla-planes-entrenamiento.module';
import { DiaService } from '../../servicios/dia.service';

@NgModule({
  imports: [
    CommonModule,
    PlanEntrenamientoRoutingModule,
    RutinaModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    MultiSelectModule,
    DropdownModule,
    InputTextareaModule,
    MatDialogModule,
    MatFormFieldModule,
    ConfirmDialogModule,
    PanelModule,
    MigasPanModule,
    BarraTituloModule,
    TablaRutinasModule,
    TablaPlanesEntrenamientoModule
  ],
  declarations: [
    CrearPlanEntrenamientoComponent,
    ListarPlanEntrenamientoComponent,
    EditarPlanEntrenamientoComponent,
    VerPlanEntrenamientoComponent,
    FormularioPlanEntrenamientoComponent,
    FormularioRutinaPlanEntrenamientoComponent,
    ModalCrearRutinaPlanEntrenamientoComponent,
    ModalEditarRutinaPlanEntrenamientoComponent,
    ModalVerPlanEntrenamientoComponent,
  ],
  providers: [
    PlanEntrenamientoService,
    RutinaPlanEntrenamientoService,
    DiaService
  ],
  entryComponents:[
    ModalCrearRutinaPlanEntrenamientoComponent,
    ModalEditarRutinaPlanEntrenamientoComponent,
    ModalVerPlanEntrenamientoComponent
  ]
})
export class PlanEntrenamientoModule { }

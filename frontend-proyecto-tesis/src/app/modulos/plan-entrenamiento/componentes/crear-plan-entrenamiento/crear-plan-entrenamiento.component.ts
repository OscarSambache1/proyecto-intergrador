import { Component, OnInit } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { FormGroup } from '@angular/forms';
import { PlanEntrenamientoService } from '../../servicios/plan-entrenamiento.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import { RutinaPlanEntrenamientoService } from '../../servicios/rutina-plan.entrenamiento.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-crear-plan-entrenamiento',
  templateUrl: './crear-plan-entrenamiento.component.html',
  styleUrls: ['./crear-plan-entrenamiento.component.css']
})
export class CrearPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoACrear: PlanEntrenamiento = {};
  arregloRutinasPlanesEntrenamiento: RutinaPlanEntrenamiento[] = [];
  esFormularioPlanEntrenamientoValido = false;
  formularioCrearPlanEntrenamiento: FormGroup;
  items: MenuItem[];
  idCliente:string;
  icono= ICONOS.planEntrenamiento;

  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _rutinaPlanEntrenamientoService: RutinaPlanEntrenamientoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,


  ) { }


  ngOnInit() {
    this.items= cargarRutas('plan-entrenamiento','Registrar', 'Listar planes de entrenamiento');
    const parametrosOpcionales$ = this._activatedRoute.paramMap;
    parametrosOpcionales$.subscribe(parametros => {
      this.idCliente = parametros.get('idCliente');
    });
  }

  escucharFormularioPlanEntrenamiento(eventoFormularioPlanEntrenamiento: FormGroup) {
    this.formularioCrearPlanEntrenamiento = eventoFormularioPlanEntrenamiento;
    this.esFormularioPlanEntrenamientoValido = this.formularioCrearPlanEntrenamiento.valid;
  }

  crearPlanEntrenamiento() {
    this._cargandoService.habilitarCargando();
    this.setearPlanEntrenamiento();
    this._planEntrenamientoService.create(this.planEntrenamientoACrear)
      .subscribe(planEntranientoCreado => {
        this.arregloRutinasPlanesEntrenamiento = this.formularioCrearPlanEntrenamiento.get('rutinasPlanEntrenamiento').value
        .map(rutinaPlanEntrenamiento=>{
          rutinaPlanEntrenamiento.planEntrenamiento=planEntranientoCreado;
          return rutinaPlanEntrenamiento
        });
        this._rutinaPlanEntrenamientoService.createMany(this.arregloRutinasPlanesEntrenamiento)
        .subscribe(rutinasPlanEntrenamientoCreadas=>{
          this._toasterService.pop('success', 'Éxito', 'El plan de entrenamiento se ha creado');
          this.verificarRuta();
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error crear el plan de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        });
      });
  }

  setearPlanEntrenamiento() {
    this.planEntrenamientoACrear.nombre = this.formularioCrearPlanEntrenamiento.get('nombre').value;
    this.planEntrenamientoACrear.descripcion = this.formularioCrearPlanEntrenamiento.get('descripcion').value;
   }

   verificarRuta(){
    if(this.idCliente !== null){
      this.irAsignacionPLanEntrenamientoCliente();
    }
    else {
      this.irListarPlanesEntrenamiento();
    }
   }

  irAsignacionPLanEntrenamientoCliente(){
    this._router.navigate(['/app','cliente',this.idCliente ,'planes-entrenamiento', 'crear']);
   }

  irListarPlanesEntrenamiento() {
      this._router.navigate(['/app', 'plan-entrenamiento', 'listar']);
  }
}

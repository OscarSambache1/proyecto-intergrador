import { Component, OnInit } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { PlanEntrenamientoService } from '../../servicios/plan-entrenamiento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-ver-plan-entrenamiento',
  templateUrl: './ver-plan-entrenamiento.component.html',
  styleUrls: ['./ver-plan-entrenamiento.component.css']
})
export class VerPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoAVer: PlanEntrenamiento;
  items: MenuItem[];
  icono= ICONOS.planEntrenamiento;

  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,

  ) { }

  ngOnInit() {
    this.setearPlanEntrenamientoAVer();
    this.items= cargarRutas('plan-entrenamiento','Ver', 'Listar planes de entrenamiento');
  }

  setearPlanEntrenamientoAVer(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPlanEntrenamiento = Number(respuestaParametros.get('id'));
      this._planEntrenamientoService.findOne(idPlanEntrenamiento).subscribe(
        planEntrenamiento => {
          this.planEntrenamientoAVer = planEntrenamiento;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar plan de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
  }

  irListarPlanesEntrenamiento(){
    this._router.navigate(['/app', 'plan-entrenamiento', 'listar']);
   }
}

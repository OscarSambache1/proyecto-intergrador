import { Component, OnInit } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { PlanEntrenamientoService } from '../../servicios/plan-entrenamiento.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-listar-plan-entrenamiento',
  templateUrl: './listar-plan-entrenamiento.component.html',
  styleUrls: ['./listar-plan-entrenamiento.component.css']
})
export class ListarPlanEntrenamientoComponent implements OnInit {
  arregloPlanesEntrenamiento: PlanEntrenamiento[] = [];
  parametroBusqueda = '';
  items: MenuItem[];
  icono= ICONOS.planEntrenamiento;

  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.obtenerPorParametro();
    this.items= cargarRutas('plan-entrenamiento','', 'Listar planes de entrenamiento');

  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const consulta =
      { order: { id: 'DESC' }, where: { nombre: `${this.parametroBusqueda}` } }

    this._planEntrenamientoService.findAll(consulta)
      .subscribe(planesEntrenamientoPorParametro => {
        this.arregloPlanesEntrenamiento = planesEntrenamientoPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar planes de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
      )
  }

}

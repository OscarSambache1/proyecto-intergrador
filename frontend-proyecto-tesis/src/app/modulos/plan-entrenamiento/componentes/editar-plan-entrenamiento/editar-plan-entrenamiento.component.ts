import { Component, OnInit } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { FormGroup } from '@angular/forms';
import { PlanEntrenamientoService } from '../../servicios/plan-entrenamiento.service';
import { RutinaPlanEntrenamientoService } from '../../servicios/rutina-plan.entrenamiento.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MenuItem } from 'primeng/api';
import { cargarRutas } from '../../../../funciones/cargarRutas';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-editar-plan-entrenamiento',
  templateUrl: './editar-plan-entrenamiento.component.html',
  styleUrls: ['./editar-plan-entrenamiento.component.css']
})
export class EditarPlanEntrenamientoComponent implements OnInit {
  planEntrenamientoAEditar: PlanEntrenamiento;
  plannEntrenamientoEditado: PlanEntrenamiento = {};
  esFormularioPlanEntrenamientoValido=false;
  formularioEditarPlanEntrenamiento: FormGroup;  
  items: MenuItem[];
  icono= ICONOS.planEntrenamiento;

  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _rutinaPlanEntrenamientoService: RutinaPlanEntrenamientoService,
    private readonly _toasterService: ToasterService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService

  ) { }

  ngOnInit() {
    this.setearPlanEntrenamientoAEditar();
    this.items= cargarRutas('plan-entrenamiento','Editar', 'Listar planes de entrenamiento');
  }

  escucharFormularioPlanEntrenamiento(eventoFormularioPlanEntrenamiento: FormGroup){
    this.formularioEditarPlanEntrenamiento = eventoFormularioPlanEntrenamiento;
    this.esFormularioPlanEntrenamientoValido = this.formularioEditarPlanEntrenamiento.valid;  
  }

  editarPlanEntrenamiento(){
    this._cargandoService.habilitarCargando();
    this.setearPLanEntrenamientoEditado();
    this._planEntrenamientoService.update(this.planEntrenamientoAEditar.id,this.plannEntrenamientoEditado)
    .subscribe(planEntrenamientoEditado=>{
      this.actualizarRutinaPlanEntrenamiento(planEntrenamientoEditado);
      this._toasterService.pop('success', 'Éxito', 'El plan de entrenamiento se ha actualizado');
      this.irListarPlanesEntrenamiento();
    }
  ,error=>{
    this._toasterService.pop('error','Error','Error al editar el plan de entrenamiento')
    this._cargandoService.deshabiltarCargando();
  });
  }

  actualizarRutinaPlanEntrenamiento(planEnternamientoEditado:PlanEntrenamiento){
    const rutinasPlanesEntrenamientoGuardados=planEnternamientoEditado.rutinasPlanEntrenamiento;
    const rutinasPlanesEntrenamientoSeleccionados = this.formularioEditarPlanEntrenamiento.get('rutinasPlanEntrenamiento').value;
    
    rutinasPlanesEntrenamientoGuardados.forEach(rutinaPlanEntrenamientoGuardado=>{
      const estaRutinaGuardadaEnRutinasSeleccionadas = rutinasPlanesEntrenamientoSeleccionados.find( 
        rutinaPlanEntrenamientoSeleccionado => 
        rutinaPlanEntrenamientoSeleccionado.id === rutinaPlanEntrenamientoGuardado.id
      );

      if(!estaRutinaGuardadaEnRutinasSeleccionadas){
       this._rutinaPlanEntrenamientoService.delete(rutinaPlanEntrenamientoGuardado.id)
       .subscribe(
         respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error al editar el plan de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
       );        
      }
    });

    rutinasPlanesEntrenamientoSeleccionados.forEach(rutinaPlanEntrenmaientoSeleccionado=>{
      if(!rutinaPlanEntrenmaientoSeleccionado.id){
        const rutinaPlanEntrenamientoACrear: RutinaPlanEntrenamiento = rutinaPlanEntrenmaientoSeleccionado;
        rutinaPlanEntrenamientoACrear.planEntrenamiento=planEnternamientoEditado;
        this._rutinaPlanEntrenamientoService.create(rutinaPlanEntrenamientoACrear)
        .subscribe(
          respuesta=>{
            this._cargandoService.deshabiltarCargando();
           },
           error=>{
            this._toasterService.pop('error','Error','Error al editar el plan de entrenamiento')
            this._cargandoService.deshabiltarCargando();
          }
        );
      }
      else{
        delete rutinaPlanEntrenmaientoSeleccionado.nombreDia;

       this._rutinaPlanEntrenamientoService.update(rutinaPlanEntrenmaientoSeleccionado.id,rutinaPlanEntrenmaientoSeleccionado)
       .subscribe(
        respuesta=>{
          this._cargandoService.deshabiltarCargando();
         },
         error=>{
          this._toasterService.pop('error','Error','Error editar el plan de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
       );
      }
    })
   }

  setearPLanEntrenamientoEditado(){
    this.plannEntrenamientoEditado.nombre = this.formularioEditarPlanEntrenamiento.get('nombre').value;
    this.plannEntrenamientoEditado.descripcion = this.formularioEditarPlanEntrenamiento.get('descripcion').value;
  }

  setearPlanEntrenamientoAEditar(){
    this._cargandoService.habilitarCargando();
    const parametroRuta$ = this._activateRoute.paramMap
    parametroRuta$.subscribe(respuestaParametros => {
      const idPlanEntrenamiento = Number(respuestaParametros.get('id'));
      this._planEntrenamientoService.findOne(idPlanEntrenamiento).subscribe(
        planEntrenamiento => {
          this.planEntrenamientoAEditar = planEntrenamiento;
          this._cargandoService.deshabiltarCargando();
        },
        error=>{
          this._toasterService.pop('error','Error','Error al mostar plan de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
      )
    })
   }

   irListarPlanesEntrenamiento(){
    this._router.navigate(['/app', 'plan-entrenamiento', 'listar']);
   }
}

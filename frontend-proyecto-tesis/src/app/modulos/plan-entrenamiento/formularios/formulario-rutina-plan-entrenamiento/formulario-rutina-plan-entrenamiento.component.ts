import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { SelectItem } from 'primeng/api';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import { DiaService } from '../../../../servicios/dia.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-formulario-rutina-plan-entrenamiento',
  templateUrl: './formulario-rutina-plan-entrenamiento.component.html',
  styleUrls: ['./formulario-rutina-plan-entrenamiento.component.css']
})
export class FormularioRutinaPlanEntrenamientoComponent implements OnInit {
  @Output() esValidoFormularioRutinaPlanEntrenamiento: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() rutinaPlanEntrenamientoSeleccionado: RutinaPlanEntrenamiento;
  formularioRutinaPlanEntrenamiento: FormGroup;
  dias: SelectItem[]=[];
  mensajesErroresDia = [];

  private mensajesValidacionDia = {
    required: "El día es obligatorio",
  };

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _diaService: DiaService,
    private readonly _toasterService: ToasterService,
  ) { }

  ngOnInit() {
    this.llenarDropdownDias();
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioRutinaPlanEntrenamiento = this._formBuilder.group({
      dia: ['', [Validators.required]],
    })
  }

  escucharFormulario() {
    this.formularioRutinaPlanEntrenamiento.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDia = setearMensajes(this.formularioRutinaPlanEntrenamiento.get('dia'), this.mensajesValidacionDia);
      });
  }

  enviarFormularioValido() {
    this.formularioRutinaPlanEntrenamiento.valueChanges.subscribe(formulario => {
      this.esValidoFormularioRutinaPlanEntrenamiento.emit(this.formularioRutinaPlanEntrenamiento);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioRutinaPlanEntrenamiento.disable();
    }

  }

  llenarFormulario() {
    this._diaService.findOne(this.rutinaPlanEntrenamientoSeleccionado.dia.id)
    .subscribe( dia => { 
      delete dia.clasesDiaHora;
      this.formularioRutinaPlanEntrenamiento.patchValue(
        {
          dia
        }
      );
    })
  }

  llenarDropdownDias() {
    const consulta = { order: { id: 'ASC' } };
    this.dias.push({
      label: 'Seleccione un día',
      value: null
    })
    this._diaService.findAll(consulta).subscribe(dias => {
      dias.forEach(dia => {
        const elemento = {
          label: dia.nombre,
          value: dia
        }
        this.dias.push(elemento)
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos')
      }
    );
  }
}

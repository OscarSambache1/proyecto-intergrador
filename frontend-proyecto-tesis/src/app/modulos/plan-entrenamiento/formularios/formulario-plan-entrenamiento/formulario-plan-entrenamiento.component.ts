import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { MatDialog } from '@angular/material';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { ToasterService } from 'angular2-toaster';
import { RutinaService } from '../../../rutina/servicios/rutina.service';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { ModalVerRutinaComponent } from '../../../rutina/modales/modal-ver-rutina/modal-ver-rutina.component';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { ModalEditarRutinaPlanEntrenamientoComponent } from '../../modales/modal-editar-rutina-plan-entrenamiento/modal-editar-rutina-plan-entrenamiento.component';
import { CargandoService } from '../../../../servicios/cargando.service';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../../shared/validaciones';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { DiaService } from '../../../../servicios/dia.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { crearBodyTablePlanEntrenamiento } from '../../../../funciones/crearBodyPlanEntrenamiento';
import { abrirPdfNavegador } from '../../../../funciones/abrirPdfNavegador';

@Component({
  selector: 'app-formulario-plan-entrenamiento',
  templateUrl: './formulario-plan-entrenamiento.component.html',
  styleUrls: ['./formulario-plan-entrenamiento.component.css']
})
export class FormularioPlanEntrenamientoComponent implements OnInit {
  @Output() esValidoFormularioPlanEntrenamiento: EventEmitter<FormGroup> = new EventEmitter();
  @Input() esFormularioCrear = false;
  @Input() esFormularioEditar = false;
  @Input() esFormularioVer = false;
  @Input() planEntrenamientoSeleccionado: PlanEntrenamiento;
  formularioPlanEntrenamiento: FormGroup;
  arregloRutina: Rutina[];
  arregloRutinaPlanEntrenamiento: RutinaPlanEntrenamiento[] = [];
  mensajesErroresNombre = [];
  mensajesErroresDescripcion = [];
  rowGroupMetadata: any;
  arregloMusculos: SelectItem[] = [];
  musculoSeleccionado: Musculo;


  private mensajesValidacionNombre = {
    required: "El nombre es obligatorio",
    pattern: 'El nombre no es válido'
  };

  private mensajesValidacionDescripcion = {
    required: "La descripción es obligatoria",
  };
  constructor(
    private readonly _toasterService: ToasterService,
    private _confirmationService: ConfirmationService,
    private readonly _cargandoService: CargandoService,
    private readonly _rutinaService: RutinaService,
    private readonly _formBuilder: FormBuilder,
    private readonly _diaService: DiaService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
    this.verificarTipoFormulario();
    this.llenarTablaRutinas();
    this.actualizarTabla();
  }

  crearFormulario() {
    this.formularioPlanEntrenamiento = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.pattern(/^\S.*\S$/)]],
      descripcion: ['', []],
      rutinasPlanEntrenamiento: [[], [Validators.required]]
    });
  }

  escucharFormulario() {
    this.formularioPlanEntrenamiento.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDescripcion = setearMensajes(this.formularioPlanEntrenamiento.get('descripcion'), this.mensajesValidacionDescripcion);
        this.mensajesErroresNombre = setearMensajes(this.formularioPlanEntrenamiento.get('nombre'), this.mensajesValidacionNombre);
      });
  }

  enviarFormularioValido() {
    this.formularioPlanEntrenamiento.valueChanges.subscribe(formulario => {
      this.esValidoFormularioPlanEntrenamiento.emit(this.formularioPlanEntrenamiento);
    });
  }

  verificarTipoFormulario() {
    if (this.esFormularioEditar) {
      this.llenarFormulario();
    }
    if (this.esFormularioVer) {
      this.llenarFormulario();
      this.formularioPlanEntrenamiento.disable();
    }
  }


  llenarFormulario() {
    this.arregloRutinaPlanEntrenamiento = this.planEntrenamientoSeleccionado.rutinasPlanEntrenamiento.map(rutinaPlanEntrenamiento => {
      rutinaPlanEntrenamiento.nombreDia = rutinaPlanEntrenamiento.dia.id;
      return rutinaPlanEntrenamiento;
    });
    this.actualizarTabla();
    this.formularioPlanEntrenamiento.patchValue(
      {
        nombre: this.planEntrenamientoSeleccionado.nombre,
        descripcion: this.planEntrenamientoSeleccionado.descripcion,
        rutinasPlanEntrenamiento: this.arregloRutinaPlanEntrenamiento
      }
    );
  }

  llenarTablaRutinas() {
    this._cargandoService.habilitarCargando();
    const consulta =
    {
      order: { id: 'DESC' },
      where: { nombre: `` },
      relations: ['ejerciciosRutina', 'rutinasPlanEntrenamiento', 'ejerciciosRutina.rutina', 'ejerciciosRutina.ejercicio']
    }

    this._rutinaService.findAll(consulta)
      .subscribe(rutinas => {
        this.arregloRutina = rutinas;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error mostrarlas rutinas')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  abrirModalVerRutina(rutinaAVer: Rutina) {
    const dialogRef = this.dialog.open(ModalVerRutinaComponent, {
      height: '600px',
      width: '800px',
      data: rutinaAVer
    });
  }

  abrirModalEditarRutinaPlanEntrenamiento(rutinaPlanEntrenamiento: RutinaPlanEntrenamiento) {
    const indice = this.arregloRutinaPlanEntrenamiento.indexOf(rutinaPlanEntrenamiento);
    const dialogRef = this.dialog.open(ModalEditarRutinaPlanEntrenamientoComponent, {
      height: '325px',
      width: '450px',
      data: rutinaPlanEntrenamiento
    });
    dialogRef.afterClosed().subscribe(rutinaPlanEntrenamientoEditado => {
      if (rutinaPlanEntrenamientoEditado) {
        const seSeleccionoMasDeUnaRutina = this.arregloRutinaPlanEntrenamiento.find(
          planEntrenaminetoRutina => {
            return (
              planEntrenaminetoRutina.dia.id == rutinaPlanEntrenamientoEditado.dia.id
              && planEntrenaminetoRutina.rutina.nombre === rutinaPlanEntrenamientoEditado.rutina.nombre
            );
          }
        );

        if (seSeleccionoMasDeUnaRutina) {
          this._toasterService.pop('warning', 'Advertencia', `Ya se ha seleccionado esta rutina para el dia ${rutinaPlanEntrenamientoEditado.dia.nombre}`);
        }
        else {
          this.arregloRutinaPlanEntrenamiento[indice] = rutinaPlanEntrenamientoEditado;
          this.arregloRutinaPlanEntrenamiento = this.arregloRutinaPlanEntrenamiento.map(rutinaPlanEntrenamiento => {
            rutinaPlanEntrenamiento.nombreDia = rutinaPlanEntrenamiento.dia.id;
            return rutinaPlanEntrenamiento;
          });
          this.formularioPlanEntrenamiento.patchValue({ ejerciciosRutina: this.arregloRutinaPlanEntrenamiento });
          this.actualizarTabla();
        }
      }
    });
  }

  quitarRutina(rutinaPlanEntrenamiento: RutinaPlanEntrenamiento) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloRutinaPlanEntrenamiento.indexOf(rutinaPlanEntrenamiento);
    this.arregloRutinaPlanEntrenamiento.splice(indice, 1);
    this.actualizarTabla();
    this.formularioPlanEntrenamiento.patchValue({ rutinasPlanEntrenamiento: this.arregloRutinaPlanEntrenamiento });
    this._cargandoService.deshabiltarCargando();
  }

  confirmar(rutinaPlanEntrenamiento: RutinaPlanEntrenamiento) {
    this._confirmationService.confirm({
      message: '¿Está seguro que quiere quitar esta rutina?',
      header: 'Confirmación',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.quitarRutina(rutinaPlanEntrenamiento)
      },
      reject: () => {
      }
    });
  }

  onSort() {
    this.actualizarTabla();
  }


  actualizarTabla() {
    this.rowGroupMetadata = {};
    if (this.arregloRutinaPlanEntrenamiento) {
      for (let i = 0; i < this.arregloRutinaPlanEntrenamiento.length; i++) {
        let rowData = this.arregloRutinaPlanEntrenamiento[i];
        let nombreDia = rowData.nombreDia;
        if (i == 0) {
          this.rowGroupMetadata[nombreDia] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.arregloRutinaPlanEntrenamiento[i - 1];
          let previousRowGroup = previousRowData.nombreDia;
          if (nombreDia === previousRowGroup)
            this.rowGroupMetadata[nombreDia].size++;
          else
            this.rowGroupMetadata[nombreDia] = { index: i, size: 1 };
        }
      }
    }

  }

  escucharRutinaPlanEntrenamientoSeleccionado(eventoRutinaPLanEntrenaiento) {
    const seSeleccionoMasDeUnaRutina = this.arregloRutinaPlanEntrenamiento.find(
      planEntrenaminetoRutina => {
        return (
          planEntrenaminetoRutina.dia.nombre === eventoRutinaPLanEntrenaiento.dia.nombre
          && planEntrenaminetoRutina.rutina.nombre === eventoRutinaPLanEntrenaiento.rutina.nombre
        )
      }
    );
    if (seSeleccionoMasDeUnaRutina) {
      this._toasterService.pop('warning', 'Advertencia', `Ya se ha seleccionado esta rutina para el dia ${eventoRutinaPLanEntrenaiento.dia.nombre}`);
    }
    else {
      this.arregloRutinaPlanEntrenamiento.push(eventoRutinaPLanEntrenaiento);
      this.arregloRutinaPlanEntrenamiento = this.arregloRutinaPlanEntrenamiento.map(rutinaPlanEntrenamiento => {
        rutinaPlanEntrenamiento.nombreDia = rutinaPlanEntrenamiento.dia.id;
        return rutinaPlanEntrenamiento;
      });
      this.formularioPlanEntrenamiento.patchValue({ rutinasPlanEntrenamiento: this.arregloRutinaPlanEntrenamiento });
      this.actualizarTabla();
    }
  }

  async imprimir() {
    const promesaDias = this._diaService.findAll({}).toPromise();
    const dias = await promesaDias;
    const  styles = {
      header: {
        fontSize: 12,
        bold: true,
        margin: [0, 0, 0, 10]
      },
      tableExample: {
        margin: [0, 5, 0, 15]
      },
      tableHeader: {
        bold: true,
        fontSize: 10,
        fillColor: '#eeeeee',
        color: 'black',
      }
    }
    abrirPdfNavegador(crearBodyTablePlanEntrenamiento(dias, this.planEntrenamientoSeleccionado), styles)
  }
}

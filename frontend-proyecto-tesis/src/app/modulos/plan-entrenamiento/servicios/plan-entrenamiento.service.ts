import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { PlanEntrenamiento } from '../../../interfaces/plan-entrenamiento.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanEntrenamientoService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/plan-entrenamiento?consulta=` + `${JSON.stringify(consulta)}`);
  }

  create(planEntrenamiento: PlanEntrenamiento): Observable<PlanEntrenamiento> {
    return this._httpClient.post(environment.url + '/plan-entrenamiento/', planEntrenamiento);
  }
  findOne(id: number): Observable<PlanEntrenamiento> {
    return this._httpClient.get(environment.url + `/plan-entrenamiento/${id}`);
  }
  update(id: number, planEntrenamiento: PlanEntrenamiento): Observable<PlanEntrenamiento> {
    return this._httpClient.put(environment.url + `/plan-entrenamiento/${id}`, planEntrenamiento);
  }
}

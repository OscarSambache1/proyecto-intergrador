import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { RutinaPlanEntrenamiento } from '../../../interfaces/rutina-plan-entrenamiento.interface';

@Injectable({
  providedIn: 'root'
})
export class RutinaPlanEntrenamientoService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  findAll(consulta: any): Observable<any> {
    return this._httpClient.get(environment.url + `/rutina-plan-entrenamiento?consulta=` + `${JSON.stringify(consulta)}`)
  }

  create(rutinaPlanEntrenamiento: RutinaPlanEntrenamiento): Observable<RutinaPlanEntrenamiento> {
    return this._httpClient.post(environment.url + '/rutina-plan-entrenamiento/', rutinaPlanEntrenamiento)
  }

  createMany(rutinaPlanEntrenamiento: RutinaPlanEntrenamiento[]): Observable<any>{
    return this._httpClient.post(environment.url + '/rutina-plan-entrenamiento/crear-varios', rutinaPlanEntrenamiento)
  }

  findOne(id: number): Observable<RutinaPlanEntrenamiento> {
    return this._httpClient.get(environment.url + `/rutina-plan-entrenamiento/${id}`)
  }
  update(id: number, rutinaPlanEntrenamiento: RutinaPlanEntrenamiento): Observable<RutinaPlanEntrenamiento> {
    return this._httpClient.put(environment.url + `/rutina-plan-entrenamiento/${id}`, rutinaPlanEntrenamiento)
  }
  delete(id: number): Observable<any> {
    return this._httpClient.delete(environment.url + `/rutina-plan-entrenamiento/${id}`)
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarPlanEntrenamientoComponent } from './componentes/listar-plan-entrenamiento/listar-plan-entrenamiento.component';
import { CrearPlanEntrenamientoComponent } from './componentes/crear-plan-entrenamiento/crear-plan-entrenamiento.component';
import { EditarPlanEntrenamientoComponent } from './componentes/editar-plan-entrenamiento/editar-plan-entrenamiento.component';
import { VerPlanEntrenamientoComponent } from './componentes/ver-plan-entrenamiento/ver-plan-entrenamiento.component';
import { RolesGuard } from '../autenticacion/guards/roles.guard';

const routes: Routes = [
  {
    path:'listar',
    component: ListarPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'crear',
    component: CrearPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path:'editar/:id',
    component: EditarPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado']}
  },
  {
    path: 'ver/:id',
    component : VerPlanEntrenamientoComponent,
    canActivate: [RolesGuard],
    data: { rolesPermitidos: ['administrador', 'empleado', 'instructor']}
  },
  {
    path:'',
    redirectTo: 'listar',
    pathMatch: 'full',  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanEntrenamientoRoutingModule { }

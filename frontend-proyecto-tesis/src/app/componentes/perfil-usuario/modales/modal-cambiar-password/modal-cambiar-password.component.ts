import { Component, OnInit, Inject } from '@angular/core';
import { ICONOS } from 'src/app/constantes/iconos';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from 'src/app/shared/validaciones';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-modal-cambiar-password',
  templateUrl: './modal-cambiar-password.component.html',
  styleUrls: ['./modal-cambiar-password.component.css']
})
export class ModalCambiarPasswordComponent implements OnInit {
  icono = ICONOS.resetearPassword;
  mensajesErroresPasswordActual = [];
  mensajesErroresPasswordNuevo = [];
  formularioCambiarPassword: FormGroup;
  esFormularioValido = false;

  private mensajesValidacionPasswordNuevo = {
    required: "La contraseña nueva es obligatoria",
    minlength: "La contraseña debe tener mínimo 10 caracteres"
  }
  private mensajesValidacionPasswordActual = {
    required: "La contraseña actual es obligatoria"
  }
  constructor(
    public dialogRef: MatDialogRef<ModalCambiarPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly _formBuilder: FormBuilder,
    private readonly _usuarioService: UsuarioService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioCambiarPassword = this._formBuilder.group({
      passwordNuevo: ['', [
        Validators.required,
        Validators.minLength(9)
      ]],
      passwordActual: ['', [
        Validators.required,
      ]]
    })
  }

  escucharFormulario() {
    this.formularioCambiarPassword.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(formulario => {
        this.mensajesErroresPasswordActual = setearMensajes(this.formularioCambiarPassword.get('passwordActual'), this.mensajesValidacionPasswordActual);
        this.mensajesErroresPasswordNuevo = setearMensajes(this.formularioCambiarPassword.get('passwordNuevo'), this.mensajesValidacionPasswordNuevo);
      });

  }


  enviarFormularioValido() {
    this.formularioCambiarPassword.valueChanges.subscribe(formulario => {
      this.esFormularioValido = this.formularioCambiarPassword.valid;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

 async cambiarPassword() {
    this._cargandoService.habilitarCargando();
    const usuario = await this._autenticacionService.obtenerUsuarioLogueado();
    this._usuarioService.cambiarPassword(
      usuario.id,
      this.formularioCambiarPassword.get('passwordNuevo').value,
      this.formularioCambiarPassword.get('passwordActual').value
    )
    .subscribe(respuesta => {
      this._cargandoService.deshabiltarCargando();
      this.dialogRef.close();
      if (respuesta) {
        this._toasterService.pop('success', 'Éxito', 'Su contrseña se ha cambiado correctamente');
      } else {
        this._toasterService.pop('warning', 'Advertencia', 'El password actual es incorrecto');
      }
    },
    error => {
      this._cargandoService.deshabiltarCargando();
      this.dialogRef.close();
      this._toasterService.pop('error', 'Error', 'Error al cambiar la contraeña');
    })
  }
}

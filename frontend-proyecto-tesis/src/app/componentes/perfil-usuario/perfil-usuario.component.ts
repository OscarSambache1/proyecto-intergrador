import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../servicios/usuario.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../../interfaces/usuario';
import { environment } from '../../../environments/environment';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';
import { AutenticacionService } from '../../modulos/autenticacion/autenticacion.service';
import { ICONOS } from 'src/app/constantes/iconos';
import { CargandoService } from 'src/app/servicios/cargando.service';
import { MatDialog } from '@angular/material';
import { ModalCambiarPasswordComponent } from './modales/modal-cambiar-password/modal-cambiar-password.component';

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.css']
})
export class PerfilUsuarioComponent implements OnInit {

  id: any;
  usuarioAEditar: Usuario;
  usuarioEditado: Usuario;
  esFormularioUsuarioValido = false;
  formularioEditarUsuario: FormGroup;
  selectedFile;
  items: MenuItem[];
  urlImagenUsuario;
  icono= ICONOS.cliente;

  constructor(
    private readonly _usuarioService: UsuarioService,
    private readonly _formBuilder: FormBuilder,
    private router: Router,
    private readonly _http: HttpClient,
    private readonly toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.setearUsuario();
    this.crearFormulario();
    this.items=[{label:'Mi perfil'}]
  }

  crearFormulario() {
    this.formularioEditarUsuario = this._formBuilder.group({});
  }

  escucharFormularioUsuario(eventoFormularioUsuario: FormGroup) {
    this.formularioEditarUsuario = eventoFormularioUsuario;
    this.esFormularioUsuarioValido = this.formularioEditarUsuario.valid
  }

  editarPerfil() {
    this._cargandoService.habilitarCargando();
    this.usuarioEditado = this.formularioEditarUsuario.value;
    this._usuarioService.update(this.usuarioAEditar.id, this.usuarioEditado)
      .subscribe(usuario => {
       localStorage.setItem("usuario", JSON.stringify(usuario));
        if (this.selectedFile) {
          this.onUpload(usuario.id)
        }
        this.mostrarToast('success', 'Éxito', 'La informacin se ha actualizado');
        this._cargandoService.deshabiltarCargando();
        this.irInicio();
      }
      , error =>{
        this.mostrarToast('warning', 'Advetencia', 'Falló al actualizar la información')
        this._cargandoService.deshabiltarCargando();
      })

  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0]
  }

  onUpload(id: number) {
    const file2 = new FormData();
    file2.append('file2', this.selectedFile, this.selectedFile.name)
    this._http.post(environment.url + `/usuario/subirArchivo/${id}`, file2)
      .subscribe(response => {
        this.mostrarToast('success', 'Éxito', 'La informacin se ha actualizado');
        this.irInicio();
      })
  }

  async setearUsuario() {
    this._cargandoService.habilitarCargando();
    const usuario = await this._autenticacionService.obtenerUsuarioLogueado();
    this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${usuario.urlFoto}`;
    this.id = usuario.id
    this._usuarioService.findOne(this.id).subscribe(
      usuarioLogueado => {
        this.usuarioAEditar = usuarioLogueado;
        this._cargandoService.deshabiltarCargando();
      }
      ,error => {
        this.mostrarToast('error', 'Error', 'Error al cargar la información');
        this._cargandoService.deshabiltarCargando();
      }
    )
  }

  irInicio() {
    this.router.navigate(['/app', 'inicio']);
  }

  mostrarToast(tipo: string, titulo: string, mensaje: string) {
    this.toasterService.pop(tipo, titulo, mensaje);
  }

  abrirModalCambiarPassword(){
    const dialogRef = this.dialog.open(ModalCambiarPasswordComponent, {
      height:  'auto',
      width: 'auto',
      data: {}
    });

  }
}

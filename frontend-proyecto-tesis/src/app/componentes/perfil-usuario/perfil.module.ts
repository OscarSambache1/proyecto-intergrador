import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { FormularioUsuarioModule } from "../../formularios/formulario-usuario/formulario-usuario.module";
import { ClienteModule } from "src/app/modulos/cliente/cliente.module";
import { FormularioClienteModule } from "../../modulos/cliente/formularios/formulario-cliente/formulario-cliente.module";
import { MigasPanModule } from '../../componentes/migas-pan/migas-pan.module';
import { BarraTituloModule } from "../barra-titulo/barra-titulo.module";
import { PerfilUsuarioComponent } from "./perfil-usuario.component";
import { PanelModule } from "primeng/panel";
import { ModalCambiarPasswordComponent } from './modales/modal-cambiar-password/modal-cambiar-password.component';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from "@angular/material";
import {FieldsetModule} from 'primeng/fieldset';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        FormularioUsuarioModule,
        FormularioClienteModule,
        ClienteModule,
        MigasPanModule,
        BarraTituloModule,
        PanelModule,
        MatDialogModule,
        MatFormFieldModule,
        MatButtonModule,
        FieldsetModule
    ],
    declarations: [
        PerfilUsuarioComponent,
        ModalCambiarPasswordComponent
    ],
    providers: [
        
    ],
    exports:[],
    entryComponents: [ModalCambiarPasswordComponent]
})
export class PerfilModule { }

import { NgModule } from "@angular/core";
import { ListaClientesComponent } from "src/app/componentes/lista-clientes/lista-clientes.component";
import { ClienteModule } from "src/app/modulos/cliente/cliente.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TableModule } from "primeng/table";
import {ToolbarModule} from 'primeng/toolbar';
import { AppRoutingModule } from "src/app/app.routes.module";
import { MigasPanModule } from "../migas-pan/migas-pan.module";

@NgModule(
    {
        imports: [
            ClienteModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule,
            TableModule,
            ToolbarModule,
            AppRoutingModule,
            MigasPanModule
        ],
        declarations: [
            ListaClientesComponent
        ],
        providers: []
    }
)
export class ListaClientesModule {

}
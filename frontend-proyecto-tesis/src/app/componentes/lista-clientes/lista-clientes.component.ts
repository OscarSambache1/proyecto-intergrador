import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { ClienteService } from '../../modulos/cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CargandoService } from '../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {
  clientes: Cliente[] = [];
  parametroBusqueda = '';
  clienteSeleccionado: Cliente;
  modulo = '';
  nombreBoton = '';
  columnas = [
    { field: 'nombres', header: 'Nombres' },
    { field: 'apellidos', header: 'Apellidos' },
    { field: 'codigo', header: 'Codigo' },
    { field: 'acciones', header: 'Acciones' },
  ];
  items: MenuItem[];
  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,

  ) { }

  ngOnInit() {
    this.items = [{ label: 'Cliente' }];
    this.obtenerPorParametro();
    const parametros$ = this._activateRoute.paramMap
    parametros$.subscribe(
      respuestaParam => {
        this.modulo = respuestaParam.get('modulo');
        if (this.modulo === 'recuperar-password') {
          this.nombreBoton = 'resetear password';
        } else {
          this.nombreBoton = 'ver' + respuestaParam.get('titulo');
        }
      }
    );
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    this._clienteService.findWhereOr(this.parametroBusqueda, true)
      .subscribe(clientesPorParametro => {
        this.clientes = clientesPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._cargandoService.deshabiltarCargando();
          this._toasterService.pop('error', 'Error', 'Error al mostrar los clientes');
        }
      )
  }

  clickBoton(cliente) {
    if (this.modulo === 'recuperar-password') {
      this._router.navigate(['/app', 'recuperar-password']);
    } else {
      console.log('modulo', this.modulo)
      this._router.navigate(['/app', 'cliente', cliente.id, this.modulo]);
    }
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { PlanDietaDia } from '../../../../interfaces/pla-dieta-dia.interface';
import { Dieta } from '../../../../interfaces/dieta.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-plan-dieta-dia',
  templateUrl: './modal-plan-dieta-dia.component.html',
  styleUrls: ['./modal-plan-dieta-dia.component.css']
})
export class ModalPlanDietaDiaComponent implements OnInit {

  planDietaDiaACrearEditar: PlanDietaDia;
  idPlanDietaDia: number;
  dieta: Dieta;
  esFormularioPlanDietaDiaValido = false;
  icono = ICONOS.dieta;

  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalPlanDietaDiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if(this.data.dietaSeleccionada){
      this.dieta= this.data.dietaSeleccionada;
    }
    if( this.data.planDietaDia){
      this.planDietaDiaACrearEditar = this.data.planDietaDia;
      this.idPlanDietaDia=this.data.planDietaDia.id;
      this.dieta= this.data.planDietaDia.dieta;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  escucharFormularioPlanDietaDia(eventoFormularioPlanDietaDia: FormGroup) {
    this.esFormularioPlanDietaDiaValido = eventoFormularioPlanDietaDia.valid;
    this.planDietaDiaACrearEditar=eventoFormularioPlanDietaDia.value;
  }

  verificarAccion(){
    if(this.data.planDietaDia){
      this.editarPlanDietaDia();
    } else {
      this.seleccionarPlanDietaDia();
    }
  }
  
  seleccionarPlanDietaDia(){
    this._cargandoService.habilitarCargando();
    this.planDietaDiaACrearEditar.dieta=this.dieta;
    this.dialogRef.close(this.planDietaDiaACrearEditar);
    this._cargandoService.deshabiltarCargando();
  }

  editarPlanDietaDia(){
    this._cargandoService.habilitarCargando();
    if(this.idPlanDietaDia){
      this.planDietaDiaACrearEditar.id=this.idPlanDietaDia;
    }
    this.planDietaDiaACrearEditar.dieta=this.dieta;
    this.dialogRef.close(this.planDietaDiaACrearEditar);
    this._cargandoService.deshabiltarCargando();
   }
}

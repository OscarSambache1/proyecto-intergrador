import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { PlanDietaDia } from '../../../interfaces/pla-dieta-dia.interface';
import { debounceTime } from 'rxjs/operators';
import { setearMensajes } from '../../../shared/validaciones';
import { DiaService } from '../../../servicios/dia.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-formulario-plan-dieta-dia',
  templateUrl: './formulario-plan-dieta-dia.component.html',
  styleUrls: ['./formulario-plan-dieta-dia.component.css']
})
export class FormularioPlanDietaDiaComponent implements OnInit {

  @Output() esValidoFormularioPlanDietaDia: EventEmitter<FormGroup> = new EventEmitter();
  @Input() planDietaDiaSeleccionado: PlanDietaDia;
  formularioPlanDietaDia: FormGroup;
  dias: SelectItem[] = [];
  mensajesErroresDia = [];

  private mensajesValidacionDia = {
    required: "El día es obligatorio",
  };
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _diaService: DiaService,
    private readonly _toasterService: ToasterService,
  ) { }

  ngOnInit() {
    this.llenarDropdownDias();
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
  }

  crearFormulario() {
    this.formularioPlanDietaDia = this._formBuilder.group({
      dia: ['', [Validators.required]],
    })
  }

  escucharFormulario() {
    this.formularioPlanDietaDia.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresDia = setearMensajes(this.formularioPlanDietaDia.get('dia'), this.mensajesValidacionDia);
      });
  }

  enviarFormularioValido() {
    this.formularioPlanDietaDia.valueChanges.subscribe(formulario => {
      this.esValidoFormularioPlanDietaDia.emit(this.formularioPlanDietaDia);
    });
  }


  verificarTipoFormulario() {
    if (this.planDietaDiaSeleccionado) {
      this.llenarFormulario();
    }
  }

  llenarFormulario() {
    this._diaService.findOne(this.planDietaDiaSeleccionado.dia.id)
    .subscribe( dia => { 
      delete dia.clasesDiaHora;
      this.formularioPlanDietaDia.patchValue(
        {
          dia
        }
      );
    });
  }

  llenarDropdownDias() {
    const consulta = { order: { id: 'ASC' } };
    this.dias.push({
      label: 'Seleccione un día',
      value: null
    })
    this._diaService.findAll(consulta).subscribe(dias => {
      dias.forEach(dia => {
        const elemento = {
          label: dia.nombre,
          value: dia
        }
        this.dias.push(elemento)
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al traer datos')
      }
    );
  }

  escucharCambiosDia() {
    const diaControl = this.formularioPlanDietaDia.get('dia')
    diaControl.valueChanges
      .subscribe(valor => {
        this.setearMensajesErroresDia(diaControl)
      }
      )
  }

  setearMensajesErroresDia(valor: AbstractControl) {
    this.mensajesErroresDia = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresDia = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionDia[atributo]
      })
    }
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Dieta } from '../../../interfaces/dieta.interface';
import { SelectItem } from 'primeng/api';
import { TipoDieta } from '../../../interfaces/tipo-dieta.interface';
import { DietaService } from '../../../modulos/dieta/servicios/dieta.service';
import { TipoDietaService } from '../../../modulos/dieta/servicios/tipo-dieta.service';
import { CargandoService } from '../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { PlanDietaDia } from '../../../interfaces/pla-dieta-dia.interface';
import { MatDialog } from '@angular/material';
import { ModalPlanDietaDiaComponent } from '../modales/modal-plan-dieta-dia/modal-plan-dieta-dia.component';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-dietas',
  templateUrl: './tabla-dietas.component.html',
  styleUrls: ['./tabla-dietas.component.css']
})
export class TablaDietasComponent implements OnInit {

  @Output() planDietaDiaSeleccionado: EventEmitter<PlanDietaDia> = new EventEmitter();
  @Input() arregloPlanDietaDia: PlanDietaDia[] = [];
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistro: number;
  arregloDietas: Dieta[] = [];
  parametroBusqueda: any;
  arregloTiposDieta: SelectItem[] = [];
  tipoDietaSeleccionado: TipoDieta;
  cols: any[];
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  filas;
  nombre = '';
  constructor(
    private readonly _dietaServce: DietaService,
    private readonly _tipoDietaService: TipoDietaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    public dialog: MatDialog,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    this.llenarDropdownTiposDieta();
    this.cols = [
      { field: 'nombre', header: 'Nombre', width: "40%" },
      { field: 'tipo', header: 'Tipos', width: "10%" },
      { field: 'estado', header: 'Estado', width: "20%" },
      { field: 'acciones', header: 'Acciones', width: "30%" },
    ];
    if (this.mostrarEnFormulario) {
      this.cols.splice(2, 1);
      this.arregloDietas = [];
    } else {
      this.parametroBusqueda = '';
      const where = !this.mostrarEnFormulario ? null
      : {estado: 1};
      this.obtenerPorParametro(where);
    }
    this.comprobarPermisos();
  }
  
  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorNombre() {
    const where = !this.mostrarEnFormulario ? {nombre: this.nombre}
    : {nombre: this.nombre, estado: 1};
    this.obtenerPorParametro(where);
  }

  obtenerPorParametro(where) {
    this._cargandoService.habilitarCargando();
    const consulta =
    {
      order: { id: 'DESC' },
      where,
      relations: ['comidasDieta', 'planesDietaDia', 'tipo', 'comidasDieta.comida', 'comidasDieta.comida.tipo']
    };

    this._dietaServce.findAll(consulta)
      .subscribe(dietasEncontradas => {
        this.arregloDietas = dietasEncontradas;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar las dietas')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  llenarDropdownTiposDieta() {
    this._cargandoService.habilitarCargando();
    const consulta = { order: { id: 'DESC' } };
    this._tipoDietaService.findAll(consulta).subscribe(tiposDieta => {
      this.arregloTiposDieta.push(
        {
          label: 'todos',
          value: null
        }
      )
      tiposDieta.forEach(tipoDieta => {
        const elemento = {
          label: tipoDieta.nombre,
          value: tipoDieta
        }
        this.arregloTiposDieta.push(elemento)
      });
      this._cargandoService.deshabiltarCargando();
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al mostrar los tipos de dieta')
        this._cargandoService.deshabiltarCargando();
      }
    );
  }

  buscarPorDieta(evento) {
    const verTodosTiposDieta = evento.value == null;
    if (verTodosTiposDieta) {
      const where = !this.mostrarEnFormulario ? null
      : {estado: 1};
      this.obtenerPorParametro(where);
    }
    else {
      this.parametroBusqueda = evento.value.id;
      const where = !this.mostrarEnFormulario ? { tipo: `${this.parametroBusqueda}`}
      : {tipo: `${this.parametroBusqueda}`, estado: 1};
      this.obtenerPorParametro(where);
    }

  }

  abrirModalSeleccionarDieta(dietaSeleccionada: Dieta) {
    const seSeleccionoDieta = this.arregloPlanDietaDia.find(planDietaDia => planDietaDia.dieta.id === dietaSeleccionada.id);
      const dialogRef = this.dialog.open(ModalPlanDietaDiaComponent, {
        height: 'auto',
        width: 'auto',
        data: { dietaSeleccionada }
      });
      dialogRef.afterClosed().subscribe(planDietaDiaSeleccionado => {
        if (planDietaDiaSeleccionado) {
          const seSeleccionoMasDeUnaDieta = this.arregloPlanDietaDia.find(planDietaDia => planDietaDia.dia === planDietaDiaSeleccionado.dia);
          if (seSeleccionoMasDeUnaDieta) {
            this._toasterService.pop('warning', 'Advertencia', `Ya se ha seleccionado dieta para el dia ${planDietaDiaSeleccionado.dia}`);
          }
          else {
            this.planDietaDiaSeleccionado.emit(planDietaDiaSeleccionado);
          }
        }
      });
  }

  cambiarEstado(dieta: Dieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloDietas.indexOf(dieta);
    const estaActivo = this.arregloDietas[indice].estado == 1;
    estaActivo ? this.arregloDietas[indice].estado = 0 : 1;
    !estaActivo ? this.arregloDietas[indice].estado = 1 : 0;
    this._dietaServce.update(this.arregloDietas[indice].id, { estado: this.arregloDietas[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }

}

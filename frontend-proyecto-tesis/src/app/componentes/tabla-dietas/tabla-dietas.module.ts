import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaDietasComponent } from './tabla-dietas/tabla-dietas.component';
import { DietaService } from '../../modulos/dieta/servicios/dieta.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { DropdownModule } from 'primeng/dropdown';
import { DietaRoutingModule } from '../../modulos/dieta/dieta-routing.module';
import { ModalPlanDietaDiaComponent } from './modales/modal-plan-dieta-dia/modal-plan-dieta-dia.component';
import { FormularioPlanDietaDiaComponent } from './formulario-plan-dieta-dia/formulario-plan-dieta-dia.component';
import { DiaService } from '../../servicios/dia.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    DropdownModule,
    DietaRoutingModule
  ],
  declarations: [
    TablaDietasComponent, 
    ModalPlanDietaDiaComponent, 
    FormularioPlanDietaDiaComponent, 
  ],
  exports:[TablaDietasComponent],
  providers:[DietaService, DiaService ],
  entryComponents:[
    ModalPlanDietaDiaComponent,
  ]
})
export class TablaDietasModule { }

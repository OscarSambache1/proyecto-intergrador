import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlanDieta } from '../../../../interfaces/pla-dieta.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { PlanDietaService } from '../../../../modulos/plan-dieta/servicios/plan-dieta.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ModalPlanDietaComponent } from '../../../../modulos/plan-dieta/modales/modal-plan-dieta/modal-plan-dieta.component';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-planes-dieta',
  templateUrl: './tabla-planes-dieta.component.html',
  styleUrls: ['./tabla-planes-dieta.component.css']
})
export class TablaPlanesDietaComponent implements OnInit {

  arregloPlanesDieta: PlanDieta[] = [];
  parametroBusqueda = '';
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistro: number;
  @Input() idCliente: number;
  @Output() planDietaSeleccionado: EventEmitter<PlanDieta> = new EventEmitter()
  @Input() arregloPlanesDietaSeleccionados: PlanDieta[];
  @Input() mostrarBotonAniadir = true;
  columnas = [
    { field: 'nombre', header: 'Nombre', width: '40%' },
    { field: 'estado', header: 'estado', width: '30%' },
    { field: 'acciones', header: 'Acciones', width: '30%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean;

  filas;

  constructor(
    private readonly _planDietaService: PlanDietaService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    public dialog: MatDialog,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    if(this.mostrarEnFormulario){
      this.columnas.splice(1,1);
      this.arregloPlanesDieta = [];
    }
    else {
      this.obtenerPorParametro();
    }
    this.comprobarPermisos();
  }
  
  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ?  { nombre: `${this.parametroBusqueda}`} 
    : 
    { nombre: `${this.parametroBusqueda}`, estado: 1};
    const consulta =
      { order: { id: 'DESC' }, where }

    this._planDietaService.findLike(consulta)
      .subscribe(planesDietaPorParametro => {
        this.arregloPlanesDieta = planesDietaPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar planes de dieta')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  seleccionarPlanDieta(planEntrenamiento) {
    this.planDietaSeleccionado.emit(planEntrenamiento);
  }

  irVistaCrear() {
    if (this.idCliente) {
      this._router.navigate(['/app', 'plan-dieta', 'crear', { idCliente: this.idCliente }]);
    }
    else {
      this._router.navigate(['/app', 'plan-dieta', 'crear']);
    }
  }

  abrirModalVerPlanDieta(planDieta: PlanDieta) {
    const dialogRef = this.dialog.open(ModalPlanDietaComponent, {
      height: '600px',
      width: 'auto',
      data: planDieta
    });
  }


  abrirModalOVistaVerPlanDieta(planDieta) {
    if (this.mostrarEnFormulario) {
      this.abrirModalVerPlanDieta(planDieta);
    } else {
      this._router.navigate(['/app', 'plan-dieta', 'ver', planDieta.id]);
    }
  }

  cambiarEstado(planDieta: PlanDieta) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloPlanesDieta.indexOf(planDieta);
    const estaActivo = this.arregloPlanesDieta[indice].estado == 1;
    estaActivo ? this.arregloPlanesDieta[indice].estado = 0 : 1;
    !estaActivo ? this.arregloPlanesDieta[indice].estado = 1 : 0;
    this._planDietaService.update(this.arregloPlanesDieta[indice].id, { estado: this.arregloPlanesDieta[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaPlanesDietaComponent } from './componentes/tabla-planes-dieta/tabla-planes-dieta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { PlanDietaRoutingModule } from '../../modulos/plan-dieta/plan-dieta-routing.module';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    PlanDietaRoutingModule,
    InputTextModule
  ],
  declarations: [TablaPlanesDietaComponent],
  exports: [TablaPlanesDietaComponent]
})
export class TablaPlanesDietaModule { }

import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { Usuario } from '../../interfaces/usuario';
import { AutenticacionService } from '../../modulos/autenticacion/autenticacion.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {
  items: MenuItem[];
  items2: MenuItem[];
  usuario: Usuario = {};
  opcionesMenuPerfil: MenuItem[];
  mostrarMenu = true;
  urlImagenUsuario;
  constructor(
    private readonly _router: Router,
    private readonly _autenticacionService: AutenticacionService
  ) { }

 async ngOnInit() {
    this.usuario = await this._autenticacionService.obtenerUsuarioLogueado();
    this.urlImagenUsuario = `${environment.url}/imagenes-usuarios/${this.usuario.urlFoto}`;
    this.items = [
      {
        label: 'Clientes',
        icon: 'fas fa-user',
        items: [
          {
            label: 'Gestión',
            icon: 'fas fa-user',
            routerLink: ['/app', 'cliente', 'listar'],
          },
          {
            label: 'Medidas',
            icon: 'fas fa-weight',
            routerLink: ['/app', 'lista-clientes',{modulo:'medidas', titulo:'medidas'}]
          },
          {
            label: 'Planes de entrenamiento',
            icon: 'fas fa-clipboard-list',
            routerLink: [
              '/app', 'lista-clientes',{modulo:'planes-entrenamiento', titulo:'planes de entrenamiento'}
            ]
          },
          {
            label: 'Planes de dieta',
            icon: 'fas fa-clipboard-list',
            routerLink:
            ['/app', 'lista-clientes',{modulo:'planes-dieta', titulo:'planes de dieta'}
          ]
          },
        ]
      },
      {
        label: 'Instructores',
        icon: 'fas fa-user-md',
        routerLink: ['/app', 'instructor', 'listar'],
      },
      {
        label: 'Entrenamiento',
        icon: 'fas fa-dumbbell',
        items: [
          {
            label: 'Ejercicios',
            icon: 'fas fa-dumbbell',
            routerLink: ['/app', 'ejercicio']

          },
          {
            label: 'Rutinas',
            icon: 'fa fa-running',
            routerLink: ['/app', 'rutina']
          },
          {
            label: 'Planes Entrenamiento',
            icon: 'fas fa-clipboard-list',
            routerLink: [
              '/app', 'plan-entrenamiento'
              // '/app', 'lista-clientes',{modulo:'planes-entrenamiento', titulo:'planes de entrenamiento'}
          ]

          }
        ]
      },
      {
        label: 'Nutrición',
        icon: 'fas fa-apple-alt',
        items: [
          {
            label: 'Comidas',
            icon: 'fas fa-apple-alt',
            routerLink: ['/app', 'comida']

          },
          {
            label: 'Dietas',
            icon: 'fas fa-utensils',
            routerLink: ['/app', 'dieta']
          },
          {
            label: 'Planes de Dieta',
            icon: 'fas fa-clipboard-list',
            routerLink: [
              '/app', 'plan-dieta'
            ]
            // routerLink: ['/app', 'lista-clientes',{modulo:'planes-dietas', titulo:'planes de dieta'}]
          }
        ]
      },
      {
        label: 'Clases',
        routerLink: [
          '/app', 'clase'
        ],
        icon: 'fa fa-calendar'
      },
      {
        label: 'Pagos',
        icon: 'fas fa-money-bill-alt',
        items: [
          {
            label: 'Gestión',
            icon: 'fas fa-money-bill-alt',
            routerLink: ['/app', 'pago']
          },
          {
            label: 'Clientes por cobrar',
            icon: 'fas fa-user',
            routerLink: ['/app', 'pago','clientes-por-cobrar']
          },
        ],
      },
      {
        label: 'Productos',
        icon: 'fa fa-shopping-cart',
        routerLink: ['/app', 'producto']
      },
      {
        label: 'Configuración',
        icon: 'fa fa-wrench',
        items: [
          {
            label: 'Músculos',
            icon: 'fa fa-running',
            routerLink: ['/app', 'musculo']
          },
          {
            label: 'Tipos de comida',
            icon: 'fas fa-apple-alt',
            routerLink: ['/app', 'tipo-comida']
          },
          {
            label: 'Tipos de dieta',
            icon: 'fas fa-utensils',
            routerLink: ['/app', 'tipo-dieta']
          },
          {
            label: 'Resetear password',
            icon:'fa fa-key',
            routerLink: ['/app', 'recuperar-password']
          }
        ]
      },
    ];
    const tienePermisosAdministrador = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado']);
    if(!tienePermisosAdministrador) {
      this.items[5].items.splice(1,1)
    }
    const tienePermisosAdministradorEmpleado = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado']);
    if(!tienePermisosAdministradorEmpleado) {
      this.items.splice(7,1)
    }

    this.opcionesMenuPerfil = [
      {
        label: 'Ver Perfil',
        routerLink: ['/app', 'mi-perfil']
      },
      { label: 'Cerrar Sesion', command: () => 
      this._autenticacionService.cerrarSesion().subscribe( respuesta => {
        console.log(respuesta)
      }, error => {
        console.log(error)
      })
      , routerLink: ['/login'] }
    ]


  }

  mostrarMenuPanel() {
    this.mostrarMenu = !this.mostrarMenu
  }

  irPerfil(){
    this._router.navigate(['/app', 'mi-perfil']);
   }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MigasPanComponent } from './componentes/migas-pan/migas-pan.component';
import {BreadcrumbModule} from 'primeng/breadcrumb';

@NgModule({
  imports: [
    CommonModule,
    BreadcrumbModule,
  ],
  declarations: [MigasPanComponent],
  exports: [MigasPanComponent]

})
export class MigasPanModule { }

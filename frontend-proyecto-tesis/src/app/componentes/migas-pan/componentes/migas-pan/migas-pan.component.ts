import { Component, OnInit, Input } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-migas-pan',
  templateUrl: './migas-pan.component.html',
  styleUrls: ['./migas-pan.component.css']
})
export class MigasPanComponent implements OnInit {
  @Input() items: MenuItem[];
  home: MenuItem;

  constructor() { }
  ngOnInit() {
    this.home = {
      icon: 'pi pi-home',  
      routerLink:['/app', 'inicio']
    };
}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaPlanesEntrenamientoComponent } from './componentes/tabla-planes-entrenamiento/tabla-planes-entrenamiento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { PlanEntrenamientoRoutingModule } from '../../modulos/plan-entrenamiento/plan-entrenamiento-routing.module';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    PlanEntrenamientoRoutingModule,
    InputTextModule
  ],
  declarations: [TablaPlanesEntrenamientoComponent],
  exports: [TablaPlanesEntrenamientoComponent]
})
export class TablaPlanesEntrenamientoModule { }

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { PlanEntrenamiento } from '../../../../interfaces/plan-entrenamiento.interface';
import { PlanEntrenamientoService } from '../../../../modulos/plan-entrenamiento/servicios/plan-entrenamiento.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ModalVerPlanEntrenamientoComponent } from 'src/app/modulos/plan-entrenamiento/modales/modal-ver-plan-entrenamiento/modal-ver-plan-entrenamiento.component';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-planes-entrenamiento',
  templateUrl: './tabla-planes-entrenamiento.component.html',
  styleUrls: ['./tabla-planes-entrenamiento.component.css']
})
export class TablaPlanesEntrenamientoComponent implements OnInit {

  arregloPlanesEntrenamiento: PlanEntrenamiento[] = [];
  parametroBusqueda = '';
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistro: number;
  @Input() idCliente: number;
  @Output() planEntrenamientoSeleccionado: EventEmitter<PlanEntrenamiento> = new EventEmitter()
  @Input() arregloPlanesEntrenamienroSeleccionados: PlanEntrenamiento[];
  @Input() mostrarBotonAniadir= true;

  columnas = [
    { field: 'nombre', header: 'Nombre', width: '50%' },
    { field: 'estado', header: 'Estado', width: '20%' },
    { field: 'acciones', header: 'Acciones', width: '30%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  filas;
  
  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    public dialog: MatDialog,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    if(this.mostrarEnFormulario){
      this.columnas.splice(1,1);
      this.arregloPlanesEntrenamiento=[];
    } else {
      this.obtenerPorParametro();
    }
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ?  { nombre: `${this.parametroBusqueda}`} 
    : 
    { nombre: `${this.parametroBusqueda}`, estado: 1};
    const consulta =
      { order: { id: 'DESC' }, where }

    this._planEntrenamientoService.findAll(consulta)
      .subscribe(planesEntrenamientoPorParametro => {
        this.arregloPlanesEntrenamiento = planesEntrenamientoPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar planes de entrenamiento')
          this._cargandoService.deshabiltarCargando();
        }
      )
  }

  seleccionarPlanEntrenamiento(planEntrenamiento){
      this.planEntrenamientoSeleccionado.emit(planEntrenamiento);
  }
  
  irVistaCrear(){
    if(this.idCliente){
      this._router.navigate(['/app','plan-entrenamiento','crear', {idCliente:this.idCliente}]);
    }
    else{
      this._router.navigate(['/app','plan-entrenamiento','crear']);
    }
  }

  cambiarEstado(planEntrenamiento: PlanEntrenamiento) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloPlanesEntrenamiento.indexOf(planEntrenamiento);
    const estaActivo = this.arregloPlanesEntrenamiento[indice].estado == 1;
    estaActivo ? this.arregloPlanesEntrenamiento[indice].estado = 0 : 1;
    !estaActivo ? this.arregloPlanesEntrenamiento[indice].estado = 1 : 0;
    this._planEntrenamientoService.update(this.arregloPlanesEntrenamiento[indice].id, { estado: this.arregloPlanesEntrenamiento[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }

  abrirModalOVistaVerPlanEntrenamiento(planEntrenamiento) {
    if (this.mostrarEnFormulario) {
      this.abrirModalVerplanEntrenamiento(planEntrenamiento);
    } else {
      this._router.navigate(['/app', 'plan-entrenamiento', 'ver', planEntrenamiento.id]);
    }
  }

  abrirModalVerplanEntrenamiento(planEntrenamiento) {
    const dialogRef = this.dialog.open(ModalVerPlanEntrenamientoComponent, {
      height: 'auto',
      width: 'auto',
      data: planEntrenamiento
    });
  }
}

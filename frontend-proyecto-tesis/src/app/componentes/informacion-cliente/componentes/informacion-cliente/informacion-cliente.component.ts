import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../../../../interfaces/cliente.interface';
import * as moment from 'moment';


@Component({
  selector: 'app-informacion-cliente',
  templateUrl: './informacion-cliente.component.html',
  styleUrls: ['./informacion-cliente.component.css']
})
export class InformacionClienteComponent implements OnInit {
  @Input() cliente: Cliente;
  edad: number;

  constructor(

  ) { }

  ngOnInit() {
    this.calcularEdad();
  }

  calcularEdad() {
    const anioNacimiento = moment(this.cliente.usuario.fechaNacimiento, "YYYY-MM-DD").year();
    const anioActual = moment().format('YYYY');
    this.edad = +anioActual - +anioNacimiento;
  }
}

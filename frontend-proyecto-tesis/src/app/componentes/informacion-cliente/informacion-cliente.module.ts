import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformacionClienteComponent } from './componentes/informacion-cliente/informacion-cliente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import {PanelModule} from 'primeng/panel';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    PanelModule,
  ],
  declarations: [InformacionClienteComponent],
  exports: [InformacionClienteComponent]
})
export class InformacionClienteModule { }

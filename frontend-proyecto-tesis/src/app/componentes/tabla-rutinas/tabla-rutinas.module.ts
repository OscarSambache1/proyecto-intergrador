import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaRutinasComponent } from './componentes/tabla-rutinas/tabla-rutinas.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { PanelModule } from 'primeng/panel';
import { RutinaRoutingModule } from '../../modulos/rutina/rutina-routing.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DropdownModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    RutinaRoutingModule,
    ConfirmDialogModule,
    InputTextareaModule,
  ],
  declarations: [TablaRutinasComponent],
  exports: [TablaRutinasComponent]
})
export class TablaRutinasModule { }

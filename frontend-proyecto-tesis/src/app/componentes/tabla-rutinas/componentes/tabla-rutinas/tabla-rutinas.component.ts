import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Rutina } from '../../../../interfaces/rutina.interface';
import { SelectItem, MenuItem } from 'primeng/api';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { RutinaService } from '../../../../modulos/rutina/servicios/rutina.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MusculoService } from '../../../../modulos/ejercicio/servicios/musculo.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { RutinaPlanEntrenamiento } from '../../../../interfaces/rutina-plan-entrenamiento.interface';
import { ModalCrearRutinaPlanEntrenamientoComponent } from '../../../../modulos/plan-entrenamiento/modales/modal-crear-rutina-plan-entrenamiento/modal-crear-rutina-plan-entrenamiento.component';
import { Router } from '@angular/router';
import { ModalVerRutinaComponent } from '../../../../modulos/rutina/modales/modal-ver-rutina/modal-ver-rutina.component';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-rutinas',
  templateUrl: './tabla-rutinas.component.html',
  styleUrls: ['./tabla-rutinas.component.css']
})
export class TablaRutinasComponent implements OnInit {
  @Input() arregloRutinaPlanEntrenamiento: RutinaPlanEntrenamiento[] = [];
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistro: number;
  @Output() rutinaPlanEntrenamientoSeleccionado: EventEmitter<RutinaPlanEntrenamiento> = new EventEmitter()
  arregloRutina: Rutina[]=[];
  parametroBusqueda = '';
  nombre = '';
  arregloMusculos: SelectItem[]=[];
  musculoSeleccionado: Musculo;
  columnas = [
    { field: 'nombre', header: 'Nombre', width: '30%' },
    { field: 'musculos', header: 'Musculos', width: '20%' },
    { field: 'estado', header: 'estado', width: '20%' },
    { field: 'acciones', header: 'Acciones', width: '30%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean
  filas;
  
  constructor(
    private readonly _rutinaService: RutinaService,
    private readonly _musculoService: MusculoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService : ToasterService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _autenticacionService: AutenticacionService

  ) { }

  ngOnInit() {
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    if(this.mostrarEnFormulario){
      this.columnas.splice(2,1);
    }
    this.llenarDropdownMusculos();
    if(this.mostrarEnFormulario){
      this.arregloRutina=[];
    }
    else{
      this.obtenerPorParametro();
    }
    this.comprobarPermisos();
  }

  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }
  obtenerPorParametro(){
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ?  { nombre: `${this.nombre}`} 
    : 
    { nombre: `${this.nombre}`, estado: 1};
    const consulta =
    { order: { id: 'DESC' }, where };
    this._rutinaService.findAll(consulta)
    .subscribe(rutinasPorParametro=>{
      this.arregloRutina=rutinasPorParametro;
      this._cargandoService.deshabiltarCargando();
    },
  error=>{
    this._cargandoService.deshabiltarCargando();
    this._toasterService.pop('error','Error','Error al mostrar rutinas');
  })
  }

  llenarDropdownMusculos(){
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ? null : {estado: 1};
    const consulta ={ order: { id: 'DESC' }, where };
    this._musculoService.findAll(consulta).subscribe(musculos =>{
      this.arregloMusculos.push(
        {
          label: 'todos',
          value: null
        }
      )
      musculos.forEach(musculo => {
        const elemento ={
          label: musculo.nombre,
          value: musculo
        }
        this.arregloMusculos.push(elemento)
      });
    },
    error=>{
      this._toasterService.pop('error','Error','Error al traer datos');

    });
  }

  buscarPorMusculo(evento){
    this._cargandoService.habilitarCargando();
    const verTodosMusculos = evento.value == null
    if(verTodosMusculos){
      this.obtenerPorParametro();
    }
    else{
      const where = !this.mostrarEnFormulario ?  { musculo: `${evento.value.nombre}` } 
      : 
      {musculo: `${evento.value.nombre}`, estado: 1};
      const consulta =
      { order: { id: 'DESC' }, where }
   
      this._rutinaService.findAll(consulta)
      .subscribe(rutinasPorParametro=>{
        this.arregloRutina=rutinasPorParametro;
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._toasterService.pop('error','Error','Error al mostrar rutinas');
        this._cargandoService.deshabiltarCargando();
      }
    )
    }
  }

  abrirModalSeleccionarRutina(rutinaSeleccionada: Rutina) {
      const dialogRef = this.dialog.open(ModalCrearRutinaPlanEntrenamientoComponent, {
        height: 'auto',
        width: 'auto',
        data: rutinaSeleccionada
      });
      dialogRef.afterClosed().subscribe(rutinaPlanEntrenamientoSeleccionado => {
        if (rutinaPlanEntrenamientoSeleccionado) {
          this.rutinaPlanEntrenamientoSeleccionado.emit(rutinaPlanEntrenamientoSeleccionado);
        }
      });
  }

  abrirModalVerRutina(rutinaAVer: Rutina) {
    const dialogRef = this.dialog.open(ModalVerRutinaComponent, {
      height: '600px',
      width: '800px',
      data: rutinaAVer
    });
  }


  abrirModalOVistaVerRutina(rutina){
    if(this.mostrarEnFormulario){
      this.abrirModalVerRutina(rutina);
    } else {
      this._router.navigate(['/app', 'rutina', 'ver' , rutina.id]);
    }
  }

  cambiarEstado(rutina: Rutina) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloRutina.indexOf(rutina);
    const estaActivo = this.arregloRutina[indice].estado == 1;
    estaActivo ? this.arregloRutina[indice].estado = 0 : 1;
    !estaActivo ? this.arregloRutina[indice].estado = 1 : 0;
    this._rutinaService.update(this.arregloRutina[indice].id, { estado: this.arregloRutina[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
          this._cargandoService.deshabiltarCargando();
        });
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-barra-titulo',
  templateUrl: './barra-titulo.component.html',
  styleUrls: ['./barra-titulo.component.css']
})
export class BarraTituloComponent implements OnInit {

  @Input() titulo: string;
  @Input() icono: string;
  urlIcono:any
  constructor(
    private readonly sanitizer: DomSanitizer

  ) { }

  ngOnInit() {
    this.urlIcono = this.sanitizer.bypassSecurityTrustResourceUrl(this.icono);
  }

}

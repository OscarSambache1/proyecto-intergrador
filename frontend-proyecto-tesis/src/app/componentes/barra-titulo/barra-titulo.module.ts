import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarraTituloComponent } from './componentes/barra-titulo/barra-titulo.component';
import {ToolbarModule} from 'primeng/toolbar';
import { PanelModule } from 'primeng/panel';


@NgModule({
  imports: [
    CommonModule,
    ToolbarModule,
    PanelModule,

  ],
  declarations: [BarraTituloComponent],
  exports: [BarraTituloComponent]
})
export class BarraTituloModule { }

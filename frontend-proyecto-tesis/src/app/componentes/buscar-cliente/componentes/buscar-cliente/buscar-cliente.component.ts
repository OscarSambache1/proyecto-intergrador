import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../../interfaces/cliente.interface';
import { MenuItem } from 'primeng/api';
import { ClienteService } from '../../../../modulos/cliente/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CargandoService } from '../../../../servicios/cargando.service';
import { UsuarioService } from '../../../../servicios/usuario.service';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-buscar-cliente',
  templateUrl: './buscar-cliente.component.html',
  styleUrls: ['./buscar-cliente.component.css']
})
export class BuscarClienteComponent implements OnInit {

  clientes: Cliente[]=[];
  codigo='';
  clienteSeleccionado:Cliente;
  modulo='';
  nombreBoton='';
  columnas=[
    { field: 'nombres', header: 'Nombres', width:'30%' },
    { field: 'apellidos', header: 'Apellidos', width:'30%' },
    { field: 'codigo', header: 'Codigo', width:'10%' },
    { field: 'acciones', header: 'Acciones', width:'30%' },
  ];
  items: MenuItem[];
  icono= ICONOS.cliente;

  constructor(
    private readonly _clienteService: ClienteService,
    private readonly _activateRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,

  ) { }

  ngOnInit() {
    this.items= [{label: 'Cliente'}];
    const parametros$ = this._activateRoute.paramMap
        parametros$.subscribe(
            respuestaParam=>{
                this.modulo=respuestaParam.get('modulo');
                this.nombreBoton='ver ' + respuestaParam.get('titulo');
            }
        );
  }

  obtenerPorParametro(){
    this._cargandoService.habilitarCargando();
    const verActivos = true;
    this._clienteService.findWhereOr(this.codigo, verActivos)
    .subscribe(clientes=>{
      this.clientes=clientes;
      this._cargandoService.deshabiltarCargando();
    },
    error=>{
      this._cargandoService.deshabiltarCargando();
      this._toasterService.pop('error','Error','Error al mostrar los clientes');
    }
  )
  }

  clickBoton(cliente) {
      this._router.navigate(['/app', 'cliente', cliente.id, this.modulo]);
  }
}

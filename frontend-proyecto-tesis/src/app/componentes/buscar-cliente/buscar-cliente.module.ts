import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuscarClienteComponent } from './componentes/buscar-cliente/buscar-cliente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../../app.routes.module';
import { MigasPanModule } from '../migas-pan/migas-pan.module';
import { ClienteModule } from '../../modulos/cliente/cliente.module';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { BarraTituloModule } from '../barra-titulo/barra-titulo.module';

@NgModule({
  imports: [
    ClienteModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    TableModule,
    AppRoutingModule,
    MigasPanModule,
    BarraTituloModule
  ],
  declarations: [BuscarClienteComponent],
  exports: [BuscarClienteComponent] 
})
export class BuscarClienteModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaEjerciciosComponent } from './componentes/tabla-ejercicios/tabla-ejercicios.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EjercicioRoutingModule } from '../../modulos/ejercicio/ejercicio-routing.module';
import { TableModule } from 'primeng/table';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EjercicioRoutingModule,
    TableModule,
    InputTextareaModule,
    DropdownModule,
    AutoCompleteModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
  ],
  declarations: [TablaEjerciciosComponent, ],
  exports: [TablaEjerciciosComponent],
})
export class TablaEjerciciosModule { }

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Ejercicio } from '../../../../interfaces/ejercicio.interface';
import { EjercicioService } from '../../../../modulos/ejercicio/servicios/ejercicio.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { ToasterService } from 'angular2-toaster';
import { MusculoService } from '../../../../modulos/ejercicio/servicios/musculo.service';
import { Musculo } from '../../../../interfaces/musculo.interface';
import { SelectItem } from 'primeng/api';
import { EjercicioMusculoService } from '../../../../modulos/ejercicio/servicios/ejercicio-musculo.service';
import { EjercicioRutina } from '../../../../interfaces/ejercicio-rutina.interface';
import { ModalCrearEjercicioRutinaComponent } from '../../../../modulos/rutina/modales/modal-crear-ejercicio-rutina/modal-crear-ejercicio-rutina.component';
import { MatDialog } from '@angular/material';
import { ModalVerEjercicioComponent } from 'src/app/modulos/ejercicio/modales/modal-ver-ejercicio/modal-ver-ejercicio.component';
import { Router } from '@angular/router';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-ejercicios',
  templateUrl: './tabla-ejercicios.component.html',
  styleUrls: ['./tabla-ejercicios.component.css']
})
export class TablaEjerciciosComponent implements OnInit {
  arregloMusculos: SelectItem[]=[];
  arregloEjercicios: Ejercicio[] = [];
  @Output() musculoSeleccionado:  EventEmitter<Musculo> = new EventEmitter();
  @Output() ejercicioRutinaSeleccionado: EventEmitter<EjercicioRutina> = new EventEmitter();
  @Input() musculo:  Musculo;
  @Input()  arregloEjerciciosRutina: EjercicioRutina[] = [];
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistros;
  musculoNgModel: Musculo;
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  nombre: string;
  columnas = [
    { field: 'nombre', header: 'Nombre', width: '40%' },
    { field: 'musculos', header: 'Músculos', width: '10%' },
    { field: 'estado', header: 'Estado', width: '20%' },
    { field: 'acciones', header: 'Acciones', width: '30%' },
  ];
  filas;

  constructor(
    private readonly _ejercicioService: EjercicioService,
    private readonly _ejercicioMusculoService: EjercicioMusculoService,
    private readonly _musculoService: MusculoService,
    private readonly _cargandoService: CargandoService,
    private readonly _toasterService: ToasterService,
    private readonly _router: Router,
    public dialog: MatDialog,
    private readonly _autenticacionService: AutenticacionService
  ) { 
     }

  ngOnInit() {
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    if(this.mostrarEnFormulario){
      this.columnas.splice(2,1);
    }
    this.llenarDropdownMusculos();
    if(this.musculo){
    const consulta = {
      where:{
        nombre: this.musculo
      }
    }
    this._musculoService.findAll(consulta).
    subscribe(musculos=>{
      this.musculoNgModel=musculos[0];
      this.obtenerPorParametro();
    });
    }
    else{
      if(this.mostrarEnFormulario){
        this.arregloEjercicios=[];
      }
      else{
        this.obtenerTodosEjercicios();
      }
    }
    this.comprobarPermisos();
  }
  
  async comprobarPermisos(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
  }

  llenarDropdownMusculos() {
    const where = !this.mostrarEnFormulario ? null : {estado: 1};
    const consulta = { order: { id: 'DESC' }, where};
    this._musculoService.findAll(consulta).subscribe(musculos => {
      if(!this.mostrarEnFormulario){
        this.arregloMusculos.push(
          {
            label: "seleccione un músculo",
            value: null
          }
        );
      }

      musculos.forEach(musculo => {
        const elemento = {
          label: musculo.nombre,
          value: musculo
        }
        this.arregloMusculos.push(elemento);
      });
    });
  }

  escucharEventoMusculo(evento){
    const musculoSeleccionado = evento.value;
    this.musculoSeleccionado.emit(musculoSeleccionado);
    const seSeleccionoMusculo = musculoSeleccionado != null;
    if (seSeleccionoMusculo) {
      this.musculoNgModel = musculoSeleccionado;
      this.obtenerPorParametro();
    }
    else {
      this.obtenerTodosEjercicios();
    };
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    let musculoId;
    if( !this.musculoNgModel) {
      musculoId = null;
    } else {
      musculoId = this.musculoNgModel.id;
    }
      const consulta =
      {
        order: { id: 'DESC' },
        where: {musculo: musculoId},
        relations: ['ejercicio','musculo','ejercicio.ejerciciosMusculo', 'ejercicio.ejerciciosMusculo.musculo']
      }
      this.arregloEjercicios = [];
      this._cargandoService.habilitarCargando();
      this._ejercicioMusculoService.findAll(consulta)
      .subscribe(ejerciciosPorMusculo => {
        ejerciciosPorMusculo.forEach(ejercicioMusculo => {
          if (ejercicioMusculo.ejercicio.estado === 1 && this.mostrarEnFormulario) {
            this.arregloEjercicios.push(ejercicioMusculo.ejercicio);
          }
          if (!this.mostrarEnFormulario) {
            this.arregloEjercicios.push(ejercicioMusculo.ejercicio);
          }
        });
        this._cargandoService.deshabiltarCargando();
      },
      error=>{
        this._cargandoService.deshabiltarCargando();
        this._toasterService.pop('error','Error','Error mostrar ejercicios');
     }
    )
  }

  obtenerTodosEjercicios(){
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ?  { nombre: this.nombre } 
    : 
    { nombre: this.nombre, estado: 1};
    const consulta =
      { 
        order: { id: 'DESC' }, 
        where,
        relations: ['ejerciciosMusculo','ejerciciosRutina', 'ejerciciosMusculo.musculo','ejerciciosMusculo.ejercicio']
      }

    this._ejercicioService.findAll(consulta)
      .subscribe(ejerciciosPorParametro => {
        this.arregloEjercicios = ejerciciosPorParametro;
        this._cargandoService.deshabiltarCargando();
      }
        ,
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar los ejercicios')
          this._cargandoService.deshabiltarCargando();
        });
  }


  abrirModalSeleccionarEjercicioRutina(ejercicio: Ejercicio) {
    const seSeleccionoEjercicio = this.arregloEjerciciosRutina.find(ejercicioRutina => ejercicioRutina.ejercicio.id === ejercicio.id);
    if (seSeleccionoEjercicio) {
      this._toasterService.pop('warning', 'Advertencia', 'El ejercicio ya se ha seleccionado');
    }
    else {
      const dialogRef = this.dialog.open(ModalCrearEjercicioRutinaComponent, {
        height: 'auto',
        width: '500px',
        data: ejercicio
      });
      dialogRef.afterClosed().subscribe(ejercicioRutinaSeleccionado => {
        if (ejercicioRutinaSeleccionado) {
        this.ejercicioRutinaSeleccionado.emit(ejercicioRutinaSeleccionado)
        }
      });
    }
  }

  cambiarEstado(ejercicio: Ejercicio) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloEjercicios.indexOf(ejercicio);
    const estaActivo = this.arregloEjercicios[indice].estado == 1;
    estaActivo ? this.arregloEjercicios[indice].estado = 0 : 1;
    !estaActivo ? this.arregloEjercicios[indice].estado = 1 : 0;
    this._ejercicioService.update(this.arregloEjercicios[indice].id, { estado: this.arregloEjercicios[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
        this._cargandoService.deshabiltarCargando();
      });
  }

  abrirModalOVistaVerEjercicio(Ejercicio) {
    if (this.mostrarEnFormulario) {
      this.abrirModalVerEjercicio(Ejercicio);
    } else {
      this._router.navigate(['/app', 'ejercicio', 'ver', Ejercicio.id]);
    }
  }

  abrirModalVerEjercicio(Ejercicio) {
    const dialogRef = this.dialog.open(ModalVerEjercicioComponent, {
      height: 'auto',
      width: '700px',
      data: Ejercicio
    });
  }
}

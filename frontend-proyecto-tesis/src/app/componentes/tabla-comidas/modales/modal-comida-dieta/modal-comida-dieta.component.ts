import { Component, OnInit, Inject } from '@angular/core';
import { ComidaDieta } from '../../../../interfaces/comidas-dieta.ineterface';
import { Comida } from '../../../../interfaces/comida.interface';
import { CargandoService } from '../../../../servicios/cargando.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { ICONOS } from '../../../../constantes/iconos';

@Component({
  selector: 'app-modal-comida-dieta',
  templateUrl: './modal-comida-dieta.component.html',
  styleUrls: ['./modal-comida-dieta.component.css']
})
export class ModalComidaDietaComponent implements OnInit {

  dietaComidaACrearEditar: ComidaDieta;
  idComidaDieta: number;
  comida: Comida;
  esFormularioComidaDietaValida = false;
  icono = ICONOS.comida;
  
  constructor(
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<ModalComidaDietaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if(this.data.comida){
      this.comida= this.data.comida;
    }

    if(this.data.comidaDieta){
      this.dietaComidaACrearEditar=this.data.comidaDieta;
      this.comida=this.dietaComidaACrearEditar.comida;
      this.idComidaDieta=this.dietaComidaACrearEditar.id;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  escucharFormularioComidaDieta(eventoFormularioComidaDieta: FormGroup) {
    this.esFormularioComidaDietaValida = eventoFormularioComidaDieta.valid;
    this.dietaComidaACrearEditar
    =eventoFormularioComidaDieta.value;
  }

  guardarComidaDieta(){
    this._cargandoService.habilitarCargando();
    if(this.dietaComidaACrearEditar.id){
      this.dietaComidaACrearEditar.id=this.data.comidaDieta.id;
    }
    this.dietaComidaACrearEditar.comida=this.comida;
    this.dialogRef.close(this.dietaComidaACrearEditar);
    this._cargandoService.deshabiltarCargando();
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comida } from '../../../../interfaces/comida.interface';
import { SelectItem, ConfirmationService } from 'primeng/api';
import { ComidaService } from '../../../../modulos/comida/servicios/comida.service';
import { CargandoService } from '../../../../servicios/cargando.service';
import { TipoComidaService } from '../../../../modulos/comida/servicios/tipo-comida.service';
import { ToasterService } from 'angular2-toaster';
import { ComidaDieta } from '../../../../interfaces/comidas-dieta.ineterface';
import { MatDialog } from '@angular/material';
import { ModalComidaDietaComponent } from '../../modales/modal-comida-dieta/modal-comida-dieta.component';
import { FILAS_TABLAS } from 'src/app/constantes/filas-tablas';
import { AutenticacionService } from 'src/app/modulos/autenticacion/autenticacion.service';

@Component({
  selector: 'app-tabla-comida',
  templateUrl: './tabla-comida.component.html',
  styleUrls: ['./tabla-comida.component.css']
})
export class TablaComidaComponent implements OnInit {
  @Output() comidaDietaSeleccionado: EventEmitter<ComidaDieta> = new EventEmitter();
  @Input() arregloComidasSeleccionadas: ComidaDieta[];
  @Input() mostrarEnFormulario = false;
  @Input() numeroRegistros;
  arregloComidas: Comida[] = [];
  arregloTiposComida: SelectItem[] = [];
  parametroBusqueda: any;
  columnas = [
    {field: 'numero', header: '#', width: '5%'},
    { field: 'tipo', header: 'Tipo', width: '15%' },
    { field: 'descripcion', header: 'Descripción', width: '40%' },
    { field: 'estado', header: 'Estado', width: '20%' },
    { field: 'acciones', header: 'Acciones', width: '20%' },
  ];
  habilitarBoton: boolean;
  mostrarBoton: boolean;
  filas;
  descripcion = '';

  constructor(
    private readonly _comidaService: ComidaService,
    private readonly _cargandoService: CargandoService,
    private readonly _tipoComidaService: TipoComidaService,
    private readonly _toasterService: ToasterService,
    public dialog: MatDialog,
    private readonly _confirmationService: ConfirmationService,
    private readonly _autenticacionService: AutenticacionService
  ) { }

  ngOnInit() {
    this.configuraciones();
  }

  async configuraciones(){
    this.habilitarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador'])
    this.mostrarBoton = await this._autenticacionService.habilitarBotonSegunRol(['administrador', 'empleado'])
    this.filas = this.mostrarEnFormulario ? FILAS_TABLAS.formulario : FILAS_TABLAS.listar;
    if(this.mostrarEnFormulario){
      this.columnas.splice(4,1);
      this.arregloComidas  = [];
    } else {
      if (!this.mostrarBoton){
        this.columnas.splice(4,1);
      }
      this.parametroBusqueda = '';
      this.obtenerPorParametro();
    }
    this.llenaDropdownTiposComidas();
  }

  llenaDropdownTiposComidas() {
    this._cargandoService.habilitarCargando();
    const consulta = { order: { id: 'ASC' }};
    this._tipoComidaService.findAll(consulta).subscribe(tiposComida => {
      this.arregloTiposComida.push(
        {
          label: "todos",
          value: null
        }
      );
      tiposComida.forEach(tipoComida => {
        const elemento = {
          label: tipoComida.nombre,
          value: tipoComida
        }
        this.arregloTiposComida.push(elemento);
        this._cargandoService.deshabiltarCargando();
      });
    },
      error => {
        this._toasterService.pop('error', 'Error', 'Error al mostrar los tipos de comida')
        this._cargandoService.deshabiltarCargando();
      });
  }

  obtenerPorParametro() {
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ? { tipo: `${this.parametroBusqueda}`}
     : {tipo: `${this.parametroBusqueda}`, estado: 1};
    const consulta =
    {
      order: { id: 'DESC' }, 
      where,
      relations: ['tipo']
    };

    this._comidaService.findAll(consulta)
      .subscribe(comidasEncontradas => {
        this.arregloComidas = comidasEncontradas;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar las comidas')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerPorDescripcion() {
    this._cargandoService.habilitarCargando();
    const where = !this.mostrarEnFormulario ? { descripcion: `${this.descripcion}`}
     : {descripcion: `${this.descripcion}`, estado: 1};
    const consulta =
    {
      order: { id: 'DESC' }, 
      where,
      relations: ['tipo']
    };

    this._comidaService.findAll(consulta)
      .subscribe(comidasEncontradas => {
        this.arregloComidas = comidasEncontradas;
        this._cargandoService.deshabiltarCargando();
      },
        error => {
          this._toasterService.pop('error', 'Error', 'Error al mostrar las comidas')
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharEventoTipoComida(evento) {
    const tipoComidaSeleccionado = evento.value;
    const seSeleccionoTipoComida = tipoComidaSeleccionado != null;
    if (seSeleccionoTipoComida) {
      this.parametroBusqueda = tipoComidaSeleccionado.id
    }
    else this.parametroBusqueda = '';
    this.obtenerPorParametro();
  }

  abrirModalComidaDieta(comidaSeleccionada: Comida) {
    const seSeleccionoComida = this.arregloComidasSeleccionadas.find(comidaDieta => comidaDieta.comida.id === comidaSeleccionada.id);
    const seSeleccionoMasDeUnaComidaDeMismoTipo = this.arregloComidasSeleccionadas.find(comidaDieta => comidaDieta.comida.tipo.nombre === comidaSeleccionada.tipo.nombre);
    if (seSeleccionoComida) {
      this._toasterService.pop('warning', 'Advertencia', 'La comida ya se ha seleccionado');
    }
    else {
      if (seSeleccionoMasDeUnaComidaDeMismoTipo) {
        this._toasterService.pop('warning', 'Advertencia', `La comida tipo ${comidaSeleccionada.tipo.nombre} ya se ha seleccionado`);
      }
      else {
        const dialogRef = this.dialog.open(ModalComidaDietaComponent, {
          height: 'auto',
          width: 'auto',
          data: { comida:comidaSeleccionada }

        });
        dialogRef.afterClosed().subscribe(comidaDietaSeleccionada => {
          if (comidaDietaSeleccionada) {
            this.comidaDietaSeleccionado.emit(comidaDietaSeleccionada);
          }
        });
      }
    }

  }

  cambiarEstado(comida: Comida) {
    this._cargandoService.habilitarCargando();
    const indice = this.arregloComidas.indexOf(comida);
    const estaActivo = this.arregloComidas[indice].estado == 1;
    estaActivo ? this.arregloComidas[indice].estado = 0 : 1;
    !estaActivo ? this.arregloComidas[indice].estado = 1 : 0;
    this._comidaService.update(this.arregloComidas[indice].id, { estado: this.arregloComidas[indice].estado })
      .subscribe(registroEditado => {
        this._cargandoService.deshabiltarCargando();
      }
      ,
      error => {
        this._toasterService.pop('error', 'Error', 'Error al cambiar estado');
        this._cargandoService.deshabiltarCargando();
      });
  }
  
}

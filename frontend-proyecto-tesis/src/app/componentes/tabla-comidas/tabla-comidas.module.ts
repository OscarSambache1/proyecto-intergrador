import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaComidaComponent } from './componentes/tabla-comida/tabla-comida.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToolbarModule } from 'primeng/toolbar';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { MatDialogModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { PanelModule } from 'primeng/panel';
import { ComidaRoutingModule } from '../../modulos/comida/comida-routing.module';
import { ModalComidaDietaComponent } from './modales/modal-comida-dieta/modal-comida-dieta.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormularioComidaDietaComponent } from './formulario-comida-dieta/formulario-comida-dieta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ToolbarModule,
    DropdownModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    PanelModule,
    ComidaRoutingModule,
    ConfirmDialogModule,
    InputTextareaModule,

  ],
  declarations: [
    TablaComidaComponent, 
    ModalComidaDietaComponent,
    FormularioComidaDietaComponent
  ],
  exports:[
    TablaComidaComponent
  ],
  entryComponents:[
    ModalComidaDietaComponent
  ],
  providers:[ConfirmationService]
})
export class TablaComidasModule { }

import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ComidaDieta } from '../../../interfaces/comidas-dieta.ineterface';
import { setearMensajes } from '../../../shared/validaciones';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-formulario-comida-dieta',
  templateUrl: './formulario-comida-dieta.component.html',
  styleUrls: ['./formulario-comida-dieta.component.css']
})
export class FormularioComidaDietaComponent implements OnInit {

  @Output() esValidoFormularioComidaDieta: EventEmitter<FormGroup> = new EventEmitter();
  @Input() comidaDietaSeleccionado: ComidaDieta;
  formularioComidaDieta: FormGroup;
  mensajesErroresHora = [];
  private mensajesValidacionHora = {
    required: "La Hora es obligatoria",
  };

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
    this.enviarFormularioValido();
    this.escucharFormulario();
  }

  crearFormulario() {
    this.formularioComidaDieta = this._formBuilder.group({
      hora: ['', [Validators.required]],
    })
  }

  escucharFormulario() {
    this.formularioComidaDieta.valueChanges
      .pipe(debounceTime(0))
      .subscribe(() => {
        this.mensajesErroresHora = setearMensajes(this.formularioComidaDieta.get('hora'), this.mensajesValidacionHora);
      });
  }

  enviarFormularioValido() {
    this.formularioComidaDieta.valueChanges.subscribe(formulario => {
      this.esValidoFormularioComidaDieta.emit(this.formularioComidaDieta);
    });
  }


  verificarTipoFormulario() {
    if (this.comidaDietaSeleccionado) {
      this.llenarFormulario();
    }
  }

  llenarFormulario() {
    this.formularioComidaDieta.patchValue(
      {
        hora: this.comidaDietaSeleccionado.hora,
      }
    )
  }

}

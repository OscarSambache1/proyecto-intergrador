import { Route, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core';
import { LoginComponent } from './modulos/autenticacion/login/login.component';
import { InicioComponent } from './rutas/inicio/inicio.component';
import { ModulosComponent } from './componentes/modulos/modulos.component';
import { IngresoLoginGuard } from './modulos/autenticacion/guards/ingreso-login.guard';
import { IngresoModulosGuard } from './modulos/autenticacion/guards/ingreso-modulos.guard';
import { PerfilUsuarioComponent } from './componentes/perfil-usuario/perfil-usuario.component';
import { BuscarClienteComponent } from './componentes/buscar-cliente/componentes/buscar-cliente/buscar-cliente.component';

const rutas: Route[] = [
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [IngresoLoginGuard],
    },

    {
        path: 'app',
        component: ModulosComponent,
        canActivate: [IngresoModulosGuard],
        children: [
            {
                path: 'inicio',
                component: InicioComponent,
            },
            {
                path: 'cliente',
                loadChildren: 'src/app/modulos/cliente/cliente.module#ClienteModule',
            },
            {
                path: 'instructor',
                loadChildren: 'src/app/modulos/instructor/instructor.module#InstructorModule',
            },
            {
                path: 'producto',
                loadChildren: 'src/app/modulos/producto/producto.module#ProductoModule',
            },
            {
                path: 'ejercicio',
                loadChildren: 'src/app/modulos/ejercicio/ejercicio.module#EjercicioModule'
            },
            {
                path:'rutina',
                loadChildren: 'src/app/modulos/rutina/rutina.module#RutinaModule'
            },
            {
                path:'plan-entrenamiento',
                loadChildren: 'src/app/modulos/plan-entrenamiento/plan-entrenamiento.module#PlanEntrenamientoModule'
            },
            {
                path: 'comida',
                loadChildren: 'src/app/modulos/comida/comida.module#ComidaModule'
            },
            {
                path: 'dieta',
                loadChildren: 'src/app/modulos/dieta/dieta.module#DietaModule'
            },
            {
                path:'plan-dieta',
                loadChildren: 'src/app/modulos/plan-dieta/plan-dieta.module#PlanDietaModule'
            },
            {
                path: 'pago',
                loadChildren: 'src/app/modulos/factura/factura.module#FacturaModule'
            },
            {
                path: 'clase',
                loadChildren: 'src/app/modulos/clase/clase.module#ClaseModule'
            },
            {
                path: 'musculo',
                loadChildren: 'src/app/modulos/musculo/musculo.module#MusculoModule'
            },
            {
                path: 'tipo-comida',
                loadChildren: 'src/app/modulos/tipo-comida/tipo-comida.module#TipoComidaModule'
            },
            {
                path: 'tipo-dieta',
                loadChildren: 'src/app/modulos/tipo-dieta/tipo-dieta.module#TipoDietaModule'
            },
            {
                path: 'recuperar-password',
                loadChildren: 'src/app/modulos/recuperar-password/recuperar-password.module#RecuperarPasswordModule'
            },
            {
                path: 'mi-perfil',
                component: PerfilUsuarioComponent,
            },
            {
                path: 'lista-clientes',
                component: BuscarClienteComponent,
            },

        ]
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },


]

@NgModule({
    imports: [RouterModule.forRoot(rutas, { useHash: true })],
    exports: [RouterModule]
})

export class AppRoutingModule {
}


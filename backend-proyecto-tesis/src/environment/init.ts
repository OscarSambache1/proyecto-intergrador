import { CONFIG_ENVIRONMENT } from './config';

export async function init() {
  let existeArchivoLocal = false;
  let config;
  try {
    config = require('./local.ts').CONFIG_ENVIRONMENT;
    existeArchivoLocal = true;
    console.log('Hay local', config);
  } catch (e) {
    console.log('No hay local');
  }

  if (existeArchivoLocal) {
    // realizar cualquier tipo de configuración de producción aquí
    CONFIG_ENVIRONMENT.dbConnections = config.dbConnections;
    CONFIG_ENVIRONMENT.redisConnection = config.redisConnection;
    CONFIG_ENVIRONMENT.port = config.port;
    CONFIG_ENVIRONMENT.expressSession = config.expressSession;
  } else {
    // No hacer nada
  }
  console.log(CONFIG_ENVIRONMENT);
}

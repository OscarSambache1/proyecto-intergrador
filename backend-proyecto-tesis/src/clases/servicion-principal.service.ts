import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, Entity } from 'typeorm';

@Injectable()
export class PrincipalService<Entidad, CrearDto, EditarDto> {
  constructor(
    @InjectRepository(Entity)
    private readonly _entidadRepository: Repository<any>,
  ) {}

  async findAll(skip: number, limit: number): Promise<Entidad[]> {
    return await this._entidadRepository.find({
      order: {
        id: 'DESC',
      },
      skip,
      take: limit,
    });
  }

  async findById(id: number): Promise<Entidad> {
    return await this._entidadRepository.findOne(id);
  }

  async createOne(elemento: CrearDto): Promise<Entidad> {
    return await this._entidadRepository.save(
      this._entidadRepository.create(elemento),
    );
  }

  async update(id: number, elemento: EditarDto): Promise<Entidad> {
    await this._entidadRepository.update(id, elemento);
    return await this.findById(id);
  }

  async delete(id: number): Promise<string> {
    const elementoAEliminar = await this.findById(id);
    await this._entidadRepository.remove(elementoAEliminar);
    return `Se ha eliminado el elemento ${id}`;
  }

  async contar(consulta: object): Promise<number> {
    return await this._entidadRepository.count({
      where: consulta,
    });
  }
}

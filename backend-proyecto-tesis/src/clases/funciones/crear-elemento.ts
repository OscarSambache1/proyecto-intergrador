export function crearElemento<CrearDto>(cliente: CrearDto): CrearDto {
   const clienteACrear: any = {};
   Object.keys(cliente).map(atributo => {
    clienteACrear[atributo] = cliente[atributo];
  });
   return clienteACrear;
}

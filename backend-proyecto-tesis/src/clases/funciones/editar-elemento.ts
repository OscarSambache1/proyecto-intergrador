export function editarElemento(cliente: any): any {
  const elementoAEditar: any = {};
  Object.keys(cliente).map(atributo => {
    elementoAEditar[atributo] = cliente[atributo];
  });
  return elementoAEditar;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
} from '@nestjs/common';

import { validate } from 'class-validator';
import { Paginacion } from 'interfaces/paginacion';
import { PrincipalService } from './servicion-principal.service';
import { editarElemento } from './funciones/editar-elemento';

@Controller()
export class PrincipakController<Entidad, CrearDto, EditarDto> {
  constructor(
    private readonly _principalService: PrincipalService<
      Entidad,
      CrearDto,
      EditarDto
    >,
  ) {}

  @Get('')
  async findAll(@Query() objetosPaginacion: Paginacion): Promise<Entidad[]> {
    return this._principalService.findAll(
      objetosPaginacion.skip,
      objetosPaginacion.limite,
    );
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Entidad> {
    const clienteEncontrado = await this._principalService.findById(id);
    if (clienteEncontrado) {
      return await this._principalService.findById(id);
    } else {
      throw new BadRequestException('cliente no existe');
    }
  }

  @Post()
  async crear(@Body(`cliente`) cliente: CrearDto): Promise<Entidad> {
    // const clienteACrearse = crearElemento<CrearDto>(cliente)
    const arregloErrores = await validate(cliente);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: creando al cliente', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._principalService.createOne(cliente);
    }
  }

  @Put(':id')
  async editar(
    @Param() id: number,
    @Body() cliente: EditarDto,
  ): Promise<Entidad> {
    const clienteEncontrado = await this._principalService.findById(id);
    if (clienteEncontrado) {
      const clienteAEditarse = editarElemento(cliente);
      const arregloErrores = await validate(clienteAEditarse);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando al cliente', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._principalService.update(id, clienteAEditarse);
      }
    } else {
      throw new BadRequestException('usuario no encotrado');
    }
  }

  @Delete(':id')
  // tslint:disable-next-line:ban-types
  async eliminar(@Param('id') id: number): Promise<String> {
    const clienteEncontrado = await this._principalService.findById(id);
    if (clienteEncontrado) {
      return await this._principalService.delete(id);
    } else {
      throw new BadRequestException('Cliente no existe');
    }
  }
}

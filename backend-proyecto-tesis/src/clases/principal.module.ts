import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrincipalService } from './servicion-principal.service';
import { Entity } from 'typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Entity], 'default')],
  controllers: [],
  providers: [PrincipalService],
  exports: [PrincipalService],
})
export class PrincipalModule {}

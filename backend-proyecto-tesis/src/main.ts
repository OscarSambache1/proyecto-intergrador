import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';
import { CONFIG_ENVIRONMENT } from './environment/config';
import { init } from './environment/init';
import * as passport from 'passport';


init();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const RedisStore = require('connect-redis')(session);
  app.use(
    session({
      secret: CONFIG_ENVIRONMENT.expressSession.secret,
      cookie: CONFIG_ENVIRONMENT.expressSession.cookie,
      resave: CONFIG_ENVIRONMENT.expressSession.resave,
      saveUninitialized: CONFIG_ENVIRONMENT.expressSession.saveUninitialized,
      // store: new RedisStore(CONFIG_ENVIRONMENT.redisConnection),
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  app.useStaticAssets(__dirname + '/../public');
  app.enableCors();
  await app.listen(CONFIG_ENVIRONMENT.port);
}
bootstrap();

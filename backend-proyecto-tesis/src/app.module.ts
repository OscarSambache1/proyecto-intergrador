import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MusculoEntity } from './modulos/musculo/musculo.entity';
import { ClienteEntity } from './modulos/cliente/cliente.entity';
import { ComidaEntity } from './modulos/comida/comida.entity';
import { ComidaDietaEntity } from './modulos/comida-dieta/comida-dieta.entity';
import { DetalleFacturaEntity } from './modulos/detalle-factura/detalle-factura.entity';
import { DietaDiaEntity } from './modulos/dieta-dia/dieta-dia.entity';
import { DietaEntity } from './modulos/dieta/dieta.entity';
import { EjercicioEntity } from './modulos/ejercicio/ejercicio.entity';
import { EjercicioMusculoEntity } from './modulos/ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from './modulos/ejercicio-rutina/ejercicio-rutina.entity';
import { MedidaEntity } from './modulos/medida/medida.entity';
import { PlanDietaEntity } from './modulos/plan-dieta/plan-dieta.entity';
import { PlanDietaClienteEntity } from './modulos/plan-dieta-cliente/plan-dieta-cliente.entity';
import { PlanEntrenamientoEntity } from './modulos/plan-entrenamiento/plan-entrenamiento.entity';
// tslint:disable-next-line:max-line-length
import { PlanEntrenamientoClienteInstructorEntity } from './modulos/plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { ProductoEntity } from './modulos/producto/producto.entity';
import { RutinaPlanEntrenamientoEntity } from './modulos/rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { RutinaEntity } from './modulos/rutina/rutina.entity';
import { ClienteModule } from './modulos/cliente/cliente.module';
import { InstructorModule } from './modulos/instructor/instructor..module';
import { UsuarioEntity } from './modulos/usuario/usuario.entity';
import { UsuarioModule } from './modulos/usuario/usuario.module';
import { MedidaModule } from './modulos/medida/medida.module';
import { ProductoModule } from './modulos/producto/producto.module';
import { MusculoModule } from './modulos/musculo/musculo.module';
import { EjercicioModule } from './modulos/ejercicio/ejercicio.module';
import { EjercicioMusculoModule } from './modulos/ejercicio-musculo/ejercicio-musculo.module';
import { RutinaModule } from './modulos/rutina/rutina.module';
import { EjercicioRutinaModule } from './modulos/ejercicio-rutina/ejercicio-rutina.module';
import { PlanEntrenamientoModule } from './modulos/plan-entrenamiento/plan-entrenamiento.module';
// tslint:disable-next-line:max-line-length
import { PlanEntrenamientoClienteInstructorModule } from './modulos/plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.module';
import { RutinaPlanEntrenamientoModule } from './modulos/rutina-plan-entrenamiento/rutina-plan-entrenamiento.module';
import { ComidaModule } from './modulos/comida/comida.module';
import { DietaModule } from './modulos/dieta/dieta.module';
import { ComidaDietaModule } from './modulos/comida-dieta/comida-dieta.module';
import { TipoComidaEntity } from './modulos/tipo-comida/tipo-comida.entity';
import { TipoComidaModule } from './modulos/tipo-comida/tipo-comida.module';
import { TipoDietaModule } from './modulos/tipo-dieta/tipo-dieta.module';
import { TipoDietaEntity } from './modulos/tipo-dieta/tipo-dieta';
import { DietaDiaModule } from './modulos/dieta-dia/dieta-dia.module';
import { PlanDietaModule } from './modulos/plan-dieta/plan-dieta.module';
import { PlanDietaClienteModule } from './modulos/plan-dieta-cliente/plan-dieta-cliente.module';
import { CabeceraFacturaEntity } from './modulos/cabecera-factura/cabecera-factura.entity';
import { CabeceraFacturaModule } from './modulos/cabecera-factura/cabecera-factura.module';
import { DetalleFacturaModule } from './modulos/detalle-factura/detalle-factura.module';
import { ClaseModule } from './modulos/clase/clase.module';
import { ClaseEntity } from './modulos/clase/clase.emtity';
import { ClaseHoraModule } from './modulos/clase-hora/clase-hora.module';
import { ClaseHoraEntity } from './modulos/clase-hora/clase-hora.entity';
import { DiaEntity } from './modulos/dia/dia.entity';
import { DiaModule } from './modulos/dia/dia.module';
import { ClaseDiaHoraModule } from './modulos/clase-hora-dia/clase-dia-hora.module';
import { ClaseDiaHoraEntity } from './modulos/clase-hora-dia/clase-dia-hora.entity';
import { CONFIG_ENVIRONMENT } from './environment/config';
import { init } from 'environment/init';
import { AuthModule } from './modulos/auth/auth.module';
import { InstructorEntity } from './modulos/instructor/instructor.entity';

// init();
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: CONFIG_ENVIRONMENT.dbConnections.mysql.type,
      name: 'default',
      host: CONFIG_ENVIRONMENT.dbConnections.mysql.host,
      port: CONFIG_ENVIRONMENT.dbConnections.mysql.port,
      username: CONFIG_ENVIRONMENT.dbConnections.mysql.username,
      password: CONFIG_ENVIRONMENT.dbConnections.mysql.password,
      database: CONFIG_ENVIRONMENT.dbConnections.mysql.database,
      entities: [
        ClienteEntity,
        CabeceraFacturaEntity,
        ComidaEntity,
        ComidaDietaEntity,
        DetalleFacturaEntity,
        DietaDiaEntity,
        DietaEntity,
        EjercicioEntity,
        EjercicioMusculoEntity,
        EjercicioRutinaEntity,
        InstructorEntity,
        MedidaEntity,
        MusculoEntity,
        PlanDietaEntity,
        PlanDietaClienteEntity,
        PlanEntrenamientoEntity,
        PlanEntrenamientoClienteInstructorEntity,
        ProductoEntity,
        RutinaEntity,
        RutinaPlanEntrenamientoEntity,
        UsuarioEntity,
        TipoComidaEntity,
        TipoDietaEntity,
        ClaseEntity,
        ClaseHoraEntity,
        ClaseDiaHoraEntity,
        DiaEntity,
      ],
      synchronize: CONFIG_ENVIRONMENT.dbConnections.mysql.synchronize,
      ssl: CONFIG_ENVIRONMENT.dbConnections.mysql.ssl,
      keepConnectionAlive:
        CONFIG_ENVIRONMENT.dbConnections.mysql.keepConnectionAlive,
      retryDelay: CONFIG_ENVIRONMENT.dbConnections.mysql.retryDelay,
      dropSchema: CONFIG_ENVIRONMENT.dbConnections.mysql.dropSchema,
      retryAttempts: CONFIG_ENVIRONMENT.dbConnections.mysql.retryAttempts,
      connectTimeout: CONFIG_ENVIRONMENT.dbConnections.mysql.connectTimeout,
      charset: CONFIG_ENVIRONMENT.dbConnections.mysql.charset,
      timezone: CONFIG_ENVIRONMENT.dbConnections.mysql.timezone,
    }),
    AuthModule,
    ClienteModule,
    InstructorModule,
    UsuarioModule,
    MedidaModule,
    ProductoModule,
    MusculoModule,
    EjercicioModule,
    EjercicioMusculoModule,
    RutinaModule,
    EjercicioRutinaModule,
    PlanEntrenamientoModule,
    PlanEntrenamientoClienteInstructorModule,
    RutinaPlanEntrenamientoModule,
    ComidaModule,
    DietaModule,
    ComidaDietaModule,
    TipoComidaModule,
    TipoDietaModule,
    DietaDiaModule,
    PlanDietaModule,
    PlanDietaClienteModule,
    CabeceraFacturaModule,
    DetalleFacturaModule,
    ClaseModule,
    ClaseHoraModule,
    DiaModule,
    ClaseDiaHoraModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor()
  {
  }
}

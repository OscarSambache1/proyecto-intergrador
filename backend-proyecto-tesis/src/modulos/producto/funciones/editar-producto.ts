import { EditarProductoDto } from '../dto/editar.producto.dto';

export function editarProducto(producto: EditarProductoDto): EditarProductoDto {
  const productoAEditar = new EditarProductoDto();
  Object.keys(producto).map(atributo => {
    productoAEditar[atributo] = producto[atributo];
  });
  return productoAEditar;
}

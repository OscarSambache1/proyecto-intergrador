import { CrearProductoDto } from '../dto/crear-producto.dto';

export function crearProducto(producto: CrearProductoDto): CrearProductoDto {
  const productoACrear = new CrearProductoDto();
  Object.keys(producto).map(atributo => {
    productoACrear[atributo] = producto[atributo];
  });
  return productoACrear;
}

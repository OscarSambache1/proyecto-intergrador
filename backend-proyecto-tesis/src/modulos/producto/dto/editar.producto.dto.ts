import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsOptional,
  Matches,
  IsCurrency,
  IsEnum,
} from 'class-validator';
import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
export class EditarProductoDto {
  @IsString()
  @IsOptional()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre: string;

  @IsString()
  @IsOptional()
  marca: string;

  @IsNotEmpty()
  @IsOptional()
  precio: number;

  @IsOptional()
  @IsEnum([1, 0])
  estado?: number;

  @IsOptional()
  detallesFactura: DetalleFacturaEntity[];
}

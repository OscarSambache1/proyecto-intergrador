import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
export class CrearProductoDto {
  @IsString()
  @IsOptional()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsString()
  @IsOptional()
  marca: string;

  @IsNotEmpty()
  precio: number;

  @IsOptional()
  detallesFactura: DetalleFacturaEntity[];
}

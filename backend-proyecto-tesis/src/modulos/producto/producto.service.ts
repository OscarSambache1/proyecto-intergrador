import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductoEntity } from './producto.entity';
import { Repository, Like } from 'typeorm';
import { CrearProductoDto } from './dto/crear-producto.dto';
import { EditarProductoDto } from './dto/editar.producto.dto';

@Injectable()
export class ProductoService {
  constructor(
    @InjectRepository(ProductoEntity)
    private readonly _productoRepository: Repository<ProductoEntity>,
  ) {}

  async findAll(consulta: any): Promise<ProductoEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._productoRepository.find(consulta);
  }

  async findLike(campo: string): Promise<ProductoEntity[]> {
    return await this._productoRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<ProductoEntity> {
    return await this._productoRepository.findOne(id, {
      relations: [],
    });
  }

  async createOne(producto: CrearProductoDto): Promise<ProductoEntity> {
    return await this._productoRepository.save(
      this._productoRepository.create(producto),
    );
  }

  async createMany(productos: CrearProductoDto[]): Promise<ProductoEntity[]> {
    const productosACrear: ProductoEntity[] = this._productoRepository.create(
      productos,
    );
    return this._productoRepository.save(productosACrear);
  }

  async update(
    id: number,
    producto: EditarProductoDto,
  ): Promise<ProductoEntity> {
    await this._productoRepository.update(id, producto);
    return await this.findById(id);
  }

  async delete(id: number) {
    const productoAEliminar = await this.findById(id);
    await this._productoRepository.remove(productoAEliminar);
  }

  async contar(consulta: object): Promise<number> {
    return await this._productoRepository.count({
      where: consulta,
    });
  }
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ProductoService } from './producto.service';
import { ProductoEntity } from './producto.entity';
import { CrearProductoDto } from './dto/crear-producto.dto';
import { crearProducto } from './funciones/crear-producto';
import { EditarProductoDto } from './dto/editar.producto.dto';
import { editarProducto } from './funciones/editar-producto';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('producto')
export class ProductoController {
  constructor(private readonly _productoService: ProductoService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<ProductoEntity[]> {
    return await this._productoService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<ProductoEntity> {
    const productoEncontrado = await this._productoService.findById(id);
    if (productoEncontrado) {
      return await this._productoService.findById(id);
    } else {
      throw new BadRequestException('EL Producto no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() producto: CrearProductoDto): Promise<ProductoEntity> {
    const productoACrearse = crearProducto(producto);
    const arregloErrores = await validate(productoACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el producto', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._productoService.createOne(productoACrearse);
    }
  }

  @Put(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() producto: EditarProductoDto,
  ): Promise<ProductoEntity> {
    const productoEncontrado = await this._productoService.findById(id);
    if (productoEncontrado) {
      const productoAEditar = editarProducto(producto);
      const arregloErrores = await validate(productoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el producto', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._productoService.update(id, productoAEditar);
      }
    } else {
      throw new BadRequestException('Producto no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const productoEncontrado = await this._productoService.findById(id);
    if (productoEncontrado) {
      await this._productoService.delete(id);
    } else {
      throw new BadRequestException('EL Producto no existe');
    }
  }
}

import { Module } from '@nestjs/common';
import { ProductoService } from './producto.service';
import { ProductoController } from './producto.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductoEntity } from './producto.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProductoEntity], 'default')],
  controllers: [ProductoController],
  providers: [ProductoService],
  exports: [],
})
export class ProductoModule {}

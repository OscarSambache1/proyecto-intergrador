import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { DetalleFacturaEntity } from '../detalle-factura/detalle-factura.entity';

@Entity('producto')
export class ProductoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'varchar', nullable: true })
  descripcion: string;

  @Column({ name: 'precio', type: 'double' })
  precio: number;

  @Column({ name: 'marca', type: 'varchar', nullable: true })
  marca: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @ManyToOne(
    type => DetalleFacturaEntity,
    detalleFactura => detalleFactura.producto,
  )
  detallesFactura: DetalleFacturaEntity[];
}

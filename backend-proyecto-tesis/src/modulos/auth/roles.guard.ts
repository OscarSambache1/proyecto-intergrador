import { Injectable, CanActivate, ExecutionContext, BadRequestException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import jwt from 'jsonwebtoken';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    return true;
    // const roles = this.reflector.get<string[]>('roles', context.getHandler());
    // if (!roles) {
    //   return true;
    // }
    // const request = context.switchToHttp().getRequest();
    // let usuario;
    // if (request.session.passport) {
    //   usuario = request.session.passport.user;
    // } else {
    //   const token = request.headers.authorization;
    //   const tokenDecoded = jwt.verify(token, 'shhhhh');
    //   usuario = tokenDecoded.usuarioLogueado;
    // }
    // const rolesUsuario = [];
    // if (usuario.cliente) {
    //   rolesUsuario.push(usuario.cliente.rol);
    // }
    // if (usuario.instructor) {
    //   rolesUsuario.push(usuario.instructor.rol);
    // }
    // const hasRole = () =>
    //   rolesUsuario.some(role => !!roles.find(item => item === role));
    // if (usuario && rolesUsuario && hasRole()) {
    //   return usuario && rolesUsuario && hasRole();
    // } else {
    //   throw new BadRequestException('el usuario no tiene permisos');
    // }
  }
}

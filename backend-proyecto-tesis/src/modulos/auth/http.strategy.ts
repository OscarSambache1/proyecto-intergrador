import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from './auth.service';
import { UsuarioLoginDto } from '../usuario/dto/usuario-login.dto';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class HttpStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly _usuarioService: UsuarioService) {
    super({
      usernameField: 'cedula',
      passReqToCallback: false,
    });
  }

  // tslint:disable-next-line:ban-types
  async validate(cedula, password, done: Function) {
    await this._usuarioService
      .autenticarUsuario({ cedula, password })
      .then(user => done(null, user))
      .catch(err => done(err, false));
  }
}

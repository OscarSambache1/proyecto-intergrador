import { Injectable } from '@nestjs/common';
import { UsuarioService } from '../usuario/usuario.service';
import { UsuarioLoginDto } from '../usuario/dto/usuario-login.dto';

@Injectable()
export class AuthService {
  constructor(private readonly _usuarioService: UsuarioService) {}

  async validarUsuario(user: UsuarioLoginDto): Promise<any> {
    return await this._usuarioService.autenticarUsuario(user);
  }
}

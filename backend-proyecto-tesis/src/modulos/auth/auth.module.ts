import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { HttpStrategy } from './http.strategy';
import { CookieSerializer } from './cookie-serializer';
import { AppAuthGuard } from './app-auth.guard';
import { UsuarioModule } from '../usuario/usuario.module';
import { SessionGuard } from './session.guard';

@Module({
  imports: [UsuarioModule],
  providers: [
    AuthService,
    HttpStrategy,
    AppAuthGuard,
    CookieSerializer,
    SessionGuard,
  ],
})
export class AuthModule {}

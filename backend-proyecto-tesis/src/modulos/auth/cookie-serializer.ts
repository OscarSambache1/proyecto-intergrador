import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';

@Injectable()
export class CookieSerializer extends PassportSerializer {
  // tslint:disable-next-line:ban-types
  serializeUser(user: any, done: Function): any {
    done(null, user);
  }

  // tslint:disable-next-line:ban-types
  deserializeUser(payload: any, done: Function): any {
    done(null, payload);
  }
}

import {
  CanActivate,
  ExecutionContext,
  BadRequestException,
} from '@nestjs/common';
import jwt from 'jsonwebtoken';

export class SessionGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest();
    if (request.session) {
      return true;
    } else {
      const token = request.headers.authorization;
      const tokenDecoded = jwt.verify(token, 'shhhhh');
      const usuario = tokenDecoded.usuarioLogueado;
      if (usuario) {
        return true;
      } else {
        throw new BadRequestException('no se ha iniciado sesion');
      }
    }
  }
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { DietaEntity } from '../dieta/dieta.entity';
import { PlanDietaEntity } from '../plan-dieta/plan-dieta.entity';
import { DiaEntity } from '../dia/dia.entity';

@Entity('dieta-dia')
export class DietaDiaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => DietaEntity, dieta => dieta.planesDietaDia)
  dieta: DietaEntity;

  @ManyToOne(type => PlanDietaEntity, planDieta => planDieta.planesDietaDia)
  planDieta: PlanDietaEntity;

  @ManyToOne(type => DiaEntity, dia => dia.dietasDia)
  dia: DiaEntity;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { DietaDiaService } from './dieta-dia.service';
import { DietaDiaEntity } from './dieta-dia.entity';
import { CrearDietaDiaDto } from './dto/crear-dieta-dia.dto';
import { crearDietaDia } from './funciones/crear-dieta-dia';
import { EditarDietaDiaDto } from './dto/editar-dieta-dia.dto';
import { editarDietaDia } from './funciones/editar-dieta-dia';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('dieta-dia')
export class DietaDiaController {
  constructor(private readonly _dietaDiaService: DietaDiaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<DietaDiaEntity[]> {
    return await this._dietaDiaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<DietaDiaEntity> {
    const dietaDiaEncontrada = await this._dietaDiaService.findById(id);
    if (dietaDiaEncontrada) {
      return await this._dietaDiaService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() dietaDia: CrearDietaDiaDto): Promise<DietaDiaEntity> {
    const dietaACrearse = crearDietaDia(dietaDia);
    const arregloErrores = await validate(dietaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._dietaDiaService.createOne(dietaACrearse);
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(@Body() dietasDia: CrearDietaDiaDto[]): Promise<DietaDiaEntity[]> {
    dietasDia.forEach(async dietaDia => {
      const dietaDiaACrearse = crearDietaDia(await dietaDia);
      const arregloErrores = await validate(dietaDiaACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._dietaDiaService.createMany(dietasDia);
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() dietaDia: EditarDietaDiaDto,
  ): Promise<DietaDiaEntity> {
    const dietaDiaEncontrada = await this._dietaDiaService.findById(id);
    if (dietaDiaEncontrada) {
      const dietaDiaAEditar = editarDietaDia(dietaDia);
      const arregloErrores = await validate(dietaDiaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._dietaDiaService.update(id, dietaDiaAEditar);
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const dietaDiaEncontrada = await this._dietaDiaService.findById(id);
    if (dietaDiaEncontrada) {
      await this._dietaDiaService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

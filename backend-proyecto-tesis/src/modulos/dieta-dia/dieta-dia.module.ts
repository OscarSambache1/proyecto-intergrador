import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DietaDiaEntity } from './dieta-dia.entity';
import { DietaDiaController } from './dieta-dia.controller';
import { DietaDiaService } from './dieta-dia.service';

@Module({
  imports: [TypeOrmModule.forFeature([DietaDiaEntity], 'default')],
  controllers: [DietaDiaController],
  providers: [DietaDiaService],
  exports: [],
})
export class DietaDiaModule {}

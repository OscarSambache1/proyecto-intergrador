import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { DietaDiaEntity } from './dieta-dia.entity';
import { CrearDietaDiaDto } from './dto/crear-dieta-dia.dto';
import { EditarDietaDiaDto } from './dto/editar-dieta-dia.dto';

@Injectable()
export class DietaDiaService {
  constructor(
    @InjectRepository(DietaDiaEntity)
    private readonly _dietaDiaRepository: Repository<DietaDiaEntity>,
  ) {}

  async findAll(consulta: any): Promise<DietaDiaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._dietaDiaRepository.find(consulta);
  }

  async findLike(campo: string): Promise<DietaDiaEntity[]> {
    return await this._dietaDiaRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<DietaDiaEntity> {
    return await this._dietaDiaRepository.findOne(id, {
      relations: ['dieta', 'planDieta'],
    });
  }

  async createOne(dietaDia: CrearDietaDiaDto): Promise<DietaDiaEntity> {
    return await this._dietaDiaRepository.save(
      this._dietaDiaRepository.create(dietaDia),
    );
  }

  async createMany(dietasDia: CrearDietaDiaDto[]): Promise<DietaDiaEntity[]> {
    const dietasGuardadas: DietaDiaEntity[] = this._dietaDiaRepository.create(
      dietasDia,
    );
    return this._dietaDiaRepository.save(dietasGuardadas);
  }

  async update(
    id: number,
    dietaDia: EditarDietaDiaDto,
  ): Promise<DietaDiaEntity> {
    await this._dietaDiaRepository.update(id, dietaDia);
    return await this.findById(id);
  }

  async delete(id: number) {
    const dietaDiaAEliminar = await this.findById(id);
    await this._dietaDiaRepository.remove(dietaDiaAEliminar);
  }
}

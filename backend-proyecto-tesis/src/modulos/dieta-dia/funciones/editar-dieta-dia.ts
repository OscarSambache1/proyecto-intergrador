import { EditarDietaDiaDto } from '../dto/editar-dieta-dia.dto';

export function editarDietaDia(dietaDia: EditarDietaDiaDto): EditarDietaDiaDto {
  const dietaDiaAEditar = new EditarDietaDiaDto();
  Object.keys(dietaDia).map(atributo => {
    dietaDiaAEditar[atributo] = dietaDia[atributo];
  });
  return dietaDiaAEditar;
}

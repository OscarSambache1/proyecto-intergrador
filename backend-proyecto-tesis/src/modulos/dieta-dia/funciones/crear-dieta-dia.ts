import { CrearDietaDiaDto } from '../dto/crear-dieta-dia.dto';

export function crearDietaDia(dietaDia: CrearDietaDiaDto): CrearDietaDiaDto {
  const dietaDiaACrear = new CrearDietaDiaDto();
  Object.keys(dietaDia).map(atributo => {
    dietaDiaACrear[atributo] = dietaDia[atributo];
  });
  return dietaDiaACrear;
}

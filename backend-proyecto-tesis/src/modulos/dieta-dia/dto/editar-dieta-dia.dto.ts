import { IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { PlanDietaEntity } from '../../plan-dieta/plan-dieta.entity';
import { DietaEntity } from '../../dieta/dieta.entity';
import { DiaEntity } from '../../dia/dia.entity';

export class EditarDietaDiaDto {
  @IsOptional()
  dieta: DietaEntity;

  @IsOptional()
  planDieta: PlanDietaEntity;

  @IsOptional()
  dia: DiaEntity;
}

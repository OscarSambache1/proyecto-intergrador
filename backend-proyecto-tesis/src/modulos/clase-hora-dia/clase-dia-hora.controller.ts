import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ClaseDiaHoraService } from './clase-dia-hora.service';
import { ClaseDiaHoraEntity } from './clase-dia-hora.entity';
import { CrearClaseDiaHoraDto } from './dto/crear-clase-dia-hora.dto';
import { crearClaseDiaHora } from './funciones/crear-clase-dia-hora';
import { EditarClaseDiaHoraDto } from './dto/editar-clase-dia-hora.dto';
import { editarClaseDiaHora } from './funciones/editar-clase-dia-hora';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('clase-dia-hora')
export class ClaseDiaHoraController {
  constructor(private readonly _claseDiaHoraService: ClaseDiaHoraService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<ClaseDiaHoraEntity[]> {
    return await this._claseDiaHoraService.findAll(JSON.parse(consulta));
  }

  @Get('findWhereOr')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findByNombreClase(
    @Query() consulta: any,
  ): Promise<ClaseDiaHoraEntity[]> {
    return this._claseDiaHoraService.findByNombreClase(consulta.expresion);
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<ClaseDiaHoraEntity> {
    const claseDiaHoraEncontrada = await this._claseDiaHoraService.findById(id);
    if (claseDiaHoraEncontrada) {
      return await this._claseDiaHoraService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(
    @Body() claseDiaHora: CrearClaseDiaHoraDto,
  ): Promise<ClaseDiaHoraEntity> {
    const claseDiaHoraACrearse = crearClaseDiaHora(claseDiaHora);
    const arregloErrores = await validate(claseDiaHoraACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._claseDiaHoraService.createOne(claseDiaHoraACrearse);
    }
  }

  @Post('crear-varios')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async createMany(
    @Body() clasesHoraDia: CrearClaseDiaHoraDto[],
  ): Promise<CrearClaseDiaHoraDto[]> {
    clasesHoraDia.forEach(async claseDia => {
      const claseDiaHoraACrearse = crearClaseDiaHora(await claseDia);
      const arregloErrores = await validate(claseDiaHoraACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._claseDiaHoraService.createMany(clasesHoraDia);
  }

  @Put(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() claseDiaHora: EditarClaseDiaHoraDto,
  ): Promise<ClaseDiaHoraEntity> {
    const claseDiaHoraEncontrada = await this._claseDiaHoraService.findById(id);
    if (claseDiaHoraEncontrada) {
      const claseDiaHoraAEditar = editarClaseDiaHora(claseDiaHora);
      const arregloErrores = await validate(claseDiaHoraAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._claseDiaHoraService.update(id, claseDiaHoraAEditar);
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const claseDiaEncontrada = await this._claseDiaHoraService.findById(id);
    if (claseDiaEncontrada) {
      await this._claseDiaHoraService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';
import { DiaEntity } from '../dia/dia.entity';

@Entity('clase-dia-hora')
export class ClaseDiaHoraEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => ClaseHoraEntity, claseHora => claseHora.clasesDiaHora)
  claseHora: ClaseHoraEntity;

  @ManyToOne(type => DiaEntity, dia => dia.clasesDiaHora)
  dia: DiaEntity;
}

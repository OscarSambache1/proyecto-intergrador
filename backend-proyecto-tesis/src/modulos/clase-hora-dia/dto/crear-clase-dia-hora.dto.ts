import { IsString, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { DiaEntity } from '../../dia/dia.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';

export class CrearClaseDiaHoraDto {
  @IsOptional()
  claseHora: ClaseHoraEntity;

  @IsOptional()
  dia: DiaEntity;
}

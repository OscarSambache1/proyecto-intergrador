import { IsString, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
import { DiaEntity } from '../../dia/dia.entity';

export class EditarClaseDiaHoraDto {
  @IsOptional()
  claseHora: ClaseHoraEntity;

  @IsOptional()
  dia: DiaEntity;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClaseDiaHoraEntity } from './clase-dia-hora.entity';
import { ClaseDiaHoraController } from './clase-dia-hora.controller';
import { ClaseDiaHoraService } from './clase-dia-hora.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClaseDiaHoraEntity], 'default')],
  controllers: [ClaseDiaHoraController],
  providers: [ClaseDiaHoraService],
  exports: [],
})
export class ClaseDiaHoraModule {}

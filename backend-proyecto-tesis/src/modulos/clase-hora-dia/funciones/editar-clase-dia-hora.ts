import { EditarClaseDiaHoraDto } from '../dto/editar-clase-dia-hora.dto';

export function editarClaseDiaHora(
  claseDiaHora: EditarClaseDiaHoraDto,
): EditarClaseDiaHoraDto {
  const claseDiaHoraAEditar = new EditarClaseDiaHoraDto();
  Object.keys(claseDiaHora).map(atributo => {
    claseDiaHoraAEditar[atributo] = claseDiaHora[atributo];
  });
  return claseDiaHoraAEditar;
}

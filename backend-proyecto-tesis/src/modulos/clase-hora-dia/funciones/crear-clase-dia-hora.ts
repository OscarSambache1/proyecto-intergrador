import { CrearClaseDiaHoraDto } from '../dto/crear-clase-dia-hora.dto';

export function crearClaseDiaHora(
  claseDiaHora: CrearClaseDiaHoraDto,
): CrearClaseDiaHoraDto {
  const claseDiaHoraACrear = new CrearClaseDiaHoraDto();
  Object.keys(claseDiaHora).map(atributo => {
    claseDiaHoraACrear[atributo] = claseDiaHora[atributo];
  });
  return claseDiaHoraACrear;
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { ClaseDiaHoraEntity } from './clase-dia-hora.entity';
import { CrearClaseDiaHoraDto } from './dto/crear-clase-dia-hora.dto';
import { EditarClaseDiaHoraDto } from './dto/editar-clase-dia-hora.dto';

@Injectable()
export class ClaseDiaHoraService {
  constructor(
    @InjectRepository(ClaseDiaHoraEntity)
    private readonly _claseDiaHoraRepository: Repository<ClaseDiaHoraEntity>,
  ) {}

  async findAll(consulta: any): Promise<ClaseDiaHoraEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._claseDiaHoraRepository.find(consulta);
  }

  async findLike(campo: string): Promise<ClaseDiaHoraEntity[]> {
    return await this._claseDiaHoraRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<ClaseDiaHoraEntity> {
    return await this._claseDiaHoraRepository.findOne(id, {
      relations: ['claseHora', 'dia'],
    });
  }

  async findByNombreClase(expresion: string): Promise<ClaseDiaHoraEntity[]> {
    return await this._claseDiaHoraRepository
      .createQueryBuilder('clase-dia-hora')
      .innerJoinAndSelect('clase-dia-hora.claseHora', 'claseHora')
      .innerJoinAndSelect('clase-dia-hora.dia', 'dia')
      .innerJoinAndSelect(
        'claseHora.clase',
        'clase',
        'clase.nombre LIKE :nombre  AND clase.estado = :estado',
        { nombre: `%${expresion}%`, estado: 1 },
      )
      .innerJoinAndSelect('claseHora.instructor', 'instructor')
      .innerJoinAndSelect('instructor.usuario', 'usuario')
      .orderBy('claseHora.horaInicio', 'DESC')
      .getMany();
  }

  async createOne(
    claseDiaHora: CrearClaseDiaHoraDto,
  ): Promise<ClaseDiaHoraEntity> {
    return await this._claseDiaHoraRepository.save(
      this._claseDiaHoraRepository.create(claseDiaHora),
    );
  }

  async createMany(
    clasesDiaHora: CrearClaseDiaHoraDto[],
  ): Promise<ClaseDiaHoraEntity[]> {
    const clasesDiaGuardadas: ClaseDiaHoraEntity[] = this._claseDiaHoraRepository.create(
      clasesDiaHora,
    );
    return this._claseDiaHoraRepository.save(clasesDiaGuardadas);
  }

  async update(
    id: number,
    claseDiaHora: EditarClaseDiaHoraDto,
  ): Promise<ClaseDiaHoraEntity> {
    await this._claseDiaHoraRepository.update(id, claseDiaHora);
    return await this.findById(id);
  }

  async delete(id: number) {
    const claseDiaHoraAEliminar = await this.findById(id);
    await this._claseDiaHoraRepository.remove(claseDiaHoraAEliminar);
  }
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { InstructorService } from './instructor.service';
import { Paginacion } from 'interfaces/paginacion';
import { InstructorEntity } from './instructor.entity';
import { CrearInstructorDto } from './dto/crear-instructor.dto';
import { crearInsructor } from './funciones/crearInstructor';
import { validate } from 'class-validator';
import { EditarInstructorDto } from './dto/editar-instructor.dto';
import { editarInstructor } from './funciones/editarInstructor';
import { Consulta } from 'interfaces/consulta';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('instructor')
export class InstructorController {
  constructor(private readonly _instructorService: InstructorService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<InstructorEntity[]> {
    return await this._instructorService.findAll(JSON.parse(consulta));
  }

  @Get('findWhereOr')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findByNombreApellido(
    @Query() consulta: any,
  ): Promise<InstructorEntity[]> {
    return this._instructorService.findByNombreApellido(
      consulta.expresion,
      consulta.verActivos,
    );
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id): Promise<InstructorEntity> {
    const instructorEncontrado = await this._instructorService.findById(id);
    if (instructorEncontrado) {
      return instructorEncontrado;
    } else {
      throw new BadRequestException('Instructor no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async crear(
    @Body() instructor: CrearInstructorDto,
  ): Promise<InstructorEntity> {
    const instructorACrearse = crearInsructor(instructor);
    const arregloErrores = await validate(instructorACrearse);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: creando al instructor', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._instructorService.createOne(instructorACrearse);
    }
  }

  @Put(':id')
  @Roles('administrador', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async editar(
    @Param() id: number,
    @Body() instructor: EditarInstructorDto,
  ): Promise<InstructorEntity> {
    const instructorEncontrado = await this._instructorService.findById(id);
    if (instructorEncontrado) {
      const instructorAEditarse = editarInstructor(instructor);
      const arregloErrores = await validate(instructorAEditarse);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando al instructor', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._instructorService.update(id, instructorAEditarse);
      }
    } else {
      throw new BadRequestException('instructor no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async eliminar(@Param('id') id: number): Promise<string> {
    const instructorEncontrado = await this._instructorService.findById(id);
    if (instructorEncontrado) {
      return await this._instructorService.delete(id);
    } else {
      throw new BadRequestException('instructor no existe');
    }
  }
}

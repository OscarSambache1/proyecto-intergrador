import { EditarInstructorDto } from '../dto/editar-instructor.dto';

export function editarInstructor(
  instructor: EditarInstructorDto,
): EditarInstructorDto {
  const instructorAEditar = new EditarInstructorDto();
  Object.keys(instructor).map(atributo => {
    instructorAEditar[atributo] = instructor[atributo];
  });
  return instructorAEditar;
}

import { CrearInstructorDto } from '../dto/crear-instructor.dto';

export function crearInsructor(
  instructor: CrearInstructorDto,
): CrearInstructorDto {
  const instructorACrear = new CrearInstructorDto();
  Object.keys(instructor).map(atributo => {
    instructorACrear[atributo] = instructor[atributo];
  });
  instructorACrear.estado = 1;
  return instructorACrear;
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';
import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';

@Entity('instructor')
export class InstructorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'espcecializacion', type: 'varchar' })
  especializacion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @Column({
    name: 'rol',
    type: 'enum',
    enum: ['administrador', 'cliente', 'instructor', 'empleado'],
  })
  rol: string;

  @OneToMany(
    type => PlanEntrenamientoClienteInstructorEntity,
    planEntrenamientoClienteInstructor =>
      planEntrenamientoClienteInstructor.instructor,
  )
  planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];

  @OneToOne(type => UsuarioEntity, usuario => usuario.instructor)
  @JoinColumn()
  usuario: UsuarioEntity;

  @OneToMany(type => ClaseHoraEntity, claseHora => claseHora.instructor)
  clasesHora: ClaseHoraEntity[];
}

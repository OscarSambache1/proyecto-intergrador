import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
} from 'class-validator';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export class EditarInstructorDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  especializacion: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @IsEnum(['administrador', 'cliente', 'instructor', 'empleado'])
  rol: string;

  @IsOptional()
  planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];

  @IsOptional()
  clasesHora: ClaseHoraEntity[];
}

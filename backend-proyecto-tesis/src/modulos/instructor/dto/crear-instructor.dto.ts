import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
} from 'class-validator';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export class CrearInstructorDto {
  @IsNotEmpty()
  @IsString()
  @IsEnum(['administrador', 'cliente', 'instructor', 'empleado'])
  rol: string;

  @IsNotEmpty()
  @IsString()
  especializacion: string;

  @IsOptional()
  planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];

  @IsOptional()
  clasesHora: ClaseHoraEntity[];

  @IsNotEmpty()
  @IsEnum([0, 1])
  estado?: number;
}

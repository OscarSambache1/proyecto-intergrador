import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { CrearInstructorDto } from './dto/crear-instructor.dto';
import { InstructorEntity } from './instructor.entity';
import { EditarInstructorDto } from './dto/editar-instructor.dto';
import { PrincipalService } from 'clases/servicion-principal.service';

@Injectable()
export class InstructorService {
  constructor(
    @InjectRepository(InstructorEntity)
    private readonly _instructorRepository: Repository<InstructorEntity>,
  ) {}

  async findAll(consulta: any): Promise<InstructorEntity[]> {
    if (!consulta.where[0]) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._instructorRepository.find(consulta);
  }

  async findByNombreApellido(
    expresion: string,
    verActivos: string,
  ): Promise<InstructorEntity[]> {
    let condicionEstado;
    if (verActivos === 'true') {
      condicionEstado = 'instructor.estado = 1';
    } else {
      condicionEstado = '';
    }
    return await this._instructorRepository
      .createQueryBuilder('instructor')
      .innerJoinAndSelect(
        'instructor.usuario',
        'usuario',
        'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos',
        { nombres: `%${expresion}%`, apellidos: `%${expresion}%` },
      )
      .where(condicionEstado)
      .getMany();
  }

  async findById(id: number): Promise<InstructorEntity> {
    return await this._instructorRepository.findOne(id, {
      relations: ['usuario'],
    });
  }

  async createOne(instructor: CrearInstructorDto): Promise<InstructorEntity> {
    return await this._instructorRepository.save(
      this._instructorRepository.create(instructor),
    );
  }

  async createMany(
    instructores: CrearInstructorDto[],
  ): Promise<InstructorEntity[]> {
    const instructoresACrear: InstructorEntity[] = this._instructorRepository.create(
      instructores,
    );
    return this._instructorRepository.save(instructoresACrear);
  }

  async update(
    id: number,
    instructor: EditarInstructorDto,
  ): Promise<InstructorEntity> {
    await this._instructorRepository.update(id, instructor);
    return await this.findById(id);
  }

  async delete(id: number): Promise<string> {
    const instructorAEliminar = await this.findById(id);
    await this._instructorRepository.remove(instructorAEliminar);
    return `Se ha eliminado el elemento ${id}`;
  }

  async contar(consulta: object): Promise<number> {
    return await this._instructorRepository.count({
      where: consulta,
    });
  }
}

import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { PlanDietaClienteEntity } from '../../plan-dieta-cliente/plan-dieta-cliente.entity';
import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';

export class CrearPlanDietaDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsOptional()
  planesDietaDia: DietaDiaEntity[];

  @IsOptional()
  planesDietaCliente: PlanDietaClienteEntity[];
}

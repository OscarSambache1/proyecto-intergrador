import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { PlanDietaClienteEntity } from '../../plan-dieta-cliente/plan-dieta-cliente.entity';
import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';

export class EditarPlanDietaDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre?: string;

  @IsOptional()
  descripcion?: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  planesDietaDia?: DietaDiaEntity[];

  @IsOptional()
  planesDietaCliente?: PlanDietaClienteEntity[];
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { PlanDietaEntity } from './plan-dieta.entity';
import { CrearPlanDietaDto } from './dto/crear-plan-dieta.dto';
import { EditarPlanDietaDto } from './dto/editar-plan-dieta.dto';

@Injectable()
export class PlanDietaService {
  constructor(
    @InjectRepository(PlanDietaEntity)
    private readonly _planDietaRepository: Repository<PlanDietaEntity>,
  ) {}

  async findAll(consulta: any): Promise<PlanDietaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._planDietaRepository.find(consulta);
  }

  async findLike(consulta: any): Promise<PlanDietaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._planDietaRepository.find(consulta);
  }

  async findById(id: number): Promise<PlanDietaEntity> {
    return await this._planDietaRepository.findOne(id, {
      relations: [
        'planesDietaDia',
        'planesDietaCliente',
        'planesDietaDia.dieta',
        'planesDietaDia.dieta.comidasDieta',
        'planesDietaDia.dieta.tipo',
        'planesDietaDia.dieta.comidasDieta.comida',
        'planesDietaDia.dieta.comidasDieta.comida.tipo',
        'planesDietaDia.dia',
      ],
    });
  }

  async createOne(planDia: CrearPlanDietaDto): Promise<PlanDietaEntity> {
    return await this._planDietaRepository.save(
      this._planDietaRepository.create(planDia),
    );
  }

  async createMany(
    planesDieta: CrearPlanDietaDto[],
  ): Promise<PlanDietaEntity[]> {
    const rutinasGuardadas: PlanDietaEntity[] = this._planDietaRepository.create(
      planesDieta,
    );
    return this._planDietaRepository.save(rutinasGuardadas);
  }

  async update(
    id: number,
    planDieta: EditarPlanDietaDto,
  ): Promise<PlanDietaEntity> {
    await this._planDietaRepository.update(id, planDieta);
    return await this.findById(id);
  }

  async delete(id: number) {
    const planDiaAEliminar = await this.findById(id);
    await this._planDietaRepository.remove(planDiaAEliminar);
  }
}

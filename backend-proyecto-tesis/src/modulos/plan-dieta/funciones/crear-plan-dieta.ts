import { CrearPlanDietaDto } from '../dto/crear-plan-dieta.dto';

export function crearPlanDieta(dieta: CrearPlanDietaDto): CrearPlanDietaDto {
  const planDietaACrear = new CrearPlanDietaDto();
  Object.keys(dieta).map(atributo => {
    planDietaACrear[atributo] = dieta[atributo];
  });
  return planDietaACrear;
}

import { EditarPlanDietaDto } from '../dto/editar-plan-dieta.dto';

export function editarPlanDieta(dieta: EditarPlanDietaDto): EditarPlanDietaDto {
  const planDietaAEditar = new EditarPlanDietaDto();
  Object.keys(dieta).map(atributo => {
    planDietaAEditar[atributo] = dieta[atributo];
  });
  return planDietaAEditar;
}

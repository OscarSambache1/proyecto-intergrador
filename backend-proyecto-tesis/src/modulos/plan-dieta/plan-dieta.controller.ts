import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { PlanDietaService } from './plan-dieta.service';
import { PlanDietaEntity } from './plan-dieta.entity';
import { CrearPlanDietaDto } from './dto/crear-plan-dieta.dto';
import { crearPlanDieta } from './funciones/crear-plan-dieta';
import { EditarPlanDietaDto } from './dto/editar-plan-dieta.dto';
import { editarPlanDieta } from './funciones/editar-plan-dieta';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('plan-dieta')
export class PlanDiaController {
  constructor(private readonly _planDietaService: PlanDietaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<PlanDietaEntity[]> {
    return await this._planDietaService.findAll(JSON.parse(consulta));
  }

  @Get('like')
  async findAllLike(
    @Query('consulta') consulta: any,
  ): Promise<PlanDietaEntity[]> {
    return await this._planDietaService.findLike(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<PlanDietaEntity> {
    const planDietaEncontrado = await this._planDietaService.findById(id);
    if (planDietaEncontrado) {
      return await this._planDietaService.findById(id);
    } else {
      throw new BadRequestException('El plan de dieta no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() planDia: CrearPlanDietaDto): Promise<PlanDietaEntity> {
    const plandietaACrearse = crearPlanDieta(planDia);
    const arregloErrores = await validate(plandietaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el plan de dieta', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._planDietaService.createOne(plandietaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() planDieta: EditarPlanDietaDto,
  ): Promise<PlanDietaEntity> {
    const planDietaEncontrado = await this._planDietaService.findById(id);
    if (planDietaEncontrado) {
      const planDietaAEditar = editarPlanDieta(planDieta);
      const arregloErrores = await validate(planDietaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el plan de dieta', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._planDietaService.update(id, planDietaAEditar);
      }
    } else {
      throw new BadRequestException('Plan de dieta no encontrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const planDiaEncontrado = await this._planDietaService.findById(id);
    if (planDiaEncontrado) {
      await this._planDietaService.delete(id);
    } else {
      throw new BadRequestException('El plan de dieta no existe');
    }
  }
}

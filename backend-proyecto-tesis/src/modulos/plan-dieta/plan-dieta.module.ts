import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanDietaEntity } from './plan-dieta.entity';
import { PlanDiaController } from './plan-dieta.controller';
import { PlanDietaService } from './plan-dieta.service';

@Module({
  imports: [TypeOrmModule.forFeature([PlanDietaEntity], 'default')],
  controllers: [PlanDiaController],
  providers: [PlanDietaService],
  exports: [],
})
export class PlanDietaModule {}

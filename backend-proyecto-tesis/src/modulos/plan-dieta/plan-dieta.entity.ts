import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { DietaDiaEntity } from '../dieta-dia/dieta-dia.entity';
import { PlanDietaClienteEntity } from '../plan-dieta-cliente/plan-dieta-cliente.entity';

@Entity('plan-dieta')
export class PlanDietaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'varchar' })
  descripcion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => DietaDiaEntity, dietaDia => dietaDia.planDieta)
  planesDietaDia: DietaDiaEntity[];

  @OneToMany(
    type => PlanDietaClienteEntity,
    planDietaCliente => planDietaCliente.planDieta,
  )
  planesDietaCliente: PlanDietaClienteEntity[];
}

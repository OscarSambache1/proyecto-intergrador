import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ComidaEntity } from '../comida/comida.entity';

@Entity('tipo-comida')
export class TipoComidaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => ComidaEntity, comida => comida.tipo)
  comidas: ComidaEntity[];
}

import { EditarTipoComidaDto } from '../dto/editar-tipo-comida.dto';

export function editarTipoComida(
  tipoComida: EditarTipoComidaDto,
): EditarTipoComidaDto {
  const tipoComidaAEditar = new EditarTipoComidaDto();
  Object.keys(tipoComida).map(atributo => {
    tipoComidaAEditar[atributo] = tipoComida[atributo];
  });
  return tipoComidaAEditar;
}

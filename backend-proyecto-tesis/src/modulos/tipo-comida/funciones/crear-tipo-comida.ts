import { CrearTipoComidaDto } from '../dto/crear-tipo-comida.dto';

export function crearTipoComida(
  tipoComida: CrearTipoComidaDto,
): CrearTipoComidaDto {
  const tipoComidaACrear = new CrearTipoComidaDto();
  Object.keys(tipoComida).map(atributo => {
    tipoComidaACrear[atributo] = tipoComida[atributo];
  });
  return tipoComidaACrear;
}

import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { ComidaEntity } from '../../comida/comida.entity';

export class CrearTipoComidaDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  comidas: ComidaEntity[];
}

import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { ComidaEntity } from '../../comida/comida.entity';

export class EditarTipoComidaDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  comidas: ComidaEntity[];
}

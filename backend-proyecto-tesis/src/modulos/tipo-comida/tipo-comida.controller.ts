import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { TipoComidaService } from './tipo-comida.service';
import { TipoComidaEntity } from './tipo-comida.entity';
import { CrearTipoComidaDto } from './dto/crear-tipo-comida.dto';
import { EditarTipoComidaDto } from './dto/editar-tipo-comida.dto';
import { crearTipoComida } from './funciones/crear-tipo-comida';
import { editarTipoComida } from './funciones/editar-tipo-comida';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('tipo-comida')
export class TipoComidaController {
  constructor(private readonly _tipoComidaService: TipoComidaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<TipoComidaEntity[]> {
    return await this._tipoComidaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<TipoComidaEntity> {
    const tipoComidaEncontrada = await this._tipoComidaService.findById(id);
    if (tipoComidaEncontrada) {
      return await this._tipoComidaService.findById(id);
    } else {
      throw new BadRequestException('El tipo de comida no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(
    @Body() tipoComida: CrearTipoComidaDto,
  ): Promise<TipoComidaEntity> {
    const tipoComidaACrearse = crearTipoComida(tipoComida);
    const arregloErrores = await validate(tipoComidaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando lel tipo de comida', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._tipoComidaService.createOne(tipoComidaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() tipoComida: EditarTipoComidaDto,
  ): Promise<TipoComidaEntity> {
    const tipoComidaEncontrada = await this._tipoComidaService.findById(id);
    if (tipoComidaEncontrada) {
      const tipoComidaAEditar = editarTipoComida(tipoComida);
      const arregloErrores = await validate(tipoComidaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el tipo de comida', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._tipoComidaService.update(id, tipoComidaAEditar);
      }
    } else {
      throw new BadRequestException('Tipo de comida no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const tipoComidaEncontrado = await this._tipoComidaService.findById(id);
    if (tipoComidaEncontrado) {
      await this._tipoComidaService.delete(id);
    } else {
      throw new BadRequestException('El tipo de comida no existe');
    }
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoComidaEntity } from './tipo-comida.entity';
import { TipoComidaController } from './tipo-comida.controller';
import { TipoComidaService } from './tipo-comida.service';

@Module({
  imports: [TypeOrmModule.forFeature([TipoComidaEntity], 'default')],
  controllers: [TipoComidaController],
  providers: [TipoComidaService],
  exports: [TipoComidaService],
})
export class TipoComidaModule {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { TipoComidaEntity } from './tipo-comida.entity';
import { CrearTipoComidaDto } from './dto/crear-tipo-comida.dto';
import { EditarTipoComidaDto } from './dto/editar-tipo-comida.dto';

@Injectable()
export class TipoComidaService {
  constructor(
    @InjectRepository(TipoComidaEntity)
    private readonly _tipoComidaRepository: Repository<TipoComidaEntity>,
  ) {}

  async findAll(consulta: any): Promise<TipoComidaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._tipoComidaRepository.find(consulta);
  }

  async findById(id: number): Promise<TipoComidaEntity> {
    return await this._tipoComidaRepository.findOne(id, {
      relations: [],
    });
  }

  async createOne(tipoComida: CrearTipoComidaDto): Promise<TipoComidaEntity> {
    return await this._tipoComidaRepository.save(
      this._tipoComidaRepository.create(tipoComida),
    );
  }

  async createMany(
    tiposComida: CrearTipoComidaDto[],
  ): Promise<TipoComidaEntity[]> {
    const tiposComidaACrear: TipoComidaEntity[] = this._tipoComidaRepository.create(
      tiposComida,
    );
    return this._tipoComidaRepository.save(tiposComidaACrear);
  }

  async update(
    id: number,
    tipoComida: EditarTipoComidaDto,
  ): Promise<TipoComidaEntity> {
    await this._tipoComidaRepository.update(id, tipoComida);
    return await this.findById(id);
  }

  async delete(id: number) {
    const tipoComiidaAEliminar = await this.findById(id);
    await this._tipoComidaRepository.remove(tipoComiidaAEliminar);
  }
}

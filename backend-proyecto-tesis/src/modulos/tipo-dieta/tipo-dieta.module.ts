import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoDietaEntity } from './tipo-dieta';
import { TipoDietaService } from './tipo-dieta.service';
import { TipoDietaController } from './tipo-dieta.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TipoDietaEntity], 'default')],
  controllers: [TipoDietaController],
  providers: [TipoDietaService],
  exports: [TipoDietaService],
})
export class TipoDietaModule {}

import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { DietaEntity } from '../../dieta/dieta.entity';

export class EditarTipoDietaDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  dietas: DietaEntity[];
}

import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { DietaEntity } from '../../dieta/dieta.entity';

export class CrearTipoDietaDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  dietas: DietaEntity[];
}

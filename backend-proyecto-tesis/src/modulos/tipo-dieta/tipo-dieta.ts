import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { DietaEntity } from '../dieta/dieta.entity';

@Entity('tipo-dieta')
export class TipoDietaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => DietaEntity, dieta => dieta.tipo)
  dietas: DietaEntity[];
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { TipoDietaService } from './tipo-dieta.service';
import { TipoDietaEntity } from './tipo-dieta';
import { CrearTipoDietaDto } from './dto/crear-tipo-dieta.dto';
import { crearTipoDieta } from './funciones/crear-tipo-dieta';
import { EditarTipoDietaDto } from './dto/editar-tipo-dieta.dto';
import { editarTipoDieta } from './funciones/editar-tipo-dieta';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('tipo-dieta')
export class TipoDietaController {
  constructor(private readonly _tipoDietaService: TipoDietaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<TipoDietaEntity[]> {
    return await this._tipoDietaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<TipoDietaEntity> {
    const tipoDietaEncontrada = await this._tipoDietaService.findById(id);
    if (tipoDietaEncontrada) {
      return await this._tipoDietaService.findById(id);
    } else {
      throw new BadRequestException('El tipo de dieta no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(@Body() tipoDieta: CrearTipoDietaDto): Promise<TipoDietaEntity> {
    const tipoDietaACrearse = crearTipoDieta(tipoDieta);
    const arregloErrores = await validate(tipoDietaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando lel tipo de dieta', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._tipoDietaService.createOne(tipoDietaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() tipoDieta: EditarTipoDietaDto,
  ): Promise<TipoDietaEntity> {
    const tipoDietaEncontrada = await this._tipoDietaService.findById(id);
    if (tipoDietaEncontrada) {
      const tipoDietaAEditar = editarTipoDieta(tipoDieta);
      const arregloErrores = await validate(tipoDietaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el tipo de dieta', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._tipoDietaService.update(id, tipoDietaAEditar);
      }
    } else {
      throw new BadRequestException('Tipo de dieta no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const tipoDietaEncontrado = await this._tipoDietaService.findById(id);
    if (tipoDietaEncontrado) {
      await this._tipoDietaService.delete(id);
    } else {
      throw new BadRequestException('El tipo de dieta no existe');
    }
  }
}

import { CrearTipoDietaDto } from '../dto/crear-tipo-dieta.dto';

export function crearTipoDieta(
  tipoDieta: CrearTipoDietaDto,
): CrearTipoDietaDto {
  const tipoDietaACrear = new CrearTipoDietaDto();
  Object.keys(tipoDieta).map(atributo => {
    tipoDietaACrear[atributo] = tipoDieta[atributo];
  });
  return tipoDietaACrear;
}

import { EditarTipoDietaDto } from '../dto/editar-tipo-dieta.dto';

export function editarTipoDieta(
  tipoDieta: EditarTipoDietaDto,
): EditarTipoDietaDto {
  const tipoDietaAEditar = new EditarTipoDietaDto();
  Object.keys(tipoDieta).map(atributo => {
    tipoDietaAEditar[atributo] = tipoDieta[atributo];
  });
  return tipoDietaAEditar;
}

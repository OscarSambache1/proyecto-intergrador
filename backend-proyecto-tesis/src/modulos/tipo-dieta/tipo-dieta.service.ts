import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { TipoDietaEntity } from './tipo-dieta';
import { CrearTipoDietaDto } from './dto/crear-tipo-dieta.dto';
import { EditarTipoDietaDto } from './dto/editar-tipo-dieta.dto';

@Injectable()
export class TipoDietaService {
  constructor(
    @InjectRepository(TipoDietaEntity)
    private readonly _tipoDietaRepository: Repository<TipoDietaEntity>,
  ) {}

  async findAll(consulta: any): Promise<TipoDietaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._tipoDietaRepository.find(consulta);
  }

  async findById(id: number): Promise<TipoDietaEntity> {
    return await this._tipoDietaRepository.findOne(id, {
      relations: [],
    });
  }

  async createOne(tipoDieta: CrearTipoDietaDto): Promise<TipoDietaEntity> {
    return await this._tipoDietaRepository.save(
      this._tipoDietaRepository.create(tipoDieta),
    );
  }

  async createMany(
    tiposDieta: CrearTipoDietaDto[],
  ): Promise<TipoDietaEntity[]> {
    const tiposDietaACrear: TipoDietaEntity[] = this._tipoDietaRepository.create(
      tiposDieta,
    );
    return this._tipoDietaRepository.save(tiposDietaACrear);
  }
  async update(
    id: number,
    tipoDieta: EditarTipoDietaDto,
  ): Promise<TipoDietaEntity> {
    await this._tipoDietaRepository.update(id, tipoDieta);
    return await this.findById(id);
  }

  async delete(id: number) {
    const tipoDietaAEliminar = await this.findById(id);
    await this._tipoDietaRepository.remove(tipoDietaAEliminar);
  }
}

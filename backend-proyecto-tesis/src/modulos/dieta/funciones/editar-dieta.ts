import { EditarDietaDto } from '../dto/editar-dieta.dto';

export function editarDieta(dieta: EditarDietaDto): EditarDietaDto {
  const dietaAEditar = new EditarDietaDto();
  Object.keys(dieta).map(atributo => {
    dietaAEditar[atributo] = dieta[atributo];
  });
  return dietaAEditar;
}

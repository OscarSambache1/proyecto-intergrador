import { CrearDietaDto } from '../dto/crear-dieta.dto';

export function crearDieta(dieta: CrearDietaDto): CrearDietaDto {
  const dietaACrear = new CrearDietaDto();
  Object.keys(dieta).map(atributo => {
    dietaACrear[atributo] = dieta[atributo];
  });
  return dietaACrear;
}

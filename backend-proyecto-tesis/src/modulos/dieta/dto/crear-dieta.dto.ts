import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { TipoDietaEntity } from '../../tipo-dieta/tipo-dieta';

export class CrearDietaDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsOptional()
  comidasDieta: ComidaDietaEntity[];

  @IsOptional()
  planesDietaDia: DietaDiaEntity[];

  @IsNotEmpty()
  tipo: TipoDietaEntity;
}

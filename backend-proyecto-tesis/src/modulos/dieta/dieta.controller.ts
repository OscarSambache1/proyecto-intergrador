import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { DietaService } from './dieta.service';
import { DietaEntity } from './dieta.entity';
import { CrearDietaDto } from './dto/crear-dieta.dto';
import { crearDieta } from './funciones/crear-dieta';
import { EditarDietaDto } from './dto/editar-dieta.dto';
import { editarDieta } from './funciones/editar-dieta';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('dieta')
export class DietaController {
  constructor(private readonly _dietaService: DietaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<DietaEntity[]> {
    return await this._dietaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<DietaEntity> {
    const dietaEncontrada = await this._dietaService.findById(id);
    if (dietaEncontrada) {
      return await this._dietaService.findById(id);
    } else {
      throw new BadRequestException('La dieta no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() dieta: CrearDietaDto): Promise<DietaEntity> {
    const dietaACrearse = crearDieta(dieta);
    const arregloErrores = await validate(dietaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando la dieta', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._dietaService.createOne(dietaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() dieta: EditarDietaDto,
  ): Promise<DietaEntity> {
    const dietaEncontrada = await this._dietaService.findById(id);
    if (dietaEncontrada) {
      const diaAEditar = editarDieta(dieta);
      const arregloErrores = await validate(diaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la dieta', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._dietaService.update(id, diaAEditar);
      }
    } else {
      throw new BadRequestException('Dieta no encotrada');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const dietaEncontrada = await this._dietaService.findById(id);
    if (dietaEncontrada) {
      await this._dietaService.delete(id);
    } else {
      throw new BadRequestException('La dieta no existe');
    }
  }
}

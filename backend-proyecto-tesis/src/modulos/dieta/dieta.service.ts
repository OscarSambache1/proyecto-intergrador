import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { DietaEntity } from './dieta.entity';
import { CrearDietaDto } from './dto/crear-dieta.dto';
import { EditarDietaDto } from './dto/editar-dieta.dto';

@Injectable()
export class DietaService {
  constructor(
    @InjectRepository(DietaEntity)
    private readonly _dietaRepository: Repository<DietaEntity>,
  ) {}

  async findAll(consulta: any): Promise<DietaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._dietaRepository.find(consulta);
  }

  async findLike(campo: string): Promise<DietaEntity[]> {
    return await this._dietaRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<DietaEntity> {
    return await this._dietaRepository.findOne(id, {
      relations: [
        'comidasDieta',
        'planesDietaDia',
        'tipo',
        'comidasDieta.comida',
        'comidasDieta.comida.tipo',
      ],
    });
  }

  async createOne(dieta: CrearDietaDto): Promise<DietaEntity> {
    return await this._dietaRepository.save(
      this._dietaRepository.create(dieta),
    );
  }

  async createMany(dietas: CrearDietaDto[]): Promise<DietaEntity[]> {
    const dietasGuardadas: DietaEntity[] = this._dietaRepository.create(dietas);
    return this._dietaRepository.save(dietasGuardadas);
  }

  async update(id: number, dieta: EditarDietaDto): Promise<DietaEntity> {
    await this._dietaRepository.update(id, dieta);
    return await this.findById(id);
  }

  async delete(id: number) {
    const dietaAEliminar = await this.findById(id);
    await this._dietaRepository.remove(dietaAEliminar);
  }
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ComidaDietaEntity } from '../comida-dieta/comida-dieta.entity';
import { DietaDiaEntity } from '../dieta-dia/dieta-dia.entity';
import { TipoDietaEntity } from '../tipo-dieta/tipo-dieta';

@Entity('dieta')
export class DietaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'varchar', nullable: true })
  descripcion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => ComidaDietaEntity, comidaDieta => comidaDieta.dieta)
  comidasDieta: ComidaDietaEntity[];

  @OneToMany(type => DietaDiaEntity, dietaDia => dietaDia.dieta)
  planesDietaDia: DietaDiaEntity[];

  @ManyToOne(tyoe => TipoDietaEntity, tipoDieta => tipoDieta.dietas)
  tipo: TipoDietaEntity;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DietaService } from './dieta.service';
import { DietaController } from './dieta.controller';
import { DietaEntity } from './dieta.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DietaEntity], 'default')],
  controllers: [DietaController],
  providers: [DietaService],
  exports: [],
})
export class DietaModule {}

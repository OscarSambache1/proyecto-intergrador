import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { EjercicioRutinaService } from './ejercicio-rutina.service';
import { EjercicioRutinaEntity } from './ejercicio-rutina.entity';
import { CrearEjercicioRutinaDto } from './dto/crear-ejercicio-rutina.dto';
import { crearEjercicioRutina } from './funciones/crear-ejercicio-rutina';
import { EditarEjercicioRutinaDto } from './dto/editar-ejercicio-rutina.dto';
import { editarEjercicioRutina } from './funciones/editar-ejercicio-rutina';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('ejercicio-rutina')
export class EjercicioRutinaController {
  constructor(
    private readonly _ejercicioRutinaService: EjercicioRutinaService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<EjercicioRutinaEntity[]> {
    return await this._ejercicioRutinaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<EjercicioRutinaEntity> {
    const ejercicioRutinaEncontrado = await this._ejercicioRutinaService.findById(
      id,
    );
    if (ejercicioRutinaEncontrado) {
      return await this._ejercicioRutinaService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() ejercicioRutina: CrearEjercicioRutinaDto,
  ): Promise<EjercicioRutinaEntity> {
    const ejercicioRutinaACrearse = crearEjercicioRutina(ejercicioRutina);
    const arregloErrores = await validate(ejercicioRutinaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._ejercicioRutinaService.createOne(ejercicioRutinaACrearse);
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body() ejerciciosRutinas: CrearEjercicioRutinaDto[],
  ): Promise<CrearEjercicioRutinaDto[]> {
    ejerciciosRutinas.forEach(async ejercicioRutina => {
      const ejercicioRutinaACrearse = crearEjercicioRutina(
        await ejercicioRutina,
      );
      const arregloErrores = await validate(ejercicioRutinaACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._ejercicioRutinaService.createMany(ejerciciosRutinas);
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() ejercicioRutina: EditarEjercicioRutinaDto,
  ): Promise<EjercicioRutinaEntity> {
    const ejercicioRutinaEncontrada = await this._ejercicioRutinaService.findById(
      id,
    );
    if (ejercicioRutinaEncontrada) {
      const ejercicioRutinaAEditar = editarEjercicioRutina(ejercicioRutina);
      const arregloErrores = await validate(ejercicioRutinaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._ejercicioRutinaService.update(id, ejercicioRutinaAEditar);
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const ejercicioRutinaEncontrado = await this._ejercicioRutinaService.findById(
      id,
    );
    if (ejercicioRutinaEncontrado) {
      await this._ejercicioRutinaService.delete(id);
    } else {
      throw new BadRequestException('EL registro no existe');
    }
  }
}

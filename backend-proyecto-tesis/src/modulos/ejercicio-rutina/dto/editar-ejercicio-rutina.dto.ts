import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { EjercicioEntity } from '../../ejercicio/ejercicio.entity';
import { RutinaEntity } from '../../rutina/rutina.entity';

export class EditarEjercicioRutinaDto {
  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  series: number;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  repeticiones: number;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  descanso: number;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  peso: number;

  @IsOptional()
  ejercicio: EjercicioEntity;

  @IsOptional()
  rutina: RutinaEntity;
}

import { IsNotEmpty, IsNumber } from 'class-validator';
import { EjercicioEntity } from '../../ejercicio/ejercicio.entity';
import { RutinaEntity } from '../../rutina/rutina.entity';

export class CrearEjercicioRutinaDto {
  @IsNotEmpty()
  @IsNumber()
  series: number;

  @IsNotEmpty()
  @IsNumber()
  repeticiones: number;

  @IsNotEmpty()
  @IsNumber()
  descanso: number;

  @IsNotEmpty()
  @IsNumber()
  peso: number;

  @IsNotEmpty()
  ejercicio: EjercicioEntity;

  @IsNotEmpty()
  rutina: RutinaEntity;
}

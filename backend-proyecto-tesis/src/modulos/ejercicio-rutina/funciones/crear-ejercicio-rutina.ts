import { CrearEjercicioRutinaDto } from '../dto/crear-ejercicio-rutina.dto';

export function crearEjercicioRutina(
  ejercicioRutina: CrearEjercicioRutinaDto,
): CrearEjercicioRutinaDto {
  const ejercicioRutinaACrear = new CrearEjercicioRutinaDto();
  Object.keys(ejercicioRutina).map(atributo => {
    ejercicioRutinaACrear[atributo] = ejercicioRutina[atributo];
  });
  return ejercicioRutinaACrear;
}

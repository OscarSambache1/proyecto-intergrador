import { EditarEjercicioRutinaDto } from '../dto/editar-ejercicio-rutina.dto';

export function editarEjercicioRutina(
  ejercicioRutina: EditarEjercicioRutinaDto,
): EditarEjercicioRutinaDto {
  const ejercicioRutinaAEditar = new EditarEjercicioRutinaDto();
  Object.keys(ejercicioRutina).map(atributo => {
    ejercicioRutinaAEditar[atributo] = ejercicioRutina[atributo];
  });
  return ejercicioRutinaAEditar;
}

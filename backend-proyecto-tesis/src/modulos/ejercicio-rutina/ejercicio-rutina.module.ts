import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EjercicioRutinaEntity } from './ejercicio-rutina.entity';
import { EjercicioRutinaController } from './ejercicio-rutina.controller';
import { EjercicioRutinaService } from './ejercicio-rutina.service';

@Module({
  imports: [TypeOrmModule.forFeature([EjercicioRutinaEntity], 'default')],
  controllers: [EjercicioRutinaController],
  providers: [EjercicioRutinaService],
  exports: [],
})
export class EjercicioRutinaModule {}

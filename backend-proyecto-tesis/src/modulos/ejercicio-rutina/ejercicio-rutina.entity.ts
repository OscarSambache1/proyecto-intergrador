import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { EjercicioEntity } from '../ejercicio/ejercicio.entity';
import { RutinaEntity } from '../rutina/rutina.entity';

@Entity('ejercicio-rutina')
export class EjercicioRutinaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'series', type: 'integer' })
  series: number;

  @Column({ name: 'repeticiones', type: 'integer' })
  repeticiones: number;

  @Column({ name: 'descanso', type: 'integer' })
  descanso: number;

  @Column({ name: 'peso', type: 'integer' })
  peso: number;

  @ManyToOne(type => EjercicioEntity, ejercicio => ejercicio.ejerciciosRutina)
  ejercicio: EjercicioEntity;

  @ManyToOne(type => RutinaEntity, rutina => rutina.ejerciciosRutina)
  rutina: RutinaEntity;
}

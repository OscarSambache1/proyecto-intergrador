import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { EjercicioRutinaEntity } from './ejercicio-rutina.entity';
import { CrearEjercicioRutinaDto } from './dto/crear-ejercicio-rutina.dto';
import { EditarEjercicioRutinaDto } from './dto/editar-ejercicio-rutina.dto';

@Injectable()
export class EjercicioRutinaService {
  constructor(
    @InjectRepository(EjercicioRutinaEntity)
    private readonly _ejercicioRutinaRepository: Repository<
      EjercicioRutinaEntity
    >,
  ) {}

  async findAll(consulta: any): Promise<EjercicioRutinaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._ejercicioRutinaRepository.find(consulta);
  }

  async findById(id: number): Promise<EjercicioRutinaEntity> {
    return await this._ejercicioRutinaRepository.findOne(id, {
      relations: ['ejercicio', 'rutina'],
    });
  }

  async createOne(
    ejercicioRutina: CrearEjercicioRutinaDto,
  ): Promise<EjercicioRutinaEntity> {
    return await this._ejercicioRutinaRepository.save(
      this._ejercicioRutinaRepository.create(ejercicioRutina),
    );
  }

  async createMany(
    ejerciciosRutina: CrearEjercicioRutinaDto[],
  ): Promise<EjercicioRutinaEntity[]> {
    const ejerciciosRutinasGuardadas: EjercicioRutinaEntity[] = this._ejercicioRutinaRepository.create(
      ejerciciosRutina,
    );
    return this._ejercicioRutinaRepository.save(ejerciciosRutinasGuardadas);
  }

  async update(
    id: number,
    ejercicioRutina: EditarEjercicioRutinaDto,
  ): Promise<EjercicioRutinaEntity> {
    await this._ejercicioRutinaRepository.update(id, ejercicioRutina);
    return await this.findById(id);
  }

  async delete(id: number) {
    const ejercicioRutinaAEliminar = await this.findById(id);
    await this._ejercicioRutinaRepository.remove(ejercicioRutinaAEliminar);
  }
}

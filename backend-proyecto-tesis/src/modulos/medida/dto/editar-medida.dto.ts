import { CrearCLienteDto } from '../../cliente/dto/crear-cliente.dto';
import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
  MinLength,
} from 'class-validator';

export class EditarMedidaDto {
  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  torax: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  abdomen: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  muslo: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  pantorrilla: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  biceps: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  peso: number;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  estatura: number;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaRegistro: string;

  cliente: CrearCLienteDto;
}

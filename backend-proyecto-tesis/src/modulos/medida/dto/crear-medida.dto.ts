import { CrearCLienteDto } from '../../cliente/dto/crear-cliente.dto';
import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
  MinLength,
} from 'class-validator';

export class CrearMedidasDto {
  @IsNumber()
  @IsNotEmpty()
  torax: number;

  @IsNumber()
  @IsNotEmpty()
  abdomen: number;

  @IsNumber()
  @IsNotEmpty()
  muslo: number;

  @IsNumber()
  @IsNotEmpty()
  pantorrilla: number;

  @IsNumber()
  @IsNotEmpty()
  biceps: number;

  @IsNumber()
  @IsNotEmpty()
  peso: number;

  @IsNumber()
  @IsNotEmpty()
  estatura: number;

  @IsString()
  @IsNotEmpty()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaRegistro: string;

  cliente: CrearCLienteDto;
}

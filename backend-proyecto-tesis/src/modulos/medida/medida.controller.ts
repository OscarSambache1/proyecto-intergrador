import {
  Controller,
  Get,
  Query,
  Param,
  BadRequestException,
  Post,
  Body,
  Put,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { Paginacion } from 'interfaces/paginacion';
import { MedidaEntity } from './medida.entity';
import { MedidaService } from './medida.service';
import { CrearMedidasDto } from './dto/crear-medida.dto';
import { crearMedida } from './funciones/crear-medida';
import { validate } from 'class-validator';
import { EditarMedidaDto } from './dto/editar-medida.dto';
import { editarMedida } from './funciones/editar-medida';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('medida')
export class MedidaController {
  constructor(private readonly _medidaService: MedidaService) {}
  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<MedidaEntity[]> {
    return await this._medidaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<MedidaEntity> {
    const medidaEncontrada = await this._medidaService.findById(id);
    if (medidaEncontrada) {
      return await this._medidaService.findById(id);
    } else {
      throw new BadRequestException('medida no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() medida: CrearMedidasDto): Promise<MedidaEntity> {
    console.log(medida);
    const medidaACrearse = crearMedida(medida);
    const arregloErrores = await validate(medidaACrearse);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: creando la medida', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._medidaService.createOne(medidaACrearse);
    }
  }

  @Put(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() medida: EditarMedidaDto,
  ): Promise<MedidaEntity> {
    const medidaEncontrada = await this._medidaService.findById(id);
    if (medidaEncontrada) {
      const medidaAEditarse = editarMedida(medida);
      const arregloErrores = await validate(medidaAEditarse);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la medida', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._medidaService.update(id, medidaAEditarse);
      }
    } else {
      throw new BadRequestException('Medida no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const medidaEncontrada = await this._medidaService.findById(id);
    if (medidaEncontrada) {
      await this._medidaService.delete(id);
    } else {
      throw new BadRequestException('Medida no existe');
    }
  }
}

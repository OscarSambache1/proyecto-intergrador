import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClienteEntity } from '../cliente/cliente.entity';

@Entity('medida')
export class MedidaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'torax',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  torax: number;

  @Column({
    name: 'abdomen',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  abdomen: number;

  @Column({
    name: 'muslo',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  muslo: number;

  @Column({
    name: 'pantorrilla',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  pantorrilla: number;

  @Column({
    name: 'biceps',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  biceps: number;

  @Column({
    name: 'peso',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  peso: number;

  @Column({
    name: 'estatura',
    type: 'decimal',
    precision: 15,
    scale: 2,
  })
  estatura: number;

  @Column({ name: 'fechaRegistro', type: 'varchar' })
  fechaRegistro: string;

  @ManyToOne(type => ClienteEntity, cliente => cliente.medidas)
  cliente: ClienteEntity;
}

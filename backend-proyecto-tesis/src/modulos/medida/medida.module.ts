import { Module } from '@nestjs/common';
import { MedidaService } from './medida.service';
import { MedidaController } from './medida.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MedidaEntity } from './medida.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MedidaEntity], 'default')],
  providers: [MedidaService],
  controllers: [MedidaController],
  exports: [],
})
export class MedidaModule {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MedidaEntity } from './medida.entity';
import { Repository } from 'typeorm';
import { CrearMedidasDto } from './dto/crear-medida.dto';
import { EditarMedidaDto } from './dto/editar-medida.dto';

@Injectable()
export class MedidaService {
  constructor(
    @InjectRepository(MedidaEntity)
    private readonly _medidaRepository: Repository<MedidaEntity>,
  ) {}

  async findAll(consulta: any): Promise<MedidaEntity[]> {
    return await this._medidaRepository.find(consulta);
  }

  async findById(id: number): Promise<MedidaEntity> {
    return await this._medidaRepository.findOne(id, {
      relations: ['cliente'],
    });
  }

  async createOne(medida: CrearMedidasDto): Promise<MedidaEntity> {
    return await this._medidaRepository.save(
      this._medidaRepository.create(medida),
    );
  }

  async createMany(medidas: CrearMedidasDto[]): Promise<MedidaEntity[]> {
    const medidasACrear: MedidaEntity[] = this._medidaRepository.create(
      medidas,
    );
    return this._medidaRepository.save(medidasACrear);
  }

  async update(id: number, medida: EditarMedidaDto): Promise<MedidaEntity> {
    await this._medidaRepository.update(id, medida);
    return await this.findById(id);
  }

  async delete(id: number) {
    const mmedidaAEliminar = await this.findById(id);
    await this._medidaRepository.remove(mmedidaAEliminar);
  }

  async contar(consulta: object): Promise<number> {
    return await this._medidaRepository.count({
      where: consulta,
    });
  }

  async findLast(): Promise<MedidaEntity[]> {
    return await this._medidaRepository.find({
      order: {
        id: 'DESC',
      },
    });
  }
}

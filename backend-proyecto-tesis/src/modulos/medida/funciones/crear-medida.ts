import { CrearMedidasDto } from '../dto/crear-medida.dto';

export function crearMedida(medida: CrearMedidasDto) {
  const medidaACrear = new CrearMedidasDto();
  Object.keys(medida).map(atributo => {
    medidaACrear[atributo] = medida[atributo];
  });
  return medidaACrear;
}

import { EditarMedidaDto } from '../dto/editar-medida.dto';

export function editarMedida(medida: EditarMedidaDto) {
  const medidaAEditar = new EditarMedidaDto();
  Object.keys(medida).map(atributo => {
    medidaAEditar[atributo] = medida[atributo];
  });
  return medidaAEditar;
}

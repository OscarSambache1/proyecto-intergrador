import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { RutinaService } from './rutina.service';
import { RutinaEntity } from './rutina.entity';
import { CrearRutinaDto } from './dto/crear-rutina.dto';
import { crearRutina } from './funciones/crear-rutina';
import { EditarRutinaDto } from './dto/editar-rutina.dto';
import { editarRutina } from './funciones/editar-rutina';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('rutina')
export class RutinaController {
  constructor(private readonly _rutinaService: RutinaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<RutinaEntity[]> {
    return await this._rutinaService.findAll(JSON.parse(consulta));
  }

  @Get('like')
  async like(@Query('parametros') parametros: any): Promise<RutinaEntity[]> {
    return await this._rutinaService.findLike(JSON.parse(parametros));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<RutinaEntity> {
    const rutinaEncontrada = await this._rutinaService.findById(id);
    if (rutinaEncontrada) {
      return await this._rutinaService.findById(id);
    } else {
      throw new BadRequestException('La rutina no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() rutina: CrearRutinaDto): Promise<RutinaEntity> {
    const rutinaACrearse = crearRutina(rutina);
    const arregloErrores = await validate(rutinaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando la rutina', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._rutinaService.createOne(rutinaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() rutina: EditarRutinaDto,
  ): Promise<RutinaEntity> {
    const rutinaEncontrada = await this._rutinaService.findById(id);
    if (rutinaEncontrada) {
      const rutinaAEditar = editarRutina(rutina);
      const arregloErrores = await validate(rutinaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la rutina', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._rutinaService.update(id, rutinaAEditar);
      }
    } else {
      throw new BadRequestException('Rutina no encotrada');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const rutinaEncontrada = await this._rutinaService.findById(id);
    if (rutinaEncontrada) {
      await this._rutinaService.delete(id);
    } else {
      throw new BadRequestException('La rutina no existe');
    }
  }
}

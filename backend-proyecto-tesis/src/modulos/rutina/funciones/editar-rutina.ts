import { EditarRutinaDto } from '../dto/editar-rutina.dto';

export function editarRutina(rutina: EditarRutinaDto): EditarRutinaDto {
  const rutinaAEditar = new EditarRutinaDto();
  Object.keys(rutina).map(atributo => {
    rutinaAEditar[atributo] = rutina[atributo];
  });
  return rutinaAEditar;
}

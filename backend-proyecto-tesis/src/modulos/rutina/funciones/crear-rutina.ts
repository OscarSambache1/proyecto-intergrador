import { CrearRutinaDto } from '../dto/crear-rutina.dto';

export function crearRutina(rutina: CrearRutinaDto): CrearRutinaDto {
  const rutinaACrear = new CrearRutinaDto();
  Object.keys(rutina).map(atributo => {
    rutinaACrear[atributo] = rutina[atributo];
  });
  return rutinaACrear;
}

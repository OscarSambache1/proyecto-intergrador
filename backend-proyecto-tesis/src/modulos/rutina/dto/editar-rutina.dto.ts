import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';

export class EditarRutinaDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre: string;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  musculo: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  ejerciciosRutina: EjercicioRutinaEntity[];

  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
}

import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';

export class CrearRutinaDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  musculo: string;

  @IsOptional()
  ejerciciosRutina: EjercicioRutinaEntity[];

  @IsOptional()
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RutinaEntity } from './rutina.entity';
import { RutinaController } from './rutina.controller';
import { RutinaService } from './rutina.service';

@Module({
  imports: [TypeOrmModule.forFeature([RutinaEntity], 'default')],
  controllers: [RutinaController],
  providers: [RutinaService],
  exports: [],
})
export class RutinaModule {}

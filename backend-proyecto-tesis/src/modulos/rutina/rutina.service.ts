import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { RutinaEntity } from './rutina.entity';
import { CrearRutinaDto } from './dto/crear-rutina.dto';
import { EditarRutinaDto } from './dto/editar-rutina.dto';

@Injectable()
export class RutinaService {
  constructor(
    @InjectRepository(RutinaEntity)
    private readonly _rutinaRepository: Repository<RutinaEntity>,
  ) {}

  async findAll(consulta: any): Promise<RutinaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._rutinaRepository.find(consulta);
  }

  async findLike(parametros: any): Promise<RutinaEntity[]> {
    return await this._rutinaRepository.find({
      order: { id: 'DESC' },
      where: {
        nombre: Like(`%${parametros.nombre}%`),
        musculo: parametros.musculo,
        estado: 1,
      },
      relations: ['ejerciciosRutina'],
    });
  }

  async findById(id: number): Promise<RutinaEntity> {
    return await this._rutinaRepository.findOne(id, {
      relations: [
        'ejerciciosRutina',
        'rutinasPlanEntrenamiento',
        'ejerciciosRutina.rutina',
        'ejerciciosRutina.ejercicio',
      ],
    });
  }

  async createOne(rutina: CrearRutinaDto): Promise<RutinaEntity> {
    return await this._rutinaRepository.save(
      this._rutinaRepository.create(rutina),
    );
  }

  async createMany(rutinas: CrearRutinaDto[]): Promise<RutinaEntity[]> {
    const rutinasGuardadas: RutinaEntity[] = this._rutinaRepository.create(
      rutinas,
    );
    return this._rutinaRepository.save(rutinasGuardadas);
  }

  async update(id: number, rutina: EditarRutinaDto): Promise<RutinaEntity> {
    await this._rutinaRepository.update(id, rutina);
    return await this.findById(id);
  }

  async delete(id: number) {
    const rutinaAEliminar = await this.findById(id);
    await this._rutinaRepository.remove(rutinaAEliminar);
  }
}

import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { EjercicioRutinaEntity } from '../ejercicio-rutina/ejercicio-rutina.entity';
import { RutinaPlanEntrenamientoEntity } from '../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';

@Entity('rutina')
export class RutinaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'varchar', nullable: true })
  descripcion: string;

  @Column({ name: 'musculo', type: 'varchar' })
  musculo: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(
    type => EjercicioRutinaEntity,
    ejercicioRutina => ejercicioRutina.rutina,
  )
  ejerciciosRutina: EjercicioRutinaEntity[];

  @OneToMany(
    type => RutinaPlanEntrenamientoEntity,
    rutinaPlanEntrenamiento => rutinaPlanEntrenamiento.rutina,
  )
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { ComidaDietaEntity } from './comida-dieta.entity';
import { CrearComidaDietaDto } from './dto/crear-comida-dieta.dto';
import { EditarComidaDietaDto } from './dto/editar-comida-dieta.dto';

@Injectable()
export class ComidaDietaService {
  constructor(
    @InjectRepository(ComidaDietaEntity)
    private readonly _comidaDietaRepository: Repository<ComidaDietaEntity>,
  ) {}

  async findAll(consulta: any): Promise<ComidaDietaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._comidaDietaRepository.find(consulta);
  }

  async findById(id: number): Promise<ComidaDietaEntity> {
    return await this._comidaDietaRepository.findOne(id, {
      relations: ['comida', 'dieta', 'comida.tipo'],
    });
  }

  async createOne(
    comidaDieta: CrearComidaDietaDto,
  ): Promise<ComidaDietaEntity> {
    return await this._comidaDietaRepository.save(
      this._comidaDietaRepository.create(comidaDieta),
    );
  }

  async createMany(
    comidaDieta: CrearComidaDietaDto[],
  ): Promise<ComidaDietaEntity[]> {
    const comidasDietasGuardadas: ComidaDietaEntity[] = this._comidaDietaRepository.create(
      comidaDieta,
    );
    return this._comidaDietaRepository.save(comidasDietasGuardadas);
  }

  async update(
    id: number,
    comidaDieta: EditarComidaDietaDto,
  ): Promise<ComidaDietaEntity> {
    await this._comidaDietaRepository.update(id, comidaDieta);
    return await this.findById(id);
  }

  async delete(id: number) {
    const comdaDietaAEliminar = await this.findById(id);
    await this._comidaDietaRepository.remove(comdaDietaAEliminar);
  }
}

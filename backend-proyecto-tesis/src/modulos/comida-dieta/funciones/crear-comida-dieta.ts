import { CrearComidaDietaDto } from '../dto/crear-comida-dieta.dto';

export function crearComidaDieta(
  comidaDieta: CrearComidaDietaDto,
): CrearComidaDietaDto {
  const comidaDietaACrear = new CrearComidaDietaDto();
  Object.keys(comidaDieta).map(atributo => {
    comidaDietaACrear[atributo] = comidaDieta[atributo];
  });
  return comidaDietaACrear;
}

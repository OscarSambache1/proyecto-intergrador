import { EditarComidaDietaDto } from '../dto/editar-comida-dieta.dto';

export function editarComidaDieta(
  comidaDieta: EditarComidaDietaDto,
): EditarComidaDietaDto {
  const comidaDietaAEditar = new EditarComidaDietaDto();
  Object.keys(comidaDieta).map(atributo => {
    comidaDietaAEditar[atributo] = comidaDieta[atributo];
  });
  return comidaDietaAEditar;
}

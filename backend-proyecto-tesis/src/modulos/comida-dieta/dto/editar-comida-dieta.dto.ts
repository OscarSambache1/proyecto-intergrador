import { IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { ComidaEntity } from '../../comida/comida.entity';
import { DietaEntity } from '../../dieta/dieta.entity';

export class EditarComidaDietaDto {
  @IsOptional()
  hora: string;

  @IsOptional()
  comida: ComidaEntity;

  @IsOptional()
  dieta: DietaEntity;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ComidaDietaService } from './comida-dieta.service';
import { ComidaDietaEntity } from './comida-dieta.entity';
import { CrearComidaDietaDto } from './dto/crear-comida-dieta.dto';
import { crearComidaDieta } from './funciones/crear-comida-dieta';
import { EditarComidaDietaDto } from './dto/editar-comida-dieta.dto';
import { editarComidaDieta } from './funciones/editar-comida-dieta';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('comida-dieta')
export class ComidaDietaController {
  constructor(private readonly _comidaDietaService: ComidaDietaService) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<ComidaDietaEntity[]> {
    return await this._comidaDietaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<ComidaDietaEntity> {
    const comidaDietaEncontrada = await this._comidaDietaService.findById(id);
    if (comidaDietaEncontrada) {
      return await this._comidaDietaService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() comidaDieta: CrearComidaDietaDto,
  ): Promise<ComidaDietaEntity> {
    const comidaDietaACrearse = crearComidaDieta(comidaDieta);
    const arregloErrores = await validate(comidaDietaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._comidaDietaService.createOne(comidaDietaACrearse);
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body() comidasDietas: CrearComidaDietaDto[],
  ): Promise<CrearComidaDietaDto[]> {
    comidasDietas.forEach(async comidaDieta => {
      const comidaDietaCrearse = crearComidaDieta(await comidaDieta);
      const arregloErrores = await validate(comidaDietaCrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._comidaDietaService.createMany(comidasDietas);
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() comidaDieta: EditarComidaDietaDto,
  ): Promise<ComidaDietaEntity> {
    const comidaDietaEncontrada = await this._comidaDietaService.findById(id);
    if (comidaDietaEncontrada) {
      const comidaDietaAEditar = editarComidaDieta(comidaDieta);
      const arregloErrores = await validate(comidaDietaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._comidaDietaService.update(id, comidaDietaAEditar);
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const comidaDietaEncontrado = await this._comidaDietaService.findById(id);
    if (comidaDietaEncontrado) {
      await this._comidaDietaService.delete(id);
    } else {
      throw new BadRequestException('EL registro no existe');
    }
  }
}

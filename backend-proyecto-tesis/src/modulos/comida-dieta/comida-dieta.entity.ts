import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ComidaEntity } from '../comida/comida.entity';
import { DietaEntity } from '../dieta/dieta.entity';

@Entity('comida-dieta')
export class ComidaDietaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'hora', type: 'varchar', nullable: true })
  hora: string;

  @ManyToOne(type => ComidaEntity, comida => comida.comidasDieta)
  comida: ComidaEntity;

  @ManyToOne(type => DietaEntity, dieta => dieta.comidasDieta)
  dieta: DietaEntity;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComidaDietaEntity } from './comida-dieta.entity';
import { ComidaDietaController } from './comida-dieta.controller';
import { ComidaDietaService } from './comida-dieta.service';

@Module({
  imports: [TypeOrmModule.forFeature([ComidaDietaEntity], 'default')],
  controllers: [ComidaDietaController],
  providers: [ComidaDietaService],
  exports: [],
})
export class ComidaDietaModule {}

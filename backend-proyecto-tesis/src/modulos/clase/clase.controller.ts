import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ClaseService } from './clase.service';
import { ClaseEntity } from './clase.emtity';
import { CrearClaseDto } from './dto/crear-clase.dto';
import { crearClase } from './funciones/crear-clase';
import { EditarClaseDto } from './dto/editar-clase.dto';
import { editarClase } from './funciones/editar-clase';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('clase')
export class ClaseController {
  constructor(private readonly _claseService: ClaseService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<ClaseEntity[]> {
    return await this._claseService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<ClaseEntity> {
    const claseEncontrada = await this._claseService.findById(id);
    if (claseEncontrada) {
      return await this._claseService.findById(id);
    } else {
      throw new BadRequestException('La clase no existe');
    }
  }

  @Post()
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(@Body() clase: CrearClaseDto): Promise<ClaseEntity> {
    const claseACrearse = crearClase(clase);
    const arregloErrores = await validate(claseACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando la clase', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._claseService.createOne(claseACrearse);
    }
  }

  @Put(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() clase: EditarClaseDto,
  ): Promise<ClaseEntity> {
    const claseEncontrada = await this._claseService.findById(id);
    if (claseEncontrada) {
      const claseAEditar = editarClase(clase);
      const arregloErrores = await validate(claseAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la clase', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._claseService.update(id, claseAEditar);
      }
    } else {
      throw new BadRequestException('Clase no encotrada');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const claseEncontrada = await this._claseService.findById(id);
    if (claseEncontrada) {
      await this._claseService.delete(id);
    } else {
      throw new BadRequestException('La clase no existe');
    }
  }
}

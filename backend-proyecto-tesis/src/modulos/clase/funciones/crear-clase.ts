import { CrearClaseDto } from '../dto/crear-clase.dto';

export function crearClase(clase: CrearClaseDto): CrearClaseDto {
  const claseACrear = new CrearClaseDto();
  Object.keys(clase).map(atributo => {
    claseACrear[atributo] = clase[atributo];
  });
  return claseACrear;
}

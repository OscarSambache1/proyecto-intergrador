import { EditarClaseDto } from '../dto/editar-clase.dto';

export function editarClase(clase: EditarClaseDto): EditarClaseDto {
  const claseAEditar = new EditarClaseDto();
  Object.keys(clase).map(atributo => {
    claseAEditar[atributo] = clase[atributo];
  });
  return claseAEditar;
}

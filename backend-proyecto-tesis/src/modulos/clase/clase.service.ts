import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { ClaseEntity } from './clase.emtity';
import { CrearClaseDto } from './dto/crear-clase.dto';
import { EditarClaseDto } from './dto/editar-clase.dto';

@Injectable()
export class ClaseService {
  constructor(
    @InjectRepository(ClaseEntity)
    private readonly _claseRepository: Repository<ClaseEntity>,
  ) {}

  async findAll(consulta: any): Promise<ClaseEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._claseRepository.find(consulta);
  }

  async findLike(campo: string): Promise<ClaseEntity[]> {
    return await this._claseRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<ClaseEntity> {
    return await this._claseRepository.findOne(id, {
      relations: [
        'clasesHora',
        'clasesHora.clase',
        'clasesHora.instructor',
        'clasesHora.instructor.usuario',
        'clasesHora.clasesDiaHora',
        'clasesHora.clasesDiaHora.dia',
        'clasesHora.clasesDiaHora.claseHora',
      ],
    });
  }

  async createOne(clase: CrearClaseDto): Promise<ClaseEntity> {
    return await this._claseRepository.save(
      this._claseRepository.create(clase),
    );
  }

  async createMany(clases: CrearClaseDto[]): Promise<ClaseEntity[]> {
    const clasesACrear: ClaseEntity[] = this._claseRepository.create(clases);
    return await this._claseRepository.save(clasesACrear);
  }

  async update(id: number, clase: EditarClaseDto): Promise<ClaseEntity> {
    await this._claseRepository.update(id, clase);
    return await this.findById(id);
  }

  async delete(id: number) {
    const claseAEliminar = await this.findById(id);
    await this._claseRepository.remove(claseAEliminar);
  }
}

import { IsString, IsNotEmpty, IsOptional, IsEnum } from 'class-validator';
import { ClaseDiaHoraEntity } from '../../clase-hora-dia/clase-dia-hora.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';

export class CrearClaseDto {
  @IsString()
  @IsNotEmpty()
  nombre?: string;

  @IsString()
  descripcion?: string;

  @IsOptional()
  clasesHora: ClaseHoraEntity[];
}

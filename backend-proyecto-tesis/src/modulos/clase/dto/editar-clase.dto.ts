import { IsString, IsNotEmpty, IsOptional, IsEnum } from 'class-validator';
import { ClaseDiaHoraEntity } from '../../clase-hora-dia/clase-dia-hora.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';

export class EditarClaseDto {
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  nombre?: string;

  @IsOptional()
  @IsString()
  descripcion?: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  clasesHora: ClaseHoraEntity[];
}

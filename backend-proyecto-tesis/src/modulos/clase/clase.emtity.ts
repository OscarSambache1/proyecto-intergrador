import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';

@Entity('clase')
export class ClaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'longtext' })
  descripcion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => ClaseHoraEntity, claseHora => claseHora.clase)
  clasesHora: ClaseHoraEntity[];
}

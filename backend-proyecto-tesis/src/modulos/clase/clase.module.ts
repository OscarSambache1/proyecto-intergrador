import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClaseEntity } from './clase.emtity';
import { ClaseController } from './clase.controller';
import { ClaseService } from './clase.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClaseEntity], 'default')],
  controllers: [ClaseController],
  providers: [ClaseService],
  exports: [],
})
export class ClaseModule {}

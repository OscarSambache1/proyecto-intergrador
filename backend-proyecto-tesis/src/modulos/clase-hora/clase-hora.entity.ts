import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClaseEntity } from '../clase/clase.emtity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { ClaseDiaHoraEntity } from '../clase-hora-dia/clase-dia-hora.entity';

@Entity('clase-hora')
export class ClaseHoraEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'hora_inicio', type: 'varchar', nullable: true})
  horaInicio: string;

  @Column({ name: 'hora_fin', type: 'varchar', nullable: true})
  horaFin: string;

  @ManyToOne(type => ClaseEntity, clase => clase.clasesHora)
  clase: ClaseEntity;

  @ManyToOne(type => InstructorEntity, instructor => instructor.clasesHora)
  instructor: InstructorEntity;

  @OneToMany(type => ClaseDiaHoraEntity, claseDiaHora => claseDiaHora.claseHora)
  clasesDiaHora: ClaseDiaHoraEntity[];
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ClaseHoraEntity } from './clase-hora.entity';
import { ClaseHoraService } from './clase-hora.service';
import { CrearClaseHoraDto } from './dto/crear-clase-hora.dto';
import { crearClaseHora } from './funciones/crear-clase-hora';
import { EditarClaseHoraDto } from './dto/editar-clase-hora.dto';
import { editarClaseHora } from './funciones/editar-clase-hora';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('clase-hora')
export class ClaseHoraController {
  constructor(private readonly _claseHoraService: ClaseHoraService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<ClaseHoraEntity[]> {
    return await this._claseHoraService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<ClaseHoraEntity> {
    const claseHoraEncontrada = await this._claseHoraService.findById(id);
    if (claseHoraEncontrada) {
      return await this._claseHoraService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(@Body() claseHora: CrearClaseHoraDto): Promise<ClaseHoraEntity> {
    const claseHoraACrearse = crearClaseHora(claseHora);
    const arregloErrores = await validate(claseHoraACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._claseHoraService.createOne(claseHoraACrearse);
    }
  }

  @Post('crear-varios')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  createMany(
    @Body() clasesHora: CrearClaseHoraDto[],
  ): Promise<CrearClaseHoraDto[]> {
    clasesHora.forEach(async claseHora => {
      const claseHoraACrearse = crearClaseHora(await claseHora);
      const arregloErrores = await validate(claseHoraACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._claseHoraService.createMany(clasesHora);
  }

  @Put(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() claseHora: EditarClaseHoraDto,
  ): Promise<ClaseHoraEntity> {
    const claseHoraEncontrada = await this._claseHoraService.findById(id);
    if (claseHoraEncontrada) {
      const claseHoraAEditar = editarClaseHora(claseHora);
      const arregloErrores = await validate(claseHoraAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._claseHoraService.update(id, claseHoraAEditar);
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const claseHoraEncontrada = await this._claseHoraService.findById(id);
    if (claseHoraEncontrada) {
      await this._claseHoraService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

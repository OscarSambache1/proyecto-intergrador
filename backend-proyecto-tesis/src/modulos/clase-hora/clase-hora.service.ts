import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { ClaseHoraEntity } from './clase-hora.entity';
import { EditarClaseHoraDto } from './dto/editar-clase-hora.dto';
import { CrearClaseHoraDto } from './dto/crear-clase-hora.dto';

@Injectable()
export class ClaseHoraService {
  constructor(
    @InjectRepository(ClaseHoraEntity)
    private readonly _claseHoraRepository: Repository<ClaseHoraEntity>,
  ) {}

  async findAll(consulta: any): Promise<ClaseHoraEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._claseHoraRepository.find(consulta);
  }

  async findLike(campo: string): Promise<ClaseHoraEntity[]> {
    return await this._claseHoraRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<ClaseHoraEntity> {
    return await this._claseHoraRepository.findOne(id, {
      relations: ['clase', 'instructor', 'clasesDiaHora', 'clasesDiaHora.dia'],
    });
  }

  async createOne(claseHora: CrearClaseHoraDto): Promise<ClaseHoraEntity> {
    return await this._claseHoraRepository.save(
      this._claseHoraRepository.create(claseHora),
    );
  }

  async createMany(claseHora: CrearClaseHoraDto[]): Promise<ClaseHoraEntity[]> {
    const clasesHoraGuardadas: ClaseHoraEntity[] = this._claseHoraRepository.create(
      claseHora,
    );
    return this._claseHoraRepository.save(clasesHoraGuardadas);
  }

  async update(
    id: number,
    claseHora: EditarClaseHoraDto,
  ): Promise<ClaseHoraEntity> {
    await this._claseHoraRepository.update(id, claseHora);
    return await this.findById(id);
  }

  async delete(id: number) {
    const claseHoraAEliminar = await this.findById(id);
    await this._claseHoraRepository.remove(claseHoraAEliminar);
  }
}

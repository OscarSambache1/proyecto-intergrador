import { InstructorEntity } from '../../instructor/instructor.entity';
import { ClaseEntity } from '../../clase/clase.emtity';
import { IsString, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { ClaseDiaHoraEntity } from '../../clase-hora-dia/clase-dia-hora.entity';

export class EditarClaseHoraDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  horaInicio?: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  horaFin?: string;

  @IsOptional()
  instructor?: InstructorEntity;

  @IsOptional()
  clase?: ClaseEntity;

  @IsOptional()
  clasesDiaHora?: ClaseDiaHoraEntity[];
}

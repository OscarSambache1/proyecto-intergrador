import { CrearClaseHoraDto } from '../dto/crear-clase-hora.dto';

export function crearClaseHora(
  claseHora: CrearClaseHoraDto,
): CrearClaseHoraDto {
  const claseHoraCrear = new CrearClaseHoraDto();
  Object.keys(claseHora).map(atributo => {
    claseHoraCrear[atributo] = claseHora[atributo];
  });
  return claseHoraCrear;
}

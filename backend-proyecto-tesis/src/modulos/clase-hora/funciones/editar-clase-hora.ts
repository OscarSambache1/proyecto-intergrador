import { EditarClaseHoraDto } from '../dto/editar-clase-hora.dto';

export function editarClaseHora(
  claseHora: EditarClaseHoraDto,
): EditarClaseHoraDto {
  const claseHoraAEditar = new EditarClaseHoraDto();
  Object.keys(claseHora).map(atributo => {
    claseHoraAEditar[atributo] = claseHora[atributo];
  });
  return claseHoraAEditar;
}

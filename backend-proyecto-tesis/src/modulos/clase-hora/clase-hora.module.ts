import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClaseHoraEntity } from './clase-hora.entity';
import { ClaseHoraController } from './clase-hora.controller';
import { ClaseHoraService } from './clase-hora.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClaseHoraEntity], 'default')],
  controllers: [ClaseHoraController],
  providers: [ClaseHoraService],
  exports: [],
})
export class ClaseHoraModule {}

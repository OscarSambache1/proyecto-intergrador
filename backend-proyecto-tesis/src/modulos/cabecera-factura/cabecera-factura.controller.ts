import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { CabeceraFacturaEntity } from './cabecera-factura.entity';
import { CabeceraFacturaService } from './cabecera-factura.service';
import { CrearCabeceraFacturaDto } from './dto/crear-cabecera-factura.dto';
import { crearCabecera } from './funciones/crear-cabecera';
import { editarCabecera } from './funciones/editar-cabecera';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('cabecera-factura')
export class CabeceraFacturaController {
  constructor(private readonly _cabeceraService: CabeceraFacturaService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<CabeceraFacturaEntity[]> {
    return await this._cabeceraService.findAll(JSON.parse(consulta));
  }

  @Get('findWhereOr')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findByNombreApellidoCodigo(
    @Query() consulta: any,
  ): Promise<CabeceraFacturaEntity[]> {
    return this._cabeceraService.findByNombreApellidoCodigo(consulta.expresion);
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<CabeceraFacturaEntity> {
    const cabeceraEncontrada = await this._cabeceraService.findById(id);
    if (cabeceraEncontrada) {
      return await this._cabeceraService.findById(id);
    } else {
      throw new BadRequestException('La cabecera no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() cabecera: CrearCabeceraFacturaDto,
  ): Promise<CabeceraFacturaEntity> {
    const cabeceraACrear = crearCabecera(cabecera);
    const arregloErrores = await validate(cabeceraACrear);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando la cabecera', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._cabeceraService.createOne(cabeceraACrear);
    }
  }

  @Put(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() cabecera: CabeceraFacturaEntity,
  ): Promise<CabeceraFacturaEntity> {
    const cabeceraEncontrada = await this._cabeceraService.findById(id);
    if (cabeceraEncontrada) {
      const cabeceraAEditar = editarCabecera(cabecera);
      const arregloErrores = await validate(cabeceraAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la cabecera', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._cabeceraService.update(id, cabeceraAEditar);
      }
    } else {
      throw new BadRequestException('Cabecera no encotrada');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const cabeceraEncontrada = await this._cabeceraService.findById(id);
    if (cabeceraEncontrada) {
      await this._cabeceraService.delete(id);
    } else {
      throw new BadRequestException('La cabecera no existe');
    }
  }
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClienteEntity } from '../cliente/cliente.entity';
import { DetalleFacturaEntity } from '../detalle-factura/detalle-factura.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';

@Entity('cabecera-factura')
export class CabeceraFacturaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'fecha', type: 'varchar' })
  fecha: string;

  @Column({
    name: 'subtotal',
    type: 'decimal',
    precision: 15,
    scale: 2,
    default: 0,
  })
  subtotal: number;

  @Column({
    name: 'total',
    type: 'decimal',
    precision: 15,
    scale: 2,
    default: 0,
  })
  total: number;

  @Column({
    name: 'estado',
    type: 'enum',
    enum: ['anulada', 'valida'],
    default: 'valida',
  })
  estado: string;

  @ManyToOne(type => UsuarioEntity, usuario => usuario.cabecerasFactura)
  usuario: UsuarioEntity;

  @OneToMany(
    type => DetalleFacturaEntity,
    detalleFactura => detalleFactura.cabeceraFactura,
  )
  detallesFactura: DetalleFacturaEntity[];
}

import { IsNotEmpty, IsString, IsOptional, IsCurrency } from 'class-validator';
import { ClienteEntity } from '../../cliente/cliente.entity';
import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
import { UsuarioEntity } from '../../usuario/usuario.entity';

export class EditarCabeceraFacturaDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  fecha?: string;

  @IsNotEmpty()
  @IsOptional()
  subtotal?: any;

  @IsNotEmpty()
  @IsOptional()
  total?: any;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  estado: string;

  @IsOptional()
  usuario?: UsuarioEntity;

  @IsOptional()
  detallesFactura?: DetalleFacturaEntity[];
}

import { ClienteEntity } from '../../cliente/cliente.entity';
import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
import {
  IsNotEmpty,
  IsCurrency,
  IsOptional,
  IsString,
  IsNumber,
} from 'class-validator';
import { UsuarioEntity } from '../../usuario/usuario.entity';

export class CrearCabeceraFacturaDto {
  @IsNotEmpty()
  @IsString()
  fecha?: string;

  @IsNotEmpty()
  @IsNumber()
  subtotal?: number;

  @IsNotEmpty()
  @IsNumber()
  total?: number;

  @IsNotEmpty()
  @IsString()
  estado: string;

  @IsNotEmpty()
  usuario?: UsuarioEntity;

  @IsOptional()
  detallesFactura?: DetalleFacturaEntity[];
}

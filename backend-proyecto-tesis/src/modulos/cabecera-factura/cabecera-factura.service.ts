import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { CabeceraFacturaEntity } from './cabecera-factura.entity';
import { CrearCabeceraFacturaDto } from './dto/crear-cabecera-factura.dto';
import { EditarCabeceraFacturaDto } from './dto/editar-cabecera-factura.dto';

@Injectable()
export class CabeceraFacturaService {
  constructor(
    @InjectRepository(CabeceraFacturaEntity)
    private readonly _cabezeraRepository: Repository<CabeceraFacturaEntity>,
  ) {}

  async findAll(consulta: any): Promise<CabeceraFacturaEntity[]> {
    return await this._cabezeraRepository.find(consulta);
  }

  async findByNombreApellidoCodigo(
    expresion: string,
  ): Promise<CabeceraFacturaEntity[]> {
    return await this._cabezeraRepository
      .createQueryBuilder('cabecera-factura')
      .innerJoinAndSelect(
        'cabecera-factura.usuario',
        'usuario',
        'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos',
        { nombres: `%${expresion}%`, apellidos: `%${expresion}%` },
      )
      .orderBy('cabecera-factura.id', 'DESC')
      .getMany();
  }

  async findById(id: number): Promise<CabeceraFacturaEntity> {
    return await this._cabezeraRepository.findOne(id, {
      relations: ['detallesFactura', 'usuario', 'detallesFactura.producto'],
    });
  }

  async createOne(
    cabeceraFactura: CrearCabeceraFacturaDto,
  ): Promise<CabeceraFacturaEntity> {
    return await this._cabezeraRepository.save(
      this._cabezeraRepository.create(cabeceraFactura),
    );
  }

  async createMany(
    cabecerasFactura: CrearCabeceraFacturaDto[],
  ): Promise<CabeceraFacturaEntity[]> {
    const cabecerasACrear: CabeceraFacturaEntity[] = this._cabezeraRepository.create(
      cabecerasFactura,
    );
    return await this._cabezeraRepository.save(cabecerasACrear);
  }

  async update(
    id: number,
    cabeceraFactura: EditarCabeceraFacturaDto,
  ): Promise<CabeceraFacturaEntity> {
    await this._cabezeraRepository.update(id, cabeceraFactura);
    return await this.findById(id);
  }

  async delete(id: number) {
    const cabeceraAEliminar = await this.findById(id);
    await this._cabezeraRepository.remove(cabeceraAEliminar);
  }

  async contar(consulta: object): Promise<number> {
    return await this._cabezeraRepository.count({
      where: consulta,
    });
  }
}

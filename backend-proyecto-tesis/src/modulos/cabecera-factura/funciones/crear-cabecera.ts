import { CrearCabeceraFacturaDto } from '../dto/crear-cabecera-factura.dto';

export function crearCabecera(
  cabeceraFactura: CrearCabeceraFacturaDto,
): CrearCabeceraFacturaDto {
  const cabeceraFacturaACrear = new CrearCabeceraFacturaDto();
  Object.keys(cabeceraFactura).map(atributo => {
    cabeceraFacturaACrear[atributo] = cabeceraFactura[atributo];
  });
  cabeceraFacturaACrear.estado = 'valida';
  return cabeceraFacturaACrear;
}

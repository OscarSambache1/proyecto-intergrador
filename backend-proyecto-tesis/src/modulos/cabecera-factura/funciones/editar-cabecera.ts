import { EditarCabeceraFacturaDto } from '../dto/editar-cabecera-factura.dto';

export function editarCabecera(
  cabeceraFactura: EditarCabeceraFacturaDto,
): EditarCabeceraFacturaDto {
  const cabeceraFacturaAEditar = new EditarCabeceraFacturaDto();
  Object.keys(cabeceraFactura).map(atributo => {
    cabeceraFacturaAEditar[atributo] = cabeceraFactura[atributo];
  });
  return cabeceraFacturaAEditar;
}

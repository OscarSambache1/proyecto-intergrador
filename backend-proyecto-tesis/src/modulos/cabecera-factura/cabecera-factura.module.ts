import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CabeceraFacturaEntity } from './cabecera-factura.entity';
import { CabeceraFacturaService } from './cabecera-factura.service';
import { CabeceraFacturaController } from './cabecera-factura.controller';

@Module({
  imports: [TypeOrmModule.forFeature([CabeceraFacturaEntity], 'default')],
  controllers: [CabeceraFacturaController],
  providers: [CabeceraFacturaService],
  exports: [],
})
export class CabeceraFacturaModule {}

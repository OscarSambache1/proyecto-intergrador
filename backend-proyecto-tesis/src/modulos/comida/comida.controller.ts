import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { ComidaService } from './comida.service';
import { ComidaEntity } from './comida.entity';
import { CrearComidaDto } from './dto/crear-comida.dto';
import { crearComida } from './funciones/crear-comida';
import { EditarComidaDto } from './dto/editar-comida.dto';
import { editarComida } from './funciones/editar-comida';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('comida')
export class ComidaController {
  constructor(private readonly _comidaService: ComidaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<ComidaEntity[]> {
    return await this._comidaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<ComidaEntity> {
    const comidaEncontrada = await this._comidaService.findById(id);
    if (comidaEncontrada) {
      return await this._comidaService.findById(id);
    } else {
      throw new BadRequestException('La comida no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Get(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() comida: CrearComidaDto): Promise<ComidaEntity> {
    const comidaACrearse = crearComida(comida);
    const arregloErrores = await validate(comidaACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando la comida', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._comidaService.createOne(comidaACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() comida: EditarComidaDto,
  ): Promise<ComidaEntity> {
    const comidaEncontrada = await this._comidaService.findById(id);
    if (comidaEncontrada) {
      const comidaAEditar = editarComida(comida);
      const arregloErrores = await validate(comidaAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando la comida', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._comidaService.update(id, comidaAEditar);
      }
    } else {
      throw new BadRequestException('Comida no encotrada');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const comidaEncontrada = await this._comidaService.findById(id);
    if (comidaEncontrada) {
      await this._comidaService.delete(id);
    } else {
      throw new BadRequestException('La comida no existe');
    }
  }
}

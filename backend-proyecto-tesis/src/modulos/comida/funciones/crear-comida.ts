import { CrearComidaDto } from '../dto/crear-comida.dto';

export function crearComida(comida: CrearComidaDto): CrearComidaDto {
  const comidaACrear = new CrearComidaDto();
  Object.keys(comida).map(atributo => {
    comidaACrear[atributo] = comida[atributo];
  });
  return comidaACrear;
}

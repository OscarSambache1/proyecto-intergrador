import { EditarComidaDto } from '../dto/editar-comida.dto';

export function editarComida(comida: EditarComidaDto): EditarComidaDto {
  const comidaAEditar = new EditarComidaDto();
  Object.keys(comida).map(atributo => {
    comidaAEditar[atributo] = comida[atributo];
  });
  return comidaAEditar;
}

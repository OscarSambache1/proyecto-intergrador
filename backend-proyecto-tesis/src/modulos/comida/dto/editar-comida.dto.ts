import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../../tipo-comida/tipo-comida.entity';

export class EditarComidaDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  descripcion: string;

  @IsNotEmpty()
  @IsOptional()
  tipo: TipoComidaEntity;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  comidasDieta: ComidaDietaEntity[];
}

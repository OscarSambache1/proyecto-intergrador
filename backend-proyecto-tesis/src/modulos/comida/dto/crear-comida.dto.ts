import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../../tipo-comida/tipo-comida.entity';

export class CrearComidaDto {
  @IsNotEmpty()
  @IsString()
  descripcion: string;

  @IsNotEmpty()
  tipo: TipoComidaEntity;

  @IsOptional()
  comidasDieta: ComidaDietaEntity[];
}

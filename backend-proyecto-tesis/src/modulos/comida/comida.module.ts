import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComidaEntity } from './comida.entity';
import { ComidaController } from './comida.controller';
import { ComidaService } from './comida.service';

@Module({
  imports: [TypeOrmModule.forFeature([ComidaEntity], 'default')],
  controllers: [ComidaController],
  providers: [ComidaService],
  exports: [],
})
export class ComidaModule {}

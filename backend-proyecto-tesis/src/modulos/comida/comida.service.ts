import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { ComidaEntity } from './comida.entity';
import { CrearComidaDto } from './dto/crear-comida.dto';
import { EditarComidaDto } from './dto/editar-comida.dto';

@Injectable()
export class ComidaService {
  constructor(
    @InjectRepository(ComidaEntity)
    private readonly _comidaRepository: Repository<ComidaEntity>,
  ) {}

  async findAll(consulta: any): Promise<ComidaEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._comidaRepository.find(consulta);
  }

  async findLike(campo: string): Promise<ComidaEntity[]> {
    return await this._comidaRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<ComidaEntity> {
    return await this._comidaRepository.findOne(id, {
      relations: [
        'tipo',
        'comidasDieta',
        'comidasDieta.comida',
        'comidasDieta.dieta',
      ],
    });
  }

  async createOne(comida: CrearComidaDto): Promise<ComidaEntity> {
    return await this._comidaRepository.save(
      this._comidaRepository.create(comida),
    );
  }

  async createMany(comidas: CrearComidaDto[]): Promise<ComidaEntity[]> {
    const comidasACrear: ComidaEntity[] = this._comidaRepository.create(
      comidas,
    );
    return this._comidaRepository.save(comidasACrear);
  }

  async update(id: number, comida: EditarComidaDto): Promise<ComidaEntity> {
    await this._comidaRepository.update(id, comida);
    return await this.findById(id);
  }

  async delete(id: number) {
    const editarAEliminar = await this.findById(id);
    await this._comidaRepository.remove(editarAEliminar);
  }
}

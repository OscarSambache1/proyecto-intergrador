import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ComidaDietaEntity } from '../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../tipo-comida/tipo-comida.entity';

@Entity('comida')
export class ComidaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'descripcion', type: 'longtext', nullable: true })
  descripcion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(type => ComidaDietaEntity, comidaDieta => comidaDieta.comida)
  comidasDieta: ComidaDietaEntity[];

  @ManyToOne(type => TipoComidaEntity, tipoComida => tipoComida.comidas)
  tipo: TipoComidaEntity;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { ClienteService } from './cliente.service';
import { ClienteEntity } from './cliente.entity';
import { CrearCLienteDto } from './dto/crear-cliente.dto';
import { validate } from 'class-validator';
import { editarCliente } from './funciones/editarCliente';
import { EditarCLienteDto } from './dto/editar-cliente.dto';
import { crearCliente } from './funciones/crearCliente';
import { Consulta } from 'interfaces/consulta';
import { SessionGuard } from '../auth/session.guard';
import { SessionUser } from '../usuario/usuario.decorator';
import { UsuarioEntity } from '../usuario/usuario.entity';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';

@UseGuards(SessionGuard)
@Controller('cliente')
export class ClienteController {
  constructor(private readonly _clienteService: ClienteService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<ClienteEntity[]> {
    return await this._clienteService.findAll(JSON.parse(consulta));
  }

  @Get('findWhereOr')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findByNombreApellidoCodigo(
    @Query() consulta: any,
    @SessionUser() user: UsuarioEntity,
  ): Promise<ClienteEntity[]> {
    return this._clienteService.findByNombreApellidoCodigo(
      consulta.expresion,
      consulta.verActivos,
    );
  }

  @Get('findLast')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findLast(@Query('consulta') consulta: any): Promise<ClienteEntity[]> {
    return await this._clienteService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<ClienteEntity> {
    const clienteEncontrado = await this._clienteService.findById(id);
    if (clienteEncontrado) {
      return await this._clienteService.findById(id);
    } else {
      throw new BadRequestException('cliente no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() cliente: CrearCLienteDto): Promise<ClienteEntity> {
    const clienteACrearse = crearCliente(cliente);
    const arregloErrores = await validate(clienteACrearse);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: creando al cliente', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._clienteService.createOne(clienteACrearse);
    }
  }

  @Put(':id')
  @Roles('administrador', 'cliente', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() cliente: EditarCLienteDto,
  ): Promise<ClienteEntity> {
    const clienteEncontrado = await this._clienteService.findById(id);
    if (clienteEncontrado) {
      const clienteAEditarse = editarCliente(cliente);
      const arregloErrores = await validate(clienteAEditarse);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando al cliente', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._clienteService.update(id, clienteAEditarse);
      }
    } else {
      throw new BadRequestException('Cliente no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const clienteEncontrado = await this._clienteService.findById(id);
    if (clienteEncontrado) {
      return await this._clienteService.delete(id);
    } else {
      throw new BadRequestException('Cliente no existe');
    }
  }
}

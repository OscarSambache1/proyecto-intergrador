import { EditarCLienteDto } from '../dto/editar-cliente.dto';
import * as moment from 'moment';

export function editarCliente(cliente: EditarCLienteDto): EditarCLienteDto {
  const clienteAEditar = new EditarCLienteDto();
  Object.keys(cliente).map(atributo => {
    clienteAEditar[atributo] = cliente[atributo];
  });
  if (clienteAEditar.fechaRegistro) {
    clienteAEditar.diaPago = moment(
      clienteAEditar.fechaRegistro,
      'YYYY-MM-DD',
    ).date();
  }
  return clienteAEditar;
}

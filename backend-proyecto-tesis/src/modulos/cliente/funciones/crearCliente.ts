import { CrearCLienteDto } from '../dto/crear-cliente.dto';

export function crearCliente(cliente: CrearCLienteDto): CrearCLienteDto {
  const clienteACrear = new CrearCLienteDto();
  Object.keys(cliente).map(atributo => {
    clienteACrear[atributo] = cliente[atributo];
  });
  clienteACrear.codigo = cliente.codigo.toString();
  clienteACrear.rol = 'cliente';
  clienteACrear.estado = 1;
  clienteACrear.diaPago = Number(cliente.fechaRegistro.substring(8, 10));
  return clienteACrear;
}

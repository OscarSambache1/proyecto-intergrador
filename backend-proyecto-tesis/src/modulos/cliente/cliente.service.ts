import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like } from 'typeorm';
import { ClienteEntity } from './cliente.entity';
import { EditarCLienteDto } from './dto/editar-cliente.dto';
import { CrearCLienteDto } from './dto/crear-cliente.dto';

@Injectable()
export class ClienteService {
  constructor(
    @InjectRepository(ClienteEntity)
    private readonly _clienteRepository: Repository<ClienteEntity>,
  ) {}

  async findAll(consulta: any): Promise<ClienteEntity[]> {
    return await this._clienteRepository.find(consulta);
  }

  async findByNombreApellidoCodigo(
    expresion: string,
    verActivos: string,
  ): Promise<ClienteEntity[]> {
    let condicionEstado;
    if (verActivos === 'true') {
      condicionEstado = 'cliente.estado = 1';
    } else {
      condicionEstado = '';
    }
    return await this._clienteRepository
      .createQueryBuilder('cliente')
      .innerJoinAndSelect(
        'cliente.usuario',
        'usuario',
        'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos or codigo LIKE :codigo',
        {
          nombres: `%${expresion}%`,
          codigo: `%${expresion}%`,
          apellidos: `%${expresion}%`,
        },
      )
      .where(condicionEstado)
      .getMany();
  }

  async findById(id: number): Promise<ClienteEntity> {
    return await this._clienteRepository.findOne(id, {
      relations: ['usuario', 'medidas', 'planesDieta', 'planesEntrenamiento'],
    });
  }

  async createOne(cliente: CrearCLienteDto): Promise<ClienteEntity> {
    cliente.codigo = await this.setearCodigoCliente();
    return await this._clienteRepository.save(
      this._clienteRepository.create(cliente),
    );
  }

  async createMany(clientes: CrearCLienteDto[]): Promise<ClienteEntity[]> {
    const clientesACrear: ClienteEntity[] = this._clienteRepository.create(
      clientes,
    );
    return this._clienteRepository.save(clientesACrear);
  }

  async update(id: number, cliente: EditarCLienteDto): Promise<ClienteEntity> {
    await this._clienteRepository.update(id, cliente);
    return await this.findById(id);
  }

  async delete(id: number): Promise<string> {
    const clienteAEliminar = await this.findById(id);
    await this._clienteRepository.remove(clienteAEliminar);
    return `Se ha eliminado el elemento ${id}`;
  }

  async contar(consulta: object): Promise<number> {
    return await this._clienteRepository.count({
      where: consulta,
    });
  }

  async findLast(): Promise<ClienteEntity[]> {
    return await this._clienteRepository.find({
      order: {
        id: 'DESC',
      },
    });
  }

  async setearCodigoCliente() {
    const clientes = await this.findLast();
    const ultimoCliente = clientes[0];
    if (ultimoCliente) {
      return (+ultimoCliente.codigo + 1).toString();
    } else {
      return '1000';
    }
  }
}

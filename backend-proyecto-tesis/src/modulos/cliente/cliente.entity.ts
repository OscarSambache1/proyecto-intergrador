import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { MedidaEntity } from '../medida/medida.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { PlanDietaClienteEntity } from '../plan-dieta-cliente/plan-dieta-cliente.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';
import { CabeceraFacturaEntity } from '../cabecera-factura/cabecera-factura.entity';

@Entity('cliente')
export class ClienteEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'codigo', type: 'varchar', length: 6, unique: true })
  codigo: string;

  @Column({ name: 'fecha-registro', type: 'date' })
  fechaRegistro: string;

  @Column({ name: 'dia-pago', type: 'int' })
  diaPago: number;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @Column({
    name: 'rol',
    type: 'enum',
    enum: ['administrador', 'cliente', 'instructor', 'empleado'],
  })
  rol: string;

  @OneToMany(type => MedidaEntity, medida => medida.cliente)
  medidas: MedidaEntity[];

  @OneToMany(
    type => PlanEntrenamientoClienteInstructorEntity,
    planEntrenamientoClienteInstructor =>
      planEntrenamientoClienteInstructor.cliente,
  )
  planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];

  @OneToMany(
    type => PlanDietaClienteEntity,
    planDietaCliente => planDietaCliente.cliente,
  )
  planesDieta: PlanDietaClienteEntity[];

  @OneToOne(type => UsuarioEntity, usuario => usuario.cliente)
  @JoinColumn()
  usuario: UsuarioEntity;
}

import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
  MinLength,
} from 'class-validator';
import { MedidaEntity } from '../../medida/medida.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { PlanDietaClienteEntity } from '../../plan-dieta-cliente/plan-dieta-cliente.entity';
import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';

export class EditarCLienteDto {
  @IsNotEmpty()
  @IsOptional()
  @IsNumberString()
  codigo: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaRegistro: string;

  @IsNotEmpty()
  @IsOptional()
  @IsNumber()
  diaPago: number;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @IsEnum(['administrador', 'cliente', 'instructor'])
  rol: string;

  @IsOptional()
  medidas: MedidaEntity[];

  @IsOptional()
  planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];

  @IsOptional()
  planesDieta: PlanDietaClienteEntity[];
}

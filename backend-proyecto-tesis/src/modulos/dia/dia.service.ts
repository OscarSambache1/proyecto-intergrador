import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { DiaEntity } from './dia.entity';

@Injectable()
export class DiaService {
  constructor(
    @InjectRepository(DiaEntity)
    private readonly _diaRepository: Repository<DiaEntity>,
  ) {}

  async findAll(consulta: any): Promise<DiaEntity[]> {
    return await this._diaRepository.find(consulta);
  }

  async findLike(campo: string): Promise<DiaEntity[]> {
    return await this._diaRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<DiaEntity> {
    return await this._diaRepository.findOne(id, {
      relations: ['clasesDiaHora'],
    });
  }

  async createMany(dias: DiaEntity[]): Promise<DiaEntity[]> {
    const diasACrear: DiaEntity[] = this._diaRepository.create(dias);
    return this._diaRepository.save(diasACrear);
  }
}

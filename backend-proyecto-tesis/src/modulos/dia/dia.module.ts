import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiaEntity } from './dia.entity';
import { DiaService } from './dia.service';
import { DiaController } from './dia.controller';

@Module({
  imports: [TypeOrmModule.forFeature([DiaEntity], 'default')],
  providers: [DiaService],
  controllers: [DiaController],
  exports: [DiaService],
})
export class DiaModule {}

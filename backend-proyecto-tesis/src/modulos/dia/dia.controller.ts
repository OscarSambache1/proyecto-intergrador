import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { DiaService } from './dia.service';
import { DiaEntity } from './dia.entity';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('dia')
export class DiaController {
  constructor(private readonly _diaService: DiaService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<DiaEntity[]> {
    return await this._diaService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<DiaEntity> {
    const dietaEncontrada = await this._diaService.findById(id);
    if (dietaEncontrada) {
      return await this._diaService.findById(id);
    } else {
      throw new BadRequestException('El dia no existe');
    }
  }
}

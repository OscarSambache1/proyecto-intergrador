import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClaseDiaHoraEntity } from '../clase-hora-dia/clase-dia-hora.entity';
import { RutinaPlanEntrenamientoEntity } from '../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { DietaDiaEntity } from '../dieta-dia/dieta-dia.entity';

@Entity('dia')
export class DiaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar', nullable: true })
  nombre: string;

  @OneToMany(type => ClaseDiaHoraEntity, claseDiaHora => claseDiaHora.dia)
  clasesDiaHora: ClaseDiaHoraEntity[];

  @OneToMany(
    type => RutinaPlanEntrenamientoEntity,
    rutinaPlanEntrenamiento => rutinaPlanEntrenamiento.dia,
  )
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];

  @OneToMany(type => DietaDiaEntity, dietaDia => dietaDia.dia)
  dietasDia: DietaDiaEntity[];
}

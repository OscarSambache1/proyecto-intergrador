import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { RutinaEntity } from '../rutina/rutina.entity';
import { PlanEntrenamientoEntity } from '../plan-entrenamiento/plan-entrenamiento.entity';
import { DiaEntity } from '../dia/dia.entity';

@Entity('rutina-plan-entrenamiento')
export class RutinaPlanEntrenamientoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => RutinaEntity, rutina => rutina.rutinasPlanEntrenamiento)
  rutina: RutinaEntity;

  @ManyToOne(
    type => PlanEntrenamientoEntity,
    planEntrenamiento => planEntrenamiento.rutinasPlanEntrenamiento,
  )
  planEntrenamiento: PlanEntrenamientoEntity;

  @ManyToOne(type => DiaEntity, dia => dia.rutinasPlanEntrenamiento)
  dia: DiaEntity;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { RutinaPlanEntrenamientoService } from './rutina-plan-entrenamiento.service';
import { RutinaPlanEntrenamientoEntity } from './rutina-plan-entrenamiento.entity';
import { CrearRutinaPlanEntrenamientoDto } from './dto/crear-rutina-plan-entrenamiento.dto';
import { crearRutinaPlanEntrenamiento } from './funciones/crear-rutina-plan-entrenamiento';
import { EditarRutinaPlanEntrenamientoDto } from './dto/editar-rutina-plan-entrenamiento.dto';
import { editarRutinaPlanEntrenamiento } from './funciones/editar-rutina-plan-entrenamiento';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('rutina-plan-entrenamiento')
export class RutinaPlanEntrenamientoController {
  constructor(
    private readonly _rutinaPlanEntrenamientoService: RutinaPlanEntrenamientoService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<RutinaPlanEntrenamientoEntity[]> {
    return await this._rutinaPlanEntrenamientoService.findAll(
      JSON.parse(consulta),
    );
  }

  @Get(':id')
  async findOne(
    @Param('id') id: number,
  ): Promise<RutinaPlanEntrenamientoEntity> {
    const rutinaPlanEntrenamientoEncontrado = await this._rutinaPlanEntrenamientoService.findById(
      id,
    );
    if (rutinaPlanEntrenamientoEncontrado) {
      return await this._rutinaPlanEntrenamientoService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() rutinaPlanEntrenamiento: CrearRutinaPlanEntrenamientoDto,
  ): Promise<RutinaPlanEntrenamientoEntity> {
    const rutinaPlanEntrenamientoACrearse = crearRutinaPlanEntrenamiento(
      rutinaPlanEntrenamiento,
    );
    const arregloErrores = await validate(rutinaPlanEntrenamientoACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._rutinaPlanEntrenamientoService.createOne(
        rutinaPlanEntrenamientoACrearse,
      );
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body() rutinasPlanesEntrenamiento: CrearRutinaPlanEntrenamientoDto[],
  ): Promise<CrearRutinaPlanEntrenamientoDto[]> {
    rutinasPlanesEntrenamiento.forEach(async rutinaPlanEntrenamiento => {
      const rutinaPlanEntrenamientoACrearse = crearRutinaPlanEntrenamiento(
        await rutinaPlanEntrenamiento,
      );
      const arregloErrores = await validate(rutinaPlanEntrenamientoACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._rutinaPlanEntrenamientoService.createMany(
      rutinasPlanesEntrenamiento,
    );
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() rutinaPlanEntrenamiento: EditarRutinaPlanEntrenamientoDto,
  ): Promise<RutinaPlanEntrenamientoEntity> {
    const planEntrenamientoEncontrado = await this._rutinaPlanEntrenamientoService.findById(
      id,
    );
    if (planEntrenamientoEncontrado) {
      const rutinaPlanEntrenamientoAEditar = editarRutinaPlanEntrenamiento(
        rutinaPlanEntrenamiento,
      );
      const arregloErrores = await validate(rutinaPlanEntrenamientoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._rutinaPlanEntrenamientoService.update(
          id,
          rutinaPlanEntrenamientoAEditar,
        );
      }
    } else {
      throw new BadRequestException('Registro no encontrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const rutinaPlanEntrenamientoEncontrado = await this._rutinaPlanEntrenamientoService.findById(
      id,
    );
    if (rutinaPlanEntrenamientoEncontrado) {
      await this._rutinaPlanEntrenamientoService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

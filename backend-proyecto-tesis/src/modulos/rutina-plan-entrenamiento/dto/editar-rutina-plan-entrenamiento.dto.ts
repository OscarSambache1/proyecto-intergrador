import { IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { RutinaEntity } from '../../rutina/rutina.entity';
import { PlanEntrenamientoEntity } from '../../plan-entrenamiento/plan-entrenamiento.entity';
import { DiaEntity } from '../../dia/dia.entity';

export class EditarRutinaPlanEntrenamientoDto {
  @IsOptional()
  rutina: RutinaEntity;

  @IsOptional()
  planEntrenamiento: PlanEntrenamientoEntity;

  @IsOptional()
  dia: DiaEntity;
}

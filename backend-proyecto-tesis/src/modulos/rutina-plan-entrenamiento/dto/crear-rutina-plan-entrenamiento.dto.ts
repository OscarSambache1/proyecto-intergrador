import { IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { RutinaEntity } from '../../rutina/rutina.entity';
import { PlanEntrenamientoEntity } from '../../plan-entrenamiento/plan-entrenamiento.entity';
import { DiaEntity } from '../../dia/dia.entity';

export class CrearRutinaPlanEntrenamientoDto {
  @IsOptional()
  rutina: RutinaEntity;

  @IsOptional()
  planEntrenamiento: PlanEntrenamientoEntity;

  @IsNotEmpty()
  @IsOptional()
  dia: DiaEntity;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RutinaPlanEntrenamientoEntity } from './rutina-plan-entrenamiento.entity';
import { RutinaPlanEntrenamientoController } from './rutina-plan-entrenamiento.controller';
import { RutinaPlanEntrenamientoService } from './rutina-plan-entrenamiento.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([RutinaPlanEntrenamientoEntity], 'default'),
  ],
  controllers: [RutinaPlanEntrenamientoController],
  providers: [RutinaPlanEntrenamientoService],
  exports: [],
})
export class RutinaPlanEntrenamientoModule {}

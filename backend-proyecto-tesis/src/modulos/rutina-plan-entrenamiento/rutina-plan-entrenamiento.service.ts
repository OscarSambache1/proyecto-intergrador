import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { RutinaPlanEntrenamientoEntity } from './rutina-plan-entrenamiento.entity';
import { CrearRutinaPlanEntrenamientoDto } from './dto/crear-rutina-plan-entrenamiento.dto';
import { EditarRutinaPlanEntrenamientoDto } from './dto/editar-rutina-plan-entrenamiento.dto';

@Injectable()
export class RutinaPlanEntrenamientoService {
  constructor(
    @InjectRepository(RutinaPlanEntrenamientoEntity)
    private readonly _rutinaPlanEntrenamientoRepository: Repository<
      RutinaPlanEntrenamientoEntity
    >,
  ) {}

  async findAll(consulta: any): Promise<RutinaPlanEntrenamientoEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._rutinaPlanEntrenamientoRepository.find(consulta);
  }

  async findLike(campo: string): Promise<RutinaPlanEntrenamientoEntity[]> {
    return await this._rutinaPlanEntrenamientoRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<RutinaPlanEntrenamientoEntity> {
    return await this._rutinaPlanEntrenamientoRepository.findOne(id, {
      relations: ['rutina', 'planEntrenamiento', 'dia'],
    });
  }

  async createOne(
    rutinaPlanEntrenamiento: CrearRutinaPlanEntrenamientoDto,
  ): Promise<RutinaPlanEntrenamientoEntity> {
    return await this._rutinaPlanEntrenamientoRepository.save(
      this._rutinaPlanEntrenamientoRepository.create(rutinaPlanEntrenamiento),
    );
  }

  async createMany(
    rutinaPlanesEntrenamiento: CrearRutinaPlanEntrenamientoDto[],
  ): Promise<RutinaPlanEntrenamientoEntity[]> {
    const rutinasGuardadas: RutinaPlanEntrenamientoEntity[] = this._rutinaPlanEntrenamientoRepository.create(
      rutinaPlanesEntrenamiento,
    );
    return this._rutinaPlanEntrenamientoRepository.save(rutinasGuardadas);
  }

  async update(
    id: number,
    planEntrenamiento: EditarRutinaPlanEntrenamientoDto,
  ): Promise<RutinaPlanEntrenamientoEntity> {
    await this._rutinaPlanEntrenamientoRepository.update(id, planEntrenamiento);
    return await this.findById(id);
  }

  async delete(id: number) {
    const rutinaPlanEntrenamientoAEliminar = await this.findById(id);
    await this._rutinaPlanEntrenamientoRepository.remove(
      rutinaPlanEntrenamientoAEliminar,
    );
  }
}

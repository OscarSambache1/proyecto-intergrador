import { EditarRutinaPlanEntrenamientoDto } from '../dto/editar-rutina-plan-entrenamiento.dto';

export function editarRutinaPlanEntrenamiento(
  rutinaPlanEntrenamiento: EditarRutinaPlanEntrenamientoDto,
): EditarRutinaPlanEntrenamientoDto {
  const rutinaPlanEntrenamientoAEditar = new EditarRutinaPlanEntrenamientoDto();
  Object.keys(rutinaPlanEntrenamiento).map(atributo => {
    rutinaPlanEntrenamientoAEditar[atributo] =
      rutinaPlanEntrenamiento[atributo];
  });
  return rutinaPlanEntrenamientoAEditar;
}

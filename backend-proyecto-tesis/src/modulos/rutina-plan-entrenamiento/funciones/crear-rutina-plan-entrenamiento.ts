import { CrearRutinaPlanEntrenamientoDto } from '../dto/crear-rutina-plan-entrenamiento.dto';

export function crearRutinaPlanEntrenamiento(
  rutinaPlanEntrenamiento: CrearRutinaPlanEntrenamientoDto,
): CrearRutinaPlanEntrenamientoDto {
  const rutinaPlanEntrenamientoACrear = new CrearRutinaPlanEntrenamientoDto();
  Object.keys(rutinaPlanEntrenamiento).map(atributo => {
    rutinaPlanEntrenamientoACrear[atributo] = rutinaPlanEntrenamiento[atributo];
  });
  return rutinaPlanEntrenamientoACrear;
}

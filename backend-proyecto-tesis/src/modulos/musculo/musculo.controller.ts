import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { MusculoService } from './musculo.service';
import { MusculoEntity } from './musculo.entity';
import { CrearMusculoDto } from './dto/crear-musculo.dto';
import { crearMusculo } from './funciones/crear-musculo';
import { EditarMusculoDto } from './dto/editar-musculo.dto';
import { editarMusculo } from './funciones/editar-musculo';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('musculo')
export class MusculoController {
  constructor(private readonly _musculoService: MusculoService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<MusculoEntity[]> {
    return await this._musculoService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<MusculoEntity> {
    const musculoEncontrado = await this._musculoService.findById(id);
    if (musculoEncontrado) {
      return await this._musculoService.findById(id);
    } else {
      throw new BadRequestException('EL Músculo no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async create(@Body() musculo: CrearMusculoDto): Promise<MusculoEntity> {
    const musculoACrearse = crearMusculo(musculo);
    const arregloErrores = await validate(musculoACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el musculo', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._musculoService.createOne(musculoACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() musculo: EditarMusculoDto,
  ): Promise<MusculoEntity> {
    const musculoEncontrado = await this._musculoService.findById(id);
    if (musculoEncontrado) {
      const musculoAEditar = editarMusculo(musculo);
      const arregloErrores = await validate(musculoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el músculo', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._musculoService.update(id, musculoAEditar);
      }
    } else {
      throw new BadRequestException('Músculo no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const musculoEncontrado = await this._musculoService.findById(id);
    if (musculoEncontrado) {
      await this._musculoService.delete(id);
    } else {
      throw new BadRequestException('EL músculo no existe');
    }
  }
}

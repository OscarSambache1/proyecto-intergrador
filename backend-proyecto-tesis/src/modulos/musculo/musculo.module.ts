import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MusculoEntity } from './musculo.entity';
import { MusculoController } from './musculo.controller';
import { MusculoService } from './musculo.service';

@Module({
  imports: [TypeOrmModule.forFeature([MusculoEntity], 'default')],
  controllers: [MusculoController],
  providers: [MusculoService],
  exports: [MusculoService],
})
export class MusculoModule {}

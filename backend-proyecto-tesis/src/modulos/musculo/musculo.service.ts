import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { MusculoEntity } from './musculo.entity';
import { CrearMusculoDto } from './dto/crear-musculo.dto';
import { EditarMusculoDto } from './dto/editar-musculo.dto';

@Injectable()
export class MusculoService {
  constructor(
    @InjectRepository(MusculoEntity)
    private readonly _musculoRepository: Repository<MusculoEntity>,
  ) {}

  async findAll(consulta: any): Promise<MusculoEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._musculoRepository.find(consulta);
  }

  async findById(id: number): Promise<MusculoEntity> {
    return await this._musculoRepository.findOne(id, {
      relations: [],
    });
  }

  async createOne(musculo: CrearMusculoDto): Promise<MusculoEntity> {
    return await this._musculoRepository.save(
      this._musculoRepository.create(musculo),
    );
  }

  async createMany(musculos: CrearMusculoDto[]): Promise<MusculoEntity[]> {
    const musculosACrear: MusculoEntity[] = this._musculoRepository.create(
      musculos,
    );
    return this._musculoRepository.save(musculosACrear);
  }

  async update(id: number, musculo: EditarMusculoDto): Promise<MusculoEntity> {
    await this._musculoRepository.update(id, musculo);
    return await this.findById(id);
  }

  async delete(id: number) {
    const musculoAEliminar = await this.findById(id);
    await this._musculoRepository.remove(musculoAEliminar);
  }
}

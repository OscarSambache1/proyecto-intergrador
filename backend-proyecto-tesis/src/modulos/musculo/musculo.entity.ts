import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { EjercicioMusculoEntity } from '../ejercicio-musculo/ejercicio-musculo.entity';

@Entity('musculo')
export class MusculoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(
    type => EjercicioMusculoEntity,
    ejercicioMusculo => ejercicioMusculo.musculo,
  )
  ejerciciosMusculo: EjercicioMusculoEntity[];
}

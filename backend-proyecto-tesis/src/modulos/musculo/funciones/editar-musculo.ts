import { EditarMusculoDto } from '../dto/editar-musculo.dto';

export function editarMusculo(musculo: EditarMusculoDto): EditarMusculoDto {
  const musculoAEditar = new EditarMusculoDto();
  Object.keys(musculo).map(atributo => {
    musculoAEditar[atributo] = musculo[atributo];
  });
  return musculoAEditar;
}

import { CrearMusculoDto } from '../dto/crear-musculo.dto';

export function crearMusculo(musculo: CrearMusculoDto): CrearMusculoDto {
  const musculoACrear = new CrearMusculoDto();
  Object.keys(musculo).map(atributo => {
    musculoACrear[atributo] = musculo[atributo];
  });
  return musculoACrear;
}

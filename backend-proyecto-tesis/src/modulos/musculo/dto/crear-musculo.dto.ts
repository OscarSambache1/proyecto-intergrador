import {
  IsNotEmpty,
  IsString,
  IsNumberString,
  IsNumber,
  IsCurrency,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
export class CrearMusculoDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  ejerciciosMusculo: EjercicioMusculoEntity[];
}

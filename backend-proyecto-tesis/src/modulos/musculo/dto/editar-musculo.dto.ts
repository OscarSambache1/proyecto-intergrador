import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
export class EditarMusculoDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  ejerciciosMusculo: EjercicioMusculoEntity[];
}

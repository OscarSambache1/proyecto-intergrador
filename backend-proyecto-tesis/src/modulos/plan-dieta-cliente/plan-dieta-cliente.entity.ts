import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClienteEntity } from '../cliente/cliente.entity';
import { PlanDietaEntity } from '../plan-dieta/plan-dieta.entity';

@Entity('plan-dieta-cliente')
export class PlanDietaClienteEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'fecha-inicio', type: 'date' })
  fechaInicio: string;

  @Column({ name: 'fecha-fin', type: 'date' })
  fechaFin: string;

  @ManyToOne(type => ClienteEntity, cliente => cliente.planesDieta)
  cliente: ClienteEntity;

  @ManyToOne(type => PlanDietaEntity, planDieta => planDieta.planesDietaCliente)
  planDieta: PlanDietaEntity;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanDietaClienteEntity } from './plan-dieta-cliente.entity';
import { PlanDietaClienteController } from './plan-dieta-cliente.controller';
import { PlanDietaClienteService } from './plan-dieta-cliente.service';

@Module({
  imports: [TypeOrmModule.forFeature([PlanDietaClienteEntity], 'default')],
  controllers: [PlanDietaClienteController],
  providers: [PlanDietaClienteService],
  exports: [],
})
export class PlanDietaClienteModule {}

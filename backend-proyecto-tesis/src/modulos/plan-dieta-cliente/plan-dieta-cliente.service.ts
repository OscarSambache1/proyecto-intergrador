import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { PlanDietaClienteEntity } from './plan-dieta-cliente.entity';
import { CrearPlanDietaClienteDto } from './dto/crear-plan-dieta-cliente.dto';
import { EditarPlanDietaClienteDto } from './dto/editar-plan-dieta-cliente.dto';

@Injectable()
export class PlanDietaClienteService {
  constructor(
    @InjectRepository(PlanDietaClienteEntity)
    private readonly _planDietaClienteRepository: Repository<
      PlanDietaClienteEntity
    >,
  ) {}

  async findAll(consulta: any): Promise<PlanDietaClienteEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._planDietaClienteRepository.find(consulta);
  }

  async findLike(campo: string): Promise<PlanDietaClienteEntity[]> {
    return await this._planDietaClienteRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<PlanDietaClienteEntity> {
    return await this._planDietaClienteRepository.findOne(id, {
      relations: ['cliente', 'planDieta', 'cliente.usuario'],
    });
  }

  async createOne(
    planDietaCliente: CrearPlanDietaClienteDto,
  ): Promise<PlanDietaClienteEntity> {
    return await this._planDietaClienteRepository.save(
      this._planDietaClienteRepository.create(planDietaCliente),
    );
  }

  async createMany(
    planesDietaCliente: CrearPlanDietaClienteDto[],
  ): Promise<PlanDietaClienteEntity[]> {
    const rutinasGuardadas: PlanDietaClienteEntity[] = this._planDietaClienteRepository.create(
      planesDietaCliente,
    );
    return this._planDietaClienteRepository.save(rutinasGuardadas);
  }

  async update(
    id: number,
    planDietaCliente: EditarPlanDietaClienteDto,
  ): Promise<PlanDietaClienteEntity> {
    await this._planDietaClienteRepository.update(id, planDietaCliente);
    return await this.findById(id);
  }

  async delete(id: number) {
    const planDietaClienteAEliminar = await this.findById(id);
    await this._planDietaClienteRepository.remove(planDietaClienteAEliminar);
  }
}

import { EditarPlanDietaClienteDto } from '../dto/editar-plan-dieta-cliente.dto';

export function editarPlanDietaCliente(
  planDietaCliente: EditarPlanDietaClienteDto,
): EditarPlanDietaClienteDto {
  const planDietaClienteAEditar = new EditarPlanDietaClienteDto();
  Object.keys(planDietaCliente).map(atributo => {
    planDietaClienteAEditar[atributo] = planDietaCliente[atributo];
  });
  return planDietaClienteAEditar;
}

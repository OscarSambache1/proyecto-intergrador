import { CrearPlanDietaClienteDto } from '../dto/crear-plan-dieta-cliente.dto';

export function crearPlanDietaCliente(
  planDietaCliente: CrearPlanDietaClienteDto,
): CrearPlanDietaClienteDto {
  const planDietaClienteACrear = new CrearPlanDietaClienteDto();
  Object.keys(planDietaCliente).map(atributo => {
    planDietaClienteACrear[atributo] = planDietaCliente[atributo];
  });
  return planDietaClienteACrear;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { PlanDietaClienteService } from './plan-dieta-cliente.service';
import { PlanDietaClienteEntity } from './plan-dieta-cliente.entity';
import { CrearPlanDietaClienteDto } from './dto/crear-plan-dieta-cliente.dto';
import { crearPlanDietaCliente } from './funciones/crear-plan-dieta-cliente';
import { EditarPlanDietaClienteDto } from './dto/editar-plan-dieta-cliente.dto';
import { editarPlanDietaCliente } from './funciones/editar-plan-dieta-cliente';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('plan-dieta-cliente')
export class PlanDietaClienteController {
  constructor(
    private readonly _planDietaClienteService: PlanDietaClienteService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<PlanDietaClienteEntity[]> {
    return await this._planDietaClienteService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<PlanDietaClienteEntity> {
    const planDietaClienteEncontrado = await this._planDietaClienteService.findById(
      id,
    );
    if (planDietaClienteEncontrado) {
      return await this._planDietaClienteService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() planDietaCliente: CrearPlanDietaClienteDto,
  ): Promise<PlanDietaClienteEntity> {
    const planDietaClienteACrearse = crearPlanDietaCliente(planDietaCliente);
    const arregloErrores = await validate(planDietaClienteACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._planDietaClienteService.createOne(planDietaClienteACrearse);
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body() planesDietaCliente: CrearPlanDietaClienteDto[],
  ): Promise<PlanDietaClienteEntity[]> {
    planesDietaCliente.forEach(async planDietaCliente => {
      const planDietaClienteACrearse = crearPlanDietaCliente(
        await planDietaCliente,
      );
      const arregloErrores = await validate(planDietaClienteACrearse);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._planDietaClienteService.createMany(planesDietaCliente);
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() planDietaCliente: EditarPlanDietaClienteDto,
  ): Promise<PlanDietaClienteEntity> {
    const planDietaClienteEncontrado = await this._planDietaClienteService.findById(
      id,
    );
    if (planDietaClienteEncontrado) {
      const planDietaClienteAEditar = editarPlanDietaCliente(planDietaCliente);
      const arregloErrores = await validate(planDietaClienteAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._planDietaClienteService.update(
          id,
          planDietaClienteAEditar,
        );
      }
    } else {
      throw new BadRequestException('Registro no encontrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const planDietaCliente = await this._planDietaClienteService.findById(id);
    if (planDietaCliente) {
      await this._planDietaClienteService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

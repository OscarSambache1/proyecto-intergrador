import { ClienteEntity } from '../../cliente/cliente.entity';
import { PlanDietaEntity } from '../../plan-dieta/plan-dieta.entity';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class CrearPlanDietaClienteDto {
  @IsNotEmpty()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaInicio: string;

  @IsNotEmpty()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaFin: string;
  @IsNotEmpty()
  cliente: ClienteEntity;

  @IsNotEmpty()
  planDieta: PlanDietaEntity;
}

import { ClienteEntity } from '../../cliente/cliente.entity';
import { PlanDietaEntity } from '../../plan-dieta/plan-dieta.entity';
import { IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator';

export class EditarPlanDietaClienteDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaInicio: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaFin: string;

  @IsOptional()
  cliente: ClienteEntity;

  @IsOptional()
  planDieta: PlanDietaEntity;
}

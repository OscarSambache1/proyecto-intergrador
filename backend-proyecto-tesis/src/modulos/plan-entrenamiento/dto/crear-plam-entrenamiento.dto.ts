import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';

export class CrearPlanEntrenamientoDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsOptional()
  @IsString()
  descripcion: string;

  @IsOptional()
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];

  @IsOptional()
  planesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructorEntity[];
}

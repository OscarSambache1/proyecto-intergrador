import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';

export class EditarPlanEntrenamientoDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre: string;

  @IsString()
  @IsOptional()
  descripcion: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];

  @IsOptional()
  planesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructorEntity[];
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { PlanEntrenamientoEntity } from './plan-entrenamiento.entity';
import { CrearPlanEntrenamientoDto } from './dto/crear-plam-entrenamiento.dto';
import { EditarPlanEntrenamientoDto } from './dto/editar-plam-entrenamiento.dto';

@Injectable()
export class PlanEntrenamientoService {
  constructor(
    @InjectRepository(PlanEntrenamientoEntity)
    private readonly _planEntrenamientoRepository: Repository<
      PlanEntrenamientoEntity
    >,
  ) {}

  async findAll(consulta: any): Promise<PlanEntrenamientoEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._planEntrenamientoRepository.find(consulta);
  }

  async findLike(campo: string): Promise<PlanEntrenamientoEntity[]> {
    return await this._planEntrenamientoRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<PlanEntrenamientoEntity> {
    return await this._planEntrenamientoRepository.findOne(id, {
      relations: [
        'rutinasPlanEntrenamiento',
        'planesEntrenamientoClienteInstructor',
        'rutinasPlanEntrenamiento.rutina',
        'rutinasPlanEntrenamiento.rutina.ejerciciosRutina',
        'rutinasPlanEntrenamiento.rutina.ejerciciosRutina.ejercicio',
        'rutinasPlanEntrenamiento.planEntrenamiento',
        'rutinasPlanEntrenamiento.dia',
      ],
    });
  }

  async createOne(
    planEntrenamiento: CrearPlanEntrenamientoDto,
  ): Promise<PlanEntrenamientoEntity> {
    return await this._planEntrenamientoRepository.save(
      this._planEntrenamientoRepository.create(planEntrenamiento),
    );
  }

  async createMany(
    planesEntrenamiento: CrearPlanEntrenamientoDto[],
  ): Promise<PlanEntrenamientoEntity[]> {
    const rutinasGuardadas: PlanEntrenamientoEntity[] = this._planEntrenamientoRepository.create(
      planesEntrenamiento,
    );
    return this._planEntrenamientoRepository.save(rutinasGuardadas);
  }

  async update(
    id: number,
    planEntrenamiento: EditarPlanEntrenamientoDto,
  ): Promise<PlanEntrenamientoEntity> {
    await this._planEntrenamientoRepository.update(id, planEntrenamiento);
    return await this.findById(id);
  }

  async delete(id: number) {
    const planEntrenamientoAEliminar = await this.findById(id);
    await this._planEntrenamientoRepository.remove(planEntrenamientoAEliminar);
  }
}

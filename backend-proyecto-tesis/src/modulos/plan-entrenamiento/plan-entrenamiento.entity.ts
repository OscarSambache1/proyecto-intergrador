import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { RutinaPlanEntrenamientoEntity } from '../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';

@Entity('plan-entrenamiento')
export class PlanEntrenamientoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'varchar', nullable: true })
  descripcion: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(
    type => RutinaPlanEntrenamientoEntity,
    rutinaPlanEntrenamiento => rutinaPlanEntrenamiento.planEntrenamiento,
  )
  rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];

  @OneToMany(
    type => PlanEntrenamientoClienteInstructorEntity,
    planEntrenamientoClienteInstructor =>
      planEntrenamientoClienteInstructor.planEntrenamiento,
  )
  planesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructorEntity[];
}

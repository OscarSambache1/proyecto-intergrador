import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanEntrenamientoEntity } from './plan-entrenamiento.entity';
import { PlanEntrenamientoController } from './plan-entrenamiento.controller';
import { PlanEntrenamientoService } from './plan-entrenamiento.service';

@Module({
  imports: [TypeOrmModule.forFeature([PlanEntrenamientoEntity], 'default')],
  controllers: [PlanEntrenamientoController],
  providers: [PlanEntrenamientoService],
  exports: [],
})
export class PlanEntrenamientoModule {}

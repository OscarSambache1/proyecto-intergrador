import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { PlanEntrenamientoService } from './plan-entrenamiento.service';
import { PlanEntrenamientoEntity } from './plan-entrenamiento.entity';
import { CrearPlanEntrenamientoDto } from './dto/crear-plam-entrenamiento.dto';
import { crearPlanEntrenamiento } from './funciones/crear-plan-entrenamiento';
import { EditarPlanEntrenamientoDto } from './dto/editar-plam-entrenamiento.dto';
import { editarPlanEntrenamiento } from './funciones/editar-plan-entrenamiento';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('plan-entrenamiento')
export class PlanEntrenamientoController {
  constructor(
    private readonly _planEntrenamientoService: PlanEntrenamientoService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<PlanEntrenamientoEntity[]> {
    return await this._planEntrenamientoService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<PlanEntrenamientoEntity> {
    const planEntrenamientoEncontrado = await this._planEntrenamientoService.findById(
      id,
    );
    if (planEntrenamientoEncontrado) {
      return await this._planEntrenamientoService.findById(id);
    } else {
      throw new BadRequestException('El plan de entrenamiento no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() planEntrenamiento: CrearPlanEntrenamientoDto,
  ): Promise<PlanEntrenamientoEntity> {
    const planEntrenamientoACrearse = crearPlanEntrenamiento(planEntrenamiento);
    const arregloErrores = await validate(planEntrenamientoACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error(
        'errores: creando el plan de entrenamiento',
        arregloErrores,
      );
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._planEntrenamientoService.createOne(
        planEntrenamientoACrearse,
      );
    }
  }

  @Put(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() planEntrenamiento: EditarPlanEntrenamientoDto,
  ): Promise<PlanEntrenamientoEntity> {
    const planEntrenamientoEncontrado = await this._planEntrenamientoService.findById(
      id,
    );
    if (planEntrenamientoEncontrado) {
      const planEntrenamientoAEditar = editarPlanEntrenamiento(
        planEntrenamiento,
      );
      const arregloErrores = await validate(planEntrenamientoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error(
          'errores editando el plan de entrenamiento',
          arregloErrores,
        );
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._planEntrenamientoService.update(
          id,
          planEntrenamientoAEditar,
        );
      }
    } else {
      throw new BadRequestException('Plan de entrenamiento no encontrado');
    }
  }

  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const planEntrenamientoEncontrado = await this._planEntrenamientoService.findById(
      id,
    );
    if (planEntrenamientoEncontrado) {
      await this._planEntrenamientoService.delete(id);
    } else {
      throw new BadRequestException('El plan de entrenamiento no existe');
    }
  }
}

import { CrearPlanEntrenamientoDto } from '../dto/crear-plam-entrenamiento.dto';

export function crearPlanEntrenamiento(
  planEntrenamiento: CrearPlanEntrenamientoDto,
): CrearPlanEntrenamientoDto {
  const planEntrenamientoACrear = new CrearPlanEntrenamientoDto();
  Object.keys(planEntrenamiento).map(atributo => {
    planEntrenamientoACrear[atributo] = planEntrenamiento[atributo];
  });
  return planEntrenamientoACrear;
}

import { EditarPlanEntrenamientoDto } from '../dto/editar-plam-entrenamiento.dto';

export function editarPlanEntrenamiento(
  planEntrenamiento: EditarPlanEntrenamientoDto,
): EditarPlanEntrenamientoDto {
  const planEntrenamientoAEditar = new EditarPlanEntrenamientoDto();
  Object.keys(planEntrenamiento).map(atributo => {
    planEntrenamientoAEditar[atributo] = planEntrenamiento[atributo];
  });
  return planEntrenamientoAEditar;
}

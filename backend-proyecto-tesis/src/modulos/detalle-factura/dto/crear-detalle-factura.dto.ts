import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
import { ProductoEntity } from '../../producto/producto.entity';
import { IsNotEmpty, IsCurrency, IsNumber } from 'class-validator';

export class CrearDetalleFacturaDto {
  @IsNotEmpty()
  @IsNumber()
  cantidad?: number;

  @IsNotEmpty()
  @IsNumber()
  total?: number;

  @IsNotEmpty()
  @IsNumber()
  precioUnitario?: number;

  @IsNotEmpty()
  cabeceraFactura?: CabeceraFacturaEntity;

  @IsNotEmpty()
  producto?: ProductoEntity;
}

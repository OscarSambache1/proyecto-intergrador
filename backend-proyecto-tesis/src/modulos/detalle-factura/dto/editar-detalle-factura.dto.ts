import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
import { ProductoEntity } from '../../producto/producto.entity';
import { IsNotEmpty, IsCurrency, IsNumber, IsOptional } from 'class-validator';

export class EditarDetalleFacturaDto {
  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  cantidad?: number;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  total?: number;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  precioUnitario?: number;

  @IsOptional()
  cabeceraFactura?: CabeceraFacturaEntity;

  @IsOptional()
  producto?: ProductoEntity;
}

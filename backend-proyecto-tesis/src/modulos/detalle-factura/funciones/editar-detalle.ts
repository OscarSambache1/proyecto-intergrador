import { EditarDetalleFacturaDto } from '../dto/editar-detalle-factura.dto';

export function editarDetalle(
  detalleFactura: EditarDetalleFacturaDto,
): EditarDetalleFacturaDto {
  const detalleFacturaAEditar = new EditarDetalleFacturaDto();
  Object.keys(detalleFactura).map(atributo => {
    detalleFacturaAEditar[atributo] = detalleFactura[atributo];
  });
  return detalleFacturaAEditar;
}

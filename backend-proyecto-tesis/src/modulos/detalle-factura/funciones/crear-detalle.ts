import { CrearDetalleFacturaDto } from '../dto/crear-detalle-factura.dto';

export function crearDetalle(
  detalleFactura: CrearDetalleFacturaDto,
): CrearDetalleFacturaDto {
  const detalleFacturaACrear = new CrearDetalleFacturaDto();
  Object.keys(detalleFactura).map(atributo => {
    detalleFacturaACrear[atributo] = detalleFactura[atributo];
  });
  return detalleFacturaACrear;
}

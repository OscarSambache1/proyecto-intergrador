import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ProductoEntity } from '../producto/producto.entity';
import { CabeceraFacturaEntity } from '../cabecera-factura/cabecera-factura.entity';

@Entity('detalle-factura')
export class DetalleFacturaEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'cantidad', type: 'int' })
  cantidad: number;

  @Column({
    name: 'total',
    type: 'decimal',
    precision: 15,
    scale: 2,
    default: 0,
  })
  total: number;

  @Column({
    name: 'precioUnitario',
    type: 'decimal',
    precision: 15,
    scale: 2,
    default: 0,
  })
  precioUnitario: number;

  @ManyToOne(
    type => CabeceraFacturaEntity,
    cabeceraFactura => cabeceraFactura.detallesFactura,
  )
  cabeceraFactura: CabeceraFacturaEntity;

  @ManyToOne(type => ProductoEntity, producto => producto.detallesFactura)
  producto: ProductoEntity;
}

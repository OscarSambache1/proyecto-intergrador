import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { DetalleFacturaService } from './detalle-factura.service';
import { DetalleFacturaEntity } from './detalle-factura.entity';
import { CrearDetalleFacturaDto } from './dto/crear-detalle-factura.dto';
import { crearDetalle } from './funciones/crear-detalle';
import { editarDetalle } from './funciones/editar-detalle';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@UseGuards(SessionGuard)
@Controller('detalle-factura')
export class DetalleFacturaController {
  constructor(private readonly _detalleService: DetalleFacturaService) {}

  @Get('')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<DetalleFacturaEntity[]> {
    return await this._detalleService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id: number): Promise<DetalleFacturaEntity> {
    const detalleEncontrado = await this._detalleService.findById(id);
    if (detalleEncontrado) {
      return await this._detalleService.findById(id);
    } else {
      throw new BadRequestException('El detalle no existe');
    }
  }

  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() detalle: CrearDetalleFacturaDto,
  ): Promise<DetalleFacturaEntity> {
    const detalleACrear = crearDetalle(detalle);
    const arregloErrores = await validate(detalleACrear);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el detalle', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._detalleService.createOne(detalleACrear);
    }
  }

  @Post('crear-varios')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body() detalles: CrearDetalleFacturaDto[],
  ): Promise<DetalleFacturaEntity[]> {
    detalles.forEach(async detalle => {
      const detalleACrear = crearDetalle(await detalle);
      const arregloErrores = await validate(detalleACrear);
      const existenErrores = arregloErrores.length > 0;
      if (existenErrores) {
        console.log(arregloErrores);
        console.error('errores: creando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      }
    });
    return this._detalleService.createMany(detalles);
  }

  @Put(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() detalle: DetalleFacturaEntity,
  ): Promise<DetalleFacturaEntity> {
    const detalleEncontrado = await this._detalleService.findById(id);
    if (detalleEncontrado) {
      const detalleAEditar = editarDetalle(detalle);
      const arregloErrores = await validate(detalleAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el detalle', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._detalleService.update(id, detalleAEditar);
      }
    } else {
      throw new BadRequestException('Detalle no encotrado');
    }
  }

  @Delete(':id')
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const detalleEncontrado = await this._detalleService.findById(id);
    if (detalleEncontrado) {
      await this._detalleService.delete(id);
    } else {
      throw new BadRequestException('El Detalle no existe');
    }
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { DetalleFacturaEntity } from './detalle-factura.entity';
import { CrearDetalleFacturaDto } from './dto/crear-detalle-factura.dto';
import { EditarDetalleFacturaDto } from './dto/editar-detalle-factura.dto';

@Injectable()
export class DetalleFacturaService {
  constructor(
    @InjectRepository(DetalleFacturaEntity)
    private readonly _detalleRepository: Repository<DetalleFacturaEntity>,
  ) {}

  async findAll(consulta: any): Promise<DetalleFacturaEntity[]> {
    return await this._detalleRepository.find(consulta);
  }

  async findById(id: number): Promise<DetalleFacturaEntity> {
    return await this._detalleRepository.findOne(id, {
      relations: ['cabeceraFactura', 'producto'],
    });
  }

  async createOne(
    detalleFactura: CrearDetalleFacturaDto,
  ): Promise<DetalleFacturaEntity> {
    return await this._detalleRepository.save(
      this._detalleRepository.create(detalleFactura),
    );
  }

  async createMany(
    detalle: CrearDetalleFacturaDto[],
  ): Promise<DetalleFacturaEntity[]> {
    const detallesGuardados: DetalleFacturaEntity[] = this._detalleRepository.create(
      detalle,
    );
    return this._detalleRepository.save(detallesGuardados);
  }

  async update(
    id: number,
    detalleFactura: EditarDetalleFacturaDto,
  ): Promise<DetalleFacturaEntity> {
    await this._detalleRepository.update(id, detalleFactura);
    return await this.findById(id);
  }

  async delete(id: number) {
    const detalleAEliminar = await this.findById(id);
    await this._detalleRepository.remove(detalleAEliminar);
  }

  async contar(consulta: object): Promise<number> {
    return await this._detalleRepository.count({
      where: consulta,
    });
  }
}

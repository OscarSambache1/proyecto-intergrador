import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { EjercicioMusculoEntity } from '../ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from '../ejercicio-rutina/ejercicio-rutina.entity';

@Entity('ejercicio')
export class EjercicioEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombre', type: 'varchar' })
  nombre: string;

  @Column({ name: 'descripcion', type: 'longtext' })
  descripcion: string;

  @Column({ name: 'urlImagen', type: 'varchar' })
  urlImagen: string;

  @Column({ name: 'estado', type: 'tinyint', default: 1 })
  estado: number;

  @OneToMany(
    type => EjercicioMusculoEntity,
    ejercicioMusculo => ejercicioMusculo.ejercicio,
  )
  ejerciciosMusculo: EjercicioMusculoEntity[];

  @OneToMany(
    type => EjercicioRutinaEntity,
    ejercicioRutina => ejercicioRutina.ejercicio,
  )
  ejerciciosRutina: EjercicioRutinaEntity[];
}

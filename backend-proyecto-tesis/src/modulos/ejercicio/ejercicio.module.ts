import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EjercicioController } from './ejercicio.controller';
import { EjercicioService } from './ejercicio.service';
import { EjercicioEntity } from './ejercicio.entity';

@Module({
  imports: [TypeOrmModule.forFeature([EjercicioEntity], 'default')],
  controllers: [EjercicioController],
  providers: [EjercicioService],
  exports: [],
})
export class EjercicioModule {}

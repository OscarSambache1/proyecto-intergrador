import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { EjercicioService } from './ejercicio.service';
import { EjercicioEntity } from './ejercicio.entity';
import { CrearEjercicioDto } from './dto/crear-ejercicio.dto';
import { crearEjercicio } from './funciones/crear-ejercicio';
import { EditarEjercicioDto } from './dto/editar-ejercicio';
import { editarEjercicio } from './funciones/editar-ejercicio';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('ejercicio')
export class EjercicioController {
  constructor(private readonly _ejercicioService: EjercicioService) {}

  @Get('')
  async findAll(@Query('consulta') consulta: any): Promise<EjercicioEntity[]> {
    return await this._ejercicioService.findAll(JSON.parse(consulta));
  }

  @Get('findByNombreMusculo')
  async findByNombreMusculo(
    @Query() consulta: any,
  ): Promise<EjercicioEntity[]> {
    return await this._ejercicioService.findByNombreYMusculo(
      consulta.parametro,
      consulta.musculo,
    );
  }

  @Get('findByNombreOMusculo')
  async findByNombreOMusculo(
    @Query() consulta: any,
  ): Promise<EjercicioEntity[]> {
    return await this._ejercicioService.findByNombreOMusculo(
      consulta.parametro,
      consulta.musculo,
    );
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<EjercicioEntity> {
    const ejercicioEncontrado = await this._ejercicioService.findById(id);
    if (ejercicioEncontrado) {
      return await this._ejercicioService.findById(id);
    } else {
      throw new BadRequestException('EL ejercicio no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() ejercicio: CrearEjercicioDto): Promise<EjercicioEntity> {
    const ejercicioACrearse = crearEjercicio(ejercicio);
    const arregloErrores = await validate(ejercicioACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el ejercicio', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._ejercicioService.createOne(ejercicioACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() ejercicio: EditarEjercicioDto,
  ): Promise<EjercicioEntity> {
    const ejercicioEncontrado = await this._ejercicioService.findById(id);
    if (ejercicioEncontrado) {
      const ejercicioAEditar = editarEjercicio(ejercicio);
      const arregloErrores = await validate(ejercicioAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el ejercicio', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._ejercicioService.update(id, ejercicioAEditar);
      }
    } else {
      throw new BadRequestException('Ejercicio no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const ejercicioEncontrado = await this._ejercicioService.findById(id);
    if (ejercicioEncontrado) {
      await this._ejercicioService.delete(id);
    } else {
      throw new BadRequestException('EL ejercicio no existe');
    }
  }

  @Post('subirArchivo/:id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  @UseInterceptors(
    FileInterceptor('file2', {
      dest: __dirname + '/../../../public/archivos-ejercicios',
      limits: {
        fieldSize: 10000000,
        fieldNameSize: 10000000,
      },
    }),
  )
  uploadFile(@Param('id') id: number, @UploadedFile() file) {
    const fs = require('fs');
    fs.rename(`${file.path}`, `${file.path}.gif`, err => {
      if (err) console.log('ERROR: ' + err);
    });

    const ejercicio = new EditarEjercicioDto();
    ejercicio.urlImagen = `${file.filename}.gif`;
    this._ejercicioService.update(id, ejercicio);
  }
}

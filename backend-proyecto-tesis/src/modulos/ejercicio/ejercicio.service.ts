import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { EjercicioEntity } from './ejercicio.entity';
import { CrearEjercicioDto } from './dto/crear-ejercicio.dto';
import { EditarEjercicioDto } from './dto/editar-ejercicio';

@Injectable()
export class EjercicioService {
  constructor(
    @InjectRepository(EjercicioEntity)
    private readonly _ejercicioRepository: Repository<EjercicioEntity>,
  ) {}

  async findAll(consulta: any): Promise<EjercicioEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._ejercicioRepository.find(consulta);
  }

  async findByNombreYMusculo(
    parametro: string,
    musculo: number,
  ): Promise<EjercicioEntity[]> {
    return await this._ejercicioRepository
      .createQueryBuilder('ejercicio')
      .innerJoinAndSelect('ejercicio.ejerciciosMusculo', 'ejerciciosMusculo')
      .innerJoinAndSelect(
        'ejerciciosMusculo.musculo',
        'musculo',
        'musculo.id  = :musculo',
        { musculo },
      )
      .where(`ejercicio.nombre LIKE :nombre`, { nombre: `%${parametro}%` })
      .getMany();
  }

  async findByNombreOMusculo(
    parametro: string,
    musculo: number,
  ): Promise<EjercicioEntity[]> {
    return await this._ejercicioRepository
      .createQueryBuilder('ejercicio')
      .innerJoinAndSelect('ejercicio.ejerciciosMusculo', 'ejerciciosMusculo')
      .innerJoinAndSelect(
        'ejerciciosMusculo.musculo',
        'musculo',
        'musculo.id  = :musculo OR ejercicio.nombre LIKE :nombre',
        { musculo, nombre: `%${parametro}%` },
      )
      .getMany();
  }

  async findLike(campo: string): Promise<EjercicioEntity[]> {
    return await this._ejercicioRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(id: number): Promise<EjercicioEntity> {
    return await this._ejercicioRepository.findOne(id, {
      relations: [
        'ejerciciosMusculo',
        'ejerciciosRutina',
        'ejerciciosMusculo.musculo',
        'ejerciciosMusculo.ejercicio',
      ],
    });
  }

  async createOne(ejercicio: CrearEjercicioDto): Promise<EjercicioEntity> {
    return await this._ejercicioRepository.save(
      this._ejercicioRepository.create(ejercicio),
    );
  }

  async createMany(
    ejercicios: CrearEjercicioDto[],
  ): Promise<EjercicioEntity[]> {
    const ejercicioACrear: EjercicioEntity[] = this._ejercicioRepository.create(
      ejercicios,
    );
    return this._ejercicioRepository.save(ejercicioACrear);
  }

  async update(
    id: number,
    ejercicio: EditarEjercicioDto,
  ): Promise<EjercicioEntity> {
    await this._ejercicioRepository.update(id, ejercicio);
    return await this.findById(id);
  }

  async delete(id: number) {
    const ejercicioAEliminar = await this.findById(id);
    await this._ejercicioRepository.remove(ejercicioAEliminar);
  }
}

import { EditarEjercicioDto } from '../dto/editar-ejercicio';

export function editarEjercicio(
  ejercicio: EditarEjercicioDto,
): EditarEjercicioDto {
  const ejercicioAEditar = new EditarEjercicioDto();
  Object.keys(ejercicio).map(atributo => {
    ejercicioAEditar[atributo] = ejercicio[atributo];
  });
  return ejercicioAEditar;
}

import { CrearEjercicioDto } from '../dto/crear-ejercicio.dto';

export function crearEjercicio(
  ejercicio: CrearEjercicioDto,
): CrearEjercicioDto {
  const ejercicioACrear = new CrearEjercicioDto();
  Object.keys(ejercicio).map(atributo => {
    ejercicioACrear[atributo] = ejercicio[atributo];
  });
  return ejercicioACrear;
}

import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';

export class EditarEjercicioDto {
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  urlImagen: string;

  @IsOptional()
  @IsEnum([0, 1])
  estado?: number;

  @IsOptional()
  ejerciciosMusculo: EjercicioMusculoEntity[];

  @IsOptional()
  ejerciciosRutina: EjercicioRutinaEntity[];
}

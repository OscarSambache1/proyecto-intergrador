import { IsNotEmpty, IsString, IsOptional, IsEnum } from 'class-validator';
import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';

export class CrearEjercicioDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  urlImagen: string;

  @IsOptional()
  ejerciciosMusculo: EjercicioMusculoEntity[];

  @IsOptional()
  ejerciciosRutina: EjercicioRutinaEntity[];
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ClienteEntity } from '../cliente/cliente.entity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { PlanEntrenamientoEntity } from '../plan-entrenamiento/plan-entrenamiento.entity';
import { PlanDietaEntity } from '../plan-dieta/plan-dieta.entity';

@Entity('plan-entrenamiento-cliente-instructor')
export class PlanEntrenamientoClienteInstructorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => ClienteEntity, cliente => cliente.planesEntrenamiento)
  cliente: ClienteEntity = null;

  @Column({ name: 'fecha-inicio', type: 'date' })
  fechaInicio: string;

  @Column({ name: 'fecha-fin', type: 'date' })
  fechaFin: string;

  @ManyToOne(
    type => InstructorEntity,
    instructor => instructor.planesEntrenamiento,
  )
  instructor: InstructorEntity = null;

  @ManyToOne(
    type => PlanEntrenamientoEntity,
    planEntrenamiento => planEntrenamiento.planesEntrenamientoClienteInstructor,
  )
  planEntrenamiento: PlanEntrenamientoEntity = null;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanEntrenamientoClienteInstructorEntity } from './plan-entrenamiento-cliente-instructor.entity';
import { PlanEntrenamientoClienteInstructorService } from './plan-entrenamiento-cliente-instructor.service';
import { PlanEntrenamientoClienteInstructorController } from './plan-entrenamiento-cliente-instructor.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [PlanEntrenamientoClienteInstructorEntity],
      'default',
    ),
  ],
  controllers: [PlanEntrenamientoClienteInstructorController],
  providers: [PlanEntrenamientoClienteInstructorService],
  exports: [],
})
export class PlanEntrenamientoClienteInstructorModule {}

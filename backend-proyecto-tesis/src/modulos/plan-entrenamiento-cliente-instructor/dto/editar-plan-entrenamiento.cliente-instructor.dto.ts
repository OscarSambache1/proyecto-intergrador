import { ClienteEntity } from '../../cliente/cliente.entity';
import { InstructorEntity } from '../../instructor/instructor.entity';
import { PlanEntrenamientoEntity } from '../../plan-entrenamiento/plan-entrenamiento.entity';
import { IsOptional, IsNotEmpty, IsString, Matches } from 'class-validator';

export class EditarPlanEntrenamientoClienteInstructorDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaInicio: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaFin: string;

  @IsOptional()
  cliente: ClienteEntity;

  @IsOptional()
  instructor: InstructorEntity;

  @IsOptional()
  planEntrenamiento: PlanEntrenamientoEntity;
}

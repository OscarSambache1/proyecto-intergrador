import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { PlanEntrenamientoClienteInstructorService } from './plan-entrenamiento-cliente-instructor.service';
import { PlanEntrenamientoClienteInstructorEntity } from './plan-entrenamiento-cliente-instructor.entity';
import { CrearPlanEntrenamientoClienteInstructorDto } from './dto/crear-plan-entrenamiento-cliente-instructor.dto';
import { crearPlanEntrenamientoClienteInstructor } from './funciones/crear-plan-entrenamiento';
import { EditarPlanEntrenamientoClienteInstructorDto } from './dto/editar-plan-entrenamiento.cliente-instructor.dto';
import { editarPlanEntrenamientoClienteInstructor } from './funciones/editar-plan-entrenamiento';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('plan-entrenamiento-cliente-instructor')
export class PlanEntrenamientoClienteInstructorController {
  constructor(
    private readonly _planEntrenamientoClienteInstructorService: PlanEntrenamientoClienteInstructorService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<PlanEntrenamientoClienteInstructorEntity[]> {
    return await this._planEntrenamientoClienteInstructorService.findAll(
      JSON.parse(consulta),
    );
  }

  @Get(':id')
  async findOne(
    @Param('id') id: number,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    const planEntrenamientoClienteInstructorEncontrado = await this._planEntrenamientoClienteInstructorService.findById(
      id,
    );
    if (planEntrenamientoClienteInstructorEncontrado) {
      return await this._planEntrenamientoClienteInstructorService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body()
    planClienteInstructorEntrenamiento: CrearPlanEntrenamientoClienteInstructorDto,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    const planEntrenamientoClienteInstructorACrearse = crearPlanEntrenamientoClienteInstructor(
      planClienteInstructorEntrenamiento,
    );
    const arregloErrores = await validate(
      planEntrenamientoClienteInstructorACrearse,
    );
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._planEntrenamientoClienteInstructorService.createOne(
        planEntrenamientoClienteInstructorACrearse,
      );
    }
  }

  @Post('crear-varios')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  createMany(
    @Body()
    planesEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto[],
  ): Promise<PlanEntrenamientoClienteInstructorEntity[]> {
    planesEntrenamientoClienteInstructor.forEach(
      async planEntrenamientoClienteInstructor => {
        const planEntrenamientoClienteInstructorACrearse = crearPlanEntrenamientoClienteInstructor(
          await planEntrenamientoClienteInstructor,
        );
        const arregloErrores = await validate(
          planEntrenamientoClienteInstructorACrearse,
        );
        const existenErrores = arregloErrores.length > 0;
        if (existenErrores) {
          console.log(arregloErrores);
          console.error('errores: creando el registro', arregloErrores);
          throw new BadRequestException('Parametros incorrectos');
        }
      },
    );
    return this._planEntrenamientoClienteInstructorService.createMany(
      planesEntrenamientoClienteInstructor,
    );
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body()
    planClienteInstructorEntrenamiento: EditarPlanEntrenamientoClienteInstructorDto,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    const planEntrenamientoClienteInstructorEncontrado = await this._planEntrenamientoClienteInstructorService.findById(
      id,
    );
    if (planEntrenamientoClienteInstructorEncontrado) {
      const planEntrenamientoAEditar = editarPlanEntrenamientoClienteInstructor(
        planClienteInstructorEntrenamiento,
      );
      const arregloErrores = await validate(planEntrenamientoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._planEntrenamientoClienteInstructorService.update(
          id,
          planEntrenamientoAEditar,
        );
      }
    } else {
      throw new BadRequestException('Registro no encontrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const planEntrenamientoClienteInstructorEncontrado = await this._planEntrenamientoClienteInstructorService.findById(
      id,
    );
    if (planEntrenamientoClienteInstructorEncontrado) {
      await this._planEntrenamientoClienteInstructorService.delete(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }
}

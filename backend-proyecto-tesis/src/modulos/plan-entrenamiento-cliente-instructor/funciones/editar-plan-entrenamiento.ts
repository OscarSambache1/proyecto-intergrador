import { EditarPlanEntrenamientoClienteInstructorDto } from '../dto/editar-plan-entrenamiento.cliente-instructor.dto';

export function editarPlanEntrenamientoClienteInstructor(
  planEntrenamientoClienteInstructor: EditarPlanEntrenamientoClienteInstructorDto,
): EditarPlanEntrenamientoClienteInstructorDto {
  const planEntrenamientoClienteInstructorAEditar = new EditarPlanEntrenamientoClienteInstructorDto();
  Object.keys(planEntrenamientoClienteInstructor).map(atributo => {
    planEntrenamientoClienteInstructorAEditar[atributo] =
      planEntrenamientoClienteInstructor[atributo];
  });
  return planEntrenamientoClienteInstructorAEditar;
}

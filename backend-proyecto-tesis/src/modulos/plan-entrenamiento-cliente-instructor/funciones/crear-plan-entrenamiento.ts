import { CrearPlanEntrenamientoClienteInstructorDto } from '../dto/crear-plan-entrenamiento-cliente-instructor.dto';

export function crearPlanEntrenamientoClienteInstructor(
  planEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto,
): CrearPlanEntrenamientoClienteInstructorDto {
  const planEntrenamientoClienteInstructorACrear = new CrearPlanEntrenamientoClienteInstructorDto();
  Object.keys(planEntrenamientoClienteInstructor).map(atributo => {
    planEntrenamientoClienteInstructorACrear[atributo] =
      planEntrenamientoClienteInstructor[atributo];
  });
  return planEntrenamientoClienteInstructorACrear;
}

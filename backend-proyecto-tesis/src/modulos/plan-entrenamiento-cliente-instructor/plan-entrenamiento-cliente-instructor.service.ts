import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { PlanEntrenamientoClienteInstructorEntity } from './plan-entrenamiento-cliente-instructor.entity';
import { CrearPlanEntrenamientoClienteInstructorDto } from './dto/crear-plan-entrenamiento-cliente-instructor.dto';
import { EditarPlanEntrenamientoClienteInstructorDto } from './dto/editar-plan-entrenamiento.cliente-instructor.dto';

@Injectable()
export class PlanEntrenamientoClienteInstructorService {
  constructor(
    @InjectRepository(PlanEntrenamientoClienteInstructorEntity)
    private readonly _planEntrenamientoClienteInstructorRepository: Repository<
      PlanEntrenamientoClienteInstructorEntity
    >,
  ) {}

  async findAll(
    consulta: any,
  ): Promise<PlanEntrenamientoClienteInstructorEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._planEntrenamientoClienteInstructorRepository.find(
      consulta,
    );
  }

  async findLike(
    campo: string,
  ): Promise<PlanEntrenamientoClienteInstructorEntity[]> {
    return await this._planEntrenamientoClienteInstructorRepository.find({
      order: { id: 'DESC' },
      where: { nombre: Like(`%${campo}%`) },
    });
  }

  async findById(
    id: number,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    return await this._planEntrenamientoClienteInstructorRepository.findOne(
      id,
      {
        relations: [
          'cliente',
          'instructor',
          'planEntrenamiento',
          'cliente.usuario',
          'instructor.usuario',
        ],
      },
    );
  }

  async createOne(
    planEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    return await this._planEntrenamientoClienteInstructorRepository.save(
      this._planEntrenamientoClienteInstructorRepository.create(
        planEntrenamientoClienteInstructor,
      ),
    );
  }

  async createMany(
    planesEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto[],
  ): Promise<PlanEntrenamientoClienteInstructorEntity[]> {
    const rutinasGuardadas: PlanEntrenamientoClienteInstructorEntity[] = this._planEntrenamientoClienteInstructorRepository.create(
      planesEntrenamientoClienteInstructor,
    );
    return this._planEntrenamientoClienteInstructorRepository.save(
      rutinasGuardadas,
    );
  }

  async update(
    id: number,
    planesEntrenamientoClienteInstructor: EditarPlanEntrenamientoClienteInstructorDto,
  ): Promise<PlanEntrenamientoClienteInstructorEntity> {
    await this._planEntrenamientoClienteInstructorRepository.update(
      id,
      planesEntrenamientoClienteInstructor,
    );
    return await this.findById(id);
  }

  async delete(id: number) {
    const planEntrenamientoClienteInstructorAEliminar = await this.findById(id);
    await this._planEntrenamientoClienteInstructorRepository.remove(
      planEntrenamientoClienteInstructorAEliminar,
    );
  }
}

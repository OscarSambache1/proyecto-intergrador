import {
  Controller,
  Post,
  Param,
  Body,
  BadRequestException,
  Get,
  Query,
  Put,
  Req,
  Res,
  Session,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioEntity } from './usuario.entity';
import { AppAuthGuard } from '../auth/app-auth.guard';
import jwt from 'jsonwebtoken';

@Controller('autenticacion')
export class AutenticacionController {
  constructor(private readonly _usuarioService: UsuarioService) {}

  @Post('login')
  @UseGuards(AppAuthGuard)
  async login(
    @Body('cedula') cedula,
    @Body('password') password,
    @Session() session,
  ): Promise<UsuarioEntity> {
    return await this._usuarioService.autenticarUsuario({ cedula, password });
  }

  @Post('login-movil')
  async loginMovil(
    @Body('cedula') cedula,
    @Body('password') password,
  ): Promise<any> {
    const usuarioLogueado = await this._usuarioService.autenticarUsuario({
      cedula,
      password,
    });
    if (usuarioLogueado) {
      const token = jwt.sign({ usuarioLogueado }, 'shhhhh');
      return { usuario: usuarioLogueado, token };
    } else {
      throw new BadRequestException('fallo login');
    }
  }

  @Get('logout')
  logout(@Req() request, @Res() response) {
    request.logout();
    request.session.destroy();
    console.log('logout');
  }

  @Post('buscarUsuarioPorCedula')
  async buscarUsuarioPorCedula(@Body('cedula') cedula): Promise<UsuarioEntity> {
    return await this._usuarioService.findOneByCedula(cedula);
  }
}

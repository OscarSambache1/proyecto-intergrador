import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  Req,
  Res,
  Session,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { UsuarioService } from './usuario.service';
import { UsuarioEntity } from './usuario.entity';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { EditarUsuarioDto } from './dto/editar-usuario.dto';
import { editarUsuario } from './funciones/editar-usuario';
import { crearUsuario } from './funciones/crear-usuariio';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { generarPassword } from './funciones/generarNuevoPassword';
import * as crypto from 'crypto';
import * as moment from 'moment';
import * as fs from 'fs';

@Controller('usuario')
export class UsuarioController {
  constructor(private readonly _usuarioService: UsuarioService) {}

  @UseGuards(SessionGuard)
  @Get('findAll')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findAll(@Query('consulta') consulta: any): Promise<UsuarioEntity[]> {
    return await this._usuarioService.findAll(JSON.parse(consulta));
  }

  @UseGuards(SessionGuard)
  @Get('findWhereOr')
  @Roles('administrador', 'cliente', 'instructor', 'empleado')
  @UseGuards(RolesGuard)
  async findWhereOr(@Query() consulta: any): Promise<UsuarioEntity[]> {
    return this._usuarioService.findWhereOr(consulta.expresion);
  }

  @Get(':id')
  @UseGuards(RolesGuard)
  async findOne(@Param('id') id): Promise<UsuarioEntity> {
    const usuarioEncontrado = await this._usuarioService.findById(id);
    if (usuarioEncontrado) {
      return usuarioEncontrado;
    } else {
      throw new BadRequestException('usuario no existe');
    }
  }

  @UseGuards(SessionGuard)
  @Post()
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(@Body() usuario: CrearUsuarioDto): Promise<UsuarioEntity> {
    const usuarioACrearse = crearUsuario(usuario);
    const arregloErrores = await validate(usuarioACrearse);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: creando al usuario', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._usuarioService.createOne(usuarioACrearse);
    }
  }

  @UseGuards(SessionGuard)
  @Put(':id')
  @Roles('administrador', 'empleado', 'cliente', 'instructor')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() usuario: EditarUsuarioDto,
  ): Promise<UsuarioEntity> {
    const usuarioEncontrado = await this._usuarioService.findById(id);
    if (usuarioEncontrado) {
      const usuarioAEditarse = editarUsuario(usuario);
      const arregloErrores = await validate(usuarioAEditarse);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando al usuario', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._usuarioService.update(id, usuarioAEditarse);
      }
    } else {
      throw new BadRequestException('Usuario no encotrado');
    }
  }

  @UseGuards(SessionGuard)
  @Delete(':id')
  @Roles('administrador')
  @UseGuards(RolesGuard)
  async eliminar(@Param('id') id: number): Promise<any> {
    const usuarioEncontrado = await this._usuarioService.findById(id);
    if (usuarioEncontrado) {
      return await this._usuarioService.delete(id);
    } else {
      throw new BadRequestException('Usuario no existe');
    }
  }

  @UseGuards(SessionGuard)
  @Post('subirArchivo/:id')
  @Roles('administrador', 'empleado', 'cliente', 'instructor')
  @UseGuards(RolesGuard)
  @UseInterceptors(
    FileInterceptor('file2', {
      dest: __dirname + '/../../../public/imagenes-usuarios',
      limits: {
        fieldSize: 10000000,
        fieldNameSize: 10000000,
      },
    }),
  )
  async uploadFile(@Param('id') id: number, @UploadedFile() file) {
    const fecha = moment(new Date()).format();
    const urlFoto =
      __dirname +
      `/../../../public/imagenes-usuarios/imagen_usuario_${fecha}.png`;
    fs.rename(`${file.path}`, urlFoto, err => {
      if (err) console.log('ERROR: ' + err);
    });

    const usuario = new EditarUsuarioDto();
    const usuarioAEditar = await this.findOne(id);
    if (!(usuarioAEditar.urlFoto === 'usuario.png')) {
      try {
        this.eliminarArchivo(usuarioAEditar.urlFoto);
      } catch (error) {
        usuario.urlFoto = `imagen_usuario_${fecha}.png`;
      }
    }
    usuario.urlFoto = `imagen_usuario_${fecha}.png`;
    return await this._usuarioService.update(id, usuario);
  }

  @UseGuards(SessionGuard)
  @Post('resetear-password')
  @Roles('administrador, empleado')
  @UseGuards(RolesGuard)
  async resetearPassword(@Body() id: number): Promise<UsuarioEntity> {
    const usuarioEncontrado = await this._usuarioService.findById(id);
    if (usuarioEncontrado) {
      const nuevoPassword = generarPassword();
      const passwordNuevoHash = crypto
        .createHmac('sha256', nuevoPassword)
        .digest('hex');
      const usuarioAEditarse = {
        password: passwordNuevoHash,
      };
      this.update(id, usuarioAEditarse);
      usuarioEncontrado.password = nuevoPassword;
      return usuarioEncontrado;
    } else {
      throw new BadRequestException('Usuario no existe');
    }
  }

  @UseGuards(SessionGuard)
  @Post('cambiar-password')
  async cambiarPassword(
    @Body('id') id: number,
    @Body('passwordNuevo') passwordNuevo: string,
    @Body('passwordActual') passwordActual: string,
  ): Promise<UsuarioEntity> {
    return await this._usuarioService.cambiarPassword(
      id,
      passwordNuevo,
      passwordActual,
    );
  }

  @UseGuards(SessionGuard)
  @Post('cambiar-foto-movil')
  async cambiarFotoMovil(
    @Body('id') id: number,
    @Body('imagenBase64') imagenBase64: string,
  ): Promise<UsuarioEntity> {
    const fecha = moment(new Date()).format();
    const urlFoto =
      __dirname +
      `/../../../public/imagenes-usuarios/imagen_usuario_${fecha}.png`;
    await fs.writeFile(
      urlFoto,
      new Buffer(imagenBase64, 'base64'),
      error => {},
    );
    const usuario = new EditarUsuarioDto();
    const usuarioAEditar = await this.findOne(id);
    if (!(usuarioAEditar.urlFoto === 'usuario.png')) {
      try {
        this.eliminarArchivo(usuarioAEditar.urlFoto);
      } catch (error) {
        usuario.urlFoto = `imagen_usuario_${fecha}.png`;
      }
    }
    usuario.urlFoto = `imagen_usuario_${fecha}.png`;
    return await this._usuarioService.update(id, usuario);
  }

  eliminarArchivo(nombreArchivo) {
    const filePath =
      __dirname + `/../../../public/imagenes-usuarios/${nombreArchivo}`;
    fs.unlinkSync(filePath);
  }
}

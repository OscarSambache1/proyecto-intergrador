import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
} from 'class-validator';
import { ClienteEntity } from '../../cliente/cliente.entity';
import { InstructorEntity } from '../../instructor/instructor.entity';
import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
export class EditarUsuarioDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/^\S[a-zA-Z\s]*\S$/)
  nombres?: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/^\S[a-zA-Z\s]*\S$/)
  apellidos?: string;

  @IsNotEmpty()
  @IsOptional()
  cedula?: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaNacimiento?: string;

  @IsNotEmpty()
  @IsOptional()
  @IsNumberString()
  @Length(10, 10)
  telefono?: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  direccion?: string;

  @IsOptional()
  @Matches(
    // tslint:disable-next-line:max-line-length
    /^$|^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  correoElectronico?: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  urlFoto?: string;

  @IsOptional()
  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  genero?: string;

  @IsOptional()
  cliente?: ClienteEntity;

  @IsOptional()
  instructor?: InstructorEntity;

  @IsOptional()
  cabecerasFactura?: CabeceraFacturaEntity[];
}

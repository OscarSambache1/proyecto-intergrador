import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
  IsNumber,
  IsEnum,
  IsOptional,
  Length,
} from 'class-validator';
import { ClienteEntity } from '../../cliente/cliente.entity';
import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
import { InstructorEntity } from '../../instructor/instructor.entity';
export class CrearUsuarioDto {
  @IsNotEmpty()
  @IsString()
  @Matches(
    /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/,
  )
  nombres: string;

  @IsNotEmpty()
  @IsString()
  @Matches(
    /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/,
  )
  apellidos: string;

  @IsNotEmpty()
  cedula: string;

  @IsNotEmpty()
  @IsString()
  genero: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  @Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
  fechaNacimiento: string;

  @IsNotEmpty()
  @IsNumberString()
  @Length(10, 10)
  telefono: string;

  @IsNotEmpty()
  @IsString()
  direccion: string;

  @IsNotEmpty()
  password: string;

  @Matches(
    // tslint:disable-next-line:max-line-length
    /^$|^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  )
  @IsOptional()
  correoElectronico: string;

  @IsNotEmpty()
  @IsString()
  urlFoto: string;

  @IsOptional()
  cliente: ClienteEntity;

  @IsOptional()
  instructor: InstructorEntity;

  @IsOptional()
  cabecerasFactura: CabeceraFacturaEntity[];
}

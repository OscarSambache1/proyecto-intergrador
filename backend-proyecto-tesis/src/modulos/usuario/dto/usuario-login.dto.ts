import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsNumberString,
} from 'class-validator';
export class UsuarioLoginDto {
  @IsNotEmpty()
  @IsNumberString()
  cedula: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}

import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioEntity } from './usuario.entity';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
import { AutenticacionController } from './autenticacion.controller';
@Global()
@Module({
  imports: [TypeOrmModule.forFeature([UsuarioEntity], 'default')],
  controllers: [UsuarioController, AutenticacionController],
  providers: [UsuarioService],
  exports: [UsuarioService],
})
export class UsuarioModule {}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { ClienteEntity } from '../cliente/cliente.entity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { CabeceraFacturaEntity } from '../cabecera-factura/cabecera-factura.entity';
import * as crypto from 'crypto';

@Entity('usuario')
export class UsuarioEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nombres', type: 'varchar' })
  nombres: string;

  @Column({ name: 'apellidos', type: 'varchar' })
  apellidos: string;

  @Column({ name: 'cedula', type: 'varchar', unique: true })
  cedula: string;

  @Column({ name: 'fecha-nacimiento', type: 'date' })
  fechaNacimiento: string;

  @Column({ name: 'telefono', type: 'varchar' })
  telefono: string;

  @Column({ name: 'direccion', type: 'varchar', nullable: true })
  direccion: string;

  @Column({ name: 'genero', type: 'enum', enum: ['masculino', 'femenino'] })
  genero: string;

  @Column({ name: 'correo-electronico', type: 'varchar', nullable: true })
  correoElectronico: string;

  @Column({ name: 'urlFoto', type: 'varchar' })
  urlFoto: string;

  @Column({ name: 'password', type: 'varchar' })
  password: string;

  @OneToOne(type => ClienteEntity, cliente => cliente.usuario)
  cliente: ClienteEntity;

  @OneToOne(type => InstructorEntity, instructor => instructor.usuario)
  instructor: InstructorEntity;

  @OneToMany(
    type => CabeceraFacturaEntity,
    cabeceraFactura => cabeceraFactura.usuario,
  )
  cabecerasFactura: CabeceraFacturaEntity[];
}

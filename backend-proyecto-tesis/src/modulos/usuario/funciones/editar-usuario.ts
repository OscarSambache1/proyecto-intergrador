import { EditarUsuarioDto } from '../dto/editar-usuario.dto';

export function editarUsuario(usuario: EditarUsuarioDto): EditarUsuarioDto {
  const usuarioAEditar = new EditarUsuarioDto();
  Object.keys(usuario).map(atributo => {
    usuarioAEditar[atributo] = usuario[atributo];
  });
  return usuarioAEditar;
}

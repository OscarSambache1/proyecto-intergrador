import { UsuarioLoginDto } from '../dto/usuario-login.dto';

export function generarUsuarioLogin(
  cedula: string,
  password: string,
): UsuarioLoginDto {
  const usuarioLogin = new UsuarioLoginDto();
  (usuarioLogin.cedula = cedula), (usuarioLogin.password = password);
  return usuarioLogin;
}

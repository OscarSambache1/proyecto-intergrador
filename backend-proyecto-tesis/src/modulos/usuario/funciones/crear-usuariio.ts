import { CrearUsuarioDto } from '../dto/crear-usuario.dto';
import * as crypto from 'crypto';

export function crearUsuario(usuario: CrearUsuarioDto): CrearUsuarioDto {
  const usuarioACrear = new CrearUsuarioDto();
  Object.keys(usuario).map(atributo => {
    usuarioACrear[atributo] = usuario[atributo];
  });
  usuarioACrear.password = usuarioACrear.cedula;
  usuarioACrear.urlFoto = 'usuario.png';
  usuarioACrear.password = crypto
    .createHmac('sha256', usuarioACrear.cedula)
    .digest('hex');
  return usuarioACrear;
}

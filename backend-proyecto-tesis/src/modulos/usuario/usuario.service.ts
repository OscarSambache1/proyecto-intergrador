import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like } from 'typeorm';
import { UsuarioEntity } from './usuario.entity';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { EditarUsuarioDto } from './dto/editar-usuario.dto';
import * as crypto from 'crypto';
import { UsuarioLoginDto } from './dto/usuario-login.dto';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(UsuarioEntity)
    private readonly _usuarioRepository: Repository<UsuarioEntity>,
  ) {}

  async findAll(consulta: any): Promise<UsuarioEntity[]> {
    // if(consulta.where){
    //     Object.keys(consulta.where).map(atributo=>{
    //         consulta.where[atributo]= Like(`%${consulta.where[atributo]}%`)
    //     })
    // }
    return await this._usuarioRepository.find(consulta);
  }

  async findWhereOr(expresion: string): Promise<UsuarioEntity[]> {
    return await this._usuarioRepository
      .createQueryBuilder('usuario')
      .where(
        'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos',
        { nombres: `%${expresion}%`, apellidos: `%${expresion}%` },
      )
      .getMany();
  }

  async findById(id: number): Promise<UsuarioEntity> {
    return await this._usuarioRepository.findOne(id, {
      relations: ['cliente', 'instructor'],
    });
  }

  async createOne(usuario: CrearUsuarioDto): Promise<UsuarioEntity> {
    return await this._usuarioRepository.save(
      this._usuarioRepository.create(usuario),
    );
  }

  async createMany(usuarios: CrearUsuarioDto[]): Promise<UsuarioEntity[]> {
    const usuariosACrear: UsuarioEntity[] = this._usuarioRepository.create(
      usuarios,
    );
    return await this._usuarioRepository.save(usuariosACrear);
  }

  async update(id: number, usuario: EditarUsuarioDto): Promise<UsuarioEntity> {
    await this._usuarioRepository.update(id, usuario);
    return await this.findById(id);
  }

  async delete(id: number): Promise<string> {
    const usuarioAEliminar = await this.findById(id);
    await this._usuarioRepository.remove(usuarioAEliminar);
    return `Se ha eliminado el elemento ${id}`;
  }

  async contar(consulta: object): Promise<number> {
    return await this._usuarioRepository.count({
      where: consulta,
    });
  }

  async findOneByCedula(cedula: string): Promise<UsuarioEntity> {
    const opciones: FindOneOptions = {
      where: {
        cedula,
      },
      relations: ['cliente', 'instructor'],
    };
    const usuarioEncontrado = this._usuarioRepository.findOne(
      undefined,
      opciones,
    );
    return usuarioEncontrado;
  }

  async autenticarUsuario(
    usuarioAAutenticar: UsuarioLoginDto,
  ): Promise<UsuarioEntity> {
    const usuarioEncontrado = await this.findOneByCedula(
      usuarioAAutenticar.cedula,
    );
    if (usuarioEncontrado) {
      const passHash = crypto
        .createHmac('sha256', usuarioAAutenticar.password)
        .digest('hex');
      if (passHash === usuarioEncontrado.password) {
        return usuarioEncontrado;
      } else {
        return null;
      }
    }
  }

  async cambiarPassword(
    idUsuario: number,
    passwordNuevo: string,
    passwordActual: string,
  ) {
    const usuarioEncontrado = await this.findById(idUsuario);
    if (usuarioEncontrado) {
      const passwordActualHash = crypto
        .createHmac('sha256', passwordActual)
        .digest('hex');
      const passwordNuevoHash = crypto
        .createHmac('sha256', passwordNuevo)
        .digest('hex');
      if (passwordActualHash === usuarioEncontrado.password) {
        this.update(idUsuario, { password: passwordNuevoHash });
        delete usuarioEncontrado.password;
        return usuarioEncontrado;
      } else {
        return null;
      }
    }
  }
}

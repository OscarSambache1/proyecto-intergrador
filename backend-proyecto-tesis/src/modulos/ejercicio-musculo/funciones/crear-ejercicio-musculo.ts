import { CrearEjercicioMusculoDto } from '../dto/crear-ejercicio-musculo.dto';

export function crearEjercicioMusculo(
  ejercicioMusculo: CrearEjercicioMusculoDto,
): CrearEjercicioMusculoDto {
  const ejercicioMusculoACrear = new CrearEjercicioMusculoDto();
  Object.keys(ejercicioMusculo).map(atributo => {
    ejercicioMusculoACrear[atributo] = ejercicioMusculo[atributo];
  });
  return ejercicioMusculoACrear;
}

import { EditarEjercicioMusculoDto } from '../dto/editar-ejercicio-musculo.dto';

export function editarEjercicioMusculo(
  ejercicioMusculo: EditarEjercicioMusculoDto,
): EditarEjercicioMusculoDto {
  const ejercicioMusculoAEditar = new EditarEjercicioMusculoDto();
  Object.keys(ejercicioMusculo).map(atributo => {
    ejercicioMusculoAEditar[atributo] = ejercicioMusculo[atributo];
  });
  return ejercicioMusculoAEditar;
}

import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { MusculoEntity } from '../musculo/musculo.entity';
import { EjercicioEntity } from '../ejercicio/ejercicio.entity';

@Entity('ejercicio-musculo')
export class EjercicioMusculoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => MusculoEntity, musculo => musculo.ejerciciosMusculo)
  musculo: MusculoEntity;

  @ManyToOne(type => EjercicioEntity, ejercicio => ejercicio.ejerciciosMusculo)
  ejercicio: EjercicioEntity;
}

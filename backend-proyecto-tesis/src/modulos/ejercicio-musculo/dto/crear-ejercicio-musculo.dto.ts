import { MusculoEntity } from '../../musculo/musculo.entity';
import { EjercicioEntity } from '../../ejercicio/ejercicio.entity';
import { IsNotEmpty } from 'class-validator';

export class CrearEjercicioMusculoDto {
  @IsNotEmpty()
  musculo: MusculoEntity;

  @IsNotEmpty()
  ejercicio: EjercicioEntity;
}

import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { EjercicioMusculoService } from './ejercicio-musculo.service';
import { EjercicioMusculoEntity } from './ejercicio-musculo.entity';
import { CrearEjercicioMusculoDto } from './dto/crear-ejercicio-musculo.dto';
import { crearEjercicioMusculo } from './funciones/crear-ejercicio-musculo';
import { EditarEjercicioMusculoDto } from './dto/editar-ejercicio-musculo.dto';
import { editarEjercicioMusculo } from './funciones/editar-ejercicio-musculo';
import { SessionGuard } from '../auth/session.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';

@Controller('ejercicio-musculo')
export class EjercicioMusculoController {
  constructor(
    private readonly _ejercicioMusculoService: EjercicioMusculoService,
  ) {}

  @Get('')
  async findAll(
    @Query('consulta') consulta: any,
  ): Promise<EjercicioMusculoEntity[]> {
    return await this._ejercicioMusculoService.findAll(JSON.parse(consulta));
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<EjercicioMusculoEntity> {
    const ejercicioMusculoEncontrado = await this._ejercicioMusculoService.findById(
      id,
    );
    if (ejercicioMusculoEncontrado) {
      return await this._ejercicioMusculoService.findById(id);
    } else {
      throw new BadRequestException('El registro no existe');
    }
  }

  @Post()
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async create(
    @Body() ejercicioMusculo: CrearEjercicioMusculoDto,
  ): Promise<EjercicioMusculoEntity> {
    const ejercicioMusculoACrearse = crearEjercicioMusculo(ejercicioMusculo);
    const arregloErrores = await validate(ejercicioMusculoACrearse);
    const existenErrores = arregloErrores.length > 0;
    if (existenErrores) {
      console.log(arregloErrores);
      console.error('errores: creando el registro', arregloErrores);
      throw new BadRequestException('Parametros incorrectos');
    } else {
      return this._ejercicioMusculoService.createOne(ejercicioMusculoACrearse);
    }
  }

  @Put(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async update(
    @Param() id: number,
    @Body() ejercicioMusculo: EditarEjercicioMusculoDto,
  ): Promise<EjercicioMusculoEntity> {
    const ejercicioMusculoEncontrado = await this._ejercicioMusculoService.findById(
      id,
    );
    if (ejercicioMusculoEncontrado) {
      const ejercicioMusculoAEditar = editarEjercicioMusculo(ejercicioMusculo);
      const arregloErrores = await validate(ejercicioMusculoAEditar);
      const existenErrores = arregloErrores.length > 0;
      console.log('errores', arregloErrores);
      if (existenErrores) {
        console.error('errores editando el registro', arregloErrores);
        throw new BadRequestException('Parametros incorrectos');
      } else {
        return this._ejercicioMusculoService.update(
          id,
          ejercicioMusculoAEditar,
        );
      }
    } else {
      throw new BadRequestException('Registro no encotrado');
    }
  }

  @Delete(':id')
  @UseGuards(SessionGuard)
  @Roles('administrador', 'empleado')
  @UseGuards(RolesGuard)
  async delete(@Param('id') id: number) {
    const ejercicioMusculoEncontrado = await this._ejercicioMusculoService.findById(
      id,
    );
    if (ejercicioMusculoEncontrado) {
      await this._ejercicioMusculoService.delete(id);
    } else {
      throw new BadRequestException('EL registro no existe');
    }
  }
}

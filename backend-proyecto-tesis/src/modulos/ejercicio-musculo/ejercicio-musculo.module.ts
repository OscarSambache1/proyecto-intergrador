import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EjercicioMusculoEntity } from './ejercicio-musculo.entity';
import { EjercicioMusculoController } from './ejercicio-musculo.controller';
import { EjercicioMusculoService } from './ejercicio-musculo.service';

@Module({
  imports: [TypeOrmModule.forFeature([EjercicioMusculoEntity], 'default')],
  controllers: [EjercicioMusculoController],
  providers: [EjercicioMusculoService],
  exports: [],
})
export class EjercicioMusculoModule {}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { EjercicioMusculoEntity } from './ejercicio-musculo.entity';
import { CrearEjercicioMusculoDto } from './dto/crear-ejercicio-musculo.dto';
import { EditarEjercicioMusculoDto } from './dto/editar-ejercicio-musculo.dto';

@Injectable()
export class EjercicioMusculoService {
  constructor(
    @InjectRepository(EjercicioMusculoEntity)
    private readonly _ejercicioMusculoRepository: Repository<
      EjercicioMusculoEntity
    >,
  ) {}

  async findAll(consulta: any): Promise<EjercicioMusculoEntity[]> {
    if (consulta.where) {
      Object.keys(consulta.where).map(atributo => {
        consulta.where[atributo] = Like(`%${consulta.where[atributo]}%`);
      });
    }
    return await this._ejercicioMusculoRepository.find(consulta);
  }

  async findById(id: number): Promise<EjercicioMusculoEntity> {
    return await this._ejercicioMusculoRepository.findOne(id, {
      relations: ['ejercicio', 'musculo'],
    });
  }

  async createOne(
    ejercicioMusculo: CrearEjercicioMusculoDto,
  ): Promise<EjercicioMusculoEntity> {
    return await this._ejercicioMusculoRepository.save(
      this._ejercicioMusculoRepository.create(ejercicioMusculo),
    );
  }

  async createMany(
    ejerciciosMusculos: CrearEjercicioMusculoDto[],
  ): Promise<EjercicioMusculoEntity[]> {
    const ejerciciosMusculoACrear: EjercicioMusculoEntity[] = this._ejercicioMusculoRepository.create(
      ejerciciosMusculos,
    );
    return this._ejercicioMusculoRepository.save(ejerciciosMusculoACrear);
  }

  async update(
    id: number,
    ejercicioMusculo: EditarEjercicioMusculoDto,
  ): Promise<EjercicioMusculoEntity> {
    await this._ejercicioMusculoRepository.update(id, ejercicioMusculo);
    return await this.findById(id);
  }

  async delete(id: number) {
    const ejercicioMusculoAEliminar = await this.findById(id);
    await this._ejercicioMusculoRepository.remove(ejercicioMusculoAEliminar);
  }
}

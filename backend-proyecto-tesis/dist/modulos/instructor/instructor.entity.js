"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const plan_entrenamiento_cliente_instructor_entity_1 = require("../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity");
const usuario_entity_1 = require("../usuario/usuario.entity");
const clase_hora_entity_1 = require("../clase-hora/clase-hora.entity");
let InstructorEntity = class InstructorEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], InstructorEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'espcecializacion', type: 'varchar' }),
    __metadata("design:type", String)
], InstructorEntity.prototype, "especializacion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], InstructorEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.Column({
        name: 'rol',
        type: 'enum',
        enum: ['administrador', 'cliente', 'instructor', 'empleado'],
    }),
    __metadata("design:type", String)
], InstructorEntity.prototype, "rol", void 0);
__decorate([
    typeorm_1.OneToMany(type => plan_entrenamiento_cliente_instructor_entity_1.PlanEntrenamientoClienteInstructorEntity, planEntrenamientoClienteInstructor => planEntrenamientoClienteInstructor.instructor),
    __metadata("design:type", Array)
], InstructorEntity.prototype, "planesEntrenamiento", void 0);
__decorate([
    typeorm_1.OneToOne(type => usuario_entity_1.UsuarioEntity, usuario => usuario.instructor),
    typeorm_1.JoinColumn(),
    __metadata("design:type", usuario_entity_1.UsuarioEntity)
], InstructorEntity.prototype, "usuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => clase_hora_entity_1.ClaseHoraEntity, claseHora => claseHora.instructor),
    __metadata("design:type", Array)
], InstructorEntity.prototype, "clasesHora", void 0);
InstructorEntity = __decorate([
    typeorm_1.Entity('instructor')
], InstructorEntity);
exports.InstructorEntity = InstructorEntity;
//# sourceMappingURL=instructor.entity.js.map
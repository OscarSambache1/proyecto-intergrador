"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_instructor_dto_1 = require("../dto/editar-instructor.dto");
function editarInstructor(instructor) {
    const instructorAEditar = new editar_instructor_dto_1.EditarInstructorDto();
    Object.keys(instructor).map(atributo => {
        instructorAEditar[atributo] = instructor[atributo];
    });
    return instructorAEditar;
}
exports.editarInstructor = editarInstructor;
//# sourceMappingURL=editarInstructor.js.map
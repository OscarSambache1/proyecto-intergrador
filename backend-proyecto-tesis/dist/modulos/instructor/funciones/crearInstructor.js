"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_instructor_dto_1 = require("../dto/crear-instructor.dto");
function crearInsructor(instructor) {
    const instructorACrear = new crear_instructor_dto_1.CrearInstructorDto();
    Object.keys(instructor).map(atributo => {
        instructorACrear[atributo] = instructor[atributo];
    });
    instructorACrear.estado = 1;
    return instructorACrear;
}
exports.crearInsructor = crearInsructor;
//# sourceMappingURL=crearInstructor.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const instructor_service_1 = require("./instructor.service");
const crear_instructor_dto_1 = require("./dto/crear-instructor.dto");
const crearInstructor_1 = require("./funciones/crearInstructor");
const class_validator_1 = require("class-validator");
const editar_instructor_dto_1 = require("./dto/editar-instructor.dto");
const editarInstructor_1 = require("./funciones/editarInstructor");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let InstructorController = class InstructorController {
    constructor(_instructorService) {
        this._instructorService = _instructorService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._instructorService.findAll(JSON.parse(consulta));
        });
    }
    findByNombreApellido(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._instructorService.findByNombreApellido(consulta.expresion, consulta.verActivos);
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const instructorEncontrado = yield this._instructorService.findById(id);
            if (instructorEncontrado) {
                return instructorEncontrado;
            }
            else {
                throw new common_1.BadRequestException('Instructor no existe');
            }
        });
    }
    crear(instructor) {
        return __awaiter(this, void 0, void 0, function* () {
            const instructorACrearse = crearInstructor_1.crearInsructor(instructor);
            const arregloErrores = yield class_validator_1.validate(instructorACrearse);
            const existenErrores = arregloErrores.length > 0;
            console.log(arregloErrores);
            if (existenErrores) {
                console.error('errores: creando al instructor', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._instructorService.createOne(instructorACrearse);
            }
        });
    }
    editar(id, instructor) {
        return __awaiter(this, void 0, void 0, function* () {
            const instructorEncontrado = yield this._instructorService.findById(id);
            if (instructorEncontrado) {
                const instructorAEditarse = editarInstructor_1.editarInstructor(instructor);
                const arregloErrores = yield class_validator_1.validate(instructorAEditarse);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando al instructor', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._instructorService.update(id, instructorAEditarse);
                }
            }
            else {
                throw new common_1.BadRequestException('instructor no encotrado');
            }
        });
    }
    eliminar(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const instructorEncontrado = yield this._instructorService.findById(id);
            if (instructorEncontrado) {
                return yield this._instructorService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('instructor no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "findAll", null);
__decorate([
    common_1.Get('findWhereOr'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "findByNombreApellido", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_instructor_dto_1.CrearInstructorDto]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "crear", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_instructor_dto_1.EditarInstructorDto]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "editar", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], InstructorController.prototype, "eliminar", null);
InstructorController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('instructor'),
    __metadata("design:paramtypes", [instructor_service_1.InstructorService])
], InstructorController);
exports.InstructorController = InstructorController;
//# sourceMappingURL=instructor.controller.js.map
import { Repository } from 'typeorm';
import { CrearInstructorDto } from './dto/crear-instructor.dto';
import { InstructorEntity } from './instructor.entity';
import { EditarInstructorDto } from './dto/editar-instructor.dto';
export declare class InstructorService {
    private readonly _instructorRepository;
    constructor(_instructorRepository: Repository<InstructorEntity>);
    findAll(consulta: any): Promise<InstructorEntity[]>;
    findByNombreApellido(expresion: string, verActivos: string): Promise<InstructorEntity[]>;
    findById(id: number): Promise<InstructorEntity>;
    createOne(instructor: CrearInstructorDto): Promise<InstructorEntity>;
    createMany(instructores: CrearInstructorDto[]): Promise<InstructorEntity[]>;
    update(id: number, instructor: EditarInstructorDto): Promise<InstructorEntity>;
    delete(id: number): Promise<string>;
    contar(consulta: object): Promise<number>;
}

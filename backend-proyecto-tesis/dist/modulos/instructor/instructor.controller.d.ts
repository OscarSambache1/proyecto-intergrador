import { InstructorService } from './instructor.service';
import { InstructorEntity } from './instructor.entity';
import { CrearInstructorDto } from './dto/crear-instructor.dto';
import { EditarInstructorDto } from './dto/editar-instructor.dto';
export declare class InstructorController {
    private readonly _instructorService;
    constructor(_instructorService: InstructorService);
    findAll(consulta: any): Promise<InstructorEntity[]>;
    findByNombreApellido(consulta: any): Promise<InstructorEntity[]>;
    findOne(id: any): Promise<InstructorEntity>;
    crear(instructor: CrearInstructorDto): Promise<InstructorEntity>;
    editar(id: number, instructor: EditarInstructorDto): Promise<InstructorEntity>;
    eliminar(id: number): Promise<string>;
}

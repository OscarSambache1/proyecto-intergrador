import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export declare class CrearInstructorDto {
    rol: string;
    especializacion: string;
    planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];
    clasesHora: ClaseHoraEntity[];
    estado?: number;
}

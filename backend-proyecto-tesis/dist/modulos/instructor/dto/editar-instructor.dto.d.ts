import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export declare class EditarInstructorDto {
    especializacion: string;
    estado?: number;
    rol: string;
    planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];
    clasesHora: ClaseHoraEntity[];
}

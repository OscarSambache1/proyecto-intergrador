import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';
import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';
export declare class InstructorEntity {
    id: number;
    especializacion: string;
    estado: number;
    rol: string;
    planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];
    usuario: UsuarioEntity;
    clasesHora: ClaseHoraEntity[];
}

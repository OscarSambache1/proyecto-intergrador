import { UsuarioService } from '../usuario/usuario.service';
declare const HttpStrategy_base: new (...args: any[]) => any;
export declare class HttpStrategy extends HttpStrategy_base {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioService);
    validate(cedula: any, password: any, done: Function): Promise<void>;
}
export {};

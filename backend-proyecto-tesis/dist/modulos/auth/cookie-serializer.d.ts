import { PassportSerializer } from '@nestjs/passport';
export declare class CookieSerializer extends PassportSerializer {
    serializeUser(user: any, done: Function): any;
    deserializeUser(payload: any, done: Function): any;
}

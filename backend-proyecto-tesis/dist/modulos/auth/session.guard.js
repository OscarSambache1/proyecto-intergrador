"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jsonwebtoken_1 = require("jsonwebtoken");
class SessionGuard {
    canActivate(context) {
        const httpContext = context.switchToHttp();
        const request = httpContext.getRequest();
        if (request.session) {
            return true;
        }
        else {
            const token = request.headers.authorization;
            const tokenDecoded = jsonwebtoken_1.default.verify(token, 'shhhhh');
            const usuario = tokenDecoded.usuarioLogueado;
            if (usuario) {
                return true;
            }
            else {
                throw new common_1.BadRequestException('no se ha iniciado sesion');
            }
        }
    }
}
exports.SessionGuard = SessionGuard;
//# sourceMappingURL=session.guard.js.map
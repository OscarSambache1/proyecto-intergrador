import { CanActivate, ExecutionContext } from '@nestjs/common';
export declare class AppAuthGuard implements CanActivate {
    canActivate(context: ExecutionContext): Promise<boolean>;
}

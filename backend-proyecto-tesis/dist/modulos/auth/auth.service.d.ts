import { UsuarioService } from '../usuario/usuario.service';
import { UsuarioLoginDto } from '../usuario/dto/usuario-login.dto';
export declare class AuthService {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioService);
    validarUsuario(user: UsuarioLoginDto): Promise<any>;
}

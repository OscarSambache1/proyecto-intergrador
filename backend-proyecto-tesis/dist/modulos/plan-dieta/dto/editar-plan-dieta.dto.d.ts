import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { PlanDietaClienteEntity } from '../../plan-dieta-cliente/plan-dieta-cliente.entity';
export declare class EditarPlanDietaDto {
    nombre?: string;
    descripcion?: string;
    estado?: number;
    planesDietaDia?: DietaDiaEntity[];
    planesDietaCliente?: PlanDietaClienteEntity[];
}

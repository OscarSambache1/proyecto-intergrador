import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { PlanDietaClienteEntity } from '../../plan-dieta-cliente/plan-dieta-cliente.entity';
export declare class CrearPlanDietaDto {
    nombre: string;
    descripcion: string;
    planesDietaDia: DietaDiaEntity[];
    planesDietaCliente: PlanDietaClienteEntity[];
}

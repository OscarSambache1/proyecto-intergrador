import { Repository } from 'typeorm';
import { PlanDietaEntity } from './plan-dieta.entity';
import { CrearPlanDietaDto } from './dto/crear-plan-dieta.dto';
import { EditarPlanDietaDto } from './dto/editar-plan-dieta.dto';
export declare class PlanDietaService {
    private readonly _planDietaRepository;
    constructor(_planDietaRepository: Repository<PlanDietaEntity>);
    findAll(consulta: any): Promise<PlanDietaEntity[]>;
    findLike(consulta: any): Promise<PlanDietaEntity[]>;
    findById(id: number): Promise<PlanDietaEntity>;
    createOne(planDia: CrearPlanDietaDto): Promise<PlanDietaEntity>;
    createMany(planesDieta: CrearPlanDietaDto[]): Promise<PlanDietaEntity[]>;
    update(id: number, planDieta: EditarPlanDietaDto): Promise<PlanDietaEntity>;
    delete(id: number): Promise<void>;
}

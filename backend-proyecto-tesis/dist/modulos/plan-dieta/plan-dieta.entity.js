"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const dieta_dia_entity_1 = require("../dieta-dia/dieta-dia.entity");
const plan_dieta_cliente_entity_1 = require("../plan-dieta-cliente/plan-dieta-cliente.entity");
let PlanDietaEntity = class PlanDietaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], PlanDietaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], PlanDietaEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'varchar' }),
    __metadata("design:type", String)
], PlanDietaEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], PlanDietaEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => dieta_dia_entity_1.DietaDiaEntity, dietaDia => dietaDia.planDieta),
    __metadata("design:type", Array)
], PlanDietaEntity.prototype, "planesDietaDia", void 0);
__decorate([
    typeorm_1.OneToMany(type => plan_dieta_cliente_entity_1.PlanDietaClienteEntity, planDietaCliente => planDietaCliente.planDieta),
    __metadata("design:type", Array)
], PlanDietaEntity.prototype, "planesDietaCliente", void 0);
PlanDietaEntity = __decorate([
    typeorm_1.Entity('plan-dieta')
], PlanDietaEntity);
exports.PlanDietaEntity = PlanDietaEntity;
//# sourceMappingURL=plan-dieta.entity.js.map
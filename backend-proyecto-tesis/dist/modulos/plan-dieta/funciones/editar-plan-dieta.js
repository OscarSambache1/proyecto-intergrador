"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_plan_dieta_dto_1 = require("../dto/editar-plan-dieta.dto");
function editarPlanDieta(dieta) {
    const planDietaAEditar = new editar_plan_dieta_dto_1.EditarPlanDietaDto();
    Object.keys(dieta).map(atributo => {
        planDietaAEditar[atributo] = dieta[atributo];
    });
    return planDietaAEditar;
}
exports.editarPlanDieta = editarPlanDieta;
//# sourceMappingURL=editar-plan-dieta.js.map
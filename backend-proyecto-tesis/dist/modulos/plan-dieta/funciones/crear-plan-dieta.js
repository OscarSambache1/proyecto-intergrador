"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_plan_dieta_dto_1 = require("../dto/crear-plan-dieta.dto");
function crearPlanDieta(dieta) {
    const planDietaACrear = new crear_plan_dieta_dto_1.CrearPlanDietaDto();
    Object.keys(dieta).map(atributo => {
        planDietaACrear[atributo] = dieta[atributo];
    });
    return planDietaACrear;
}
exports.crearPlanDieta = crearPlanDieta;
//# sourceMappingURL=crear-plan-dieta.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const plan_dieta_service_1 = require("./plan-dieta.service");
const crear_plan_dieta_dto_1 = require("./dto/crear-plan-dieta.dto");
const crear_plan_dieta_1 = require("./funciones/crear-plan-dieta");
const editar_plan_dieta_dto_1 = require("./dto/editar-plan-dieta.dto");
const editar_plan_dieta_1 = require("./funciones/editar-plan-dieta");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let PlanDiaController = class PlanDiaController {
    constructor(_planDietaService) {
        this._planDietaService = _planDietaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._planDietaService.findAll(JSON.parse(consulta));
        });
    }
    findAllLike(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._planDietaService.findLike(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planDietaEncontrado = yield this._planDietaService.findById(id);
            if (planDietaEncontrado) {
                return yield this._planDietaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El plan de dieta no existe');
            }
        });
    }
    create(planDia) {
        return __awaiter(this, void 0, void 0, function* () {
            const plandietaACrearse = crear_plan_dieta_1.crearPlanDieta(planDia);
            const arregloErrores = yield class_validator_1.validate(plandietaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el plan de dieta', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._planDietaService.createOne(plandietaACrearse);
            }
        });
    }
    update(id, planDieta) {
        return __awaiter(this, void 0, void 0, function* () {
            const planDietaEncontrado = yield this._planDietaService.findById(id);
            if (planDietaEncontrado) {
                const planDietaAEditar = editar_plan_dieta_1.editarPlanDieta(planDieta);
                const arregloErrores = yield class_validator_1.validate(planDietaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el plan de dieta', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._planDietaService.update(id, planDietaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Plan de dieta no encontrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planDiaEncontrado = yield this._planDietaService.findById(id);
            if (planDiaEncontrado) {
                yield this._planDietaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El plan de dieta no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "findAll", null);
__decorate([
    common_1.Get('like'),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "findAllLike", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_plan_dieta_dto_1.CrearPlanDietaDto]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_plan_dieta_dto_1.EditarPlanDietaDto]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanDiaController.prototype, "delete", null);
PlanDiaController = __decorate([
    common_1.Controller('plan-dieta'),
    __metadata("design:paramtypes", [plan_dieta_service_1.PlanDietaService])
], PlanDiaController);
exports.PlanDiaController = PlanDiaController;
//# sourceMappingURL=plan-dieta.controller.js.map
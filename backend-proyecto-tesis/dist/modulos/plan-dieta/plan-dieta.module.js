"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const plan_dieta_entity_1 = require("./plan-dieta.entity");
const plan_dieta_controller_1 = require("./plan-dieta.controller");
const plan_dieta_service_1 = require("./plan-dieta.service");
let PlanDietaModule = class PlanDietaModule {
};
PlanDietaModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([plan_dieta_entity_1.PlanDietaEntity], 'default')],
        controllers: [plan_dieta_controller_1.PlanDiaController],
        providers: [plan_dieta_service_1.PlanDietaService],
        exports: [],
    })
], PlanDietaModule);
exports.PlanDietaModule = PlanDietaModule;
//# sourceMappingURL=plan-dieta.module.js.map
import { PlanDietaService } from './plan-dieta.service';
import { PlanDietaEntity } from './plan-dieta.entity';
import { CrearPlanDietaDto } from './dto/crear-plan-dieta.dto';
import { EditarPlanDietaDto } from './dto/editar-plan-dieta.dto';
export declare class PlanDiaController {
    private readonly _planDietaService;
    constructor(_planDietaService: PlanDietaService);
    findAll(consulta: any): Promise<PlanDietaEntity[]>;
    findAllLike(consulta: any): Promise<PlanDietaEntity[]>;
    findOne(id: number): Promise<PlanDietaEntity>;
    create(planDia: CrearPlanDietaDto): Promise<PlanDietaEntity>;
    update(id: number, planDieta: EditarPlanDietaDto): Promise<PlanDietaEntity>;
    delete(id: number): Promise<void>;
}

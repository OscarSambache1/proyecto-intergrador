"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const ejercicio_service_1 = require("./ejercicio.service");
const crear_ejercicio_dto_1 = require("./dto/crear-ejercicio.dto");
const crear_ejercicio_1 = require("./funciones/crear-ejercicio");
const editar_ejercicio_1 = require("./dto/editar-ejercicio");
const editar_ejercicio_2 = require("./funciones/editar-ejercicio");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let EjercicioController = class EjercicioController {
    constructor(_ejercicioService) {
        this._ejercicioService = _ejercicioService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioService.findAll(JSON.parse(consulta));
        });
    }
    findByNombreMusculo(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioService.findByNombreYMusculo(consulta.parametro, consulta.musculo);
        });
    }
    findByNombreOMusculo(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioService.findByNombreOMusculo(consulta.parametro, consulta.musculo);
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioEncontrado = yield this._ejercicioService.findById(id);
            if (ejercicioEncontrado) {
                return yield this._ejercicioService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('EL ejercicio no existe');
            }
        });
    }
    create(ejercicio) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioACrearse = crear_ejercicio_1.crearEjercicio(ejercicio);
            const arregloErrores = yield class_validator_1.validate(ejercicioACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el ejercicio', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._ejercicioService.createOne(ejercicioACrearse);
            }
        });
    }
    update(id, ejercicio) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioEncontrado = yield this._ejercicioService.findById(id);
            if (ejercicioEncontrado) {
                const ejercicioAEditar = editar_ejercicio_2.editarEjercicio(ejercicio);
                const arregloErrores = yield class_validator_1.validate(ejercicioAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el ejercicio', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._ejercicioService.update(id, ejercicioAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Ejercicio no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioEncontrado = yield this._ejercicioService.findById(id);
            if (ejercicioEncontrado) {
                yield this._ejercicioService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL ejercicio no existe');
            }
        });
    }
    uploadFile(id, file) {
        const fs = require('fs');
        fs.rename(`${file.path}`, `${file.path}.gif`, err => {
            if (err)
                console.log('ERROR: ' + err);
        });
        const ejercicio = new editar_ejercicio_1.EditarEjercicioDto();
        ejercicio.urlImagen = `${file.filename}.gif`;
        this._ejercicioService.update(id, ejercicio);
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "findAll", null);
__decorate([
    common_1.Get('findByNombreMusculo'),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "findByNombreMusculo", null);
__decorate([
    common_1.Get('findByNombreOMusculo'),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "findByNombreOMusculo", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_ejercicio_dto_1.CrearEjercicioDto]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_ejercicio_1.EditarEjercicioDto]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioController.prototype, "delete", null);
__decorate([
    common_1.Post('subirArchivo/:id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    common_1.UseInterceptors(common_1.FileInterceptor('file2', {
        dest: __dirname + '/../../../public/archivos-ejercicios',
        limits: {
            fieldSize: 10000000,
            fieldNameSize: 10000000,
        },
    })),
    __param(0, common_1.Param('id')), __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", void 0)
], EjercicioController.prototype, "uploadFile", null);
EjercicioController = __decorate([
    common_1.Controller('ejercicio'),
    __metadata("design:paramtypes", [ejercicio_service_1.EjercicioService])
], EjercicioController);
exports.EjercicioController = EjercicioController;
//# sourceMappingURL=ejercicio.controller.js.map
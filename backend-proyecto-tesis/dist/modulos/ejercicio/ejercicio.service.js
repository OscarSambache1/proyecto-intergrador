"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const ejercicio_entity_1 = require("./ejercicio.entity");
let EjercicioService = class EjercicioService {
    constructor(_ejercicioRepository) {
        this._ejercicioRepository = _ejercicioRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._ejercicioRepository.find(consulta);
        });
    }
    findByNombreYMusculo(parametro, musculo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRepository
                .createQueryBuilder('ejercicio')
                .innerJoinAndSelect('ejercicio.ejerciciosMusculo', 'ejerciciosMusculo')
                .innerJoinAndSelect('ejerciciosMusculo.musculo', 'musculo', 'musculo.id  = :musculo', { musculo })
                .where(`ejercicio.nombre LIKE :nombre`, { nombre: `%${parametro}%` })
                .getMany();
        });
    }
    findByNombreOMusculo(parametro, musculo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRepository
                .createQueryBuilder('ejercicio')
                .innerJoinAndSelect('ejercicio.ejerciciosMusculo', 'ejerciciosMusculo')
                .innerJoinAndSelect('ejerciciosMusculo.musculo', 'musculo', 'musculo.id  = :musculo OR ejercicio.nombre LIKE :nombre', { musculo, nombre: `%${parametro}%` })
                .getMany();
        });
    }
    findLike(campo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRepository.find({
                order: { id: 'DESC' },
                where: { nombre: typeorm_2.Like(`%${campo}%`) },
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRepository.findOne(id, {
                relations: [
                    'ejerciciosMusculo',
                    'ejerciciosRutina',
                    'ejerciciosMusculo.musculo',
                    'ejerciciosMusculo.ejercicio',
                ],
            });
        });
    }
    createOne(ejercicio) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRepository.save(this._ejercicioRepository.create(ejercicio));
        });
    }
    createMany(ejercicios) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioACrear = this._ejercicioRepository.create(ejercicios);
            return this._ejercicioRepository.save(ejercicioACrear);
        });
    }
    update(id, ejercicio) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._ejercicioRepository.update(id, ejercicio);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioAEliminar = yield this.findById(id);
            yield this._ejercicioRepository.remove(ejercicioAEliminar);
        });
    }
};
EjercicioService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(ejercicio_entity_1.EjercicioEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], EjercicioService);
exports.EjercicioService = EjercicioService;
//# sourceMappingURL=ejercicio.service.js.map
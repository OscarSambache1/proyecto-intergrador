"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const ejercicio_musculo_entity_1 = require("../ejercicio-musculo/ejercicio-musculo.entity");
const ejercicio_rutina_entity_1 = require("../ejercicio-rutina/ejercicio-rutina.entity");
let EjercicioEntity = class EjercicioEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EjercicioEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], EjercicioEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'longtext' }),
    __metadata("design:type", String)
], EjercicioEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'urlImagen', type: 'varchar' }),
    __metadata("design:type", String)
], EjercicioEntity.prototype, "urlImagen", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], EjercicioEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => ejercicio_musculo_entity_1.EjercicioMusculoEntity, ejercicioMusculo => ejercicioMusculo.ejercicio),
    __metadata("design:type", Array)
], EjercicioEntity.prototype, "ejerciciosMusculo", void 0);
__decorate([
    typeorm_1.OneToMany(type => ejercicio_rutina_entity_1.EjercicioRutinaEntity, ejercicioRutina => ejercicioRutina.ejercicio),
    __metadata("design:type", Array)
], EjercicioEntity.prototype, "ejerciciosRutina", void 0);
EjercicioEntity = __decorate([
    typeorm_1.Entity('ejercicio')
], EjercicioEntity);
exports.EjercicioEntity = EjercicioEntity;
//# sourceMappingURL=ejercicio.entity.js.map
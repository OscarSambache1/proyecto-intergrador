import { EjercicioService } from './ejercicio.service';
import { EjercicioEntity } from './ejercicio.entity';
import { CrearEjercicioDto } from './dto/crear-ejercicio.dto';
import { EditarEjercicioDto } from './dto/editar-ejercicio';
export declare class EjercicioController {
    private readonly _ejercicioService;
    constructor(_ejercicioService: EjercicioService);
    findAll(consulta: any): Promise<EjercicioEntity[]>;
    findByNombreMusculo(consulta: any): Promise<EjercicioEntity[]>;
    findByNombreOMusculo(consulta: any): Promise<EjercicioEntity[]>;
    findOne(id: number): Promise<EjercicioEntity>;
    create(ejercicio: CrearEjercicioDto): Promise<EjercicioEntity>;
    update(id: number, ejercicio: EditarEjercicioDto): Promise<EjercicioEntity>;
    delete(id: number): Promise<void>;
    uploadFile(id: number, file: any): void;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_ejercicio_1 = require("../dto/editar-ejercicio");
function editarEjercicio(ejercicio) {
    const ejercicioAEditar = new editar_ejercicio_1.EditarEjercicioDto();
    Object.keys(ejercicio).map(atributo => {
        ejercicioAEditar[atributo] = ejercicio[atributo];
    });
    return ejercicioAEditar;
}
exports.editarEjercicio = editarEjercicio;
//# sourceMappingURL=editar-ejercicio.js.map
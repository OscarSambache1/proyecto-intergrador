"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_ejercicio_dto_1 = require("../dto/crear-ejercicio.dto");
function crearEjercicio(ejercicio) {
    const ejercicioACrear = new crear_ejercicio_dto_1.CrearEjercicioDto();
    Object.keys(ejercicio).map(atributo => {
        ejercicioACrear[atributo] = ejercicio[atributo];
    });
    return ejercicioACrear;
}
exports.crearEjercicio = crearEjercicio;
//# sourceMappingURL=crear-ejercicio.js.map
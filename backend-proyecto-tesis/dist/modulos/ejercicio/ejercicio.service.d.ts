import { Repository } from 'typeorm';
import { EjercicioEntity } from './ejercicio.entity';
import { CrearEjercicioDto } from './dto/crear-ejercicio.dto';
import { EditarEjercicioDto } from './dto/editar-ejercicio';
export declare class EjercicioService {
    private readonly _ejercicioRepository;
    constructor(_ejercicioRepository: Repository<EjercicioEntity>);
    findAll(consulta: any): Promise<EjercicioEntity[]>;
    findByNombreYMusculo(parametro: string, musculo: number): Promise<EjercicioEntity[]>;
    findByNombreOMusculo(parametro: string, musculo: number): Promise<EjercicioEntity[]>;
    findLike(campo: string): Promise<EjercicioEntity[]>;
    findById(id: number): Promise<EjercicioEntity>;
    createOne(ejercicio: CrearEjercicioDto): Promise<EjercicioEntity>;
    createMany(ejercicios: CrearEjercicioDto[]): Promise<EjercicioEntity[]>;
    update(id: number, ejercicio: EditarEjercicioDto): Promise<EjercicioEntity>;
    delete(id: number): Promise<void>;
}

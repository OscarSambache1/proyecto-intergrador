import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
export declare class CrearEjercicioDto {
    nombre: string;
    descripcion: string;
    urlImagen: string;
    ejerciciosMusculo: EjercicioMusculoEntity[];
    ejerciciosRutina: EjercicioRutinaEntity[];
}

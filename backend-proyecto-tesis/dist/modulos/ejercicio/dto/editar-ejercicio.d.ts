import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
export declare class EditarEjercicioDto {
    nombre: string;
    descripcion: string;
    urlImagen: string;
    estado?: number;
    ejerciciosMusculo: EjercicioMusculoEntity[];
    ejerciciosRutina: EjercicioRutinaEntity[];
}

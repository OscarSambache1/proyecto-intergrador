import { ClaseEntity } from '../clase/clase.emtity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { ClaseDiaHoraEntity } from '../clase-hora-dia/clase-dia-hora.entity';
export declare class ClaseHoraEntity {
    id: number;
    horaInicio: string;
    horaFin: string;
    clase: ClaseEntity;
    instructor: InstructorEntity;
    clasesDiaHora: ClaseDiaHoraEntity[];
}

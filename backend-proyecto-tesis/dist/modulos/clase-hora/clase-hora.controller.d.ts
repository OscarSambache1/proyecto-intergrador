import { ClaseHoraEntity } from './clase-hora.entity';
import { ClaseHoraService } from './clase-hora.service';
import { CrearClaseHoraDto } from './dto/crear-clase-hora.dto';
import { EditarClaseHoraDto } from './dto/editar-clase-hora.dto';
export declare class ClaseHoraController {
    private readonly _claseHoraService;
    constructor(_claseHoraService: ClaseHoraService);
    findAll(consulta: any): Promise<ClaseHoraEntity[]>;
    findOne(id: number): Promise<ClaseHoraEntity>;
    create(claseHora: CrearClaseHoraDto): Promise<ClaseHoraEntity>;
    createMany(clasesHora: CrearClaseHoraDto[]): Promise<CrearClaseHoraDto[]>;
    update(id: number, claseHora: EditarClaseHoraDto): Promise<ClaseHoraEntity>;
    delete(id: number): Promise<void>;
}

import { Repository } from 'typeorm';
import { ClaseHoraEntity } from './clase-hora.entity';
import { EditarClaseHoraDto } from './dto/editar-clase-hora.dto';
import { CrearClaseHoraDto } from './dto/crear-clase-hora.dto';
export declare class ClaseHoraService {
    private readonly _claseHoraRepository;
    constructor(_claseHoraRepository: Repository<ClaseHoraEntity>);
    findAll(consulta: any): Promise<ClaseHoraEntity[]>;
    findLike(campo: string): Promise<ClaseHoraEntity[]>;
    findById(id: number): Promise<ClaseHoraEntity>;
    createOne(claseHora: CrearClaseHoraDto): Promise<ClaseHoraEntity>;
    createMany(claseHora: CrearClaseHoraDto[]): Promise<ClaseHoraEntity[]>;
    update(id: number, claseHora: EditarClaseHoraDto): Promise<ClaseHoraEntity>;
    delete(id: number): Promise<void>;
}

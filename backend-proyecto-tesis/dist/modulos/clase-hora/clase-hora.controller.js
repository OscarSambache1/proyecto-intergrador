"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const clase_hora_service_1 = require("./clase-hora.service");
const crear_clase_hora_dto_1 = require("./dto/crear-clase-hora.dto");
const crear_clase_hora_1 = require("./funciones/crear-clase-hora");
const editar_clase_hora_dto_1 = require("./dto/editar-clase-hora.dto");
const editar_clase_hora_1 = require("./funciones/editar-clase-hora");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ClaseHoraController = class ClaseHoraController {
    constructor(_claseHoraService) {
        this._claseHoraService = _claseHoraService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseHoraService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseHoraEncontrada = yield this._claseHoraService.findById(id);
            if (claseHoraEncontrada) {
                return yield this._claseHoraService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(claseHora) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseHoraACrearse = crear_clase_hora_1.crearClaseHora(claseHora);
            const arregloErrores = yield class_validator_1.validate(claseHoraACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._claseHoraService.createOne(claseHoraACrearse);
            }
        });
    }
    createMany(clasesHora) {
        clasesHora.forEach((claseHora) => __awaiter(this, void 0, void 0, function* () {
            const claseHoraACrearse = crear_clase_hora_1.crearClaseHora(yield claseHora);
            const arregloErrores = yield class_validator_1.validate(claseHoraACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._claseHoraService.createMany(clasesHora);
    }
    update(id, claseHora) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseHoraEncontrada = yield this._claseHoraService.findById(id);
            if (claseHoraEncontrada) {
                const claseHoraAEditar = editar_clase_hora_1.editarClaseHora(claseHora);
                const arregloErrores = yield class_validator_1.validate(claseHoraAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._claseHoraService.update(id, claseHoraAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseHoraEncontrada = yield this._claseHoraService.findById(id);
            if (claseHoraEncontrada) {
                yield this._claseHoraService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_clase_hora_dto_1.CrearClaseHoraDto]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_clase_hora_dto_1.EditarClaseHoraDto]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseHoraController.prototype, "delete", null);
ClaseHoraController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('clase-hora'),
    __metadata("design:paramtypes", [clase_hora_service_1.ClaseHoraService])
], ClaseHoraController);
exports.ClaseHoraController = ClaseHoraController;
//# sourceMappingURL=clase-hora.controller.js.map
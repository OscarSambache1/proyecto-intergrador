"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_clase_hora_dto_1 = require("../dto/editar-clase-hora.dto");
function editarClaseHora(claseHora) {
    const claseHoraAEditar = new editar_clase_hora_dto_1.EditarClaseHoraDto();
    Object.keys(claseHora).map(atributo => {
        claseHoraAEditar[atributo] = claseHora[atributo];
    });
    return claseHoraAEditar;
}
exports.editarClaseHora = editarClaseHora;
//# sourceMappingURL=editar-clase-hora.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_clase_hora_dto_1 = require("../dto/crear-clase-hora.dto");
function crearClaseHora(claseHora) {
    const claseHoraCrear = new crear_clase_hora_dto_1.CrearClaseHoraDto();
    Object.keys(claseHora).map(atributo => {
        claseHoraCrear[atributo] = claseHora[atributo];
    });
    return claseHoraCrear;
}
exports.crearClaseHora = crearClaseHora;
//# sourceMappingURL=crear-clase-hora.js.map
import { InstructorEntity } from '../../instructor/instructor.entity';
import { ClaseEntity } from '../../clase/clase.emtity';
import { ClaseDiaHoraEntity } from '../../clase-hora-dia/clase-dia-hora.entity';
export declare class CrearClaseHoraDto {
    horaInicio: string;
    horaFin: string;
    instructor: InstructorEntity;
    clase: ClaseEntity;
    clasesDiaHora: ClaseDiaHoraEntity[];
}

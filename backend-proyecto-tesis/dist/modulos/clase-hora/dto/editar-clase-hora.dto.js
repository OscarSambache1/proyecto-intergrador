"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const instructor_entity_1 = require("../../instructor/instructor.entity");
const clase_emtity_1 = require("../../clase/clase.emtity");
const class_validator_1 = require("class-validator");
class EditarClaseHoraDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditarClaseHoraDto.prototype, "horaInicio", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], EditarClaseHoraDto.prototype, "horaFin", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", instructor_entity_1.InstructorEntity)
], EditarClaseHoraDto.prototype, "instructor", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", clase_emtity_1.ClaseEntity)
], EditarClaseHoraDto.prototype, "clase", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Array)
], EditarClaseHoraDto.prototype, "clasesDiaHora", void 0);
exports.EditarClaseHoraDto = EditarClaseHoraDto;
//# sourceMappingURL=editar-clase-hora.dto.js.map
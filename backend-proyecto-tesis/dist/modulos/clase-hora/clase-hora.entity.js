"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const clase_emtity_1 = require("../clase/clase.emtity");
const instructor_entity_1 = require("../instructor/instructor.entity");
const clase_dia_hora_entity_1 = require("../clase-hora-dia/clase-dia-hora.entity");
let ClaseHoraEntity = class ClaseHoraEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ClaseHoraEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'hora_inicio', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ClaseHoraEntity.prototype, "horaInicio", void 0);
__decorate([
    typeorm_1.Column({ name: 'hora_fin', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ClaseHoraEntity.prototype, "horaFin", void 0);
__decorate([
    typeorm_1.ManyToOne(type => clase_emtity_1.ClaseEntity, clase => clase.clasesHora),
    __metadata("design:type", clase_emtity_1.ClaseEntity)
], ClaseHoraEntity.prototype, "clase", void 0);
__decorate([
    typeorm_1.ManyToOne(type => instructor_entity_1.InstructorEntity, instructor => instructor.clasesHora),
    __metadata("design:type", instructor_entity_1.InstructorEntity)
], ClaseHoraEntity.prototype, "instructor", void 0);
__decorate([
    typeorm_1.OneToMany(type => clase_dia_hora_entity_1.ClaseDiaHoraEntity, claseDiaHora => claseDiaHora.claseHora),
    __metadata("design:type", Array)
], ClaseHoraEntity.prototype, "clasesDiaHora", void 0);
ClaseHoraEntity = __decorate([
    typeorm_1.Entity('clase-hora')
], ClaseHoraEntity);
exports.ClaseHoraEntity = ClaseHoraEntity;
//# sourceMappingURL=clase-hora.entity.js.map
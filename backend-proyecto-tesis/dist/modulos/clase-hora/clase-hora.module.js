"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const clase_hora_entity_1 = require("./clase-hora.entity");
const clase_hora_controller_1 = require("./clase-hora.controller");
const clase_hora_service_1 = require("./clase-hora.service");
let ClaseHoraModule = class ClaseHoraModule {
};
ClaseHoraModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([clase_hora_entity_1.ClaseHoraEntity], 'default')],
        controllers: [clase_hora_controller_1.ClaseHoraController],
        providers: [clase_hora_service_1.ClaseHoraService],
        exports: [],
    })
], ClaseHoraModule);
exports.ClaseHoraModule = ClaseHoraModule;
//# sourceMappingURL=clase-hora.module.js.map
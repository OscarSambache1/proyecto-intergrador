"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const cabecera_factura_entity_1 = require("./cabecera-factura.entity");
const cabecera_factura_service_1 = require("./cabecera-factura.service");
const crear_cabecera_factura_dto_1 = require("./dto/crear-cabecera-factura.dto");
const crear_cabecera_1 = require("./funciones/crear-cabecera");
const editar_cabecera_1 = require("./funciones/editar-cabecera");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let CabeceraFacturaController = class CabeceraFacturaController {
    constructor(_cabeceraService) {
        this._cabeceraService = _cabeceraService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabeceraService.findAll(JSON.parse(consulta));
        });
    }
    findByNombreApellidoCodigo(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._cabeceraService.findByNombreApellidoCodigo(consulta.expresion);
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabeceraEncontrada = yield this._cabeceraService.findById(id);
            if (cabeceraEncontrada) {
                return yield this._cabeceraService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('La cabecera no existe');
            }
        });
    }
    create(cabecera) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabeceraACrear = crear_cabecera_1.crearCabecera(cabecera);
            const arregloErrores = yield class_validator_1.validate(cabeceraACrear);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando la cabecera', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._cabeceraService.createOne(cabeceraACrear);
            }
        });
    }
    update(id, cabecera) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabeceraEncontrada = yield this._cabeceraService.findById(id);
            if (cabeceraEncontrada) {
                const cabeceraAEditar = editar_cabecera_1.editarCabecera(cabecera);
                const arregloErrores = yield class_validator_1.validate(cabeceraAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando la cabecera', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._cabeceraService.update(id, cabeceraAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Cabecera no encotrada');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabeceraEncontrada = yield this._cabeceraService.findById(id);
            if (cabeceraEncontrada) {
                yield this._cabeceraService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('La cabecera no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "findAll", null);
__decorate([
    common_1.Get('findWhereOr'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "findByNombreApellidoCodigo", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_cabecera_factura_dto_1.CrearCabeceraFacturaDto]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, cabecera_factura_entity_1.CabeceraFacturaEntity]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CabeceraFacturaController.prototype, "delete", null);
CabeceraFacturaController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('cabecera-factura'),
    __metadata("design:paramtypes", [cabecera_factura_service_1.CabeceraFacturaService])
], CabeceraFacturaController);
exports.CabeceraFacturaController = CabeceraFacturaController;
//# sourceMappingURL=cabecera-factura.controller.js.map
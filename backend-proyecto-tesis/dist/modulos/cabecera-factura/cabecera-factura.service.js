"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const cabecera_factura_entity_1 = require("./cabecera-factura.entity");
let CabeceraFacturaService = class CabeceraFacturaService {
    constructor(_cabezeraRepository) {
        this._cabezeraRepository = _cabezeraRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabezeraRepository.find(consulta);
        });
    }
    findByNombreApellidoCodigo(expresion) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabezeraRepository
                .createQueryBuilder('cabecera-factura')
                .innerJoinAndSelect('cabecera-factura.usuario', 'usuario', 'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos', { nombres: `%${expresion}%`, apellidos: `%${expresion}%` })
                .orderBy('cabecera-factura.id', 'DESC')
                .getMany();
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabezeraRepository.findOne(id, {
                relations: ['detallesFactura', 'usuario', 'detallesFactura.producto'],
            });
        });
    }
    createOne(cabeceraFactura) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabezeraRepository.save(this._cabezeraRepository.create(cabeceraFactura));
        });
    }
    createMany(cabecerasFactura) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabecerasACrear = this._cabezeraRepository.create(cabecerasFactura);
            return yield this._cabezeraRepository.save(cabecerasACrear);
        });
    }
    update(id, cabeceraFactura) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._cabezeraRepository.update(id, cabeceraFactura);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const cabeceraAEliminar = yield this.findById(id);
            yield this._cabezeraRepository.remove(cabeceraAEliminar);
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._cabezeraRepository.count({
                where: consulta,
            });
        });
    }
};
CabeceraFacturaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(cabecera_factura_entity_1.CabeceraFacturaEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CabeceraFacturaService);
exports.CabeceraFacturaService = CabeceraFacturaService;
//# sourceMappingURL=cabecera-factura.service.js.map
import { CabeceraFacturaEntity } from './cabecera-factura.entity';
import { CabeceraFacturaService } from './cabecera-factura.service';
import { CrearCabeceraFacturaDto } from './dto/crear-cabecera-factura.dto';
export declare class CabeceraFacturaController {
    private readonly _cabeceraService;
    constructor(_cabeceraService: CabeceraFacturaService);
    findAll(consulta: any): Promise<CabeceraFacturaEntity[]>;
    findByNombreApellidoCodigo(consulta: any): Promise<CabeceraFacturaEntity[]>;
    findOne(id: number): Promise<CabeceraFacturaEntity>;
    create(cabecera: CrearCabeceraFacturaDto): Promise<CabeceraFacturaEntity>;
    update(id: number, cabecera: CabeceraFacturaEntity): Promise<CabeceraFacturaEntity>;
    delete(id: number): Promise<void>;
}

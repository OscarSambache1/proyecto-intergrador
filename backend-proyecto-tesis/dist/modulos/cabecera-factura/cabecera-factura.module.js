"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const cabecera_factura_entity_1 = require("./cabecera-factura.entity");
const cabecera_factura_service_1 = require("./cabecera-factura.service");
const cabecera_factura_controller_1 = require("./cabecera-factura.controller");
let CabeceraFacturaModule = class CabeceraFacturaModule {
};
CabeceraFacturaModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([cabecera_factura_entity_1.CabeceraFacturaEntity], 'default')],
        controllers: [cabecera_factura_controller_1.CabeceraFacturaController],
        providers: [cabecera_factura_service_1.CabeceraFacturaService],
        exports: [],
    })
], CabeceraFacturaModule);
exports.CabeceraFacturaModule = CabeceraFacturaModule;
//# sourceMappingURL=cabecera-factura.module.js.map
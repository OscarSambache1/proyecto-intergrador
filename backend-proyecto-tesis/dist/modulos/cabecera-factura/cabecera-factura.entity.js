"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const detalle_factura_entity_1 = require("../detalle-factura/detalle-factura.entity");
const usuario_entity_1 = require("../usuario/usuario.entity");
let CabeceraFacturaEntity = class CabeceraFacturaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], CabeceraFacturaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'fecha', type: 'varchar' }),
    __metadata("design:type", String)
], CabeceraFacturaEntity.prototype, "fecha", void 0);
__decorate([
    typeorm_1.Column({
        name: 'subtotal',
        type: 'decimal',
        precision: 15,
        scale: 2,
        default: 0,
    }),
    __metadata("design:type", Number)
], CabeceraFacturaEntity.prototype, "subtotal", void 0);
__decorate([
    typeorm_1.Column({
        name: 'total',
        type: 'decimal',
        precision: 15,
        scale: 2,
        default: 0,
    }),
    __metadata("design:type", Number)
], CabeceraFacturaEntity.prototype, "total", void 0);
__decorate([
    typeorm_1.Column({
        name: 'estado',
        type: 'enum',
        enum: ['anulada', 'valida'],
        default: 'valida',
    }),
    __metadata("design:type", String)
], CabeceraFacturaEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.ManyToOne(type => usuario_entity_1.UsuarioEntity, usuario => usuario.cabecerasFactura),
    __metadata("design:type", usuario_entity_1.UsuarioEntity)
], CabeceraFacturaEntity.prototype, "usuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => detalle_factura_entity_1.DetalleFacturaEntity, detalleFactura => detalleFactura.cabeceraFactura),
    __metadata("design:type", Array)
], CabeceraFacturaEntity.prototype, "detallesFactura", void 0);
CabeceraFacturaEntity = __decorate([
    typeorm_1.Entity('cabecera-factura')
], CabeceraFacturaEntity);
exports.CabeceraFacturaEntity = CabeceraFacturaEntity;
//# sourceMappingURL=cabecera-factura.entity.js.map
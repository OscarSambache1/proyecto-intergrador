import { DetalleFacturaEntity } from '../detalle-factura/detalle-factura.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';
export declare class CabeceraFacturaEntity {
    id: number;
    fecha: string;
    subtotal: number;
    total: number;
    estado: string;
    usuario: UsuarioEntity;
    detallesFactura: DetalleFacturaEntity[];
}

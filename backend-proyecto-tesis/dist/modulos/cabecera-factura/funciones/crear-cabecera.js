"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_cabecera_factura_dto_1 = require("../dto/crear-cabecera-factura.dto");
function crearCabecera(cabeceraFactura) {
    const cabeceraFacturaACrear = new crear_cabecera_factura_dto_1.CrearCabeceraFacturaDto();
    Object.keys(cabeceraFactura).map(atributo => {
        cabeceraFacturaACrear[atributo] = cabeceraFactura[atributo];
    });
    cabeceraFacturaACrear.estado = 'valida';
    return cabeceraFacturaACrear;
}
exports.crearCabecera = crearCabecera;
//# sourceMappingURL=crear-cabecera.js.map
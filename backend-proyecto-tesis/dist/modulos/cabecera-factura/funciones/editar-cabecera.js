"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_cabecera_factura_dto_1 = require("../dto/editar-cabecera-factura.dto");
function editarCabecera(cabeceraFactura) {
    const cabeceraFacturaAEditar = new editar_cabecera_factura_dto_1.EditarCabeceraFacturaDto();
    Object.keys(cabeceraFactura).map(atributo => {
        cabeceraFacturaAEditar[atributo] = cabeceraFactura[atributo];
    });
    return cabeceraFacturaAEditar;
}
exports.editarCabecera = editarCabecera;
//# sourceMappingURL=editar-cabecera.js.map
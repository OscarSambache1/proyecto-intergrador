import { Repository } from 'typeorm';
import { CabeceraFacturaEntity } from './cabecera-factura.entity';
import { CrearCabeceraFacturaDto } from './dto/crear-cabecera-factura.dto';
import { EditarCabeceraFacturaDto } from './dto/editar-cabecera-factura.dto';
export declare class CabeceraFacturaService {
    private readonly _cabezeraRepository;
    constructor(_cabezeraRepository: Repository<CabeceraFacturaEntity>);
    findAll(consulta: any): Promise<CabeceraFacturaEntity[]>;
    findByNombreApellidoCodigo(expresion: string): Promise<CabeceraFacturaEntity[]>;
    findById(id: number): Promise<CabeceraFacturaEntity>;
    createOne(cabeceraFactura: CrearCabeceraFacturaDto): Promise<CabeceraFacturaEntity>;
    createMany(cabecerasFactura: CrearCabeceraFacturaDto[]): Promise<CabeceraFacturaEntity[]>;
    update(id: number, cabeceraFactura: EditarCabeceraFacturaDto): Promise<CabeceraFacturaEntity>;
    delete(id: number): Promise<void>;
    contar(consulta: object): Promise<number>;
}

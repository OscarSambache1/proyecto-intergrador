import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
import { UsuarioEntity } from '../../usuario/usuario.entity';
export declare class EditarCabeceraFacturaDto {
    fecha?: string;
    subtotal?: any;
    total?: any;
    estado: string;
    usuario?: UsuarioEntity;
    detallesFactura?: DetalleFacturaEntity[];
}

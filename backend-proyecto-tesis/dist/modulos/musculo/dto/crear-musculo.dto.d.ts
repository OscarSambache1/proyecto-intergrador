import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
export declare class CrearMusculoDto {
    nombre: string;
    ejerciciosMusculo: EjercicioMusculoEntity[];
}

import { EjercicioMusculoEntity } from '../../ejercicio-musculo/ejercicio-musculo.entity';
export declare class EditarMusculoDto {
    nombre: string;
    estado?: number;
    ejerciciosMusculo: EjercicioMusculoEntity[];
}

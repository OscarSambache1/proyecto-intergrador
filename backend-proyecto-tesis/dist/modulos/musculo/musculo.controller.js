"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const musculo_service_1 = require("./musculo.service");
const crear_musculo_dto_1 = require("./dto/crear-musculo.dto");
const crear_musculo_1 = require("./funciones/crear-musculo");
const editar_musculo_dto_1 = require("./dto/editar-musculo.dto");
const editar_musculo_1 = require("./funciones/editar-musculo");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let MusculoController = class MusculoController {
    constructor(_musculoService) {
        this._musculoService = _musculoService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._musculoService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const musculoEncontrado = yield this._musculoService.findById(id);
            if (musculoEncontrado) {
                return yield this._musculoService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('EL Músculo no existe');
            }
        });
    }
    create(musculo) {
        return __awaiter(this, void 0, void 0, function* () {
            const musculoACrearse = crear_musculo_1.crearMusculo(musculo);
            const arregloErrores = yield class_validator_1.validate(musculoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el musculo', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._musculoService.createOne(musculoACrearse);
            }
        });
    }
    update(id, musculo) {
        return __awaiter(this, void 0, void 0, function* () {
            const musculoEncontrado = yield this._musculoService.findById(id);
            if (musculoEncontrado) {
                const musculoAEditar = editar_musculo_1.editarMusculo(musculo);
                const arregloErrores = yield class_validator_1.validate(musculoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el músculo', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._musculoService.update(id, musculoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Músculo no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const musculoEncontrado = yield this._musculoService.findById(id);
            if (musculoEncontrado) {
                yield this._musculoService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL músculo no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MusculoController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], MusculoController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_musculo_dto_1.CrearMusculoDto]),
    __metadata("design:returntype", Promise)
], MusculoController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_musculo_dto_1.EditarMusculoDto]),
    __metadata("design:returntype", Promise)
], MusculoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], MusculoController.prototype, "delete", null);
MusculoController = __decorate([
    common_1.Controller('musculo'),
    __metadata("design:paramtypes", [musculo_service_1.MusculoService])
], MusculoController);
exports.MusculoController = MusculoController;
//# sourceMappingURL=musculo.controller.js.map
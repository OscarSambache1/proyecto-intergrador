"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_musculo_dto_1 = require("../dto/editar-musculo.dto");
function editarMusculo(musculo) {
    const musculoAEditar = new editar_musculo_dto_1.EditarMusculoDto();
    Object.keys(musculo).map(atributo => {
        musculoAEditar[atributo] = musculo[atributo];
    });
    return musculoAEditar;
}
exports.editarMusculo = editarMusculo;
//# sourceMappingURL=editar-musculo.js.map
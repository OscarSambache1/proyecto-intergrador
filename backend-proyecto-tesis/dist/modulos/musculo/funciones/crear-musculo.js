"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_musculo_dto_1 = require("../dto/crear-musculo.dto");
function crearMusculo(musculo) {
    const musculoACrear = new crear_musculo_dto_1.CrearMusculoDto();
    Object.keys(musculo).map(atributo => {
        musculoACrear[atributo] = musculo[atributo];
    });
    return musculoACrear;
}
exports.crearMusculo = crearMusculo;
//# sourceMappingURL=crear-musculo.js.map
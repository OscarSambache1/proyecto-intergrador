import { Repository } from 'typeorm';
import { MusculoEntity } from './musculo.entity';
import { CrearMusculoDto } from './dto/crear-musculo.dto';
import { EditarMusculoDto } from './dto/editar-musculo.dto';
export declare class MusculoService {
    private readonly _musculoRepository;
    constructor(_musculoRepository: Repository<MusculoEntity>);
    findAll(consulta: any): Promise<MusculoEntity[]>;
    findById(id: number): Promise<MusculoEntity>;
    createOne(musculo: CrearMusculoDto): Promise<MusculoEntity>;
    createMany(musculos: CrearMusculoDto[]): Promise<MusculoEntity[]>;
    update(id: number, musculo: EditarMusculoDto): Promise<MusculoEntity>;
    delete(id: number): Promise<void>;
}

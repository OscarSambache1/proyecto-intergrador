import { EjercicioMusculoEntity } from '../ejercicio-musculo/ejercicio-musculo.entity';
export declare class MusculoEntity {
    id: number;
    nombre: string;
    estado: number;
    ejerciciosMusculo: EjercicioMusculoEntity[];
}

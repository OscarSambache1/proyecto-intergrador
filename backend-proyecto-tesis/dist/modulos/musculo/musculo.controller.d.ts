import { MusculoService } from './musculo.service';
import { MusculoEntity } from './musculo.entity';
import { CrearMusculoDto } from './dto/crear-musculo.dto';
import { EditarMusculoDto } from './dto/editar-musculo.dto';
export declare class MusculoController {
    private readonly _musculoService;
    constructor(_musculoService: MusculoService);
    findAll(consulta: any): Promise<MusculoEntity[]>;
    findOne(id: number): Promise<MusculoEntity>;
    create(musculo: CrearMusculoDto): Promise<MusculoEntity>;
    update(id: number, musculo: EditarMusculoDto): Promise<MusculoEntity>;
    delete(id: number): Promise<void>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const tipo_dieta_1 = require("./tipo-dieta");
const tipo_dieta_service_1 = require("./tipo-dieta.service");
const tipo_dieta_controller_1 = require("./tipo-dieta.controller");
let TipoDietaModule = class TipoDietaModule {
};
TipoDietaModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([tipo_dieta_1.TipoDietaEntity], 'default')],
        controllers: [tipo_dieta_controller_1.TipoDietaController],
        providers: [tipo_dieta_service_1.TipoDietaService],
        exports: [tipo_dieta_service_1.TipoDietaService],
    })
], TipoDietaModule);
exports.TipoDietaModule = TipoDietaModule;
//# sourceMappingURL=tipo-dieta.module.js.map
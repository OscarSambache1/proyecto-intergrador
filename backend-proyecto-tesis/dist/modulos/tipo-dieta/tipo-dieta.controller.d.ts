import { TipoDietaService } from './tipo-dieta.service';
import { TipoDietaEntity } from './tipo-dieta';
import { CrearTipoDietaDto } from './dto/crear-tipo-dieta.dto';
import { EditarTipoDietaDto } from './dto/editar-tipo-dieta.dto';
export declare class TipoDietaController {
    private readonly _tipoDietaService;
    constructor(_tipoDietaService: TipoDietaService);
    findAll(consulta: any): Promise<TipoDietaEntity[]>;
    findOne(id: number): Promise<TipoDietaEntity>;
    create(tipoDieta: CrearTipoDietaDto): Promise<TipoDietaEntity>;
    update(id: number, tipoDieta: EditarTipoDietaDto): Promise<TipoDietaEntity>;
    delete(id: number): Promise<void>;
}

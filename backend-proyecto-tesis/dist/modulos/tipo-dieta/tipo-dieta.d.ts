import { DietaEntity } from '../dieta/dieta.entity';
export declare class TipoDietaEntity {
    id: number;
    nombre: string;
    estado: number;
    dietas: DietaEntity[];
}

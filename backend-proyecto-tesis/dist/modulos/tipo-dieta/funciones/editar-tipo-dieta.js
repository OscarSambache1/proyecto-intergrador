"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_tipo_dieta_dto_1 = require("../dto/editar-tipo-dieta.dto");
function editarTipoDieta(tipoDieta) {
    const tipoDietaAEditar = new editar_tipo_dieta_dto_1.EditarTipoDietaDto();
    Object.keys(tipoDieta).map(atributo => {
        tipoDietaAEditar[atributo] = tipoDieta[atributo];
    });
    return tipoDietaAEditar;
}
exports.editarTipoDieta = editarTipoDieta;
//# sourceMappingURL=editar-tipo-dieta.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_tipo_dieta_dto_1 = require("../dto/crear-tipo-dieta.dto");
function crearTipoDieta(tipoDieta) {
    const tipoDietaACrear = new crear_tipo_dieta_dto_1.CrearTipoDietaDto();
    Object.keys(tipoDieta).map(atributo => {
        tipoDietaACrear[atributo] = tipoDieta[atributo];
    });
    return tipoDietaACrear;
}
exports.crearTipoDieta = crearTipoDieta;
//# sourceMappingURL=crear-tipo-dieta.js.map
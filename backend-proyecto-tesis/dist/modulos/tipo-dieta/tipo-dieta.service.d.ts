import { Repository } from 'typeorm';
import { TipoDietaEntity } from './tipo-dieta';
import { CrearTipoDietaDto } from './dto/crear-tipo-dieta.dto';
import { EditarTipoDietaDto } from './dto/editar-tipo-dieta.dto';
export declare class TipoDietaService {
    private readonly _tipoDietaRepository;
    constructor(_tipoDietaRepository: Repository<TipoDietaEntity>);
    findAll(consulta: any): Promise<TipoDietaEntity[]>;
    findById(id: number): Promise<TipoDietaEntity>;
    createOne(tipoDieta: CrearTipoDietaDto): Promise<TipoDietaEntity>;
    createMany(tiposDieta: CrearTipoDietaDto[]): Promise<TipoDietaEntity[]>;
    update(id: number, tipoDieta: EditarTipoDietaDto): Promise<TipoDietaEntity>;
    delete(id: number): Promise<void>;
}

import { DietaEntity } from '../../dieta/dieta.entity';
export declare class EditarTipoDietaDto {
    nombre: string;
    estado?: number;
    dietas: DietaEntity[];
}

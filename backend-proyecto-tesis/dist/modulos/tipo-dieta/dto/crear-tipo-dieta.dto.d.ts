import { DietaEntity } from '../../dieta/dieta.entity';
export declare class CrearTipoDietaDto {
    nombre: string;
    dietas: DietaEntity[];
}

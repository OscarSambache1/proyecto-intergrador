"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const tipo_dieta_service_1 = require("./tipo-dieta.service");
const crear_tipo_dieta_dto_1 = require("./dto/crear-tipo-dieta.dto");
const crear_tipo_dieta_1 = require("./funciones/crear-tipo-dieta");
const editar_tipo_dieta_dto_1 = require("./dto/editar-tipo-dieta.dto");
const editar_tipo_dieta_1 = require("./funciones/editar-tipo-dieta");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let TipoDietaController = class TipoDietaController {
    constructor(_tipoDietaService) {
        this._tipoDietaService = _tipoDietaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._tipoDietaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoDietaEncontrada = yield this._tipoDietaService.findById(id);
            if (tipoDietaEncontrada) {
                return yield this._tipoDietaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El tipo de dieta no existe');
            }
        });
    }
    create(tipoDieta) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoDietaACrearse = crear_tipo_dieta_1.crearTipoDieta(tipoDieta);
            const arregloErrores = yield class_validator_1.validate(tipoDietaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando lel tipo de dieta', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._tipoDietaService.createOne(tipoDietaACrearse);
            }
        });
    }
    update(id, tipoDieta) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoDietaEncontrada = yield this._tipoDietaService.findById(id);
            if (tipoDietaEncontrada) {
                const tipoDietaAEditar = editar_tipo_dieta_1.editarTipoDieta(tipoDieta);
                const arregloErrores = yield class_validator_1.validate(tipoDietaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el tipo de dieta', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._tipoDietaService.update(id, tipoDietaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Tipo de dieta no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoDietaEncontrado = yield this._tipoDietaService.findById(id);
            if (tipoDietaEncontrado) {
                yield this._tipoDietaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El tipo de dieta no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TipoDietaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TipoDietaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_tipo_dieta_dto_1.CrearTipoDietaDto]),
    __metadata("design:returntype", Promise)
], TipoDietaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_tipo_dieta_dto_1.EditarTipoDietaDto]),
    __metadata("design:returntype", Promise)
], TipoDietaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TipoDietaController.prototype, "delete", null);
TipoDietaController = __decorate([
    common_1.Controller('tipo-dieta'),
    __metadata("design:paramtypes", [tipo_dieta_service_1.TipoDietaService])
], TipoDietaController);
exports.TipoDietaController = TipoDietaController;
//# sourceMappingURL=tipo-dieta.controller.js.map
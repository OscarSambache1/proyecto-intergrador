"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const clase_dia_hora_entity_1 = require("./clase-dia-hora.entity");
let ClaseDiaHoraService = class ClaseDiaHoraService {
    constructor(_claseDiaHoraRepository) {
        this._claseDiaHoraRepository = _claseDiaHoraRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._claseDiaHoraRepository.find(consulta);
        });
    }
    findLike(campo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseDiaHoraRepository.find({
                order: { id: 'DESC' },
                where: { nombre: typeorm_2.Like(`%${campo}%`) },
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseDiaHoraRepository.findOne(id, {
                relations: ['claseHora', 'dia'],
            });
        });
    }
    findByNombreClase(expresion) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseDiaHoraRepository
                .createQueryBuilder('clase-dia-hora')
                .innerJoinAndSelect('clase-dia-hora.claseHora', 'claseHora')
                .innerJoinAndSelect('clase-dia-hora.dia', 'dia')
                .innerJoinAndSelect('claseHora.clase', 'clase', 'clase.nombre LIKE :nombre  AND clase.estado = :estado', { nombre: `%${expresion}%`, estado: 1 })
                .innerJoinAndSelect('claseHora.instructor', 'instructor')
                .innerJoinAndSelect('instructor.usuario', 'usuario')
                .orderBy('claseHora.horaInicio', 'DESC')
                .getMany();
        });
    }
    createOne(claseDiaHora) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseDiaHoraRepository.save(this._claseDiaHoraRepository.create(claseDiaHora));
        });
    }
    createMany(clasesDiaHora) {
        return __awaiter(this, void 0, void 0, function* () {
            const clasesDiaGuardadas = this._claseDiaHoraRepository.create(clasesDiaHora);
            return this._claseDiaHoraRepository.save(clasesDiaGuardadas);
        });
    }
    update(id, claseDiaHora) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._claseDiaHoraRepository.update(id, claseDiaHora);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseDiaHoraAEliminar = yield this.findById(id);
            yield this._claseDiaHoraRepository.remove(claseDiaHoraAEliminar);
        });
    }
};
ClaseDiaHoraService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(clase_dia_hora_entity_1.ClaseDiaHoraEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ClaseDiaHoraService);
exports.ClaseDiaHoraService = ClaseDiaHoraService;
//# sourceMappingURL=clase-dia-hora.service.js.map
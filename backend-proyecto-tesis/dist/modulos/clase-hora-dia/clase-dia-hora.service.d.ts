import { Repository } from 'typeorm';
import { ClaseDiaHoraEntity } from './clase-dia-hora.entity';
import { CrearClaseDiaHoraDto } from './dto/crear-clase-dia-hora.dto';
import { EditarClaseDiaHoraDto } from './dto/editar-clase-dia-hora.dto';
export declare class ClaseDiaHoraService {
    private readonly _claseDiaHoraRepository;
    constructor(_claseDiaHoraRepository: Repository<ClaseDiaHoraEntity>);
    findAll(consulta: any): Promise<ClaseDiaHoraEntity[]>;
    findLike(campo: string): Promise<ClaseDiaHoraEntity[]>;
    findById(id: number): Promise<ClaseDiaHoraEntity>;
    findByNombreClase(expresion: string): Promise<ClaseDiaHoraEntity[]>;
    createOne(claseDiaHora: CrearClaseDiaHoraDto): Promise<ClaseDiaHoraEntity>;
    createMany(clasesDiaHora: CrearClaseDiaHoraDto[]): Promise<ClaseDiaHoraEntity[]>;
    update(id: number, claseDiaHora: EditarClaseDiaHoraDto): Promise<ClaseDiaHoraEntity>;
    delete(id: number): Promise<void>;
}

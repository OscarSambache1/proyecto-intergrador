import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';
import { DiaEntity } from '../dia/dia.entity';
export declare class ClaseDiaHoraEntity {
    id: number;
    claseHora: ClaseHoraEntity;
    dia: DiaEntity;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_clase_dia_hora_dto_1 = require("../dto/crear-clase-dia-hora.dto");
function crearClaseDiaHora(claseDiaHora) {
    const claseDiaHoraACrear = new crear_clase_dia_hora_dto_1.CrearClaseDiaHoraDto();
    Object.keys(claseDiaHora).map(atributo => {
        claseDiaHoraACrear[atributo] = claseDiaHora[atributo];
    });
    return claseDiaHoraACrear;
}
exports.crearClaseDiaHora = crearClaseDiaHora;
//# sourceMappingURL=crear-clase-dia-hora.js.map
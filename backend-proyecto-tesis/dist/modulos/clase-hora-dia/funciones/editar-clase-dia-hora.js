"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_clase_dia_hora_dto_1 = require("../dto/editar-clase-dia-hora.dto");
function editarClaseDiaHora(claseDiaHora) {
    const claseDiaHoraAEditar = new editar_clase_dia_hora_dto_1.EditarClaseDiaHoraDto();
    Object.keys(claseDiaHora).map(atributo => {
        claseDiaHoraAEditar[atributo] = claseDiaHora[atributo];
    });
    return claseDiaHoraAEditar;
}
exports.editarClaseDiaHora = editarClaseDiaHora;
//# sourceMappingURL=editar-clase-dia-hora.js.map
import { DiaEntity } from '../../dia/dia.entity';
import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export declare class CrearClaseDiaHoraDto {
    claseHora: ClaseHoraEntity;
    dia: DiaEntity;
}

import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
import { DiaEntity } from '../../dia/dia.entity';
export declare class EditarClaseDiaHoraDto {
    claseHora: ClaseHoraEntity;
    dia: DiaEntity;
}

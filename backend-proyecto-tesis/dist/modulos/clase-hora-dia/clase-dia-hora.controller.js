"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const clase_dia_hora_service_1 = require("./clase-dia-hora.service");
const crear_clase_dia_hora_dto_1 = require("./dto/crear-clase-dia-hora.dto");
const crear_clase_dia_hora_1 = require("./funciones/crear-clase-dia-hora");
const editar_clase_dia_hora_dto_1 = require("./dto/editar-clase-dia-hora.dto");
const editar_clase_dia_hora_1 = require("./funciones/editar-clase-dia-hora");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ClaseDiaHoraController = class ClaseDiaHoraController {
    constructor(_claseDiaHoraService) {
        this._claseDiaHoraService = _claseDiaHoraService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseDiaHoraService.findAll(JSON.parse(consulta));
        });
    }
    findByNombreClase(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._claseDiaHoraService.findByNombreClase(consulta.expresion);
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseDiaHoraEncontrada = yield this._claseDiaHoraService.findById(id);
            if (claseDiaHoraEncontrada) {
                return yield this._claseDiaHoraService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(claseDiaHora) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseDiaHoraACrearse = crear_clase_dia_hora_1.crearClaseDiaHora(claseDiaHora);
            const arregloErrores = yield class_validator_1.validate(claseDiaHoraACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._claseDiaHoraService.createOne(claseDiaHoraACrearse);
            }
        });
    }
    createMany(clasesHoraDia) {
        return __awaiter(this, void 0, void 0, function* () {
            clasesHoraDia.forEach((claseDia) => __awaiter(this, void 0, void 0, function* () {
                const claseDiaHoraACrearse = crear_clase_dia_hora_1.crearClaseDiaHora(yield claseDia);
                const arregloErrores = yield class_validator_1.validate(claseDiaHoraACrearse);
                const existenErrores = arregloErrores.length > 0;
                if (existenErrores) {
                    console.log(arregloErrores);
                    console.error('errores: creando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
            }));
            return this._claseDiaHoraService.createMany(clasesHoraDia);
        });
    }
    update(id, claseDiaHora) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseDiaHoraEncontrada = yield this._claseDiaHoraService.findById(id);
            if (claseDiaHoraEncontrada) {
                const claseDiaHoraAEditar = editar_clase_dia_hora_1.editarClaseDiaHora(claseDiaHora);
                const arregloErrores = yield class_validator_1.validate(claseDiaHoraAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._claseDiaHoraService.update(id, claseDiaHoraAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseDiaEncontrada = yield this._claseDiaHoraService.findById(id);
            if (claseDiaEncontrada) {
                yield this._claseDiaHoraService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "findAll", null);
__decorate([
    common_1.Get('findWhereOr'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "findByNombreClase", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_clase_dia_hora_dto_1.CrearClaseDiaHoraDto]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_clase_dia_hora_dto_1.EditarClaseDiaHoraDto]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseDiaHoraController.prototype, "delete", null);
ClaseDiaHoraController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('clase-dia-hora'),
    __metadata("design:paramtypes", [clase_dia_hora_service_1.ClaseDiaHoraService])
], ClaseDiaHoraController);
exports.ClaseDiaHoraController = ClaseDiaHoraController;
//# sourceMappingURL=clase-dia-hora.controller.js.map
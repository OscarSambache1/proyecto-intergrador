import { ClaseDiaHoraService } from './clase-dia-hora.service';
import { ClaseDiaHoraEntity } from './clase-dia-hora.entity';
import { CrearClaseDiaHoraDto } from './dto/crear-clase-dia-hora.dto';
import { EditarClaseDiaHoraDto } from './dto/editar-clase-dia-hora.dto';
export declare class ClaseDiaHoraController {
    private readonly _claseDiaHoraService;
    constructor(_claseDiaHoraService: ClaseDiaHoraService);
    findAll(consulta: any): Promise<ClaseDiaHoraEntity[]>;
    findByNombreClase(consulta: any): Promise<ClaseDiaHoraEntity[]>;
    findOne(id: number): Promise<ClaseDiaHoraEntity>;
    create(claseDiaHora: CrearClaseDiaHoraDto): Promise<ClaseDiaHoraEntity>;
    createMany(clasesHoraDia: CrearClaseDiaHoraDto[]): Promise<CrearClaseDiaHoraDto[]>;
    update(id: number, claseDiaHora: EditarClaseDiaHoraDto): Promise<ClaseDiaHoraEntity>;
    delete(id: number): Promise<void>;
}

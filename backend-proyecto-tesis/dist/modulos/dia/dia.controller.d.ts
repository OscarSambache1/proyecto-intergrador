import { DiaService } from './dia.service';
import { DiaEntity } from './dia.entity';
export declare class DiaController {
    private readonly _diaService;
    constructor(_diaService: DiaService);
    findAll(consulta: any): Promise<DiaEntity[]>;
    findOne(id: number): Promise<DiaEntity>;
}

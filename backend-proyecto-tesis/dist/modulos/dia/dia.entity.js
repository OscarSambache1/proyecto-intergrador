"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const clase_dia_hora_entity_1 = require("../clase-hora-dia/clase-dia-hora.entity");
const rutina_plan_entrenamiento_entity_1 = require("../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity");
const dieta_dia_entity_1 = require("../dieta-dia/dieta-dia.entity");
let DiaEntity = class DiaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], DiaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], DiaEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.OneToMany(type => clase_dia_hora_entity_1.ClaseDiaHoraEntity, claseDiaHora => claseDiaHora.dia),
    __metadata("design:type", Array)
], DiaEntity.prototype, "clasesDiaHora", void 0);
__decorate([
    typeorm_1.OneToMany(type => rutina_plan_entrenamiento_entity_1.RutinaPlanEntrenamientoEntity, rutinaPlanEntrenamiento => rutinaPlanEntrenamiento.dia),
    __metadata("design:type", Array)
], DiaEntity.prototype, "rutinasPlanEntrenamiento", void 0);
__decorate([
    typeorm_1.OneToMany(type => dieta_dia_entity_1.DietaDiaEntity, dietaDia => dietaDia.dia),
    __metadata("design:type", Array)
], DiaEntity.prototype, "dietasDia", void 0);
DiaEntity = __decorate([
    typeorm_1.Entity('dia')
], DiaEntity);
exports.DiaEntity = DiaEntity;
//# sourceMappingURL=dia.entity.js.map
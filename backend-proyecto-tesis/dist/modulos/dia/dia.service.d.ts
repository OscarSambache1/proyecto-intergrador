import { Repository } from 'typeorm';
import { DiaEntity } from './dia.entity';
export declare class DiaService {
    private readonly _diaRepository;
    constructor(_diaRepository: Repository<DiaEntity>);
    findAll(consulta: any): Promise<DiaEntity[]>;
    findLike(campo: string): Promise<DiaEntity[]>;
    findById(id: number): Promise<DiaEntity>;
    createMany(dias: DiaEntity[]): Promise<DiaEntity[]>;
}

import { ClaseDiaHoraEntity } from '../clase-hora-dia/clase-dia-hora.entity';
import { RutinaPlanEntrenamientoEntity } from '../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { DietaDiaEntity } from '../dieta-dia/dieta-dia.entity';
export declare class DiaEntity {
    id: number;
    nombre: string;
    clasesDiaHora: ClaseDiaHoraEntity[];
    rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
    dietasDia: DietaDiaEntity[];
}

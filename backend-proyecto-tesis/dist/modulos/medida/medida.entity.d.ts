import { ClienteEntity } from '../cliente/cliente.entity';
export declare class MedidaEntity {
    id: number;
    torax: number;
    abdomen: number;
    muslo: number;
    pantorrilla: number;
    biceps: number;
    peso: number;
    estatura: number;
    fechaRegistro: string;
    cliente: ClienteEntity;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const medida_entity_1 = require("./medida.entity");
const typeorm_2 = require("typeorm");
let MedidaService = class MedidaService {
    constructor(_medidaRepository) {
        this._medidaRepository = _medidaRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaRepository.find(consulta);
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaRepository.findOne(id, {
                relations: ['cliente'],
            });
        });
    }
    createOne(medida) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaRepository.save(this._medidaRepository.create(medida));
        });
    }
    createMany(medidas) {
        return __awaiter(this, void 0, void 0, function* () {
            const medidasACrear = this._medidaRepository.create(medidas);
            return this._medidaRepository.save(medidasACrear);
        });
    }
    update(id, medida) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._medidaRepository.update(id, medida);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const mmedidaAEliminar = yield this.findById(id);
            yield this._medidaRepository.remove(mmedidaAEliminar);
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaRepository.count({
                where: consulta,
            });
        });
    }
    findLast() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaRepository.find({
                order: {
                    id: 'DESC',
                },
            });
        });
    }
};
MedidaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(medida_entity_1.MedidaEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], MedidaService);
exports.MedidaService = MedidaService;
//# sourceMappingURL=medida.service.js.map
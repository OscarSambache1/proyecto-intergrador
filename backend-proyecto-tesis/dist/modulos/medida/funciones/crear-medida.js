"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_medida_dto_1 = require("../dto/crear-medida.dto");
function crearMedida(medida) {
    const medidaACrear = new crear_medida_dto_1.CrearMedidasDto();
    Object.keys(medida).map(atributo => {
        medidaACrear[atributo] = medida[atributo];
    });
    return medidaACrear;
}
exports.crearMedida = crearMedida;
//# sourceMappingURL=crear-medida.js.map
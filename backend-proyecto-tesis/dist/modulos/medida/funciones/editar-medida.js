"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_medida_dto_1 = require("../dto/editar-medida.dto");
function editarMedida(medida) {
    const medidaAEditar = new editar_medida_dto_1.EditarMedidaDto();
    Object.keys(medida).map(atributo => {
        medidaAEditar[atributo] = medida[atributo];
    });
    return medidaAEditar;
}
exports.editarMedida = editarMedida;
//# sourceMappingURL=editar-medida.js.map
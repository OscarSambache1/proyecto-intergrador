"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const medida_service_1 = require("./medida.service");
const crear_medida_dto_1 = require("./dto/crear-medida.dto");
const crear_medida_1 = require("./funciones/crear-medida");
const class_validator_1 = require("class-validator");
const editar_medida_dto_1 = require("./dto/editar-medida.dto");
const editar_medida_1 = require("./funciones/editar-medida");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let MedidaController = class MedidaController {
    constructor(_medidaService) {
        this._medidaService = _medidaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._medidaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const medidaEncontrada = yield this._medidaService.findById(id);
            if (medidaEncontrada) {
                return yield this._medidaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('medida no existe');
            }
        });
    }
    create(medida) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(medida);
            const medidaACrearse = crear_medida_1.crearMedida(medida);
            const arregloErrores = yield class_validator_1.validate(medidaACrearse);
            const existenErrores = arregloErrores.length > 0;
            console.log(arregloErrores);
            if (existenErrores) {
                console.error('errores: creando la medida', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._medidaService.createOne(medidaACrearse);
            }
        });
    }
    update(id, medida) {
        return __awaiter(this, void 0, void 0, function* () {
            const medidaEncontrada = yield this._medidaService.findById(id);
            if (medidaEncontrada) {
                const medidaAEditarse = editar_medida_1.editarMedida(medida);
                const arregloErrores = yield class_validator_1.validate(medidaAEditarse);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando la medida', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._medidaService.update(id, medidaAEditarse);
                }
            }
            else {
                throw new common_1.BadRequestException('Medida no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const medidaEncontrada = yield this._medidaService.findById(id);
            if (medidaEncontrada) {
                yield this._medidaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('Medida no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MedidaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], MedidaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_medida_dto_1.CrearMedidasDto]),
    __metadata("design:returntype", Promise)
], MedidaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_medida_dto_1.EditarMedidaDto]),
    __metadata("design:returntype", Promise)
], MedidaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], MedidaController.prototype, "delete", null);
MedidaController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('medida'),
    __metadata("design:paramtypes", [medida_service_1.MedidaService])
], MedidaController);
exports.MedidaController = MedidaController;
//# sourceMappingURL=medida.controller.js.map
import { MedidaEntity } from './medida.entity';
import { MedidaService } from './medida.service';
import { CrearMedidasDto } from './dto/crear-medida.dto';
import { EditarMedidaDto } from './dto/editar-medida.dto';
export declare class MedidaController {
    private readonly _medidaService;
    constructor(_medidaService: MedidaService);
    findAll(consulta: any): Promise<MedidaEntity[]>;
    findOne(id: number): Promise<MedidaEntity>;
    create(medida: CrearMedidasDto): Promise<MedidaEntity>;
    update(id: number, medida: EditarMedidaDto): Promise<MedidaEntity>;
    delete(id: number): Promise<void>;
}

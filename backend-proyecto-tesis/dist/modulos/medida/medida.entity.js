"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const cliente_entity_1 = require("../cliente/cliente.entity");
let MedidaEntity = class MedidaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        name: 'torax',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "torax", void 0);
__decorate([
    typeorm_1.Column({
        name: 'abdomen',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "abdomen", void 0);
__decorate([
    typeorm_1.Column({
        name: 'muslo',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "muslo", void 0);
__decorate([
    typeorm_1.Column({
        name: 'pantorrilla',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "pantorrilla", void 0);
__decorate([
    typeorm_1.Column({
        name: 'biceps',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "biceps", void 0);
__decorate([
    typeorm_1.Column({
        name: 'peso',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "peso", void 0);
__decorate([
    typeorm_1.Column({
        name: 'estatura',
        type: 'decimal',
        precision: 15,
        scale: 2,
    }),
    __metadata("design:type", Number)
], MedidaEntity.prototype, "estatura", void 0);
__decorate([
    typeorm_1.Column({ name: 'fechaRegistro', type: 'varchar' }),
    __metadata("design:type", String)
], MedidaEntity.prototype, "fechaRegistro", void 0);
__decorate([
    typeorm_1.ManyToOne(type => cliente_entity_1.ClienteEntity, cliente => cliente.medidas),
    __metadata("design:type", cliente_entity_1.ClienteEntity)
], MedidaEntity.prototype, "cliente", void 0);
MedidaEntity = __decorate([
    typeorm_1.Entity('medida')
], MedidaEntity);
exports.MedidaEntity = MedidaEntity;
//# sourceMappingURL=medida.entity.js.map
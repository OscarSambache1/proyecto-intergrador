import { CrearCLienteDto } from '../../cliente/dto/crear-cliente.dto';
export declare class CrearMedidasDto {
    torax: number;
    abdomen: number;
    muslo: number;
    pantorrilla: number;
    biceps: number;
    peso: number;
    estatura: number;
    fechaRegistro: string;
    cliente: CrearCLienteDto;
}

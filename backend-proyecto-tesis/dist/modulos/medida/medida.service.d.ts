import { MedidaEntity } from './medida.entity';
import { Repository } from 'typeorm';
import { CrearMedidasDto } from './dto/crear-medida.dto';
import { EditarMedidaDto } from './dto/editar-medida.dto';
export declare class MedidaService {
    private readonly _medidaRepository;
    constructor(_medidaRepository: Repository<MedidaEntity>);
    findAll(consulta: any): Promise<MedidaEntity[]>;
    findById(id: number): Promise<MedidaEntity>;
    createOne(medida: CrearMedidasDto): Promise<MedidaEntity>;
    createMany(medidas: CrearMedidasDto[]): Promise<MedidaEntity[]>;
    update(id: number, medida: EditarMedidaDto): Promise<MedidaEntity>;
    delete(id: number): Promise<void>;
    contar(consulta: object): Promise<number>;
    findLast(): Promise<MedidaEntity[]>;
}

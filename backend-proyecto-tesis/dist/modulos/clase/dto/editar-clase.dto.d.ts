import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export declare class EditarClaseDto {
    nombre?: string;
    descripcion?: string;
    estado?: number;
    clasesHora: ClaseHoraEntity[];
}

import { ClaseHoraEntity } from '../../clase-hora/clase-hora.entity';
export declare class CrearClaseDto {
    nombre?: string;
    descripcion?: string;
    clasesHora: ClaseHoraEntity[];
}

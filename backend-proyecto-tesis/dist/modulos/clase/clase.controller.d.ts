import { ClaseService } from './clase.service';
import { ClaseEntity } from './clase.emtity';
import { CrearClaseDto } from './dto/crear-clase.dto';
import { EditarClaseDto } from './dto/editar-clase.dto';
export declare class ClaseController {
    private readonly _claseService;
    constructor(_claseService: ClaseService);
    findAll(consulta: any): Promise<ClaseEntity[]>;
    findOne(id: number): Promise<ClaseEntity>;
    create(clase: CrearClaseDto): Promise<ClaseEntity>;
    update(id: number, clase: EditarClaseDto): Promise<ClaseEntity>;
    delete(id: number): Promise<void>;
}

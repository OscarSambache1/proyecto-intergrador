"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const clase_service_1 = require("./clase.service");
const crear_clase_dto_1 = require("./dto/crear-clase.dto");
const crear_clase_1 = require("./funciones/crear-clase");
const editar_clase_dto_1 = require("./dto/editar-clase.dto");
const editar_clase_1 = require("./funciones/editar-clase");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ClaseController = class ClaseController {
    constructor(_claseService) {
        this._claseService = _claseService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._claseService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseEncontrada = yield this._claseService.findById(id);
            if (claseEncontrada) {
                return yield this._claseService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('La clase no existe');
            }
        });
    }
    create(clase) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseACrearse = crear_clase_1.crearClase(clase);
            const arregloErrores = yield class_validator_1.validate(claseACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando la clase', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._claseService.createOne(claseACrearse);
            }
        });
    }
    update(id, clase) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseEncontrada = yield this._claseService.findById(id);
            if (claseEncontrada) {
                const claseAEditar = editar_clase_1.editarClase(clase);
                const arregloErrores = yield class_validator_1.validate(claseAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando la clase', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._claseService.update(id, claseAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Clase no encotrada');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const claseEncontrada = yield this._claseService.findById(id);
            if (claseEncontrada) {
                yield this._claseService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('La clase no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClaseController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_clase_dto_1.CrearClaseDto]),
    __metadata("design:returntype", Promise)
], ClaseController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_clase_dto_1.EditarClaseDto]),
    __metadata("design:returntype", Promise)
], ClaseController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClaseController.prototype, "delete", null);
ClaseController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('clase'),
    __metadata("design:paramtypes", [clase_service_1.ClaseService])
], ClaseController);
exports.ClaseController = ClaseController;
//# sourceMappingURL=clase.controller.js.map
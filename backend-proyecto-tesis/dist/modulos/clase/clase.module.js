"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const clase_emtity_1 = require("./clase.emtity");
const clase_controller_1 = require("./clase.controller");
const clase_service_1 = require("./clase.service");
let ClaseModule = class ClaseModule {
};
ClaseModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([clase_emtity_1.ClaseEntity], 'default')],
        controllers: [clase_controller_1.ClaseController],
        providers: [clase_service_1.ClaseService],
        exports: [],
    })
], ClaseModule);
exports.ClaseModule = ClaseModule;
//# sourceMappingURL=clase.module.js.map
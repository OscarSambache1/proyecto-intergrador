import { ClaseHoraEntity } from '../clase-hora/clase-hora.entity';
export declare class ClaseEntity {
    id: number;
    nombre: string;
    descripcion: string;
    estado: number;
    clasesHora: ClaseHoraEntity[];
}

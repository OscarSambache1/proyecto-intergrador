"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_clase_dto_1 = require("../dto/editar-clase.dto");
function editarClase(clase) {
    const claseAEditar = new editar_clase_dto_1.EditarClaseDto();
    Object.keys(clase).map(atributo => {
        claseAEditar[atributo] = clase[atributo];
    });
    return claseAEditar;
}
exports.editarClase = editarClase;
//# sourceMappingURL=editar-clase.js.map
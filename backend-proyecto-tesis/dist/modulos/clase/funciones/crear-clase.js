"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_clase_dto_1 = require("../dto/crear-clase.dto");
function crearClase(clase) {
    const claseACrear = new crear_clase_dto_1.CrearClaseDto();
    Object.keys(clase).map(atributo => {
        claseACrear[atributo] = clase[atributo];
    });
    return claseACrear;
}
exports.crearClase = crearClase;
//# sourceMappingURL=crear-clase.js.map
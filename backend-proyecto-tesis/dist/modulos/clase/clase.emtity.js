"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const clase_hora_entity_1 = require("../clase-hora/clase-hora.entity");
let ClaseEntity = class ClaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ClaseEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], ClaseEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'longtext' }),
    __metadata("design:type", String)
], ClaseEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], ClaseEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => clase_hora_entity_1.ClaseHoraEntity, claseHora => claseHora.clase),
    __metadata("design:type", Array)
], ClaseEntity.prototype, "clasesHora", void 0);
ClaseEntity = __decorate([
    typeorm_1.Entity('clase')
], ClaseEntity);
exports.ClaseEntity = ClaseEntity;
//# sourceMappingURL=clase.emtity.js.map
import { Repository } from 'typeorm';
import { ClaseEntity } from './clase.emtity';
import { CrearClaseDto } from './dto/crear-clase.dto';
import { EditarClaseDto } from './dto/editar-clase.dto';
export declare class ClaseService {
    private readonly _claseRepository;
    constructor(_claseRepository: Repository<ClaseEntity>);
    findAll(consulta: any): Promise<ClaseEntity[]>;
    findLike(campo: string): Promise<ClaseEntity[]>;
    findById(id: number): Promise<ClaseEntity>;
    createOne(clase: CrearClaseDto): Promise<ClaseEntity>;
    createMany(clases: CrearClaseDto[]): Promise<ClaseEntity[]>;
    update(id: number, clase: EditarClaseDto): Promise<ClaseEntity>;
    delete(id: number): Promise<void>;
}

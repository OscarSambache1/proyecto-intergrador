"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const rutina_plan_entrenamiento_entity_1 = require("../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity");
const plan_entrenamiento_cliente_instructor_entity_1 = require("../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity");
let PlanEntrenamientoEntity = class PlanEntrenamientoEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], PlanEntrenamientoEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], PlanEntrenamientoEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], PlanEntrenamientoEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], PlanEntrenamientoEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => rutina_plan_entrenamiento_entity_1.RutinaPlanEntrenamientoEntity, rutinaPlanEntrenamiento => rutinaPlanEntrenamiento.planEntrenamiento),
    __metadata("design:type", Array)
], PlanEntrenamientoEntity.prototype, "rutinasPlanEntrenamiento", void 0);
__decorate([
    typeorm_1.OneToMany(type => plan_entrenamiento_cliente_instructor_entity_1.PlanEntrenamientoClienteInstructorEntity, planEntrenamientoClienteInstructor => planEntrenamientoClienteInstructor.planEntrenamiento),
    __metadata("design:type", Array)
], PlanEntrenamientoEntity.prototype, "planesEntrenamientoClienteInstructor", void 0);
PlanEntrenamientoEntity = __decorate([
    typeorm_1.Entity('plan-entrenamiento')
], PlanEntrenamientoEntity);
exports.PlanEntrenamientoEntity = PlanEntrenamientoEntity;
//# sourceMappingURL=plan-entrenamiento.entity.js.map
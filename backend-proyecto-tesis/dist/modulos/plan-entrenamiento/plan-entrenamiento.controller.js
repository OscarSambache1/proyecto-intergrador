"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const plan_entrenamiento_service_1 = require("./plan-entrenamiento.service");
const crear_plam_entrenamiento_dto_1 = require("./dto/crear-plam-entrenamiento.dto");
const crear_plan_entrenamiento_1 = require("./funciones/crear-plan-entrenamiento");
const editar_plam_entrenamiento_dto_1 = require("./dto/editar-plam-entrenamiento.dto");
const editar_plan_entrenamiento_1 = require("./funciones/editar-plan-entrenamiento");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let PlanEntrenamientoController = class PlanEntrenamientoController {
    constructor(_planEntrenamientoService) {
        this._planEntrenamientoService = _planEntrenamientoService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._planEntrenamientoService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoEncontrado = yield this._planEntrenamientoService.findById(id);
            if (planEntrenamientoEncontrado) {
                return yield this._planEntrenamientoService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El plan de entrenamiento no existe');
            }
        });
    }
    create(planEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoACrearse = crear_plan_entrenamiento_1.crearPlanEntrenamiento(planEntrenamiento);
            const arregloErrores = yield class_validator_1.validate(planEntrenamientoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el plan de entrenamiento', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._planEntrenamientoService.createOne(planEntrenamientoACrearse);
            }
        });
    }
    update(id, planEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoEncontrado = yield this._planEntrenamientoService.findById(id);
            if (planEntrenamientoEncontrado) {
                const planEntrenamientoAEditar = editar_plan_entrenamiento_1.editarPlanEntrenamiento(planEntrenamiento);
                const arregloErrores = yield class_validator_1.validate(planEntrenamientoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el plan de entrenamiento', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._planEntrenamientoService.update(id, planEntrenamientoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Plan de entrenamiento no encontrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoEncontrado = yield this._planEntrenamientoService.findById(id);
            if (planEntrenamientoEncontrado) {
                yield this._planEntrenamientoService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El plan de entrenamiento no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_plam_entrenamiento_dto_1.CrearPlanEntrenamientoDto]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_plam_entrenamiento_dto_1.EditarPlanEntrenamientoDto]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoController.prototype, "delete", null);
PlanEntrenamientoController = __decorate([
    common_1.Controller('plan-entrenamiento'),
    __metadata("design:paramtypes", [plan_entrenamiento_service_1.PlanEntrenamientoService])
], PlanEntrenamientoController);
exports.PlanEntrenamientoController = PlanEntrenamientoController;
//# sourceMappingURL=plan-entrenamiento.controller.js.map
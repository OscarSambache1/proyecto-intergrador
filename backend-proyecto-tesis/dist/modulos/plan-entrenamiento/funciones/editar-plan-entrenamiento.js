"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_plam_entrenamiento_dto_1 = require("../dto/editar-plam-entrenamiento.dto");
function editarPlanEntrenamiento(planEntrenamiento) {
    const planEntrenamientoAEditar = new editar_plam_entrenamiento_dto_1.EditarPlanEntrenamientoDto();
    Object.keys(planEntrenamiento).map(atributo => {
        planEntrenamientoAEditar[atributo] = planEntrenamiento[atributo];
    });
    return planEntrenamientoAEditar;
}
exports.editarPlanEntrenamiento = editarPlanEntrenamiento;
//# sourceMappingURL=editar-plan-entrenamiento.js.map
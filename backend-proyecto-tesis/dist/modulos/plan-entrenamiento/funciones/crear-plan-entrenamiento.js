"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_plam_entrenamiento_dto_1 = require("../dto/crear-plam-entrenamiento.dto");
function crearPlanEntrenamiento(planEntrenamiento) {
    const planEntrenamientoACrear = new crear_plam_entrenamiento_dto_1.CrearPlanEntrenamientoDto();
    Object.keys(planEntrenamiento).map(atributo => {
        planEntrenamientoACrear[atributo] = planEntrenamiento[atributo];
    });
    return planEntrenamientoACrear;
}
exports.crearPlanEntrenamiento = crearPlanEntrenamiento;
//# sourceMappingURL=crear-plan-entrenamiento.js.map
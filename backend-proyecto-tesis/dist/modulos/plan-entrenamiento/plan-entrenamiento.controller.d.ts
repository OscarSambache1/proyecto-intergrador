import { PlanEntrenamientoService } from './plan-entrenamiento.service';
import { PlanEntrenamientoEntity } from './plan-entrenamiento.entity';
import { CrearPlanEntrenamientoDto } from './dto/crear-plam-entrenamiento.dto';
import { EditarPlanEntrenamientoDto } from './dto/editar-plam-entrenamiento.dto';
export declare class PlanEntrenamientoController {
    private readonly _planEntrenamientoService;
    constructor(_planEntrenamientoService: PlanEntrenamientoService);
    findAll(consulta: any): Promise<PlanEntrenamientoEntity[]>;
    findOne(id: number): Promise<PlanEntrenamientoEntity>;
    create(planEntrenamiento: CrearPlanEntrenamientoDto): Promise<PlanEntrenamientoEntity>;
    update(id: number, planEntrenamiento: EditarPlanEntrenamientoDto): Promise<PlanEntrenamientoEntity>;
    delete(id: number): Promise<void>;
}

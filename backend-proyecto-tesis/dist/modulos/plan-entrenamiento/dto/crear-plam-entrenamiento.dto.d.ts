import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
export declare class CrearPlanEntrenamientoDto {
    nombre: string;
    descripcion: string;
    rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
    planesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructorEntity[];
}

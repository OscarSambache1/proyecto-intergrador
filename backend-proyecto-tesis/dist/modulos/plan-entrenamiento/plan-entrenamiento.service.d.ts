import { Repository } from 'typeorm';
import { PlanEntrenamientoEntity } from './plan-entrenamiento.entity';
import { CrearPlanEntrenamientoDto } from './dto/crear-plam-entrenamiento.dto';
import { EditarPlanEntrenamientoDto } from './dto/editar-plam-entrenamiento.dto';
export declare class PlanEntrenamientoService {
    private readonly _planEntrenamientoRepository;
    constructor(_planEntrenamientoRepository: Repository<PlanEntrenamientoEntity>);
    findAll(consulta: any): Promise<PlanEntrenamientoEntity[]>;
    findLike(campo: string): Promise<PlanEntrenamientoEntity[]>;
    findById(id: number): Promise<PlanEntrenamientoEntity>;
    createOne(planEntrenamiento: CrearPlanEntrenamientoDto): Promise<PlanEntrenamientoEntity>;
    createMany(planesEntrenamiento: CrearPlanEntrenamientoDto[]): Promise<PlanEntrenamientoEntity[]>;
    update(id: number, planEntrenamiento: EditarPlanEntrenamientoDto): Promise<PlanEntrenamientoEntity>;
    delete(id: number): Promise<void>;
}

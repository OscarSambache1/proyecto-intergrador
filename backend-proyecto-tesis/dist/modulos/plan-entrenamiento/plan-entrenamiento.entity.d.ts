import { RutinaPlanEntrenamientoEntity } from '../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
export declare class PlanEntrenamientoEntity {
    id: number;
    nombre: string;
    descripcion: string;
    estado: number;
    rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
    planesEntrenamientoClienteInstructor: PlanEntrenamientoClienteInstructorEntity[];
}

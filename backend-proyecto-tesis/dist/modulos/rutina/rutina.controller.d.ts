import { RutinaService } from './rutina.service';
import { RutinaEntity } from './rutina.entity';
import { CrearRutinaDto } from './dto/crear-rutina.dto';
import { EditarRutinaDto } from './dto/editar-rutina.dto';
export declare class RutinaController {
    private readonly _rutinaService;
    constructor(_rutinaService: RutinaService);
    findAll(consulta: any): Promise<RutinaEntity[]>;
    like(parametros: any): Promise<RutinaEntity[]>;
    findOne(id: number): Promise<RutinaEntity>;
    create(rutina: CrearRutinaDto): Promise<RutinaEntity>;
    update(id: number, rutina: EditarRutinaDto): Promise<RutinaEntity>;
    delete(id: number): Promise<void>;
}

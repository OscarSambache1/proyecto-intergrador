"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_rutina_dto_1 = require("../dto/editar-rutina.dto");
function editarRutina(rutina) {
    const rutinaAEditar = new editar_rutina_dto_1.EditarRutinaDto();
    Object.keys(rutina).map(atributo => {
        rutinaAEditar[atributo] = rutina[atributo];
    });
    return rutinaAEditar;
}
exports.editarRutina = editarRutina;
//# sourceMappingURL=editar-rutina.js.map
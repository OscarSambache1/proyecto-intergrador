"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_rutina_dto_1 = require("../dto/crear-rutina.dto");
function crearRutina(rutina) {
    const rutinaACrear = new crear_rutina_dto_1.CrearRutinaDto();
    Object.keys(rutina).map(atributo => {
        rutinaACrear[atributo] = rutina[atributo];
    });
    return rutinaACrear;
}
exports.crearRutina = crearRutina;
//# sourceMappingURL=crear-rutina.js.map
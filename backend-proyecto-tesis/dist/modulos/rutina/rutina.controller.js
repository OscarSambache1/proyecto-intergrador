"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const rutina_service_1 = require("./rutina.service");
const crear_rutina_dto_1 = require("./dto/crear-rutina.dto");
const crear_rutina_1 = require("./funciones/crear-rutina");
const editar_rutina_dto_1 = require("./dto/editar-rutina.dto");
const editar_rutina_1 = require("./funciones/editar-rutina");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let RutinaController = class RutinaController {
    constructor(_rutinaService) {
        this._rutinaService = _rutinaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaService.findAll(JSON.parse(consulta));
        });
    }
    like(parametros) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaService.findLike(JSON.parse(parametros));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaEncontrada = yield this._rutinaService.findById(id);
            if (rutinaEncontrada) {
                return yield this._rutinaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('La rutina no existe');
            }
        });
    }
    create(rutina) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaACrearse = crear_rutina_1.crearRutina(rutina);
            const arregloErrores = yield class_validator_1.validate(rutinaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando la rutina', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._rutinaService.createOne(rutinaACrearse);
            }
        });
    }
    update(id, rutina) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaEncontrada = yield this._rutinaService.findById(id);
            if (rutinaEncontrada) {
                const rutinaAEditar = editar_rutina_1.editarRutina(rutina);
                const arregloErrores = yield class_validator_1.validate(rutinaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando la rutina', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._rutinaService.update(id, rutinaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Rutina no encotrada');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaEncontrada = yield this._rutinaService.findById(id);
            if (rutinaEncontrada) {
                yield this._rutinaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('La rutina no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "findAll", null);
__decorate([
    common_1.Get('like'),
    __param(0, common_1.Query('parametros')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "like", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_rutina_dto_1.CrearRutinaDto]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_rutina_dto_1.EditarRutinaDto]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RutinaController.prototype, "delete", null);
RutinaController = __decorate([
    common_1.Controller('rutina'),
    __metadata("design:paramtypes", [rutina_service_1.RutinaService])
], RutinaController);
exports.RutinaController = RutinaController;
//# sourceMappingURL=rutina.controller.js.map
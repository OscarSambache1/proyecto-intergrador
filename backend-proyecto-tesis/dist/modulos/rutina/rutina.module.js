"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const rutina_entity_1 = require("./rutina.entity");
const rutina_controller_1 = require("./rutina.controller");
const rutina_service_1 = require("./rutina.service");
let RutinaModule = class RutinaModule {
};
RutinaModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([rutina_entity_1.RutinaEntity], 'default')],
        controllers: [rutina_controller_1.RutinaController],
        providers: [rutina_service_1.RutinaService],
        exports: [],
    })
], RutinaModule);
exports.RutinaModule = RutinaModule;
//# sourceMappingURL=rutina.module.js.map
import { Repository } from 'typeorm';
import { RutinaEntity } from './rutina.entity';
import { CrearRutinaDto } from './dto/crear-rutina.dto';
import { EditarRutinaDto } from './dto/editar-rutina.dto';
export declare class RutinaService {
    private readonly _rutinaRepository;
    constructor(_rutinaRepository: Repository<RutinaEntity>);
    findAll(consulta: any): Promise<RutinaEntity[]>;
    findLike(parametros: any): Promise<RutinaEntity[]>;
    findById(id: number): Promise<RutinaEntity>;
    createOne(rutina: CrearRutinaDto): Promise<RutinaEntity>;
    createMany(rutinas: CrearRutinaDto[]): Promise<RutinaEntity[]>;
    update(id: number, rutina: EditarRutinaDto): Promise<RutinaEntity>;
    delete(id: number): Promise<void>;
}

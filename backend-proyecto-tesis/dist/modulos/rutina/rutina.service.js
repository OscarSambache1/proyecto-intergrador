"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const rutina_entity_1 = require("./rutina.entity");
let RutinaService = class RutinaService {
    constructor(_rutinaRepository) {
        this._rutinaRepository = _rutinaRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._rutinaRepository.find(consulta);
        });
    }
    findLike(parametros) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaRepository.find({
                order: { id: 'DESC' },
                where: {
                    nombre: typeorm_2.Like(`%${parametros.nombre}%`),
                    musculo: parametros.musculo,
                    estado: 1,
                },
                relations: ['ejerciciosRutina'],
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaRepository.findOne(id, {
                relations: [
                    'ejerciciosRutina',
                    'rutinasPlanEntrenamiento',
                    'ejerciciosRutina.rutina',
                    'ejerciciosRutina.ejercicio',
                ],
            });
        });
    }
    createOne(rutina) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaRepository.save(this._rutinaRepository.create(rutina));
        });
    }
    createMany(rutinas) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinasGuardadas = this._rutinaRepository.create(rutinas);
            return this._rutinaRepository.save(rutinasGuardadas);
        });
    }
    update(id, rutina) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._rutinaRepository.update(id, rutina);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaAEliminar = yield this.findById(id);
            yield this._rutinaRepository.remove(rutinaAEliminar);
        });
    }
};
RutinaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(rutina_entity_1.RutinaEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], RutinaService);
exports.RutinaService = RutinaService;
//# sourceMappingURL=rutina.service.js.map
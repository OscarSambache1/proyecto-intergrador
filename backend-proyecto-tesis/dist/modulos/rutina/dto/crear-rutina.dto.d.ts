import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
export declare class CrearRutinaDto {
    nombre: string;
    descripcion: string;
    musculo: string;
    ejerciciosRutina: EjercicioRutinaEntity[];
    rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
}

import { EjercicioRutinaEntity } from '../../ejercicio-rutina/ejercicio-rutina.entity';
import { RutinaPlanEntrenamientoEntity } from '../../rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity';
export declare class EditarRutinaDto {
    nombre: string;
    descripcion: string;
    musculo: string;
    estado?: number;
    ejerciciosRutina: EjercicioRutinaEntity[];
    rutinasPlanEntrenamiento: RutinaPlanEntrenamientoEntity[];
}

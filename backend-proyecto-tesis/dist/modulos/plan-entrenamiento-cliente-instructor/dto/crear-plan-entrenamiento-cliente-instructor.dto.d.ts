import { ClienteEntity } from '../../cliente/cliente.entity';
import { InstructorEntity } from '../../instructor/instructor.entity';
import { PlanEntrenamientoEntity } from '../../plan-entrenamiento/plan-entrenamiento.entity';
export declare class CrearPlanEntrenamientoClienteInstructorDto {
    fechaInicio: string;
    fechaFin: string;
    cliente: ClienteEntity;
    instructor: InstructorEntity;
    planEntrenamiento: PlanEntrenamientoEntity;
}

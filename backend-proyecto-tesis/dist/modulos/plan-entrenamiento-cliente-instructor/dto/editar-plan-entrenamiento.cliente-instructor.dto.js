"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const cliente_entity_1 = require("../../cliente/cliente.entity");
const instructor_entity_1 = require("../../instructor/instructor.entity");
const plan_entrenamiento_entity_1 = require("../../plan-entrenamiento/plan-entrenamiento.entity");
const class_validator_1 = require("class-validator");
class EditarPlanEntrenamientoClienteInstructorDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/),
    __metadata("design:type", String)
], EditarPlanEntrenamientoClienteInstructorDto.prototype, "fechaInicio", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsOptional(),
    class_validator_1.IsString(),
    class_validator_1.Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/),
    __metadata("design:type", String)
], EditarPlanEntrenamientoClienteInstructorDto.prototype, "fechaFin", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", cliente_entity_1.ClienteEntity)
], EditarPlanEntrenamientoClienteInstructorDto.prototype, "cliente", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", instructor_entity_1.InstructorEntity)
], EditarPlanEntrenamientoClienteInstructorDto.prototype, "instructor", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", plan_entrenamiento_entity_1.PlanEntrenamientoEntity)
], EditarPlanEntrenamientoClienteInstructorDto.prototype, "planEntrenamiento", void 0);
exports.EditarPlanEntrenamientoClienteInstructorDto = EditarPlanEntrenamientoClienteInstructorDto;
//# sourceMappingURL=editar-plan-entrenamiento.cliente-instructor.dto.js.map
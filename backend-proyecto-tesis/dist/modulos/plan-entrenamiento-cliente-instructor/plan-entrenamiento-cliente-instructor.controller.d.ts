import { PlanEntrenamientoClienteInstructorService } from './plan-entrenamiento-cliente-instructor.service';
import { PlanEntrenamientoClienteInstructorEntity } from './plan-entrenamiento-cliente-instructor.entity';
import { CrearPlanEntrenamientoClienteInstructorDto } from './dto/crear-plan-entrenamiento-cliente-instructor.dto';
import { EditarPlanEntrenamientoClienteInstructorDto } from './dto/editar-plan-entrenamiento.cliente-instructor.dto';
export declare class PlanEntrenamientoClienteInstructorController {
    private readonly _planEntrenamientoClienteInstructorService;
    constructor(_planEntrenamientoClienteInstructorService: PlanEntrenamientoClienteInstructorService);
    findAll(consulta: any): Promise<PlanEntrenamientoClienteInstructorEntity[]>;
    findOne(id: number): Promise<PlanEntrenamientoClienteInstructorEntity>;
    create(planClienteInstructorEntrenamiento: CrearPlanEntrenamientoClienteInstructorDto): Promise<PlanEntrenamientoClienteInstructorEntity>;
    createMany(planesEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto[]): Promise<PlanEntrenamientoClienteInstructorEntity[]>;
    update(id: number, planClienteInstructorEntrenamiento: EditarPlanEntrenamientoClienteInstructorDto): Promise<PlanEntrenamientoClienteInstructorEntity>;
    delete(id: number): Promise<void>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const plan_entrenamiento_cliente_instructor_service_1 = require("./plan-entrenamiento-cliente-instructor.service");
const crear_plan_entrenamiento_cliente_instructor_dto_1 = require("./dto/crear-plan-entrenamiento-cliente-instructor.dto");
const crear_plan_entrenamiento_1 = require("./funciones/crear-plan-entrenamiento");
const editar_plan_entrenamiento_cliente_instructor_dto_1 = require("./dto/editar-plan-entrenamiento.cliente-instructor.dto");
const editar_plan_entrenamiento_1 = require("./funciones/editar-plan-entrenamiento");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let PlanEntrenamientoClienteInstructorController = class PlanEntrenamientoClienteInstructorController {
    constructor(_planEntrenamientoClienteInstructorService) {
        this._planEntrenamientoClienteInstructorService = _planEntrenamientoClienteInstructorService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._planEntrenamientoClienteInstructorService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoClienteInstructorEncontrado = yield this._planEntrenamientoClienteInstructorService.findById(id);
            if (planEntrenamientoClienteInstructorEncontrado) {
                return yield this._planEntrenamientoClienteInstructorService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(planClienteInstructorEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoClienteInstructorACrearse = crear_plan_entrenamiento_1.crearPlanEntrenamientoClienteInstructor(planClienteInstructorEntrenamiento);
            const arregloErrores = yield class_validator_1.validate(planEntrenamientoClienteInstructorACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._planEntrenamientoClienteInstructorService.createOne(planEntrenamientoClienteInstructorACrearse);
            }
        });
    }
    createMany(planesEntrenamientoClienteInstructor) {
        planesEntrenamientoClienteInstructor.forEach((planEntrenamientoClienteInstructor) => __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoClienteInstructorACrearse = crear_plan_entrenamiento_1.crearPlanEntrenamientoClienteInstructor(yield planEntrenamientoClienteInstructor);
            const arregloErrores = yield class_validator_1.validate(planEntrenamientoClienteInstructorACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._planEntrenamientoClienteInstructorService.createMany(planesEntrenamientoClienteInstructor);
    }
    update(id, planClienteInstructorEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoClienteInstructorEncontrado = yield this._planEntrenamientoClienteInstructorService.findById(id);
            if (planEntrenamientoClienteInstructorEncontrado) {
                const planEntrenamientoAEditar = editar_plan_entrenamiento_1.editarPlanEntrenamientoClienteInstructor(planClienteInstructorEntrenamiento);
                const arregloErrores = yield class_validator_1.validate(planEntrenamientoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._planEntrenamientoClienteInstructorService.update(id, planEntrenamientoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encontrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoClienteInstructorEncontrado = yield this._planEntrenamientoClienteInstructorService.findById(id);
            if (planEntrenamientoClienteInstructorEncontrado) {
                yield this._planEntrenamientoClienteInstructorService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_plan_entrenamiento_cliente_instructor_dto_1.CrearPlanEntrenamientoClienteInstructorDto]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_plan_entrenamiento_cliente_instructor_dto_1.EditarPlanEntrenamientoClienteInstructorDto]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], PlanEntrenamientoClienteInstructorController.prototype, "delete", null);
PlanEntrenamientoClienteInstructorController = __decorate([
    common_1.Controller('plan-entrenamiento-cliente-instructor'),
    __metadata("design:paramtypes", [plan_entrenamiento_cliente_instructor_service_1.PlanEntrenamientoClienteInstructorService])
], PlanEntrenamientoClienteInstructorController);
exports.PlanEntrenamientoClienteInstructorController = PlanEntrenamientoClienteInstructorController;
//# sourceMappingURL=plan-entrenamiento-cliente-instructor.controller.js.map
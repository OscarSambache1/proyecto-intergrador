"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_plan_entrenamiento_cliente_instructor_dto_1 = require("../dto/crear-plan-entrenamiento-cliente-instructor.dto");
function crearPlanEntrenamientoClienteInstructor(planEntrenamientoClienteInstructor) {
    const planEntrenamientoClienteInstructorACrear = new crear_plan_entrenamiento_cliente_instructor_dto_1.CrearPlanEntrenamientoClienteInstructorDto();
    Object.keys(planEntrenamientoClienteInstructor).map(atributo => {
        planEntrenamientoClienteInstructorACrear[atributo] =
            planEntrenamientoClienteInstructor[atributo];
    });
    return planEntrenamientoClienteInstructorACrear;
}
exports.crearPlanEntrenamientoClienteInstructor = crearPlanEntrenamientoClienteInstructor;
//# sourceMappingURL=crear-plan-entrenamiento.js.map
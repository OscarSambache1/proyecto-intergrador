"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_plan_entrenamiento_cliente_instructor_dto_1 = require("../dto/editar-plan-entrenamiento.cliente-instructor.dto");
function editarPlanEntrenamientoClienteInstructor(planEntrenamientoClienteInstructor) {
    const planEntrenamientoClienteInstructorAEditar = new editar_plan_entrenamiento_cliente_instructor_dto_1.EditarPlanEntrenamientoClienteInstructorDto();
    Object.keys(planEntrenamientoClienteInstructor).map(atributo => {
        planEntrenamientoClienteInstructorAEditar[atributo] =
            planEntrenamientoClienteInstructor[atributo];
    });
    return planEntrenamientoClienteInstructorAEditar;
}
exports.editarPlanEntrenamientoClienteInstructor = editarPlanEntrenamientoClienteInstructor;
//# sourceMappingURL=editar-plan-entrenamiento.js.map
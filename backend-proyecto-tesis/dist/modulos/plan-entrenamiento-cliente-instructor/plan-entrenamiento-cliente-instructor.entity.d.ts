import { ClienteEntity } from '../cliente/cliente.entity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { PlanEntrenamientoEntity } from '../plan-entrenamiento/plan-entrenamiento.entity';
export declare class PlanEntrenamientoClienteInstructorEntity {
    id: number;
    cliente: ClienteEntity;
    fechaInicio: string;
    fechaFin: string;
    instructor: InstructorEntity;
    planEntrenamiento: PlanEntrenamientoEntity;
}

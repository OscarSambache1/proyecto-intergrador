import { Repository } from 'typeorm';
import { PlanEntrenamientoClienteInstructorEntity } from './plan-entrenamiento-cliente-instructor.entity';
import { CrearPlanEntrenamientoClienteInstructorDto } from './dto/crear-plan-entrenamiento-cliente-instructor.dto';
import { EditarPlanEntrenamientoClienteInstructorDto } from './dto/editar-plan-entrenamiento.cliente-instructor.dto';
export declare class PlanEntrenamientoClienteInstructorService {
    private readonly _planEntrenamientoClienteInstructorRepository;
    constructor(_planEntrenamientoClienteInstructorRepository: Repository<PlanEntrenamientoClienteInstructorEntity>);
    findAll(consulta: any): Promise<PlanEntrenamientoClienteInstructorEntity[]>;
    findLike(campo: string): Promise<PlanEntrenamientoClienteInstructorEntity[]>;
    findById(id: number): Promise<PlanEntrenamientoClienteInstructorEntity>;
    createOne(planEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto): Promise<PlanEntrenamientoClienteInstructorEntity>;
    createMany(planesEntrenamientoClienteInstructor: CrearPlanEntrenamientoClienteInstructorDto[]): Promise<PlanEntrenamientoClienteInstructorEntity[]>;
    update(id: number, planesEntrenamientoClienteInstructor: EditarPlanEntrenamientoClienteInstructorDto): Promise<PlanEntrenamientoClienteInstructorEntity>;
    delete(id: number): Promise<void>;
}

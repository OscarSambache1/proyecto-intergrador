"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const plan_entrenamiento_cliente_instructor_entity_1 = require("./plan-entrenamiento-cliente-instructor.entity");
const plan_entrenamiento_cliente_instructor_service_1 = require("./plan-entrenamiento-cliente-instructor.service");
const plan_entrenamiento_cliente_instructor_controller_1 = require("./plan-entrenamiento-cliente-instructor.controller");
let PlanEntrenamientoClienteInstructorModule = class PlanEntrenamientoClienteInstructorModule {
};
PlanEntrenamientoClienteInstructorModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([plan_entrenamiento_cliente_instructor_entity_1.PlanEntrenamientoClienteInstructorEntity], 'default'),
        ],
        controllers: [plan_entrenamiento_cliente_instructor_controller_1.PlanEntrenamientoClienteInstructorController],
        providers: [plan_entrenamiento_cliente_instructor_service_1.PlanEntrenamientoClienteInstructorService],
        exports: [],
    })
], PlanEntrenamientoClienteInstructorModule);
exports.PlanEntrenamientoClienteInstructorModule = PlanEntrenamientoClienteInstructorModule;
//# sourceMappingURL=plan-entrenamiento-cliente-instructor.module.js.map
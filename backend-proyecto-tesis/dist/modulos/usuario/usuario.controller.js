"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const usuario_service_1 = require("./usuario.service");
const crear_usuario_dto_1 = require("./dto/crear-usuario.dto");
const editar_usuario_dto_1 = require("./dto/editar-usuario.dto");
const editar_usuario_1 = require("./funciones/editar-usuario");
const crear_usuariio_1 = require("./funciones/crear-usuariio");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
const generarNuevoPassword_1 = require("./funciones/generarNuevoPassword");
const crypto = require("crypto");
const moment = require("moment");
const fs = require("fs");
let UsuarioController = class UsuarioController {
    constructor(_usuarioService) {
        this._usuarioService = _usuarioService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioService.findAll(JSON.parse(consulta));
        });
    }
    findWhereOr(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._usuarioService.findWhereOr(consulta.expresion);
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this._usuarioService.findById(id);
            if (usuarioEncontrado) {
                return usuarioEncontrado;
            }
            else {
                throw new common_1.BadRequestException('usuario no existe');
            }
        });
    }
    create(usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioACrearse = crear_usuariio_1.crearUsuario(usuario);
            const arregloErrores = yield class_validator_1.validate(usuarioACrearse);
            const existenErrores = arregloErrores.length > 0;
            console.log(arregloErrores);
            if (existenErrores) {
                console.error('errores: creando al usuario', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._usuarioService.createOne(usuarioACrearse);
            }
        });
    }
    update(id, usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this._usuarioService.findById(id);
            if (usuarioEncontrado) {
                const usuarioAEditarse = editar_usuario_1.editarUsuario(usuario);
                const arregloErrores = yield class_validator_1.validate(usuarioAEditarse);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando al usuario', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._usuarioService.update(id, usuarioAEditarse);
                }
            }
            else {
                throw new common_1.BadRequestException('Usuario no encotrado');
            }
        });
    }
    eliminar(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this._usuarioService.findById(id);
            if (usuarioEncontrado) {
                return yield this._usuarioService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('Usuario no existe');
            }
        });
    }
    uploadFile(id, file) {
        return __awaiter(this, void 0, void 0, function* () {
            const fecha = moment(new Date()).format();
            const urlFoto = __dirname +
                `/../../../public/imagenes-usuarios/imagen_usuario_${fecha}.png`;
            fs.rename(`${file.path}`, urlFoto, err => {
                if (err)
                    console.log('ERROR: ' + err);
            });
            const usuario = new editar_usuario_dto_1.EditarUsuarioDto();
            const usuarioAEditar = yield this.findOne(id);
            if (!(usuarioAEditar.urlFoto === 'usuario.png')) {
                try {
                    this.eliminarArchivo(usuarioAEditar.urlFoto);
                }
                catch (error) {
                    usuario.urlFoto = `imagen_usuario_${fecha}.png`;
                }
            }
            usuario.urlFoto = `imagen_usuario_${fecha}.png`;
            return yield this._usuarioService.update(id, usuario);
        });
    }
    resetearPassword(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this._usuarioService.findById(id);
            if (usuarioEncontrado) {
                const nuevoPassword = generarNuevoPassword_1.generarPassword();
                const passwordNuevoHash = crypto
                    .createHmac('sha256', nuevoPassword)
                    .digest('hex');
                const usuarioAEditarse = {
                    password: passwordNuevoHash,
                };
                this.update(id, usuarioAEditarse);
                usuarioEncontrado.password = nuevoPassword;
                return usuarioEncontrado;
            }
            else {
                throw new common_1.BadRequestException('Usuario no existe');
            }
        });
    }
    cambiarPassword(id, passwordNuevo, passwordActual) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioService.cambiarPassword(id, passwordNuevo, passwordActual);
        });
    }
    cambiarFotoMovil(id, imagenBase64) {
        return __awaiter(this, void 0, void 0, function* () {
            const fecha = moment(new Date()).format();
            const urlFoto = __dirname +
                `/../../../public/imagenes-usuarios/imagen_usuario_${fecha}.png`;
            yield fs.writeFile(urlFoto, new Buffer(imagenBase64, 'base64'), error => { });
            const usuario = new editar_usuario_dto_1.EditarUsuarioDto();
            const usuarioAEditar = yield this.findOne(id);
            if (!(usuarioAEditar.urlFoto === 'usuario.png')) {
                try {
                    this.eliminarArchivo(usuarioAEditar.urlFoto);
                }
                catch (error) {
                    usuario.urlFoto = `imagen_usuario_${fecha}.png`;
                }
            }
            usuario.urlFoto = `imagen_usuario_${fecha}.png`;
            return yield this._usuarioService.update(id, usuario);
        });
    }
    eliminarArchivo(nombreArchivo) {
        const filePath = __dirname + `/../../../public/imagenes-usuarios/${nombreArchivo}`;
        fs.unlinkSync(filePath);
    }
};
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Get('findAll'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "findAll", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Get('findWhereOr'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "findWhereOr", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "findOne", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_usuario_dto_1.CrearUsuarioDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "create", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'empleado', 'cliente', 'instructor'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_usuario_dto_1.EditarUsuarioDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "update", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "eliminar", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Post('subirArchivo/:id'),
    roles_decorator_1.Roles('administrador', 'empleado', 'cliente', 'instructor'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    common_1.UseInterceptors(common_1.FileInterceptor('file2', {
        dest: __dirname + '/../../../public/imagenes-usuarios',
        limits: {
            fieldSize: 10000000,
            fieldNameSize: 10000000,
        },
    })),
    __param(0, common_1.Param('id')), __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "uploadFile", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Post('resetear-password'),
    roles_decorator_1.Roles('administrador, empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "resetearPassword", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Post('cambiar-password'),
    __param(0, common_1.Body('id')),
    __param(1, common_1.Body('passwordNuevo')),
    __param(2, common_1.Body('passwordActual')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "cambiarPassword", null);
__decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Post('cambiar-foto-movil'),
    __param(0, common_1.Body('id')),
    __param(1, common_1.Body('imagenBase64')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "cambiarFotoMovil", null);
UsuarioController = __decorate([
    common_1.Controller('usuario'),
    __metadata("design:paramtypes", [usuario_service_1.UsuarioService])
], UsuarioController);
exports.UsuarioController = UsuarioController;
//# sourceMappingURL=usuario.controller.js.map
import { ClienteEntity } from '../cliente/cliente.entity';
import { InstructorEntity } from '../instructor/instructor.entity';
import { CabeceraFacturaEntity } from '../cabecera-factura/cabecera-factura.entity';
export declare class UsuarioEntity {
    id: number;
    nombres: string;
    apellidos: string;
    cedula: string;
    fechaNacimiento: string;
    telefono: string;
    direccion: string;
    genero: string;
    correoElectronico: string;
    urlFoto: string;
    password: string;
    cliente: ClienteEntity;
    instructor: InstructorEntity;
    cabecerasFactura: CabeceraFacturaEntity[];
}

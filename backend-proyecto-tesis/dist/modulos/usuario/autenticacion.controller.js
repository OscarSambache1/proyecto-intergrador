"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const usuario_service_1 = require("./usuario.service");
const app_auth_guard_1 = require("../auth/app-auth.guard");
const jsonwebtoken_1 = require("jsonwebtoken");
let AutenticacionController = class AutenticacionController {
    constructor(_usuarioService) {
        this._usuarioService = _usuarioService;
    }
    login(cedula, password, session) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioService.autenticarUsuario({ cedula, password });
        });
    }
    loginMovil(cedula, password) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioLogueado = yield this._usuarioService.autenticarUsuario({
                cedula,
                password,
            });
            if (usuarioLogueado) {
                const token = jsonwebtoken_1.default.sign({ usuarioLogueado }, 'shhhhh');
                return { usuario: usuarioLogueado, token };
            }
            else {
                throw new common_1.BadRequestException('fallo login');
            }
        });
    }
    logout(request, response) {
        request.logout();
        request.session.destroy();
        console.log('logout');
    }
    buscarUsuarioPorCedula(cedula) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioService.findOneByCedula(cedula);
        });
    }
};
__decorate([
    common_1.Post('login'),
    common_1.UseGuards(app_auth_guard_1.AppAuthGuard),
    __param(0, common_1.Body('cedula')),
    __param(1, common_1.Body('password')),
    __param(2, common_1.Session()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AutenticacionController.prototype, "login", null);
__decorate([
    common_1.Post('login-movil'),
    __param(0, common_1.Body('cedula')),
    __param(1, common_1.Body('password')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AutenticacionController.prototype, "loginMovil", null);
__decorate([
    common_1.Get('logout'),
    __param(0, common_1.Req()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], AutenticacionController.prototype, "logout", null);
__decorate([
    common_1.Post('buscarUsuarioPorCedula'),
    __param(0, common_1.Body('cedula')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AutenticacionController.prototype, "buscarUsuarioPorCedula", null);
AutenticacionController = __decorate([
    common_1.Controller('autenticacion'),
    __metadata("design:paramtypes", [usuario_service_1.UsuarioService])
], AutenticacionController);
exports.AutenticacionController = AutenticacionController;
//# sourceMappingURL=autenticacion.controller.js.map
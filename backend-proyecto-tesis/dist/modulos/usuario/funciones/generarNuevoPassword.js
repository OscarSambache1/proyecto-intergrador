"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function generarPassword() {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let passwordNuevo = '';
    for (let i = 6; i > 0; --i) {
        passwordNuevo += chars[Math.floor(Math.random() * chars.length)];
    }
    return passwordNuevo;
}
exports.generarPassword = generarPassword;
//# sourceMappingURL=generarNuevoPassword.js.map
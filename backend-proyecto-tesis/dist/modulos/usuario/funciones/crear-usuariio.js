"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_usuario_dto_1 = require("../dto/crear-usuario.dto");
const crypto = require("crypto");
function crearUsuario(usuario) {
    const usuarioACrear = new crear_usuario_dto_1.CrearUsuarioDto();
    Object.keys(usuario).map(atributo => {
        usuarioACrear[atributo] = usuario[atributo];
    });
    usuarioACrear.password = usuarioACrear.cedula;
    usuarioACrear.urlFoto = 'usuario.png';
    usuarioACrear.password = crypto
        .createHmac('sha256', usuarioACrear.cedula)
        .digest('hex');
    return usuarioACrear;
}
exports.crearUsuario = crearUsuario;
//# sourceMappingURL=crear-usuariio.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_usuario_dto_1 = require("../dto/editar-usuario.dto");
function editarUsuario(usuario) {
    const usuarioAEditar = new editar_usuario_dto_1.EditarUsuarioDto();
    Object.keys(usuario).map(atributo => {
        usuarioAEditar[atributo] = usuario[atributo];
    });
    return usuarioAEditar;
}
exports.editarUsuario = editarUsuario;
//# sourceMappingURL=editar-usuario.js.map
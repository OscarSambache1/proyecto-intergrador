"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usuario_login_dto_1 = require("../dto/usuario-login.dto");
function generarUsuarioLogin(cedula, password) {
    const usuarioLogin = new usuario_login_dto_1.UsuarioLoginDto();
    (usuarioLogin.cedula = cedula), (usuarioLogin.password = password);
    return usuarioLogin;
}
exports.generarUsuarioLogin = generarUsuarioLogin;
//# sourceMappingURL=generarUsuarioLogin.js.map
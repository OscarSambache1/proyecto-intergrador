import { Repository } from 'typeorm';
import { UsuarioEntity } from './usuario.entity';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { EditarUsuarioDto } from './dto/editar-usuario.dto';
import { UsuarioLoginDto } from './dto/usuario-login.dto';
export declare class UsuarioService {
    private readonly _usuarioRepository;
    constructor(_usuarioRepository: Repository<UsuarioEntity>);
    findAll(consulta: any): Promise<UsuarioEntity[]>;
    findWhereOr(expresion: string): Promise<UsuarioEntity[]>;
    findById(id: number): Promise<UsuarioEntity>;
    createOne(usuario: CrearUsuarioDto): Promise<UsuarioEntity>;
    createMany(usuarios: CrearUsuarioDto[]): Promise<UsuarioEntity[]>;
    update(id: number, usuario: EditarUsuarioDto): Promise<UsuarioEntity>;
    delete(id: number): Promise<string>;
    contar(consulta: object): Promise<number>;
    findOneByCedula(cedula: string): Promise<UsuarioEntity>;
    autenticarUsuario(usuarioAAutenticar: UsuarioLoginDto): Promise<UsuarioEntity>;
    cambiarPassword(idUsuario: number, passwordNuevo: string, passwordActual: string): Promise<UsuarioEntity>;
}

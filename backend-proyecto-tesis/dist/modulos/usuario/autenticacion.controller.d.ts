import { UsuarioService } from './usuario.service';
import { UsuarioEntity } from './usuario.entity';
export declare class AutenticacionController {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioService);
    login(cedula: any, password: any, session: any): Promise<UsuarioEntity>;
    loginMovil(cedula: any, password: any): Promise<any>;
    logout(request: any, response: any): void;
    buscarUsuarioPorCedula(cedula: any): Promise<UsuarioEntity>;
}

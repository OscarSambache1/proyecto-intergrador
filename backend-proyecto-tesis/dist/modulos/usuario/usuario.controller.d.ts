import { UsuarioService } from './usuario.service';
import { UsuarioEntity } from './usuario.entity';
import { CrearUsuarioDto } from './dto/crear-usuario.dto';
import { EditarUsuarioDto } from './dto/editar-usuario.dto';
export declare class UsuarioController {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioService);
    findAll(consulta: any): Promise<UsuarioEntity[]>;
    findWhereOr(consulta: any): Promise<UsuarioEntity[]>;
    findOne(id: any): Promise<UsuarioEntity>;
    create(usuario: CrearUsuarioDto): Promise<UsuarioEntity>;
    update(id: number, usuario: EditarUsuarioDto): Promise<UsuarioEntity>;
    eliminar(id: number): Promise<any>;
    uploadFile(id: number, file: any): Promise<UsuarioEntity>;
    resetearPassword(id: number): Promise<UsuarioEntity>;
    cambiarPassword(id: number, passwordNuevo: string, passwordActual: string): Promise<UsuarioEntity>;
    cambiarFotoMovil(id: number, imagenBase64: string): Promise<UsuarioEntity>;
    eliminarArchivo(nombreArchivo: any): void;
}

import { ClienteEntity } from '../../cliente/cliente.entity';
import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
import { InstructorEntity } from '../../instructor/instructor.entity';
export declare class CrearUsuarioDto {
    nombres: string;
    apellidos: string;
    cedula: string;
    genero: string;
    fechaNacimiento: string;
    telefono: string;
    direccion: string;
    password: string;
    correoElectronico: string;
    urlFoto: string;
    cliente: ClienteEntity;
    instructor: InstructorEntity;
    cabecerasFactura: CabeceraFacturaEntity[];
}

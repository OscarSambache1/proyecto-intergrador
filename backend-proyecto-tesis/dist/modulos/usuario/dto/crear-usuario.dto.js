"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const cliente_entity_1 = require("../../cliente/cliente.entity");
const instructor_entity_1 = require("../../instructor/instructor.entity");
class CrearUsuarioDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Matches(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "nombres", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.Matches(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "apellidos", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "cedula", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "genero", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.Matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "fechaNacimiento", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumberString(),
    class_validator_1.Length(10, 10),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "telefono", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "direccion", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "password", void 0);
__decorate([
    class_validator_1.Matches(/^$|^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "correoElectronico", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CrearUsuarioDto.prototype, "urlFoto", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", cliente_entity_1.ClienteEntity)
], CrearUsuarioDto.prototype, "cliente", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", instructor_entity_1.InstructorEntity)
], CrearUsuarioDto.prototype, "instructor", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Array)
], CrearUsuarioDto.prototype, "cabecerasFactura", void 0);
exports.CrearUsuarioDto = CrearUsuarioDto;
//# sourceMappingURL=crear-usuario.dto.js.map
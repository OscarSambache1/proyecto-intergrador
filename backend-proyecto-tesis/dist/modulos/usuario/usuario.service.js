"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const usuario_entity_1 = require("./usuario.entity");
const crypto = require("crypto");
let UsuarioService = class UsuarioService {
    constructor(_usuarioRepository) {
        this._usuarioRepository = _usuarioRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioRepository.find(consulta);
        });
    }
    findWhereOr(expresion) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioRepository
                .createQueryBuilder('usuario')
                .where('usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos', { nombres: `%${expresion}%`, apellidos: `%${expresion}%` })
                .getMany();
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioRepository.findOne(id, {
                relations: ['cliente', 'instructor'],
            });
        });
    }
    createOne(usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioRepository.save(this._usuarioRepository.create(usuario));
        });
    }
    createMany(usuarios) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuariosACrear = this._usuarioRepository.create(usuarios);
            return yield this._usuarioRepository.save(usuariosACrear);
        });
    }
    update(id, usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._usuarioRepository.update(id, usuario);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioAEliminar = yield this.findById(id);
            yield this._usuarioRepository.remove(usuarioAEliminar);
            return `Se ha eliminado el elemento ${id}`;
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._usuarioRepository.count({
                where: consulta,
            });
        });
    }
    findOneByCedula(cedula) {
        return __awaiter(this, void 0, void 0, function* () {
            const opciones = {
                where: {
                    cedula,
                },
                relations: ['cliente', 'instructor'],
            };
            const usuarioEncontrado = this._usuarioRepository.findOne(undefined, opciones);
            return usuarioEncontrado;
        });
    }
    autenticarUsuario(usuarioAAutenticar) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this.findOneByCedula(usuarioAAutenticar.cedula);
            if (usuarioEncontrado) {
                const passHash = crypto
                    .createHmac('sha256', usuarioAAutenticar.password)
                    .digest('hex');
                if (passHash === usuarioEncontrado.password) {
                    return usuarioEncontrado;
                }
                else {
                    return null;
                }
            }
        });
    }
    cambiarPassword(idUsuario, passwordNuevo, passwordActual) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarioEncontrado = yield this.findById(idUsuario);
            if (usuarioEncontrado) {
                const passwordActualHash = crypto
                    .createHmac('sha256', passwordActual)
                    .digest('hex');
                const passwordNuevoHash = crypto
                    .createHmac('sha256', passwordNuevo)
                    .digest('hex');
                if (passwordActualHash === usuarioEncontrado.password) {
                    this.update(idUsuario, { password: passwordNuevoHash });
                    delete usuarioEncontrado.password;
                    return usuarioEncontrado;
                }
                else {
                    return null;
                }
            }
        });
    }
};
UsuarioService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(usuario_entity_1.UsuarioEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UsuarioService);
exports.UsuarioService = UsuarioService;
//# sourceMappingURL=usuario.service.js.map
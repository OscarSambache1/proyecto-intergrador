"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
exports.SessionUser = common_1.createParamDecorator((data, req) => {
    return req.session.passport;
});
//# sourceMappingURL=usuario.decorator.js.map
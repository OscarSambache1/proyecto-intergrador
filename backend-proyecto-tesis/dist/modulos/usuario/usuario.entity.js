"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const cliente_entity_1 = require("../cliente/cliente.entity");
const instructor_entity_1 = require("../instructor/instructor.entity");
const cabecera_factura_entity_1 = require("../cabecera-factura/cabecera-factura.entity");
let UsuarioEntity = class UsuarioEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], UsuarioEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombres', type: 'varchar' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "nombres", void 0);
__decorate([
    typeorm_1.Column({ name: 'apellidos', type: 'varchar' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "apellidos", void 0);
__decorate([
    typeorm_1.Column({ name: 'cedula', type: 'varchar', unique: true }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "cedula", void 0);
__decorate([
    typeorm_1.Column({ name: 'fecha-nacimiento', type: 'date' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "fechaNacimiento", void 0);
__decorate([
    typeorm_1.Column({ name: 'telefono', type: 'varchar' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "telefono", void 0);
__decorate([
    typeorm_1.Column({ name: 'direccion', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "direccion", void 0);
__decorate([
    typeorm_1.Column({ name: 'genero', type: 'enum', enum: ['masculino', 'femenino'] }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "genero", void 0);
__decorate([
    typeorm_1.Column({ name: 'correo-electronico', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "correoElectronico", void 0);
__decorate([
    typeorm_1.Column({ name: 'urlFoto', type: 'varchar' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "urlFoto", void 0);
__decorate([
    typeorm_1.Column({ name: 'password', type: 'varchar' }),
    __metadata("design:type", String)
], UsuarioEntity.prototype, "password", void 0);
__decorate([
    typeorm_1.OneToOne(type => cliente_entity_1.ClienteEntity, cliente => cliente.usuario),
    __metadata("design:type", cliente_entity_1.ClienteEntity)
], UsuarioEntity.prototype, "cliente", void 0);
__decorate([
    typeorm_1.OneToOne(type => instructor_entity_1.InstructorEntity, instructor => instructor.usuario),
    __metadata("design:type", instructor_entity_1.InstructorEntity)
], UsuarioEntity.prototype, "instructor", void 0);
__decorate([
    typeorm_1.OneToMany(type => cabecera_factura_entity_1.CabeceraFacturaEntity, cabeceraFactura => cabeceraFactura.usuario),
    __metadata("design:type", Array)
], UsuarioEntity.prototype, "cabecerasFactura", void 0);
UsuarioEntity = __decorate([
    typeorm_1.Entity('usuario')
], UsuarioEntity);
exports.UsuarioEntity = UsuarioEntity;
//# sourceMappingURL=usuario.entity.js.map
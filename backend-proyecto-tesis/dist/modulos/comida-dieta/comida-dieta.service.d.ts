import { Repository } from 'typeorm';
import { ComidaDietaEntity } from './comida-dieta.entity';
import { CrearComidaDietaDto } from './dto/crear-comida-dieta.dto';
import { EditarComidaDietaDto } from './dto/editar-comida-dieta.dto';
export declare class ComidaDietaService {
    private readonly _comidaDietaRepository;
    constructor(_comidaDietaRepository: Repository<ComidaDietaEntity>);
    findAll(consulta: any): Promise<ComidaDietaEntity[]>;
    findById(id: number): Promise<ComidaDietaEntity>;
    createOne(comidaDieta: CrearComidaDietaDto): Promise<ComidaDietaEntity>;
    createMany(comidaDieta: CrearComidaDietaDto[]): Promise<ComidaDietaEntity[]>;
    update(id: number, comidaDieta: EditarComidaDietaDto): Promise<ComidaDietaEntity>;
    delete(id: number): Promise<void>;
}

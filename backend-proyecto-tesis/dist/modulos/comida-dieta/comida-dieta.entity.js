"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const comida_entity_1 = require("../comida/comida.entity");
const dieta_entity_1 = require("../dieta/dieta.entity");
let ComidaDietaEntity = class ComidaDietaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ComidaDietaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'hora', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ComidaDietaEntity.prototype, "hora", void 0);
__decorate([
    typeorm_1.ManyToOne(type => comida_entity_1.ComidaEntity, comida => comida.comidasDieta),
    __metadata("design:type", comida_entity_1.ComidaEntity)
], ComidaDietaEntity.prototype, "comida", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dieta_entity_1.DietaEntity, dieta => dieta.comidasDieta),
    __metadata("design:type", dieta_entity_1.DietaEntity)
], ComidaDietaEntity.prototype, "dieta", void 0);
ComidaDietaEntity = __decorate([
    typeorm_1.Entity('comida-dieta')
], ComidaDietaEntity);
exports.ComidaDietaEntity = ComidaDietaEntity;
//# sourceMappingURL=comida-dieta.entity.js.map
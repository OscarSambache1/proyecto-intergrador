import { ComidaEntity } from '../../comida/comida.entity';
import { DietaEntity } from '../../dieta/dieta.entity';
export declare class CrearComidaDietaDto {
    hora: string;
    comida: ComidaEntity;
    dieta: DietaEntity;
}

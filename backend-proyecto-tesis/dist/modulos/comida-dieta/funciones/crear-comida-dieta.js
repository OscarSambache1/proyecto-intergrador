"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_comida_dieta_dto_1 = require("../dto/crear-comida-dieta.dto");
function crearComidaDieta(comidaDieta) {
    const comidaDietaACrear = new crear_comida_dieta_dto_1.CrearComidaDietaDto();
    Object.keys(comidaDieta).map(atributo => {
        comidaDietaACrear[atributo] = comidaDieta[atributo];
    });
    return comidaDietaACrear;
}
exports.crearComidaDieta = crearComidaDieta;
//# sourceMappingURL=crear-comida-dieta.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_comida_dieta_dto_1 = require("../dto/editar-comida-dieta.dto");
function editarComidaDieta(comidaDieta) {
    const comidaDietaAEditar = new editar_comida_dieta_dto_1.EditarComidaDietaDto();
    Object.keys(comidaDieta).map(atributo => {
        comidaDietaAEditar[atributo] = comidaDieta[atributo];
    });
    return comidaDietaAEditar;
}
exports.editarComidaDieta = editarComidaDieta;
//# sourceMappingURL=editar-comida-dieta.js.map
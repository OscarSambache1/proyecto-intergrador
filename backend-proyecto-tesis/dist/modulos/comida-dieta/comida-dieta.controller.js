"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const comida_dieta_service_1 = require("./comida-dieta.service");
const crear_comida_dieta_dto_1 = require("./dto/crear-comida-dieta.dto");
const crear_comida_dieta_1 = require("./funciones/crear-comida-dieta");
const editar_comida_dieta_dto_1 = require("./dto/editar-comida-dieta.dto");
const editar_comida_dieta_1 = require("./funciones/editar-comida-dieta");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ComidaDietaController = class ComidaDietaController {
    constructor(_comidaDietaService) {
        this._comidaDietaService = _comidaDietaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._comidaDietaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaDietaEncontrada = yield this._comidaDietaService.findById(id);
            if (comidaDietaEncontrada) {
                return yield this._comidaDietaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(comidaDieta) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaDietaACrearse = crear_comida_dieta_1.crearComidaDieta(comidaDieta);
            const arregloErrores = yield class_validator_1.validate(comidaDietaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._comidaDietaService.createOne(comidaDietaACrearse);
            }
        });
    }
    createMany(comidasDietas) {
        comidasDietas.forEach((comidaDieta) => __awaiter(this, void 0, void 0, function* () {
            const comidaDietaCrearse = crear_comida_dieta_1.crearComidaDieta(yield comidaDieta);
            const arregloErrores = yield class_validator_1.validate(comidaDietaCrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._comidaDietaService.createMany(comidasDietas);
    }
    update(id, comidaDieta) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaDietaEncontrada = yield this._comidaDietaService.findById(id);
            if (comidaDietaEncontrada) {
                const comidaDietaAEditar = editar_comida_dieta_1.editarComidaDieta(comidaDieta);
                const arregloErrores = yield class_validator_1.validate(comidaDietaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._comidaDietaService.update(id, comidaDietaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaDietaEncontrado = yield this._comidaDietaService.findById(id);
            if (comidaDietaEncontrado) {
                yield this._comidaDietaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_comida_dieta_dto_1.CrearComidaDietaDto]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_comida_dieta_dto_1.EditarComidaDietaDto]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ComidaDietaController.prototype, "delete", null);
ComidaDietaController = __decorate([
    common_1.Controller('comida-dieta'),
    __metadata("design:paramtypes", [comida_dieta_service_1.ComidaDietaService])
], ComidaDietaController);
exports.ComidaDietaController = ComidaDietaController;
//# sourceMappingURL=comida-dieta.controller.js.map
import { ComidaDietaService } from './comida-dieta.service';
import { ComidaDietaEntity } from './comida-dieta.entity';
import { CrearComidaDietaDto } from './dto/crear-comida-dieta.dto';
import { EditarComidaDietaDto } from './dto/editar-comida-dieta.dto';
export declare class ComidaDietaController {
    private readonly _comidaDietaService;
    constructor(_comidaDietaService: ComidaDietaService);
    findAll(consulta: any): Promise<ComidaDietaEntity[]>;
    findOne(id: number): Promise<ComidaDietaEntity>;
    create(comidaDieta: CrearComidaDietaDto): Promise<ComidaDietaEntity>;
    createMany(comidasDietas: CrearComidaDietaDto[]): Promise<CrearComidaDietaDto[]>;
    update(id: number, comidaDieta: EditarComidaDietaDto): Promise<ComidaDietaEntity>;
    delete(id: number): Promise<void>;
}

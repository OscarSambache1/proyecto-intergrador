import { ClienteService } from './cliente.service';
import { ClienteEntity } from './cliente.entity';
import { CrearCLienteDto } from './dto/crear-cliente.dto';
import { EditarCLienteDto } from './dto/editar-cliente.dto';
import { UsuarioEntity } from '../usuario/usuario.entity';
export declare class ClienteController {
    private readonly _clienteService;
    constructor(_clienteService: ClienteService);
    findAll(consulta: any): Promise<ClienteEntity[]>;
    findByNombreApellidoCodigo(consulta: any, user: UsuarioEntity): Promise<ClienteEntity[]>;
    findLast(consulta: any): Promise<ClienteEntity[]>;
    findOne(id: number): Promise<ClienteEntity>;
    create(cliente: CrearCLienteDto): Promise<ClienteEntity>;
    update(id: number, cliente: EditarCLienteDto): Promise<ClienteEntity>;
    delete(id: number): Promise<string>;
}

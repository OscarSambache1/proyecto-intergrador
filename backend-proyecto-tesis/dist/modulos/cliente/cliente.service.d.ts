import { Repository } from 'typeorm';
import { ClienteEntity } from './cliente.entity';
import { EditarCLienteDto } from './dto/editar-cliente.dto';
import { CrearCLienteDto } from './dto/crear-cliente.dto';
export declare class ClienteService {
    private readonly _clienteRepository;
    constructor(_clienteRepository: Repository<ClienteEntity>);
    findAll(consulta: any): Promise<ClienteEntity[]>;
    findByNombreApellidoCodigo(expresion: string, verActivos: string): Promise<ClienteEntity[]>;
    findById(id: number): Promise<ClienteEntity>;
    createOne(cliente: CrearCLienteDto): Promise<ClienteEntity>;
    createMany(clientes: CrearCLienteDto[]): Promise<ClienteEntity[]>;
    update(id: number, cliente: EditarCLienteDto): Promise<ClienteEntity>;
    delete(id: number): Promise<string>;
    contar(consulta: object): Promise<number>;
    findLast(): Promise<ClienteEntity[]>;
    setearCodigoCliente(): Promise<string>;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_cliente_dto_1 = require("../dto/crear-cliente.dto");
function crearCliente(cliente) {
    const clienteACrear = new crear_cliente_dto_1.CrearCLienteDto();
    Object.keys(cliente).map(atributo => {
        clienteACrear[atributo] = cliente[atributo];
    });
    clienteACrear.codigo = cliente.codigo.toString();
    clienteACrear.rol = 'cliente';
    clienteACrear.estado = 1;
    clienteACrear.diaPago = Number(cliente.fechaRegistro.substring(8, 10));
    return clienteACrear;
}
exports.crearCliente = crearCliente;
//# sourceMappingURL=crearCliente.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_cliente_dto_1 = require("../dto/editar-cliente.dto");
const moment = require("moment");
function editarCliente(cliente) {
    const clienteAEditar = new editar_cliente_dto_1.EditarCLienteDto();
    Object.keys(cliente).map(atributo => {
        clienteAEditar[atributo] = cliente[atributo];
    });
    if (clienteAEditar.fechaRegistro) {
        clienteAEditar.diaPago = moment(clienteAEditar.fechaRegistro, 'YYYY-MM-DD').date();
    }
    return clienteAEditar;
}
exports.editarCliente = editarCliente;
//# sourceMappingURL=editarCliente.js.map
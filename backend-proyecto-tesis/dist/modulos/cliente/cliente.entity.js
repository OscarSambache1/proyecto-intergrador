"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const medida_entity_1 = require("../medida/medida.entity");
const plan_entrenamiento_cliente_instructor_entity_1 = require("../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity");
const plan_dieta_cliente_entity_1 = require("../plan-dieta-cliente/plan-dieta-cliente.entity");
const usuario_entity_1 = require("../usuario/usuario.entity");
let ClienteEntity = class ClienteEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ClienteEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'codigo', type: 'varchar', length: 6, unique: true }),
    __metadata("design:type", String)
], ClienteEntity.prototype, "codigo", void 0);
__decorate([
    typeorm_1.Column({ name: 'fecha-registro', type: 'date' }),
    __metadata("design:type", String)
], ClienteEntity.prototype, "fechaRegistro", void 0);
__decorate([
    typeorm_1.Column({ name: 'dia-pago', type: 'int' }),
    __metadata("design:type", Number)
], ClienteEntity.prototype, "diaPago", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], ClienteEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.Column({
        name: 'rol',
        type: 'enum',
        enum: ['administrador', 'cliente', 'instructor', 'empleado'],
    }),
    __metadata("design:type", String)
], ClienteEntity.prototype, "rol", void 0);
__decorate([
    typeorm_1.OneToMany(type => medida_entity_1.MedidaEntity, medida => medida.cliente),
    __metadata("design:type", Array)
], ClienteEntity.prototype, "medidas", void 0);
__decorate([
    typeorm_1.OneToMany(type => plan_entrenamiento_cliente_instructor_entity_1.PlanEntrenamientoClienteInstructorEntity, planEntrenamientoClienteInstructor => planEntrenamientoClienteInstructor.cliente),
    __metadata("design:type", Array)
], ClienteEntity.prototype, "planesEntrenamiento", void 0);
__decorate([
    typeorm_1.OneToMany(type => plan_dieta_cliente_entity_1.PlanDietaClienteEntity, planDietaCliente => planDietaCliente.cliente),
    __metadata("design:type", Array)
], ClienteEntity.prototype, "planesDieta", void 0);
__decorate([
    typeorm_1.OneToOne(type => usuario_entity_1.UsuarioEntity, usuario => usuario.cliente),
    typeorm_1.JoinColumn(),
    __metadata("design:type", usuario_entity_1.UsuarioEntity)
], ClienteEntity.prototype, "usuario", void 0);
ClienteEntity = __decorate([
    typeorm_1.Entity('cliente')
], ClienteEntity);
exports.ClienteEntity = ClienteEntity;
//# sourceMappingURL=cliente.entity.js.map
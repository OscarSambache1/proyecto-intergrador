"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const cliente_service_1 = require("./cliente.service");
const crear_cliente_dto_1 = require("./dto/crear-cliente.dto");
const class_validator_1 = require("class-validator");
const editarCliente_1 = require("./funciones/editarCliente");
const editar_cliente_dto_1 = require("./dto/editar-cliente.dto");
const crearCliente_1 = require("./funciones/crearCliente");
const session_guard_1 = require("../auth/session.guard");
const usuario_decorator_1 = require("../usuario/usuario.decorator");
const usuario_entity_1 = require("../usuario/usuario.entity");
const roles_guard_1 = require("../auth/roles.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
let ClienteController = class ClienteController {
    constructor(_clienteService) {
        this._clienteService = _clienteService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteService.findAll(JSON.parse(consulta));
        });
    }
    findByNombreApellidoCodigo(consulta, user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._clienteService.findByNombreApellidoCodigo(consulta.expresion, consulta.verActivos);
        });
    }
    findLast(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const clienteEncontrado = yield this._clienteService.findById(id);
            if (clienteEncontrado) {
                return yield this._clienteService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('cliente no existe');
            }
        });
    }
    create(cliente) {
        return __awaiter(this, void 0, void 0, function* () {
            const clienteACrearse = crearCliente_1.crearCliente(cliente);
            const arregloErrores = yield class_validator_1.validate(clienteACrearse);
            const existenErrores = arregloErrores.length > 0;
            console.log(arregloErrores);
            if (existenErrores) {
                console.error('errores: creando al cliente', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._clienteService.createOne(clienteACrearse);
            }
        });
    }
    update(id, cliente) {
        return __awaiter(this, void 0, void 0, function* () {
            const clienteEncontrado = yield this._clienteService.findById(id);
            if (clienteEncontrado) {
                const clienteAEditarse = editarCliente_1.editarCliente(cliente);
                const arregloErrores = yield class_validator_1.validate(clienteAEditarse);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando al cliente', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._clienteService.update(id, clienteAEditarse);
                }
            }
            else {
                throw new common_1.BadRequestException('Cliente no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const clienteEncontrado = yield this._clienteService.findById(id);
            if (clienteEncontrado) {
                return yield this._clienteService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('Cliente no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "findAll", null);
__decorate([
    common_1.Get('findWhereOr'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query()),
    __param(1, usuario_decorator_1.SessionUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, usuario_entity_1.UsuarioEntity]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "findByNombreApellidoCodigo", null);
__decorate([
    common_1.Get('findLast'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "findLast", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_cliente_dto_1.CrearCLienteDto]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_cliente_dto_1.EditarCLienteDto]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ClienteController.prototype, "delete", null);
ClienteController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('cliente'),
    __metadata("design:paramtypes", [cliente_service_1.ClienteService])
], ClienteController);
exports.ClienteController = ClienteController;
//# sourceMappingURL=cliente.controller.js.map
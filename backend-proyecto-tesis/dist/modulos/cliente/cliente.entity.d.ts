import { MedidaEntity } from '../medida/medida.entity';
import { PlanEntrenamientoClienteInstructorEntity } from '../plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity';
import { PlanDietaClienteEntity } from '../plan-dieta-cliente/plan-dieta-cliente.entity';
import { UsuarioEntity } from '../usuario/usuario.entity';
export declare class ClienteEntity {
    id: number;
    codigo: string;
    fechaRegistro: string;
    diaPago: number;
    estado: number;
    rol: string;
    medidas: MedidaEntity[];
    planesEntrenamiento: PlanEntrenamientoClienteInstructorEntity[];
    planesDieta: PlanDietaClienteEntity[];
    usuario: UsuarioEntity;
}

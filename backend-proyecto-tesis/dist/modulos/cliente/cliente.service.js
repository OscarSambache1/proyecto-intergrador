"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const cliente_entity_1 = require("./cliente.entity");
let ClienteService = class ClienteService {
    constructor(_clienteRepository) {
        this._clienteRepository = _clienteRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteRepository.find(consulta);
        });
    }
    findByNombreApellidoCodigo(expresion, verActivos) {
        return __awaiter(this, void 0, void 0, function* () {
            let condicionEstado;
            if (verActivos === 'true') {
                condicionEstado = 'cliente.estado = 1';
            }
            else {
                condicionEstado = '';
            }
            return yield this._clienteRepository
                .createQueryBuilder('cliente')
                .innerJoinAndSelect('cliente.usuario', 'usuario', 'usuario.nombres LIKE :nombres or usuario.apellidos LIKE :apellidos or codigo LIKE :codigo', {
                nombres: `%${expresion}%`,
                codigo: `%${expresion}%`,
                apellidos: `%${expresion}%`,
            })
                .where(condicionEstado)
                .getMany();
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteRepository.findOne(id, {
                relations: ['usuario', 'medidas', 'planesDieta', 'planesEntrenamiento'],
            });
        });
    }
    createOne(cliente) {
        return __awaiter(this, void 0, void 0, function* () {
            cliente.codigo = yield this.setearCodigoCliente();
            return yield this._clienteRepository.save(this._clienteRepository.create(cliente));
        });
    }
    createMany(clientes) {
        return __awaiter(this, void 0, void 0, function* () {
            const clientesACrear = this._clienteRepository.create(clientes);
            return this._clienteRepository.save(clientesACrear);
        });
    }
    update(id, cliente) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._clienteRepository.update(id, cliente);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const clienteAEliminar = yield this.findById(id);
            yield this._clienteRepository.remove(clienteAEliminar);
            return `Se ha eliminado el elemento ${id}`;
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteRepository.count({
                where: consulta,
            });
        });
    }
    findLast() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._clienteRepository.find({
                order: {
                    id: 'DESC',
                },
            });
        });
    }
    setearCodigoCliente() {
        return __awaiter(this, void 0, void 0, function* () {
            const clientes = yield this.findLast();
            const ultimoCliente = clientes[0];
            if (ultimoCliente) {
                return (+ultimoCliente.codigo + 1).toString();
            }
            else {
                return '1000';
            }
        });
    }
};
ClienteService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(cliente_entity_1.ClienteEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ClienteService);
exports.ClienteService = ClienteService;
//# sourceMappingURL=cliente.service.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const comida_dieta_entity_1 = require("../comida-dieta/comida-dieta.entity");
const dieta_dia_entity_1 = require("../dieta-dia/dieta-dia.entity");
const tipo_dieta_1 = require("../tipo-dieta/tipo-dieta");
let DietaEntity = class DietaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], DietaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], DietaEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], DietaEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], DietaEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => comida_dieta_entity_1.ComidaDietaEntity, comidaDieta => comidaDieta.dieta),
    __metadata("design:type", Array)
], DietaEntity.prototype, "comidasDieta", void 0);
__decorate([
    typeorm_1.OneToMany(type => dieta_dia_entity_1.DietaDiaEntity, dietaDia => dietaDia.dieta),
    __metadata("design:type", Array)
], DietaEntity.prototype, "planesDietaDia", void 0);
__decorate([
    typeorm_1.ManyToOne(tyoe => tipo_dieta_1.TipoDietaEntity, tipoDieta => tipoDieta.dietas),
    __metadata("design:type", tipo_dieta_1.TipoDietaEntity)
], DietaEntity.prototype, "tipo", void 0);
DietaEntity = __decorate([
    typeorm_1.Entity('dieta')
], DietaEntity);
exports.DietaEntity = DietaEntity;
//# sourceMappingURL=dieta.entity.js.map
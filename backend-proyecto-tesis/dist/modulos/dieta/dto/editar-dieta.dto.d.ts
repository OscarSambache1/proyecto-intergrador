import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { TipoDietaEntity } from '../../tipo-dieta/tipo-dieta';
export declare class EditarDietaDto {
    nombre: string;
    descripcion: string;
    estado?: number;
    comidasDieta: ComidaDietaEntity[];
    planesDietaDia: DietaDiaEntity[];
    tipo: TipoDietaEntity;
}

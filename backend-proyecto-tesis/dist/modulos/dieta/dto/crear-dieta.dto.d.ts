import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { DietaDiaEntity } from '../../dieta-dia/dieta-dia.entity';
import { TipoDietaEntity } from '../../tipo-dieta/tipo-dieta';
export declare class CrearDietaDto {
    nombre: string;
    descripcion: string;
    comidasDieta: ComidaDietaEntity[];
    planesDietaDia: DietaDiaEntity[];
    tipo: TipoDietaEntity;
}

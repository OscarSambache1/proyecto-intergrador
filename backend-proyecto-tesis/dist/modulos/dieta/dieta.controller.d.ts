import { DietaService } from './dieta.service';
import { DietaEntity } from './dieta.entity';
import { CrearDietaDto } from './dto/crear-dieta.dto';
import { EditarDietaDto } from './dto/editar-dieta.dto';
export declare class DietaController {
    private readonly _dietaService;
    constructor(_dietaService: DietaService);
    findAll(consulta: any): Promise<DietaEntity[]>;
    findOne(id: number): Promise<DietaEntity>;
    create(dieta: CrearDietaDto): Promise<DietaEntity>;
    update(id: number, dieta: EditarDietaDto): Promise<DietaEntity>;
    delete(id: number): Promise<void>;
}

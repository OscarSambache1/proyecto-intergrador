import { Repository } from 'typeorm';
import { DietaEntity } from './dieta.entity';
import { CrearDietaDto } from './dto/crear-dieta.dto';
import { EditarDietaDto } from './dto/editar-dieta.dto';
export declare class DietaService {
    private readonly _dietaRepository;
    constructor(_dietaRepository: Repository<DietaEntity>);
    findAll(consulta: any): Promise<DietaEntity[]>;
    findLike(campo: string): Promise<DietaEntity[]>;
    findById(id: number): Promise<DietaEntity>;
    createOne(dieta: CrearDietaDto): Promise<DietaEntity>;
    createMany(dietas: CrearDietaDto[]): Promise<DietaEntity[]>;
    update(id: number, dieta: EditarDietaDto): Promise<DietaEntity>;
    delete(id: number): Promise<void>;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_dieta_dto_1 = require("../dto/crear-dieta.dto");
function crearDieta(dieta) {
    const dietaACrear = new crear_dieta_dto_1.CrearDietaDto();
    Object.keys(dieta).map(atributo => {
        dietaACrear[atributo] = dieta[atributo];
    });
    return dietaACrear;
}
exports.crearDieta = crearDieta;
//# sourceMappingURL=crear-dieta.js.map
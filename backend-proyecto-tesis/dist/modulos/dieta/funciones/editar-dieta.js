"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_dieta_dto_1 = require("../dto/editar-dieta.dto");
function editarDieta(dieta) {
    const dietaAEditar = new editar_dieta_dto_1.EditarDietaDto();
    Object.keys(dieta).map(atributo => {
        dietaAEditar[atributo] = dieta[atributo];
    });
    return dietaAEditar;
}
exports.editarDieta = editarDieta;
//# sourceMappingURL=editar-dieta.js.map
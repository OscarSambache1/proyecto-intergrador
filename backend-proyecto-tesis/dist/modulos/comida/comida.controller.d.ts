import { ComidaService } from './comida.service';
import { ComidaEntity } from './comida.entity';
import { CrearComidaDto } from './dto/crear-comida.dto';
import { EditarComidaDto } from './dto/editar-comida.dto';
export declare class ComidaController {
    private readonly _comidaService;
    constructor(_comidaService: ComidaService);
    findAll(consulta: any): Promise<ComidaEntity[]>;
    findOne(id: number): Promise<ComidaEntity>;
    create(comida: CrearComidaDto): Promise<ComidaEntity>;
    update(id: number, comida: EditarComidaDto): Promise<ComidaEntity>;
    delete(id: number): Promise<void>;
}

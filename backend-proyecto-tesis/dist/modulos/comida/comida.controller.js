"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const comida_service_1 = require("./comida.service");
const crear_comida_dto_1 = require("./dto/crear-comida.dto");
const crear_comida_1 = require("./funciones/crear-comida");
const editar_comida_dto_1 = require("./dto/editar-comida.dto");
const editar_comida_1 = require("./funciones/editar-comida");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ComidaController = class ComidaController {
    constructor(_comidaService) {
        this._comidaService = _comidaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._comidaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaEncontrada = yield this._comidaService.findById(id);
            if (comidaEncontrada) {
                return yield this._comidaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('La comida no existe');
            }
        });
    }
    create(comida) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaACrearse = crear_comida_1.crearComida(comida);
            const arregloErrores = yield class_validator_1.validate(comidaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando la comida', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._comidaService.createOne(comidaACrearse);
            }
        });
    }
    update(id, comida) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaEncontrada = yield this._comidaService.findById(id);
            if (comidaEncontrada) {
                const comidaAEditar = editar_comida_1.editarComida(comida);
                const arregloErrores = yield class_validator_1.validate(comidaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando la comida', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._comidaService.update(id, comidaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Comida no encotrada');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const comidaEncontrada = yield this._comidaService.findById(id);
            if (comidaEncontrada) {
                yield this._comidaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('La comida no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ComidaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ComidaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_comida_dto_1.CrearComidaDto]),
    __metadata("design:returntype", Promise)
], ComidaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_comida_dto_1.EditarComidaDto]),
    __metadata("design:returntype", Promise)
], ComidaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ComidaController.prototype, "delete", null);
ComidaController = __decorate([
    common_1.Controller('comida'),
    __metadata("design:paramtypes", [comida_service_1.ComidaService])
], ComidaController);
exports.ComidaController = ComidaController;
//# sourceMappingURL=comida.controller.js.map
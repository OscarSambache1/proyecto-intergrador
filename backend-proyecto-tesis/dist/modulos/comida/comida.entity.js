"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const comida_dieta_entity_1 = require("../comida-dieta/comida-dieta.entity");
const tipo_comida_entity_1 = require("../tipo-comida/tipo-comida.entity");
let ComidaEntity = class ComidaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ComidaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'longtext', nullable: true }),
    __metadata("design:type", String)
], ComidaEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], ComidaEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.OneToMany(type => comida_dieta_entity_1.ComidaDietaEntity, comidaDieta => comidaDieta.comida),
    __metadata("design:type", Array)
], ComidaEntity.prototype, "comidasDieta", void 0);
__decorate([
    typeorm_1.ManyToOne(type => tipo_comida_entity_1.TipoComidaEntity, tipoComida => tipoComida.comidas),
    __metadata("design:type", tipo_comida_entity_1.TipoComidaEntity)
], ComidaEntity.prototype, "tipo", void 0);
ComidaEntity = __decorate([
    typeorm_1.Entity('comida')
], ComidaEntity);
exports.ComidaEntity = ComidaEntity;
//# sourceMappingURL=comida.entity.js.map
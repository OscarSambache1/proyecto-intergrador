import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../../tipo-comida/tipo-comida.entity';
export declare class EditarComidaDto {
    descripcion: string;
    tipo: TipoComidaEntity;
    estado?: number;
    comidasDieta: ComidaDietaEntity[];
}

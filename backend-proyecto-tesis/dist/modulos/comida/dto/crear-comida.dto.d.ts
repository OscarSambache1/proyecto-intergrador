import { ComidaDietaEntity } from '../../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../../tipo-comida/tipo-comida.entity';
export declare class CrearComidaDto {
    descripcion: string;
    tipo: TipoComidaEntity;
    comidasDieta: ComidaDietaEntity[];
}

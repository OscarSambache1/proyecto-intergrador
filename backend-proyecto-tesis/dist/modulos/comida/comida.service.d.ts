import { Repository } from 'typeorm';
import { ComidaEntity } from './comida.entity';
import { CrearComidaDto } from './dto/crear-comida.dto';
import { EditarComidaDto } from './dto/editar-comida.dto';
export declare class ComidaService {
    private readonly _comidaRepository;
    constructor(_comidaRepository: Repository<ComidaEntity>);
    findAll(consulta: any): Promise<ComidaEntity[]>;
    findLike(campo: string): Promise<ComidaEntity[]>;
    findById(id: number): Promise<ComidaEntity>;
    createOne(comida: CrearComidaDto): Promise<ComidaEntity>;
    createMany(comidas: CrearComidaDto[]): Promise<ComidaEntity[]>;
    update(id: number, comida: EditarComidaDto): Promise<ComidaEntity>;
    delete(id: number): Promise<void>;
}

import { ComidaDietaEntity } from '../comida-dieta/comida-dieta.entity';
import { TipoComidaEntity } from '../tipo-comida/tipo-comida.entity';
export declare class ComidaEntity {
    id: number;
    descripcion: string;
    estado: number;
    comidasDieta: ComidaDietaEntity[];
    tipo: TipoComidaEntity;
}

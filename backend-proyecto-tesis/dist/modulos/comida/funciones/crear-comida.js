"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_comida_dto_1 = require("../dto/crear-comida.dto");
function crearComida(comida) {
    const comidaACrear = new crear_comida_dto_1.CrearComidaDto();
    Object.keys(comida).map(atributo => {
        comidaACrear[atributo] = comida[atributo];
    });
    return comidaACrear;
}
exports.crearComida = crearComida;
//# sourceMappingURL=crear-comida.js.map
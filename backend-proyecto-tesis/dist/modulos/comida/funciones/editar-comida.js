"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_comida_dto_1 = require("../dto/editar-comida.dto");
function editarComida(comida) {
    const comidaAEditar = new editar_comida_dto_1.EditarComidaDto();
    Object.keys(comida).map(atributo => {
        comidaAEditar[atributo] = comida[atributo];
    });
    return comidaAEditar;
}
exports.editarComida = editarComida;
//# sourceMappingURL=editar-comida.js.map
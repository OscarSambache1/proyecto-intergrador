import { ProductoService } from './producto.service';
import { ProductoEntity } from './producto.entity';
import { CrearProductoDto } from './dto/crear-producto.dto';
import { EditarProductoDto } from './dto/editar.producto.dto';
export declare class ProductoController {
    private readonly _productoService;
    constructor(_productoService: ProductoService);
    findAll(consulta: any): Promise<ProductoEntity[]>;
    findOne(id: number): Promise<ProductoEntity>;
    create(producto: CrearProductoDto): Promise<ProductoEntity>;
    update(id: number, producto: EditarProductoDto): Promise<ProductoEntity>;
    delete(id: number): Promise<void>;
}

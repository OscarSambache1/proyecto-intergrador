import { ProductoEntity } from './producto.entity';
import { Repository } from 'typeorm';
import { CrearProductoDto } from './dto/crear-producto.dto';
import { EditarProductoDto } from './dto/editar.producto.dto';
export declare class ProductoService {
    private readonly _productoRepository;
    constructor(_productoRepository: Repository<ProductoEntity>);
    findAll(consulta: any): Promise<ProductoEntity[]>;
    findLike(campo: string): Promise<ProductoEntity[]>;
    findById(id: number): Promise<ProductoEntity>;
    createOne(producto: CrearProductoDto): Promise<ProductoEntity>;
    createMany(productos: CrearProductoDto[]): Promise<ProductoEntity[]>;
    update(id: number, producto: EditarProductoDto): Promise<ProductoEntity>;
    delete(id: number): Promise<void>;
    contar(consulta: object): Promise<number>;
}

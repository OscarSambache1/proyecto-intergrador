import { DetalleFacturaEntity } from '../detalle-factura/detalle-factura.entity';
export declare class ProductoEntity {
    id: number;
    nombre: string;
    descripcion: string;
    precio: number;
    marca: string;
    estado: number;
    detallesFactura: DetalleFacturaEntity[];
}

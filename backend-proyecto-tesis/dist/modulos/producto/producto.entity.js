"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const detalle_factura_entity_1 = require("../detalle-factura/detalle-factura.entity");
let ProductoEntity = class ProductoEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ProductoEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'nombre', type: 'varchar' }),
    __metadata("design:type", String)
], ProductoEntity.prototype, "nombre", void 0);
__decorate([
    typeorm_1.Column({ name: 'descripcion', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ProductoEntity.prototype, "descripcion", void 0);
__decorate([
    typeorm_1.Column({ name: 'precio', type: 'double' }),
    __metadata("design:type", Number)
], ProductoEntity.prototype, "precio", void 0);
__decorate([
    typeorm_1.Column({ name: 'marca', type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], ProductoEntity.prototype, "marca", void 0);
__decorate([
    typeorm_1.Column({ name: 'estado', type: 'tinyint', default: 1 }),
    __metadata("design:type", Number)
], ProductoEntity.prototype, "estado", void 0);
__decorate([
    typeorm_1.ManyToOne(type => detalle_factura_entity_1.DetalleFacturaEntity, detalleFactura => detalleFactura.producto),
    __metadata("design:type", Array)
], ProductoEntity.prototype, "detallesFactura", void 0);
ProductoEntity = __decorate([
    typeorm_1.Entity('producto')
], ProductoEntity);
exports.ProductoEntity = ProductoEntity;
//# sourceMappingURL=producto.entity.js.map
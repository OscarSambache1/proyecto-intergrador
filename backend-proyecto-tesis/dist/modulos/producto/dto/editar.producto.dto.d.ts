import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
export declare class EditarProductoDto {
    descripcion: string;
    nombre: string;
    marca: string;
    precio: number;
    estado?: number;
    detallesFactura: DetalleFacturaEntity[];
}

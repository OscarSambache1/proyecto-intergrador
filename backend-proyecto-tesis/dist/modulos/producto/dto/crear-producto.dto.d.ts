import { DetalleFacturaEntity } from '../../detalle-factura/detalle-factura.entity';
export declare class CrearProductoDto {
    descripcion: string;
    nombre: string;
    marca: string;
    precio: number;
    detallesFactura: DetalleFacturaEntity[];
}

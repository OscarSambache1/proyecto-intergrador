"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const producto_service_1 = require("./producto.service");
const crear_producto_dto_1 = require("./dto/crear-producto.dto");
const crear_producto_1 = require("./funciones/crear-producto");
const editar_producto_dto_1 = require("./dto/editar.producto.dto");
const editar_producto_1 = require("./funciones/editar-producto");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let ProductoController = class ProductoController {
    constructor(_productoService) {
        this._productoService = _productoService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._productoService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const productoEncontrado = yield this._productoService.findById(id);
            if (productoEncontrado) {
                return yield this._productoService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('EL Producto no existe');
            }
        });
    }
    create(producto) {
        return __awaiter(this, void 0, void 0, function* () {
            const productoACrearse = crear_producto_1.crearProducto(producto);
            const arregloErrores = yield class_validator_1.validate(productoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el producto', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._productoService.createOne(productoACrearse);
            }
        });
    }
    update(id, producto) {
        return __awaiter(this, void 0, void 0, function* () {
            const productoEncontrado = yield this._productoService.findById(id);
            if (productoEncontrado) {
                const productoAEditar = editar_producto_1.editarProducto(producto);
                const arregloErrores = yield class_validator_1.validate(productoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el producto', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._productoService.update(id, productoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Producto no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const productoEncontrado = yield this._productoService.findById(id);
            if (productoEncontrado) {
                yield this._productoService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL Producto no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductoController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProductoController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_producto_dto_1.CrearProductoDto]),
    __metadata("design:returntype", Promise)
], ProductoController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_producto_dto_1.EditarProductoDto]),
    __metadata("design:returntype", Promise)
], ProductoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ProductoController.prototype, "delete", null);
ProductoController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('producto'),
    __metadata("design:paramtypes", [producto_service_1.ProductoService])
], ProductoController);
exports.ProductoController = ProductoController;
//# sourceMappingURL=producto.controller.js.map
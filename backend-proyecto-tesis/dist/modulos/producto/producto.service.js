"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const producto_entity_1 = require("./producto.entity");
const typeorm_2 = require("typeorm");
let ProductoService = class ProductoService {
    constructor(_productoRepository) {
        this._productoRepository = _productoRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._productoRepository.find(consulta);
        });
    }
    findLike(campo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._productoRepository.find({
                order: { id: 'DESC' },
                where: { nombre: typeorm_2.Like(`%${campo}%`) },
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._productoRepository.findOne(id, {
                relations: [],
            });
        });
    }
    createOne(producto) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._productoRepository.save(this._productoRepository.create(producto));
        });
    }
    createMany(productos) {
        return __awaiter(this, void 0, void 0, function* () {
            const productosACrear = this._productoRepository.create(productos);
            return this._productoRepository.save(productosACrear);
        });
    }
    update(id, producto) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._productoRepository.update(id, producto);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const productoAEliminar = yield this.findById(id);
            yield this._productoRepository.remove(productoAEliminar);
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._productoRepository.count({
                where: consulta,
            });
        });
    }
};
ProductoService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(producto_entity_1.ProductoEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ProductoService);
exports.ProductoService = ProductoService;
//# sourceMappingURL=producto.service.js.map
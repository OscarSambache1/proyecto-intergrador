"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_producto_dto_1 = require("../dto/editar.producto.dto");
function editarProducto(producto) {
    const productoAEditar = new editar_producto_dto_1.EditarProductoDto();
    Object.keys(producto).map(atributo => {
        productoAEditar[atributo] = producto[atributo];
    });
    return productoAEditar;
}
exports.editarProducto = editarProducto;
//# sourceMappingURL=editar-producto.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_producto_dto_1 = require("../dto/crear-producto.dto");
function crearProducto(producto) {
    const productoACrear = new crear_producto_dto_1.CrearProductoDto();
    Object.keys(producto).map(atributo => {
        productoACrear[atributo] = producto[atributo];
    });
    return productoACrear;
}
exports.crearProducto = crearProducto;
//# sourceMappingURL=crear-producto.js.map
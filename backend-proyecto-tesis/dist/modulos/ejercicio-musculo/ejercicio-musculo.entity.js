"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const musculo_entity_1 = require("../musculo/musculo.entity");
const ejercicio_entity_1 = require("../ejercicio/ejercicio.entity");
let EjercicioMusculoEntity = class EjercicioMusculoEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EjercicioMusculoEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => musculo_entity_1.MusculoEntity, musculo => musculo.ejerciciosMusculo),
    __metadata("design:type", musculo_entity_1.MusculoEntity)
], EjercicioMusculoEntity.prototype, "musculo", void 0);
__decorate([
    typeorm_1.ManyToOne(type => ejercicio_entity_1.EjercicioEntity, ejercicio => ejercicio.ejerciciosMusculo),
    __metadata("design:type", ejercicio_entity_1.EjercicioEntity)
], EjercicioMusculoEntity.prototype, "ejercicio", void 0);
EjercicioMusculoEntity = __decorate([
    typeorm_1.Entity('ejercicio-musculo')
], EjercicioMusculoEntity);
exports.EjercicioMusculoEntity = EjercicioMusculoEntity;
//# sourceMappingURL=ejercicio-musculo.entity.js.map
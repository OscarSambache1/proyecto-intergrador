import { EjercicioMusculoService } from './ejercicio-musculo.service';
import { EjercicioMusculoEntity } from './ejercicio-musculo.entity';
import { CrearEjercicioMusculoDto } from './dto/crear-ejercicio-musculo.dto';
import { EditarEjercicioMusculoDto } from './dto/editar-ejercicio-musculo.dto';
export declare class EjercicioMusculoController {
    private readonly _ejercicioMusculoService;
    constructor(_ejercicioMusculoService: EjercicioMusculoService);
    findAll(consulta: any): Promise<EjercicioMusculoEntity[]>;
    findOne(id: number): Promise<EjercicioMusculoEntity>;
    create(ejercicioMusculo: CrearEjercicioMusculoDto): Promise<EjercicioMusculoEntity>;
    update(id: number, ejercicioMusculo: EditarEjercicioMusculoDto): Promise<EjercicioMusculoEntity>;
    delete(id: number): Promise<void>;
}

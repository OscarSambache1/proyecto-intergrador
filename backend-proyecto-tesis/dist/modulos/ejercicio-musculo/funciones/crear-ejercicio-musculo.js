"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_ejercicio_musculo_dto_1 = require("../dto/crear-ejercicio-musculo.dto");
function crearEjercicioMusculo(ejercicioMusculo) {
    const ejercicioMusculoACrear = new crear_ejercicio_musculo_dto_1.CrearEjercicioMusculoDto();
    Object.keys(ejercicioMusculo).map(atributo => {
        ejercicioMusculoACrear[atributo] = ejercicioMusculo[atributo];
    });
    return ejercicioMusculoACrear;
}
exports.crearEjercicioMusculo = crearEjercicioMusculo;
//# sourceMappingURL=crear-ejercicio-musculo.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_ejercicio_musculo_dto_1 = require("../dto/editar-ejercicio-musculo.dto");
function editarEjercicioMusculo(ejercicioMusculo) {
    const ejercicioMusculoAEditar = new editar_ejercicio_musculo_dto_1.EditarEjercicioMusculoDto();
    Object.keys(ejercicioMusculo).map(atributo => {
        ejercicioMusculoAEditar[atributo] = ejercicioMusculo[atributo];
    });
    return ejercicioMusculoAEditar;
}
exports.editarEjercicioMusculo = editarEjercicioMusculo;
//# sourceMappingURL=editar-ejercicio-musculo.js.map
import { MusculoEntity } from '../musculo/musculo.entity';
import { EjercicioEntity } from '../ejercicio/ejercicio.entity';
export declare class EjercicioMusculoEntity {
    id: number;
    musculo: MusculoEntity;
    ejercicio: EjercicioEntity;
}

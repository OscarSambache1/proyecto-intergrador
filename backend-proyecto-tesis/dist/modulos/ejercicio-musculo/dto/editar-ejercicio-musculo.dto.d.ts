import { MusculoEntity } from '../../musculo/musculo.entity';
import { EjercicioEntity } from '../../ejercicio/ejercicio.entity';
export declare class EditarEjercicioMusculoDto {
    musculo: MusculoEntity;
    ejercicio: EjercicioEntity;
}

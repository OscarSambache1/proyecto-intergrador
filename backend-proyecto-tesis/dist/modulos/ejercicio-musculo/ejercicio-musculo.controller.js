"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const ejercicio_musculo_service_1 = require("./ejercicio-musculo.service");
const crear_ejercicio_musculo_dto_1 = require("./dto/crear-ejercicio-musculo.dto");
const crear_ejercicio_musculo_1 = require("./funciones/crear-ejercicio-musculo");
const editar_ejercicio_musculo_dto_1 = require("./dto/editar-ejercicio-musculo.dto");
const editar_ejercicio_musculo_1 = require("./funciones/editar-ejercicio-musculo");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let EjercicioMusculoController = class EjercicioMusculoController {
    constructor(_ejercicioMusculoService) {
        this._ejercicioMusculoService = _ejercicioMusculoService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioMusculoService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioMusculoEncontrado = yield this._ejercicioMusculoService.findById(id);
            if (ejercicioMusculoEncontrado) {
                return yield this._ejercicioMusculoService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(ejercicioMusculo) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioMusculoACrearse = crear_ejercicio_musculo_1.crearEjercicioMusculo(ejercicioMusculo);
            const arregloErrores = yield class_validator_1.validate(ejercicioMusculoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._ejercicioMusculoService.createOne(ejercicioMusculoACrearse);
            }
        });
    }
    update(id, ejercicioMusculo) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioMusculoEncontrado = yield this._ejercicioMusculoService.findById(id);
            if (ejercicioMusculoEncontrado) {
                const ejercicioMusculoAEditar = editar_ejercicio_musculo_1.editarEjercicioMusculo(ejercicioMusculo);
                const arregloErrores = yield class_validator_1.validate(ejercicioMusculoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._ejercicioMusculoService.update(id, ejercicioMusculoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioMusculoEncontrado = yield this._ejercicioMusculoService.findById(id);
            if (ejercicioMusculoEncontrado) {
                yield this._ejercicioMusculoService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EjercicioMusculoController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioMusculoController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_ejercicio_musculo_dto_1.CrearEjercicioMusculoDto]),
    __metadata("design:returntype", Promise)
], EjercicioMusculoController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_ejercicio_musculo_dto_1.EditarEjercicioMusculoDto]),
    __metadata("design:returntype", Promise)
], EjercicioMusculoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioMusculoController.prototype, "delete", null);
EjercicioMusculoController = __decorate([
    common_1.Controller('ejercicio-musculo'),
    __metadata("design:paramtypes", [ejercicio_musculo_service_1.EjercicioMusculoService])
], EjercicioMusculoController);
exports.EjercicioMusculoController = EjercicioMusculoController;
//# sourceMappingURL=ejercicio-musculo.controller.js.map
import { Repository } from 'typeorm';
import { EjercicioMusculoEntity } from './ejercicio-musculo.entity';
import { CrearEjercicioMusculoDto } from './dto/crear-ejercicio-musculo.dto';
import { EditarEjercicioMusculoDto } from './dto/editar-ejercicio-musculo.dto';
export declare class EjercicioMusculoService {
    private readonly _ejercicioMusculoRepository;
    constructor(_ejercicioMusculoRepository: Repository<EjercicioMusculoEntity>);
    findAll(consulta: any): Promise<EjercicioMusculoEntity[]>;
    findById(id: number): Promise<EjercicioMusculoEntity>;
    createOne(ejercicioMusculo: CrearEjercicioMusculoDto): Promise<EjercicioMusculoEntity>;
    createMany(ejerciciosMusculos: CrearEjercicioMusculoDto[]): Promise<EjercicioMusculoEntity[]>;
    update(id: number, ejercicioMusculo: EditarEjercicioMusculoDto): Promise<EjercicioMusculoEntity>;
    delete(id: number): Promise<void>;
}

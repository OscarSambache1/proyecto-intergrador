import { Repository } from 'typeorm';
import { RutinaPlanEntrenamientoEntity } from './rutina-plan-entrenamiento.entity';
import { CrearRutinaPlanEntrenamientoDto } from './dto/crear-rutina-plan-entrenamiento.dto';
import { EditarRutinaPlanEntrenamientoDto } from './dto/editar-rutina-plan-entrenamiento.dto';
export declare class RutinaPlanEntrenamientoService {
    private readonly _rutinaPlanEntrenamientoRepository;
    constructor(_rutinaPlanEntrenamientoRepository: Repository<RutinaPlanEntrenamientoEntity>);
    findAll(consulta: any): Promise<RutinaPlanEntrenamientoEntity[]>;
    findLike(campo: string): Promise<RutinaPlanEntrenamientoEntity[]>;
    findById(id: number): Promise<RutinaPlanEntrenamientoEntity>;
    createOne(rutinaPlanEntrenamiento: CrearRutinaPlanEntrenamientoDto): Promise<RutinaPlanEntrenamientoEntity>;
    createMany(rutinaPlanesEntrenamiento: CrearRutinaPlanEntrenamientoDto[]): Promise<RutinaPlanEntrenamientoEntity[]>;
    update(id: number, planEntrenamiento: EditarRutinaPlanEntrenamientoDto): Promise<RutinaPlanEntrenamientoEntity>;
    delete(id: number): Promise<void>;
}

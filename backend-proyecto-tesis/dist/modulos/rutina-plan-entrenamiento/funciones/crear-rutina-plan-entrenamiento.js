"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_rutina_plan_entrenamiento_dto_1 = require("../dto/crear-rutina-plan-entrenamiento.dto");
function crearRutinaPlanEntrenamiento(rutinaPlanEntrenamiento) {
    const rutinaPlanEntrenamientoACrear = new crear_rutina_plan_entrenamiento_dto_1.CrearRutinaPlanEntrenamientoDto();
    Object.keys(rutinaPlanEntrenamiento).map(atributo => {
        rutinaPlanEntrenamientoACrear[atributo] = rutinaPlanEntrenamiento[atributo];
    });
    return rutinaPlanEntrenamientoACrear;
}
exports.crearRutinaPlanEntrenamiento = crearRutinaPlanEntrenamiento;
//# sourceMappingURL=crear-rutina-plan-entrenamiento.js.map
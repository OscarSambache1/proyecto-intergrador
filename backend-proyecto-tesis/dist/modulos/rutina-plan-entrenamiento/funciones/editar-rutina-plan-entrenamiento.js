"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_rutina_plan_entrenamiento_dto_1 = require("../dto/editar-rutina-plan-entrenamiento.dto");
function editarRutinaPlanEntrenamiento(rutinaPlanEntrenamiento) {
    const rutinaPlanEntrenamientoAEditar = new editar_rutina_plan_entrenamiento_dto_1.EditarRutinaPlanEntrenamientoDto();
    Object.keys(rutinaPlanEntrenamiento).map(atributo => {
        rutinaPlanEntrenamientoAEditar[atributo] =
            rutinaPlanEntrenamiento[atributo];
    });
    return rutinaPlanEntrenamientoAEditar;
}
exports.editarRutinaPlanEntrenamiento = editarRutinaPlanEntrenamiento;
//# sourceMappingURL=editar-rutina-plan-entrenamiento.js.map
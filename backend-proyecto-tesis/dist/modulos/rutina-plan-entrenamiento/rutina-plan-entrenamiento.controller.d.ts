import { RutinaPlanEntrenamientoService } from './rutina-plan-entrenamiento.service';
import { RutinaPlanEntrenamientoEntity } from './rutina-plan-entrenamiento.entity';
import { CrearRutinaPlanEntrenamientoDto } from './dto/crear-rutina-plan-entrenamiento.dto';
import { EditarRutinaPlanEntrenamientoDto } from './dto/editar-rutina-plan-entrenamiento.dto';
export declare class RutinaPlanEntrenamientoController {
    private readonly _rutinaPlanEntrenamientoService;
    constructor(_rutinaPlanEntrenamientoService: RutinaPlanEntrenamientoService);
    findAll(consulta: any): Promise<RutinaPlanEntrenamientoEntity[]>;
    findOne(id: number): Promise<RutinaPlanEntrenamientoEntity>;
    create(rutinaPlanEntrenamiento: CrearRutinaPlanEntrenamientoDto): Promise<RutinaPlanEntrenamientoEntity>;
    createMany(rutinasPlanesEntrenamiento: CrearRutinaPlanEntrenamientoDto[]): Promise<CrearRutinaPlanEntrenamientoDto[]>;
    update(id: number, rutinaPlanEntrenamiento: EditarRutinaPlanEntrenamientoDto): Promise<RutinaPlanEntrenamientoEntity>;
    delete(id: number): Promise<void>;
}

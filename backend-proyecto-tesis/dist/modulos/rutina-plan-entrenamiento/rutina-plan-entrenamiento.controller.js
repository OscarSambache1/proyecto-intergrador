"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const rutina_plan_entrenamiento_service_1 = require("./rutina-plan-entrenamiento.service");
const crear_rutina_plan_entrenamiento_dto_1 = require("./dto/crear-rutina-plan-entrenamiento.dto");
const crear_rutina_plan_entrenamiento_1 = require("./funciones/crear-rutina-plan-entrenamiento");
const editar_rutina_plan_entrenamiento_dto_1 = require("./dto/editar-rutina-plan-entrenamiento.dto");
const editar_rutina_plan_entrenamiento_1 = require("./funciones/editar-rutina-plan-entrenamiento");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let RutinaPlanEntrenamientoController = class RutinaPlanEntrenamientoController {
    constructor(_rutinaPlanEntrenamientoService) {
        this._rutinaPlanEntrenamientoService = _rutinaPlanEntrenamientoService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._rutinaPlanEntrenamientoService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaPlanEntrenamientoEncontrado = yield this._rutinaPlanEntrenamientoService.findById(id);
            if (rutinaPlanEntrenamientoEncontrado) {
                return yield this._rutinaPlanEntrenamientoService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(rutinaPlanEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaPlanEntrenamientoACrearse = crear_rutina_plan_entrenamiento_1.crearRutinaPlanEntrenamiento(rutinaPlanEntrenamiento);
            const arregloErrores = yield class_validator_1.validate(rutinaPlanEntrenamientoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._rutinaPlanEntrenamientoService.createOne(rutinaPlanEntrenamientoACrearse);
            }
        });
    }
    createMany(rutinasPlanesEntrenamiento) {
        rutinasPlanesEntrenamiento.forEach((rutinaPlanEntrenamiento) => __awaiter(this, void 0, void 0, function* () {
            const rutinaPlanEntrenamientoACrearse = crear_rutina_plan_entrenamiento_1.crearRutinaPlanEntrenamiento(yield rutinaPlanEntrenamiento);
            const arregloErrores = yield class_validator_1.validate(rutinaPlanEntrenamientoACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._rutinaPlanEntrenamientoService.createMany(rutinasPlanesEntrenamiento);
    }
    update(id, rutinaPlanEntrenamiento) {
        return __awaiter(this, void 0, void 0, function* () {
            const planEntrenamientoEncontrado = yield this._rutinaPlanEntrenamientoService.findById(id);
            if (planEntrenamientoEncontrado) {
                const rutinaPlanEntrenamientoAEditar = editar_rutina_plan_entrenamiento_1.editarRutinaPlanEntrenamiento(rutinaPlanEntrenamiento);
                const arregloErrores = yield class_validator_1.validate(rutinaPlanEntrenamientoAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._rutinaPlanEntrenamientoService.update(id, rutinaPlanEntrenamientoAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encontrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const rutinaPlanEntrenamientoEncontrado = yield this._rutinaPlanEntrenamientoService.findById(id);
            if (rutinaPlanEntrenamientoEncontrado) {
                yield this._rutinaPlanEntrenamientoService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_rutina_plan_entrenamiento_dto_1.CrearRutinaPlanEntrenamientoDto]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_rutina_plan_entrenamiento_dto_1.EditarRutinaPlanEntrenamientoDto]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RutinaPlanEntrenamientoController.prototype, "delete", null);
RutinaPlanEntrenamientoController = __decorate([
    common_1.Controller('rutina-plan-entrenamiento'),
    __metadata("design:paramtypes", [rutina_plan_entrenamiento_service_1.RutinaPlanEntrenamientoService])
], RutinaPlanEntrenamientoController);
exports.RutinaPlanEntrenamientoController = RutinaPlanEntrenamientoController;
//# sourceMappingURL=rutina-plan-entrenamiento.controller.js.map
import { RutinaEntity } from '../../rutina/rutina.entity';
import { PlanEntrenamientoEntity } from '../../plan-entrenamiento/plan-entrenamiento.entity';
import { DiaEntity } from '../../dia/dia.entity';
export declare class EditarRutinaPlanEntrenamientoDto {
    rutina: RutinaEntity;
    planEntrenamiento: PlanEntrenamientoEntity;
    dia: DiaEntity;
}

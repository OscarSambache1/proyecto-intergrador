import { RutinaEntity } from '../rutina/rutina.entity';
import { PlanEntrenamientoEntity } from '../plan-entrenamiento/plan-entrenamiento.entity';
import { DiaEntity } from '../dia/dia.entity';
export declare class RutinaPlanEntrenamientoEntity {
    id: number;
    rutina: RutinaEntity;
    planEntrenamiento: PlanEntrenamientoEntity;
    dia: DiaEntity;
}

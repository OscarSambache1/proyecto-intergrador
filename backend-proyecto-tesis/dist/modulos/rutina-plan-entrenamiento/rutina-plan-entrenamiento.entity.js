"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const rutina_entity_1 = require("../rutina/rutina.entity");
const plan_entrenamiento_entity_1 = require("../plan-entrenamiento/plan-entrenamiento.entity");
const dia_entity_1 = require("../dia/dia.entity");
let RutinaPlanEntrenamientoEntity = class RutinaPlanEntrenamientoEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], RutinaPlanEntrenamientoEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => rutina_entity_1.RutinaEntity, rutina => rutina.rutinasPlanEntrenamiento),
    __metadata("design:type", rutina_entity_1.RutinaEntity)
], RutinaPlanEntrenamientoEntity.prototype, "rutina", void 0);
__decorate([
    typeorm_1.ManyToOne(type => plan_entrenamiento_entity_1.PlanEntrenamientoEntity, planEntrenamiento => planEntrenamiento.rutinasPlanEntrenamiento),
    __metadata("design:type", plan_entrenamiento_entity_1.PlanEntrenamientoEntity)
], RutinaPlanEntrenamientoEntity.prototype, "planEntrenamiento", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dia_entity_1.DiaEntity, dia => dia.rutinasPlanEntrenamiento),
    __metadata("design:type", dia_entity_1.DiaEntity)
], RutinaPlanEntrenamientoEntity.prototype, "dia", void 0);
RutinaPlanEntrenamientoEntity = __decorate([
    typeorm_1.Entity('rutina-plan-entrenamiento')
], RutinaPlanEntrenamientoEntity);
exports.RutinaPlanEntrenamientoEntity = RutinaPlanEntrenamientoEntity;
//# sourceMappingURL=rutina-plan-entrenamiento.entity.js.map
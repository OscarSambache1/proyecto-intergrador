import { DietaEntity } from '../dieta/dieta.entity';
import { PlanDietaEntity } from '../plan-dieta/plan-dieta.entity';
import { DiaEntity } from '../dia/dia.entity';
export declare class DietaDiaEntity {
    id: number;
    dieta: DietaEntity;
    planDieta: PlanDietaEntity;
    dia: DiaEntity;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const dieta_dia_service_1 = require("./dieta-dia.service");
const crear_dieta_dia_dto_1 = require("./dto/crear-dieta-dia.dto");
const crear_dieta_dia_1 = require("./funciones/crear-dieta-dia");
const editar_dieta_dia_dto_1 = require("./dto/editar-dieta-dia.dto");
const editar_dieta_dia_1 = require("./funciones/editar-dieta-dia");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let DietaDiaController = class DietaDiaController {
    constructor(_dietaDiaService) {
        this._dietaDiaService = _dietaDiaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._dietaDiaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietaDiaEncontrada = yield this._dietaDiaService.findById(id);
            if (dietaDiaEncontrada) {
                return yield this._dietaDiaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(dietaDia) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietaACrearse = crear_dieta_dia_1.crearDietaDia(dietaDia);
            const arregloErrores = yield class_validator_1.validate(dietaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._dietaDiaService.createOne(dietaACrearse);
            }
        });
    }
    createMany(dietasDia) {
        dietasDia.forEach((dietaDia) => __awaiter(this, void 0, void 0, function* () {
            const dietaDiaACrearse = crear_dieta_dia_1.crearDietaDia(yield dietaDia);
            const arregloErrores = yield class_validator_1.validate(dietaDiaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._dietaDiaService.createMany(dietasDia);
    }
    update(id, dietaDia) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietaDiaEncontrada = yield this._dietaDiaService.findById(id);
            if (dietaDiaEncontrada) {
                const dietaDiaAEditar = editar_dieta_dia_1.editarDietaDia(dietaDia);
                const arregloErrores = yield class_validator_1.validate(dietaDiaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._dietaDiaService.update(id, dietaDiaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietaDiaEncontrada = yield this._dietaDiaService.findById(id);
            if (dietaDiaEncontrada) {
                yield this._dietaDiaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_dieta_dia_dto_1.CrearDietaDiaDto]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_dieta_dia_dto_1.EditarDietaDiaDto]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DietaDiaController.prototype, "delete", null);
DietaDiaController = __decorate([
    common_1.Controller('dieta-dia'),
    __metadata("design:paramtypes", [dieta_dia_service_1.DietaDiaService])
], DietaDiaController);
exports.DietaDiaController = DietaDiaController;
//# sourceMappingURL=dieta-dia.controller.js.map
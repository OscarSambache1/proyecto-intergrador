"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const plan_dieta_entity_1 = require("../../plan-dieta/plan-dieta.entity");
const dieta_entity_1 = require("../../dieta/dieta.entity");
const dia_entity_1 = require("../../dia/dia.entity");
class EditarDietaDiaDto {
}
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", dieta_entity_1.DietaEntity)
], EditarDietaDiaDto.prototype, "dieta", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", plan_dieta_entity_1.PlanDietaEntity)
], EditarDietaDiaDto.prototype, "planDieta", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", dia_entity_1.DiaEntity)
], EditarDietaDiaDto.prototype, "dia", void 0);
exports.EditarDietaDiaDto = EditarDietaDiaDto;
//# sourceMappingURL=editar-dieta-dia.dto.js.map
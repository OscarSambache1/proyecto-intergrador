import { PlanDietaEntity } from '../../plan-dieta/plan-dieta.entity';
import { DietaEntity } from '../../dieta/dieta.entity';
import { DiaEntity } from '../../dia/dia.entity';
export declare class CrearDietaDiaDto {
    dieta: DietaEntity;
    planDieta: PlanDietaEntity;
    dia: DiaEntity;
}

import { DietaDiaService } from './dieta-dia.service';
import { DietaDiaEntity } from './dieta-dia.entity';
import { CrearDietaDiaDto } from './dto/crear-dieta-dia.dto';
import { EditarDietaDiaDto } from './dto/editar-dieta-dia.dto';
export declare class DietaDiaController {
    private readonly _dietaDiaService;
    constructor(_dietaDiaService: DietaDiaService);
    findAll(consulta: any): Promise<DietaDiaEntity[]>;
    findOne(id: number): Promise<DietaDiaEntity>;
    create(dietaDia: CrearDietaDiaDto): Promise<DietaDiaEntity>;
    createMany(dietasDia: CrearDietaDiaDto[]): Promise<DietaDiaEntity[]>;
    update(id: number, dietaDia: EditarDietaDiaDto): Promise<DietaDiaEntity>;
    delete(id: number): Promise<void>;
}

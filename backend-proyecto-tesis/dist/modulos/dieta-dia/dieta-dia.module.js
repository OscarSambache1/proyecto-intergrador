"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const dieta_dia_entity_1 = require("./dieta-dia.entity");
const dieta_dia_controller_1 = require("./dieta-dia.controller");
const dieta_dia_service_1 = require("./dieta-dia.service");
let DietaDiaModule = class DietaDiaModule {
};
DietaDiaModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([dieta_dia_entity_1.DietaDiaEntity], 'default')],
        controllers: [dieta_dia_controller_1.DietaDiaController],
        providers: [dieta_dia_service_1.DietaDiaService],
        exports: [],
    })
], DietaDiaModule);
exports.DietaDiaModule = DietaDiaModule;
//# sourceMappingURL=dieta-dia.module.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const dieta_dia_entity_1 = require("./dieta-dia.entity");
let DietaDiaService = class DietaDiaService {
    constructor(_dietaDiaRepository) {
        this._dietaDiaRepository = _dietaDiaRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._dietaDiaRepository.find(consulta);
        });
    }
    findLike(campo) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._dietaDiaRepository.find({
                order: { id: 'DESC' },
                where: { nombre: typeorm_2.Like(`%${campo}%`) },
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._dietaDiaRepository.findOne(id, {
                relations: ['dieta', 'planDieta'],
            });
        });
    }
    createOne(dietaDia) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._dietaDiaRepository.save(this._dietaDiaRepository.create(dietaDia));
        });
    }
    createMany(dietasDia) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietasGuardadas = this._dietaDiaRepository.create(dietasDia);
            return this._dietaDiaRepository.save(dietasGuardadas);
        });
    }
    update(id, dietaDia) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._dietaDiaRepository.update(id, dietaDia);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const dietaDiaAEliminar = yield this.findById(id);
            yield this._dietaDiaRepository.remove(dietaDiaAEliminar);
        });
    }
};
DietaDiaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(dieta_dia_entity_1.DietaDiaEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], DietaDiaService);
exports.DietaDiaService = DietaDiaService;
//# sourceMappingURL=dieta-dia.service.js.map
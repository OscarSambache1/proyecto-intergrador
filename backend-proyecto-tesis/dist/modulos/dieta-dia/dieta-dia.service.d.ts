import { Repository } from 'typeorm';
import { DietaDiaEntity } from './dieta-dia.entity';
import { CrearDietaDiaDto } from './dto/crear-dieta-dia.dto';
import { EditarDietaDiaDto } from './dto/editar-dieta-dia.dto';
export declare class DietaDiaService {
    private readonly _dietaDiaRepository;
    constructor(_dietaDiaRepository: Repository<DietaDiaEntity>);
    findAll(consulta: any): Promise<DietaDiaEntity[]>;
    findLike(campo: string): Promise<DietaDiaEntity[]>;
    findById(id: number): Promise<DietaDiaEntity>;
    createOne(dietaDia: CrearDietaDiaDto): Promise<DietaDiaEntity>;
    createMany(dietasDia: CrearDietaDiaDto[]): Promise<DietaDiaEntity[]>;
    update(id: number, dietaDia: EditarDietaDiaDto): Promise<DietaDiaEntity>;
    delete(id: number): Promise<void>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const dieta_entity_1 = require("../dieta/dieta.entity");
const plan_dieta_entity_1 = require("../plan-dieta/plan-dieta.entity");
const dia_entity_1 = require("../dia/dia.entity");
let DietaDiaEntity = class DietaDiaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], DietaDiaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dieta_entity_1.DietaEntity, dieta => dieta.planesDietaDia),
    __metadata("design:type", dieta_entity_1.DietaEntity)
], DietaDiaEntity.prototype, "dieta", void 0);
__decorate([
    typeorm_1.ManyToOne(type => plan_dieta_entity_1.PlanDietaEntity, planDieta => planDieta.planesDietaDia),
    __metadata("design:type", plan_dieta_entity_1.PlanDietaEntity)
], DietaDiaEntity.prototype, "planDieta", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dia_entity_1.DiaEntity, dia => dia.dietasDia),
    __metadata("design:type", dia_entity_1.DiaEntity)
], DietaDiaEntity.prototype, "dia", void 0);
DietaDiaEntity = __decorate([
    typeorm_1.Entity('dieta-dia')
], DietaDiaEntity);
exports.DietaDiaEntity = DietaDiaEntity;
//# sourceMappingURL=dieta-dia.entity.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_dieta_dia_dto_1 = require("../dto/editar-dieta-dia.dto");
function editarDietaDia(dietaDia) {
    const dietaDiaAEditar = new editar_dieta_dia_dto_1.EditarDietaDiaDto();
    Object.keys(dietaDia).map(atributo => {
        dietaDiaAEditar[atributo] = dietaDia[atributo];
    });
    return dietaDiaAEditar;
}
exports.editarDietaDia = editarDietaDia;
//# sourceMappingURL=editar-dieta-dia.js.map
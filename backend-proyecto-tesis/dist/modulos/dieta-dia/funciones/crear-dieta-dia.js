"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_dieta_dia_dto_1 = require("../dto/crear-dieta-dia.dto");
function crearDietaDia(dietaDia) {
    const dietaDiaACrear = new crear_dieta_dia_dto_1.CrearDietaDiaDto();
    Object.keys(dietaDia).map(atributo => {
        dietaDiaACrear[atributo] = dietaDia[atributo];
    });
    return dietaDiaACrear;
}
exports.crearDietaDia = crearDietaDia;
//# sourceMappingURL=crear-dieta-dia.js.map
import { ClienteEntity } from '../../cliente/cliente.entity';
import { PlanDietaEntity } from '../../plan-dieta/plan-dieta.entity';
export declare class CrearPlanDietaClienteDto {
    fechaInicio: string;
    fechaFin: string;
    cliente: ClienteEntity;
    planDieta: PlanDietaEntity;
}

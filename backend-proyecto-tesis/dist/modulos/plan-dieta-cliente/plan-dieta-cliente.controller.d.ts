import { PlanDietaClienteService } from './plan-dieta-cliente.service';
import { PlanDietaClienteEntity } from './plan-dieta-cliente.entity';
import { CrearPlanDietaClienteDto } from './dto/crear-plan-dieta-cliente.dto';
import { EditarPlanDietaClienteDto } from './dto/editar-plan-dieta-cliente.dto';
export declare class PlanDietaClienteController {
    private readonly _planDietaClienteService;
    constructor(_planDietaClienteService: PlanDietaClienteService);
    findAll(consulta: any): Promise<PlanDietaClienteEntity[]>;
    findOne(id: number): Promise<PlanDietaClienteEntity>;
    create(planDietaCliente: CrearPlanDietaClienteDto): Promise<PlanDietaClienteEntity>;
    createMany(planesDietaCliente: CrearPlanDietaClienteDto[]): Promise<PlanDietaClienteEntity[]>;
    update(id: number, planDietaCliente: EditarPlanDietaClienteDto): Promise<PlanDietaClienteEntity>;
    delete(id: number): Promise<void>;
}

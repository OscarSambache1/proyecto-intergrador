"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const cliente_entity_1 = require("../cliente/cliente.entity");
const plan_dieta_entity_1 = require("../plan-dieta/plan-dieta.entity");
let PlanDietaClienteEntity = class PlanDietaClienteEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], PlanDietaClienteEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'fecha-inicio', type: 'date' }),
    __metadata("design:type", String)
], PlanDietaClienteEntity.prototype, "fechaInicio", void 0);
__decorate([
    typeorm_1.Column({ name: 'fecha-fin', type: 'date' }),
    __metadata("design:type", String)
], PlanDietaClienteEntity.prototype, "fechaFin", void 0);
__decorate([
    typeorm_1.ManyToOne(type => cliente_entity_1.ClienteEntity, cliente => cliente.planesDieta),
    __metadata("design:type", cliente_entity_1.ClienteEntity)
], PlanDietaClienteEntity.prototype, "cliente", void 0);
__decorate([
    typeorm_1.ManyToOne(type => plan_dieta_entity_1.PlanDietaEntity, planDieta => planDieta.planesDietaCliente),
    __metadata("design:type", plan_dieta_entity_1.PlanDietaEntity)
], PlanDietaClienteEntity.prototype, "planDieta", void 0);
PlanDietaClienteEntity = __decorate([
    typeorm_1.Entity('plan-dieta-cliente')
], PlanDietaClienteEntity);
exports.PlanDietaClienteEntity = PlanDietaClienteEntity;
//# sourceMappingURL=plan-dieta-cliente.entity.js.map
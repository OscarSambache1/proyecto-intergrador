import { Repository } from 'typeorm';
import { PlanDietaClienteEntity } from './plan-dieta-cliente.entity';
import { CrearPlanDietaClienteDto } from './dto/crear-plan-dieta-cliente.dto';
import { EditarPlanDietaClienteDto } from './dto/editar-plan-dieta-cliente.dto';
export declare class PlanDietaClienteService {
    private readonly _planDietaClienteRepository;
    constructor(_planDietaClienteRepository: Repository<PlanDietaClienteEntity>);
    findAll(consulta: any): Promise<PlanDietaClienteEntity[]>;
    findLike(campo: string): Promise<PlanDietaClienteEntity[]>;
    findById(id: number): Promise<PlanDietaClienteEntity>;
    createOne(planDietaCliente: CrearPlanDietaClienteDto): Promise<PlanDietaClienteEntity>;
    createMany(planesDietaCliente: CrearPlanDietaClienteDto[]): Promise<PlanDietaClienteEntity[]>;
    update(id: number, planDietaCliente: EditarPlanDietaClienteDto): Promise<PlanDietaClienteEntity>;
    delete(id: number): Promise<void>;
}

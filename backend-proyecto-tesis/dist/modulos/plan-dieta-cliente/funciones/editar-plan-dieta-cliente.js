"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_plan_dieta_cliente_dto_1 = require("../dto/editar-plan-dieta-cliente.dto");
function editarPlanDietaCliente(planDietaCliente) {
    const planDietaClienteAEditar = new editar_plan_dieta_cliente_dto_1.EditarPlanDietaClienteDto();
    Object.keys(planDietaCliente).map(atributo => {
        planDietaClienteAEditar[atributo] = planDietaCliente[atributo];
    });
    return planDietaClienteAEditar;
}
exports.editarPlanDietaCliente = editarPlanDietaCliente;
//# sourceMappingURL=editar-plan-dieta-cliente.js.map
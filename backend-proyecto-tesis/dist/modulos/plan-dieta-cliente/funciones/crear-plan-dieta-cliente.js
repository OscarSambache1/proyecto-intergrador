"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_plan_dieta_cliente_dto_1 = require("../dto/crear-plan-dieta-cliente.dto");
function crearPlanDietaCliente(planDietaCliente) {
    const planDietaClienteACrear = new crear_plan_dieta_cliente_dto_1.CrearPlanDietaClienteDto();
    Object.keys(planDietaCliente).map(atributo => {
        planDietaClienteACrear[atributo] = planDietaCliente[atributo];
    });
    return planDietaClienteACrear;
}
exports.crearPlanDietaCliente = crearPlanDietaCliente;
//# sourceMappingURL=crear-plan-dieta-cliente.js.map
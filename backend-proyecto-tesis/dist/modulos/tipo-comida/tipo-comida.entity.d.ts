import { ComidaEntity } from '../comida/comida.entity';
export declare class TipoComidaEntity {
    id: number;
    nombre: string;
    estado: number;
    comidas: ComidaEntity[];
}

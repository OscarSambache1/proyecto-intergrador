"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const tipo_comida_entity_1 = require("./tipo-comida.entity");
let TipoComidaService = class TipoComidaService {
    constructor(_tipoComidaRepository) {
        this._tipoComidaRepository = _tipoComidaRepository;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            if (consulta.where) {
                Object.keys(consulta.where).map(atributo => {
                    consulta.where[atributo] = typeorm_2.Like(`%${consulta.where[atributo]}%`);
                });
            }
            return yield this._tipoComidaRepository.find(consulta);
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._tipoComidaRepository.findOne(id, {
                relations: [],
            });
        });
    }
    createOne(tipoComida) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._tipoComidaRepository.save(this._tipoComidaRepository.create(tipoComida));
        });
    }
    createMany(tiposComida) {
        return __awaiter(this, void 0, void 0, function* () {
            const tiposComidaACrear = this._tipoComidaRepository.create(tiposComida);
            return this._tipoComidaRepository.save(tiposComidaACrear);
        });
    }
    update(id, tipoComida) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._tipoComidaRepository.update(id, tipoComida);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoComiidaAEliminar = yield this.findById(id);
            yield this._tipoComidaRepository.remove(tipoComiidaAEliminar);
        });
    }
};
TipoComidaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(tipo_comida_entity_1.TipoComidaEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], TipoComidaService);
exports.TipoComidaService = TipoComidaService;
//# sourceMappingURL=tipo-comida.service.js.map
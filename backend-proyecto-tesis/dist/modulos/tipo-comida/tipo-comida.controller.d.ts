import { TipoComidaService } from './tipo-comida.service';
import { TipoComidaEntity } from './tipo-comida.entity';
import { CrearTipoComidaDto } from './dto/crear-tipo-comida.dto';
import { EditarTipoComidaDto } from './dto/editar-tipo-comida.dto';
export declare class TipoComidaController {
    private readonly _tipoComidaService;
    constructor(_tipoComidaService: TipoComidaService);
    findAll(consulta: any): Promise<TipoComidaEntity[]>;
    findOne(id: number): Promise<TipoComidaEntity>;
    create(tipoComida: CrearTipoComidaDto): Promise<TipoComidaEntity>;
    update(id: number, tipoComida: EditarTipoComidaDto): Promise<TipoComidaEntity>;
    delete(id: number): Promise<void>;
}

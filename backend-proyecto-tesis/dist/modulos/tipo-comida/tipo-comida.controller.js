"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const tipo_comida_service_1 = require("./tipo-comida.service");
const crear_tipo_comida_dto_1 = require("./dto/crear-tipo-comida.dto");
const editar_tipo_comida_dto_1 = require("./dto/editar-tipo-comida.dto");
const crear_tipo_comida_1 = require("./funciones/crear-tipo-comida");
const editar_tipo_comida_1 = require("./funciones/editar-tipo-comida");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let TipoComidaController = class TipoComidaController {
    constructor(_tipoComidaService) {
        this._tipoComidaService = _tipoComidaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._tipoComidaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoComidaEncontrada = yield this._tipoComidaService.findById(id);
            if (tipoComidaEncontrada) {
                return yield this._tipoComidaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El tipo de comida no existe');
            }
        });
    }
    create(tipoComida) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoComidaACrearse = crear_tipo_comida_1.crearTipoComida(tipoComida);
            const arregloErrores = yield class_validator_1.validate(tipoComidaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando lel tipo de comida', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._tipoComidaService.createOne(tipoComidaACrearse);
            }
        });
    }
    update(id, tipoComida) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoComidaEncontrada = yield this._tipoComidaService.findById(id);
            if (tipoComidaEncontrada) {
                const tipoComidaAEditar = editar_tipo_comida_1.editarTipoComida(tipoComida);
                const arregloErrores = yield class_validator_1.validate(tipoComidaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el tipo de comida', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._tipoComidaService.update(id, tipoComidaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Tipo de comida no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tipoComidaEncontrado = yield this._tipoComidaService.findById(id);
            if (tipoComidaEncontrado) {
                yield this._tipoComidaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El tipo de comida no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TipoComidaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TipoComidaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_tipo_comida_dto_1.CrearTipoComidaDto]),
    __metadata("design:returntype", Promise)
], TipoComidaController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_tipo_comida_dto_1.EditarTipoComidaDto]),
    __metadata("design:returntype", Promise)
], TipoComidaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TipoComidaController.prototype, "delete", null);
TipoComidaController = __decorate([
    common_1.Controller('tipo-comida'),
    __metadata("design:paramtypes", [tipo_comida_service_1.TipoComidaService])
], TipoComidaController);
exports.TipoComidaController = TipoComidaController;
//# sourceMappingURL=tipo-comida.controller.js.map
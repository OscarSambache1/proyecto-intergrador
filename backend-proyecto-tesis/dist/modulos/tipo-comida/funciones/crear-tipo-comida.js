"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_tipo_comida_dto_1 = require("../dto/crear-tipo-comida.dto");
function crearTipoComida(tipoComida) {
    const tipoComidaACrear = new crear_tipo_comida_dto_1.CrearTipoComidaDto();
    Object.keys(tipoComida).map(atributo => {
        tipoComidaACrear[atributo] = tipoComida[atributo];
    });
    return tipoComidaACrear;
}
exports.crearTipoComida = crearTipoComida;
//# sourceMappingURL=crear-tipo-comida.js.map
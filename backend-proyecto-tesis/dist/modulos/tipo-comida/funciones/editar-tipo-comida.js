"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_tipo_comida_dto_1 = require("../dto/editar-tipo-comida.dto");
function editarTipoComida(tipoComida) {
    const tipoComidaAEditar = new editar_tipo_comida_dto_1.EditarTipoComidaDto();
    Object.keys(tipoComida).map(atributo => {
        tipoComidaAEditar[atributo] = tipoComida[atributo];
    });
    return tipoComidaAEditar;
}
exports.editarTipoComida = editarTipoComida;
//# sourceMappingURL=editar-tipo-comida.js.map
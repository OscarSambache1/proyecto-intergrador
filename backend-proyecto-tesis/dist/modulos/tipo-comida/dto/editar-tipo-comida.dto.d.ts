import { ComidaEntity } from '../../comida/comida.entity';
export declare class EditarTipoComidaDto {
    nombre: string;
    estado?: number;
    comidas: ComidaEntity[];
}

import { ComidaEntity } from '../../comida/comida.entity';
export declare class CrearTipoComidaDto {
    nombre: string;
    comidas: ComidaEntity[];
}

import { Repository } from 'typeorm';
import { TipoComidaEntity } from './tipo-comida.entity';
import { CrearTipoComidaDto } from './dto/crear-tipo-comida.dto';
import { EditarTipoComidaDto } from './dto/editar-tipo-comida.dto';
export declare class TipoComidaService {
    private readonly _tipoComidaRepository;
    constructor(_tipoComidaRepository: Repository<TipoComidaEntity>);
    findAll(consulta: any): Promise<TipoComidaEntity[]>;
    findById(id: number): Promise<TipoComidaEntity>;
    createOne(tipoComida: CrearTipoComidaDto): Promise<TipoComidaEntity>;
    createMany(tiposComida: CrearTipoComidaDto[]): Promise<TipoComidaEntity[]>;
    update(id: number, tipoComida: EditarTipoComidaDto): Promise<TipoComidaEntity>;
    delete(id: number): Promise<void>;
}

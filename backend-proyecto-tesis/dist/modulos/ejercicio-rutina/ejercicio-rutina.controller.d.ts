import { EjercicioRutinaService } from './ejercicio-rutina.service';
import { EjercicioRutinaEntity } from './ejercicio-rutina.entity';
import { CrearEjercicioRutinaDto } from './dto/crear-ejercicio-rutina.dto';
import { EditarEjercicioRutinaDto } from './dto/editar-ejercicio-rutina.dto';
export declare class EjercicioRutinaController {
    private readonly _ejercicioRutinaService;
    constructor(_ejercicioRutinaService: EjercicioRutinaService);
    findAll(consulta: any): Promise<EjercicioRutinaEntity[]>;
    findOne(id: number): Promise<EjercicioRutinaEntity>;
    create(ejercicioRutina: CrearEjercicioRutinaDto): Promise<EjercicioRutinaEntity>;
    createMany(ejerciciosRutinas: CrearEjercicioRutinaDto[]): Promise<CrearEjercicioRutinaDto[]>;
    update(id: number, ejercicioRutina: EditarEjercicioRutinaDto): Promise<EjercicioRutinaEntity>;
    delete(id: number): Promise<void>;
}

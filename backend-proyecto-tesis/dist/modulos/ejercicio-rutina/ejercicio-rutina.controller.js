"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const ejercicio_rutina_service_1 = require("./ejercicio-rutina.service");
const crear_ejercicio_rutina_dto_1 = require("./dto/crear-ejercicio-rutina.dto");
const crear_ejercicio_rutina_1 = require("./funciones/crear-ejercicio-rutina");
const editar_ejercicio_rutina_dto_1 = require("./dto/editar-ejercicio-rutina.dto");
const editar_ejercicio_rutina_1 = require("./funciones/editar-ejercicio-rutina");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let EjercicioRutinaController = class EjercicioRutinaController {
    constructor(_ejercicioRutinaService) {
        this._ejercicioRutinaService = _ejercicioRutinaService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._ejercicioRutinaService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioRutinaEncontrado = yield this._ejercicioRutinaService.findById(id);
            if (ejercicioRutinaEncontrado) {
                return yield this._ejercicioRutinaService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El registro no existe');
            }
        });
    }
    create(ejercicioRutina) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioRutinaACrearse = crear_ejercicio_rutina_1.crearEjercicioRutina(ejercicioRutina);
            const arregloErrores = yield class_validator_1.validate(ejercicioRutinaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._ejercicioRutinaService.createOne(ejercicioRutinaACrearse);
            }
        });
    }
    createMany(ejerciciosRutinas) {
        ejerciciosRutinas.forEach((ejercicioRutina) => __awaiter(this, void 0, void 0, function* () {
            const ejercicioRutinaACrearse = crear_ejercicio_rutina_1.crearEjercicioRutina(yield ejercicioRutina);
            const arregloErrores = yield class_validator_1.validate(ejercicioRutinaACrearse);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._ejercicioRutinaService.createMany(ejerciciosRutinas);
    }
    update(id, ejercicioRutina) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioRutinaEncontrada = yield this._ejercicioRutinaService.findById(id);
            if (ejercicioRutinaEncontrada) {
                const ejercicioRutinaAEditar = editar_ejercicio_rutina_1.editarEjercicioRutina(ejercicioRutina);
                const arregloErrores = yield class_validator_1.validate(ejercicioRutinaAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el registro', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._ejercicioRutinaService.update(id, ejercicioRutinaAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Registro no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const ejercicioRutinaEncontrado = yield this._ejercicioRutinaService.findById(id);
            if (ejercicioRutinaEncontrado) {
                yield this._ejercicioRutinaService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('EL registro no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_ejercicio_rutina_dto_1.CrearEjercicioRutinaDto]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, editar_ejercicio_rutina_dto_1.EditarEjercicioRutinaDto]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(session_guard_1.SessionGuard),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EjercicioRutinaController.prototype, "delete", null);
EjercicioRutinaController = __decorate([
    common_1.Controller('ejercicio-rutina'),
    __metadata("design:paramtypes", [ejercicio_rutina_service_1.EjercicioRutinaService])
], EjercicioRutinaController);
exports.EjercicioRutinaController = EjercicioRutinaController;
//# sourceMappingURL=ejercicio-rutina.controller.js.map
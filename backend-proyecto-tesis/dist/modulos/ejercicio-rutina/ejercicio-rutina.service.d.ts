import { Repository } from 'typeorm';
import { EjercicioRutinaEntity } from './ejercicio-rutina.entity';
import { CrearEjercicioRutinaDto } from './dto/crear-ejercicio-rutina.dto';
import { EditarEjercicioRutinaDto } from './dto/editar-ejercicio-rutina.dto';
export declare class EjercicioRutinaService {
    private readonly _ejercicioRutinaRepository;
    constructor(_ejercicioRutinaRepository: Repository<EjercicioRutinaEntity>);
    findAll(consulta: any): Promise<EjercicioRutinaEntity[]>;
    findById(id: number): Promise<EjercicioRutinaEntity>;
    createOne(ejercicioRutina: CrearEjercicioRutinaDto): Promise<EjercicioRutinaEntity>;
    createMany(ejerciciosRutina: CrearEjercicioRutinaDto[]): Promise<EjercicioRutinaEntity[]>;
    update(id: number, ejercicioRutina: EditarEjercicioRutinaDto): Promise<EjercicioRutinaEntity>;
    delete(id: number): Promise<void>;
}

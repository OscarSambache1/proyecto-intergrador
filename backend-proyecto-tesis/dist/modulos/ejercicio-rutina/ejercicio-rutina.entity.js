"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const ejercicio_entity_1 = require("../ejercicio/ejercicio.entity");
const rutina_entity_1 = require("../rutina/rutina.entity");
let EjercicioRutinaEntity = class EjercicioRutinaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EjercicioRutinaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'series', type: 'integer' }),
    __metadata("design:type", Number)
], EjercicioRutinaEntity.prototype, "series", void 0);
__decorate([
    typeorm_1.Column({ name: 'repeticiones', type: 'integer' }),
    __metadata("design:type", Number)
], EjercicioRutinaEntity.prototype, "repeticiones", void 0);
__decorate([
    typeorm_1.Column({ name: 'descanso', type: 'integer' }),
    __metadata("design:type", Number)
], EjercicioRutinaEntity.prototype, "descanso", void 0);
__decorate([
    typeorm_1.Column({ name: 'peso', type: 'integer' }),
    __metadata("design:type", Number)
], EjercicioRutinaEntity.prototype, "peso", void 0);
__decorate([
    typeorm_1.ManyToOne(type => ejercicio_entity_1.EjercicioEntity, ejercicio => ejercicio.ejerciciosRutina),
    __metadata("design:type", ejercicio_entity_1.EjercicioEntity)
], EjercicioRutinaEntity.prototype, "ejercicio", void 0);
__decorate([
    typeorm_1.ManyToOne(type => rutina_entity_1.RutinaEntity, rutina => rutina.ejerciciosRutina),
    __metadata("design:type", rutina_entity_1.RutinaEntity)
], EjercicioRutinaEntity.prototype, "rutina", void 0);
EjercicioRutinaEntity = __decorate([
    typeorm_1.Entity('ejercicio-rutina')
], EjercicioRutinaEntity);
exports.EjercicioRutinaEntity = EjercicioRutinaEntity;
//# sourceMappingURL=ejercicio-rutina.entity.js.map
import { EjercicioEntity } from '../ejercicio/ejercicio.entity';
import { RutinaEntity } from '../rutina/rutina.entity';
export declare class EjercicioRutinaEntity {
    id: number;
    series: number;
    repeticiones: number;
    descanso: number;
    peso: number;
    ejercicio: EjercicioEntity;
    rutina: RutinaEntity;
}

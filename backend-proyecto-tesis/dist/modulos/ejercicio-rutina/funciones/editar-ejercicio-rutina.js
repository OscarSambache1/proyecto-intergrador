"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_ejercicio_rutina_dto_1 = require("../dto/editar-ejercicio-rutina.dto");
function editarEjercicioRutina(ejercicioRutina) {
    const ejercicioRutinaAEditar = new editar_ejercicio_rutina_dto_1.EditarEjercicioRutinaDto();
    Object.keys(ejercicioRutina).map(atributo => {
        ejercicioRutinaAEditar[atributo] = ejercicioRutina[atributo];
    });
    return ejercicioRutinaAEditar;
}
exports.editarEjercicioRutina = editarEjercicioRutina;
//# sourceMappingURL=editar-ejercicio-rutina.js.map
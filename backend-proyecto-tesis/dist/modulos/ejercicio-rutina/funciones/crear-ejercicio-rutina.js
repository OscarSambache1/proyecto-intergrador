"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_ejercicio_rutina_dto_1 = require("../dto/crear-ejercicio-rutina.dto");
function crearEjercicioRutina(ejercicioRutina) {
    const ejercicioRutinaACrear = new crear_ejercicio_rutina_dto_1.CrearEjercicioRutinaDto();
    Object.keys(ejercicioRutina).map(atributo => {
        ejercicioRutinaACrear[atributo] = ejercicioRutina[atributo];
    });
    return ejercicioRutinaACrear;
}
exports.crearEjercicioRutina = crearEjercicioRutina;
//# sourceMappingURL=crear-ejercicio-rutina.js.map
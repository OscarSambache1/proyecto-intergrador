"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const ejercicio_entity_1 = require("../../ejercicio/ejercicio.entity");
const rutina_entity_1 = require("../../rutina/rutina.entity");
class CrearEjercicioRutinaDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearEjercicioRutinaDto.prototype, "series", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearEjercicioRutinaDto.prototype, "repeticiones", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearEjercicioRutinaDto.prototype, "descanso", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearEjercicioRutinaDto.prototype, "peso", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", ejercicio_entity_1.EjercicioEntity)
], CrearEjercicioRutinaDto.prototype, "ejercicio", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", rutina_entity_1.RutinaEntity)
], CrearEjercicioRutinaDto.prototype, "rutina", void 0);
exports.CrearEjercicioRutinaDto = CrearEjercicioRutinaDto;
//# sourceMappingURL=crear-ejercicio-rutina.dto.js.map
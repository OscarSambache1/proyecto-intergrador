import { EjercicioEntity } from '../../ejercicio/ejercicio.entity';
import { RutinaEntity } from '../../rutina/rutina.entity';
export declare class EditarEjercicioRutinaDto {
    series: number;
    repeticiones: number;
    descanso: number;
    peso: number;
    ejercicio: EjercicioEntity;
    rutina: RutinaEntity;
}

import { DetalleFacturaService } from './detalle-factura.service';
import { DetalleFacturaEntity } from './detalle-factura.entity';
import { CrearDetalleFacturaDto } from './dto/crear-detalle-factura.dto';
export declare class DetalleFacturaController {
    private readonly _detalleService;
    constructor(_detalleService: DetalleFacturaService);
    findAll(consulta: any): Promise<DetalleFacturaEntity[]>;
    findOne(id: number): Promise<DetalleFacturaEntity>;
    create(detalle: CrearDetalleFacturaDto): Promise<DetalleFacturaEntity>;
    createMany(detalles: CrearDetalleFacturaDto[]): Promise<DetalleFacturaEntity[]>;
    update(id: number, detalle: DetalleFacturaEntity): Promise<DetalleFacturaEntity>;
    delete(id: number): Promise<void>;
}

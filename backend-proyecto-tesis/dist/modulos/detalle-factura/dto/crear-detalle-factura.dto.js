"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const cabecera_factura_entity_1 = require("../../cabecera-factura/cabecera-factura.entity");
const producto_entity_1 = require("../../producto/producto.entity");
const class_validator_1 = require("class-validator");
class CrearDetalleFacturaDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearDetalleFacturaDto.prototype, "cantidad", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearDetalleFacturaDto.prototype, "total", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CrearDetalleFacturaDto.prototype, "precioUnitario", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", cabecera_factura_entity_1.CabeceraFacturaEntity)
], CrearDetalleFacturaDto.prototype, "cabeceraFactura", void 0);
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", producto_entity_1.ProductoEntity)
], CrearDetalleFacturaDto.prototype, "producto", void 0);
exports.CrearDetalleFacturaDto = CrearDetalleFacturaDto;
//# sourceMappingURL=crear-detalle-factura.dto.js.map
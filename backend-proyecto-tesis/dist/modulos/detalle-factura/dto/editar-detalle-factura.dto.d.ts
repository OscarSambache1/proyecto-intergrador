import { CabeceraFacturaEntity } from '../../cabecera-factura/cabecera-factura.entity';
import { ProductoEntity } from '../../producto/producto.entity';
export declare class EditarDetalleFacturaDto {
    cantidad?: number;
    total?: number;
    precioUnitario?: number;
    cabeceraFactura?: CabeceraFacturaEntity;
    producto?: ProductoEntity;
}

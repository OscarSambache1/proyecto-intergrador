import { ProductoEntity } from '../producto/producto.entity';
import { CabeceraFacturaEntity } from '../cabecera-factura/cabecera-factura.entity';
export declare class DetalleFacturaEntity {
    id: number;
    cantidad: number;
    total: number;
    precioUnitario: number;
    cabeceraFactura: CabeceraFacturaEntity;
    producto: ProductoEntity;
}

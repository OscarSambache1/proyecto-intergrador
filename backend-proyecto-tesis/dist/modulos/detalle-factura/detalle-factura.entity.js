"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const producto_entity_1 = require("../producto/producto.entity");
const cabecera_factura_entity_1 = require("../cabecera-factura/cabecera-factura.entity");
let DetalleFacturaEntity = class DetalleFacturaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], DetalleFacturaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'cantidad', type: 'int' }),
    __metadata("design:type", Number)
], DetalleFacturaEntity.prototype, "cantidad", void 0);
__decorate([
    typeorm_1.Column({
        name: 'total',
        type: 'decimal',
        precision: 15,
        scale: 2,
        default: 0,
    }),
    __metadata("design:type", Number)
], DetalleFacturaEntity.prototype, "total", void 0);
__decorate([
    typeorm_1.Column({
        name: 'precioUnitario',
        type: 'decimal',
        precision: 15,
        scale: 2,
        default: 0,
    }),
    __metadata("design:type", Number)
], DetalleFacturaEntity.prototype, "precioUnitario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => cabecera_factura_entity_1.CabeceraFacturaEntity, cabeceraFactura => cabeceraFactura.detallesFactura),
    __metadata("design:type", cabecera_factura_entity_1.CabeceraFacturaEntity)
], DetalleFacturaEntity.prototype, "cabeceraFactura", void 0);
__decorate([
    typeorm_1.ManyToOne(type => producto_entity_1.ProductoEntity, producto => producto.detallesFactura),
    __metadata("design:type", producto_entity_1.ProductoEntity)
], DetalleFacturaEntity.prototype, "producto", void 0);
DetalleFacturaEntity = __decorate([
    typeorm_1.Entity('detalle-factura')
], DetalleFacturaEntity);
exports.DetalleFacturaEntity = DetalleFacturaEntity;
//# sourceMappingURL=detalle-factura.entity.js.map
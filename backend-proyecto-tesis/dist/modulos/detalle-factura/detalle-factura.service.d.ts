import { Repository } from 'typeorm';
import { DetalleFacturaEntity } from './detalle-factura.entity';
import { CrearDetalleFacturaDto } from './dto/crear-detalle-factura.dto';
import { EditarDetalleFacturaDto } from './dto/editar-detalle-factura.dto';
export declare class DetalleFacturaService {
    private readonly _detalleRepository;
    constructor(_detalleRepository: Repository<DetalleFacturaEntity>);
    findAll(consulta: any): Promise<DetalleFacturaEntity[]>;
    findById(id: number): Promise<DetalleFacturaEntity>;
    createOne(detalleFactura: CrearDetalleFacturaDto): Promise<DetalleFacturaEntity>;
    createMany(detalle: CrearDetalleFacturaDto[]): Promise<DetalleFacturaEntity[]>;
    update(id: number, detalleFactura: EditarDetalleFacturaDto): Promise<DetalleFacturaEntity>;
    delete(id: number): Promise<void>;
    contar(consulta: object): Promise<number>;
}

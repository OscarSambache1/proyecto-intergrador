"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const detalle_factura_service_1 = require("./detalle-factura.service");
const detalle_factura_entity_1 = require("./detalle-factura.entity");
const crear_detalle_factura_dto_1 = require("./dto/crear-detalle-factura.dto");
const crear_detalle_1 = require("./funciones/crear-detalle");
const editar_detalle_1 = require("./funciones/editar-detalle");
const session_guard_1 = require("../auth/session.guard");
const roles_decorator_1 = require("../auth/roles.decorator");
const roles_guard_1 = require("../auth/roles.guard");
let DetalleFacturaController = class DetalleFacturaController {
    constructor(_detalleService) {
        this._detalleService = _detalleService;
    }
    findAll(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._detalleService.findAll(JSON.parse(consulta));
        });
    }
    findOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const detalleEncontrado = yield this._detalleService.findById(id);
            if (detalleEncontrado) {
                return yield this._detalleService.findById(id);
            }
            else {
                throw new common_1.BadRequestException('El detalle no existe');
            }
        });
    }
    create(detalle) {
        return __awaiter(this, void 0, void 0, function* () {
            const detalleACrear = crear_detalle_1.crearDetalle(detalle);
            const arregloErrores = yield class_validator_1.validate(detalleACrear);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el detalle', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
            else {
                return this._detalleService.createOne(detalleACrear);
            }
        });
    }
    createMany(detalles) {
        detalles.forEach((detalle) => __awaiter(this, void 0, void 0, function* () {
            const detalleACrear = crear_detalle_1.crearDetalle(yield detalle);
            const arregloErrores = yield class_validator_1.validate(detalleACrear);
            const existenErrores = arregloErrores.length > 0;
            if (existenErrores) {
                console.log(arregloErrores);
                console.error('errores: creando el registro', arregloErrores);
                throw new common_1.BadRequestException('Parametros incorrectos');
            }
        }));
        return this._detalleService.createMany(detalles);
    }
    update(id, detalle) {
        return __awaiter(this, void 0, void 0, function* () {
            const detalleEncontrado = yield this._detalleService.findById(id);
            if (detalleEncontrado) {
                const detalleAEditar = editar_detalle_1.editarDetalle(detalle);
                const arregloErrores = yield class_validator_1.validate(detalleAEditar);
                const existenErrores = arregloErrores.length > 0;
                console.log('errores', arregloErrores);
                if (existenErrores) {
                    console.error('errores editando el detalle', arregloErrores);
                    throw new common_1.BadRequestException('Parametros incorrectos');
                }
                else {
                    return this._detalleService.update(id, detalleAEditar);
                }
            }
            else {
                throw new common_1.BadRequestException('Detalle no encotrado');
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const detalleEncontrado = yield this._detalleService.findById(id);
            if (detalleEncontrado) {
                yield this._detalleService.delete(id);
            }
            else {
                throw new common_1.BadRequestException('El Detalle no existe');
            }
        });
    }
};
__decorate([
    common_1.Get(''),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Query('consulta')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    roles_decorator_1.Roles('administrador', 'cliente', 'instructor', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crear_detalle_factura_dto_1.CrearDetalleFacturaDto]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "create", null);
__decorate([
    common_1.Post('crear-varios'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "createMany", null);
__decorate([
    common_1.Put(':id'),
    roles_decorator_1.Roles('administrador'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, detalle_factura_entity_1.DetalleFacturaEntity]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    roles_decorator_1.Roles('administrador', 'empleado'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DetalleFacturaController.prototype, "delete", null);
DetalleFacturaController = __decorate([
    common_1.UseGuards(session_guard_1.SessionGuard),
    common_1.Controller('detalle-factura'),
    __metadata("design:paramtypes", [detalle_factura_service_1.DetalleFacturaService])
], DetalleFacturaController);
exports.DetalleFacturaController = DetalleFacturaController;
//# sourceMappingURL=detalle-factura.controller.js.map
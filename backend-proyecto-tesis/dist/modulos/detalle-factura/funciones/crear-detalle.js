"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crear_detalle_factura_dto_1 = require("../dto/crear-detalle-factura.dto");
function crearDetalle(detalleFactura) {
    const detalleFacturaACrear = new crear_detalle_factura_dto_1.CrearDetalleFacturaDto();
    Object.keys(detalleFactura).map(atributo => {
        detalleFacturaACrear[atributo] = detalleFactura[atributo];
    });
    return detalleFacturaACrear;
}
exports.crearDetalle = crearDetalle;
//# sourceMappingURL=crear-detalle.js.map
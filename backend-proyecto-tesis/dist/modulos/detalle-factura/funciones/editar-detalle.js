"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const editar_detalle_factura_dto_1 = require("../dto/editar-detalle-factura.dto");
function editarDetalle(detalleFactura) {
    const detalleFacturaAEditar = new editar_detalle_factura_dto_1.EditarDetalleFacturaDto();
    Object.keys(detalleFactura).map(atributo => {
        detalleFacturaAEditar[atributo] = detalleFactura[atributo];
    });
    return detalleFacturaAEditar;
}
exports.editarDetalle = editarDetalle;
//# sourceMappingURL=editar-detalle.js.map
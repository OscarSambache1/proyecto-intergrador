"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CONFIG_ENVIRONMENT = {
    production: false,
    dbConnections: {
        mysql: {
            type: 'mysql',
            name: 'default',
            host: 'localhost',
            port: 3306,
            username: 'oscar',
            password: 'oscar',
            database: 'GYM',
            synchronize: true,
            retryDelay: 40000,
            retryAttempts: 3,
            connectTimeout: 40000,
            keepConnectionAlive: true,
            dropSchema: false,
            charset: 'utf8mb4',
            timezone: 'local',
            ssl: false,
        },
    },
    redisConnection: {
        port: 14327,
        pass: 'vqLLxYIBn4AEkx0QyIT0NesJCYX0WehB',
        host: 'redis-14327.c9.us-east-1-4.ec2.cloud.redislabs.com',
        db: 0,
        ttl: 60000 * 24 * 30,
    },
    expressSession: {
        secret: 'Secreto',
        cookie: { maxAge: 60000 * 24 * 30 },
        resave: true,
        saveUninitialized: false,
        unset: 'destroy',
    },
    port: 8080,
};
//# sourceMappingURL=local.js.map
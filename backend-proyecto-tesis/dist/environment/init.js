"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        let existeArchivoLocal = false;
        let config;
        try {
            config = require('./local.ts').CONFIG_ENVIRONMENT;
            existeArchivoLocal = true;
            console.log('Hay local', config);
        }
        catch (e) {
            console.log('No hay local');
        }
        if (existeArchivoLocal) {
            config_1.CONFIG_ENVIRONMENT.dbConnections = config.dbConnections;
            config_1.CONFIG_ENVIRONMENT.redisConnection = config.redisConnection;
            config_1.CONFIG_ENVIRONMENT.port = config.port;
            config_1.CONFIG_ENVIRONMENT.expressSession = config.expressSession;
        }
        else {
        }
        console.log(config_1.CONFIG_ENVIRONMENT);
    });
}
exports.init = init;
//# sourceMappingURL=init.js.map
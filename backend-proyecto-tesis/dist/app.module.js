"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const musculo_entity_1 = require("./modulos/musculo/musculo.entity");
const cliente_entity_1 = require("./modulos/cliente/cliente.entity");
const comida_entity_1 = require("./modulos/comida/comida.entity");
const comida_dieta_entity_1 = require("./modulos/comida-dieta/comida-dieta.entity");
const detalle_factura_entity_1 = require("./modulos/detalle-factura/detalle-factura.entity");
const dieta_dia_entity_1 = require("./modulos/dieta-dia/dieta-dia.entity");
const dieta_entity_1 = require("./modulos/dieta/dieta.entity");
const ejercicio_entity_1 = require("./modulos/ejercicio/ejercicio.entity");
const ejercicio_musculo_entity_1 = require("./modulos/ejercicio-musculo/ejercicio-musculo.entity");
const ejercicio_rutina_entity_1 = require("./modulos/ejercicio-rutina/ejercicio-rutina.entity");
const medida_entity_1 = require("./modulos/medida/medida.entity");
const plan_dieta_entity_1 = require("./modulos/plan-dieta/plan-dieta.entity");
const plan_dieta_cliente_entity_1 = require("./modulos/plan-dieta-cliente/plan-dieta-cliente.entity");
const plan_entrenamiento_entity_1 = require("./modulos/plan-entrenamiento/plan-entrenamiento.entity");
const plan_entrenamiento_cliente_instructor_entity_1 = require("./modulos/plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.entity");
const producto_entity_1 = require("./modulos/producto/producto.entity");
const rutina_plan_entrenamiento_entity_1 = require("./modulos/rutina-plan-entrenamiento/rutina-plan-entrenamiento.entity");
const rutina_entity_1 = require("./modulos/rutina/rutina.entity");
const cliente_module_1 = require("./modulos/cliente/cliente.module");
const instructor__module_1 = require("./modulos/instructor/instructor..module");
const usuario_entity_1 = require("./modulos/usuario/usuario.entity");
const usuario_module_1 = require("./modulos/usuario/usuario.module");
const medida_module_1 = require("./modulos/medida/medida.module");
const producto_module_1 = require("./modulos/producto/producto.module");
const musculo_module_1 = require("./modulos/musculo/musculo.module");
const ejercicio_module_1 = require("./modulos/ejercicio/ejercicio.module");
const ejercicio_musculo_module_1 = require("./modulos/ejercicio-musculo/ejercicio-musculo.module");
const rutina_module_1 = require("./modulos/rutina/rutina.module");
const ejercicio_rutina_module_1 = require("./modulos/ejercicio-rutina/ejercicio-rutina.module");
const plan_entrenamiento_module_1 = require("./modulos/plan-entrenamiento/plan-entrenamiento.module");
const plan_entrenamiento_cliente_instructor_module_1 = require("./modulos/plan-entrenamiento-cliente-instructor/plan-entrenamiento-cliente-instructor.module");
const rutina_plan_entrenamiento_module_1 = require("./modulos/rutina-plan-entrenamiento/rutina-plan-entrenamiento.module");
const comida_module_1 = require("./modulos/comida/comida.module");
const dieta_module_1 = require("./modulos/dieta/dieta.module");
const comida_dieta_module_1 = require("./modulos/comida-dieta/comida-dieta.module");
const tipo_comida_entity_1 = require("./modulos/tipo-comida/tipo-comida.entity");
const tipo_comida_module_1 = require("./modulos/tipo-comida/tipo-comida.module");
const tipo_dieta_module_1 = require("./modulos/tipo-dieta/tipo-dieta.module");
const tipo_dieta_1 = require("./modulos/tipo-dieta/tipo-dieta");
const dieta_dia_module_1 = require("./modulos/dieta-dia/dieta-dia.module");
const plan_dieta_module_1 = require("./modulos/plan-dieta/plan-dieta.module");
const plan_dieta_cliente_module_1 = require("./modulos/plan-dieta-cliente/plan-dieta-cliente.module");
const cabecera_factura_entity_1 = require("./modulos/cabecera-factura/cabecera-factura.entity");
const cabecera_factura_module_1 = require("./modulos/cabecera-factura/cabecera-factura.module");
const detalle_factura_module_1 = require("./modulos/detalle-factura/detalle-factura.module");
const clase_module_1 = require("./modulos/clase/clase.module");
const clase_emtity_1 = require("./modulos/clase/clase.emtity");
const clase_hora_module_1 = require("./modulos/clase-hora/clase-hora.module");
const clase_hora_entity_1 = require("./modulos/clase-hora/clase-hora.entity");
const dia_entity_1 = require("./modulos/dia/dia.entity");
const dia_module_1 = require("./modulos/dia/dia.module");
const clase_dia_hora_module_1 = require("./modulos/clase-hora-dia/clase-dia-hora.module");
const clase_dia_hora_entity_1 = require("./modulos/clase-hora-dia/clase-dia-hora.entity");
const config_1 = require("./environment/config");
const auth_module_1 = require("./modulos/auth/auth.module");
const instructor_entity_1 = require("./modulos/instructor/instructor.entity");
let AppModule = class AppModule {
    constructor() {
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.type,
                name: 'default',
                host: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.host,
                port: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.port,
                username: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.username,
                password: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.password,
                database: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.database,
                entities: [
                    cliente_entity_1.ClienteEntity,
                    cabecera_factura_entity_1.CabeceraFacturaEntity,
                    comida_entity_1.ComidaEntity,
                    comida_dieta_entity_1.ComidaDietaEntity,
                    detalle_factura_entity_1.DetalleFacturaEntity,
                    dieta_dia_entity_1.DietaDiaEntity,
                    dieta_entity_1.DietaEntity,
                    ejercicio_entity_1.EjercicioEntity,
                    ejercicio_musculo_entity_1.EjercicioMusculoEntity,
                    ejercicio_rutina_entity_1.EjercicioRutinaEntity,
                    instructor_entity_1.InstructorEntity,
                    medida_entity_1.MedidaEntity,
                    musculo_entity_1.MusculoEntity,
                    plan_dieta_entity_1.PlanDietaEntity,
                    plan_dieta_cliente_entity_1.PlanDietaClienteEntity,
                    plan_entrenamiento_entity_1.PlanEntrenamientoEntity,
                    plan_entrenamiento_cliente_instructor_entity_1.PlanEntrenamientoClienteInstructorEntity,
                    producto_entity_1.ProductoEntity,
                    rutina_entity_1.RutinaEntity,
                    rutina_plan_entrenamiento_entity_1.RutinaPlanEntrenamientoEntity,
                    usuario_entity_1.UsuarioEntity,
                    tipo_comida_entity_1.TipoComidaEntity,
                    tipo_dieta_1.TipoDietaEntity,
                    clase_emtity_1.ClaseEntity,
                    clase_hora_entity_1.ClaseHoraEntity,
                    clase_dia_hora_entity_1.ClaseDiaHoraEntity,
                    dia_entity_1.DiaEntity,
                ],
                synchronize: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.synchronize,
                ssl: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.ssl,
                keepConnectionAlive: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.keepConnectionAlive,
                retryDelay: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.retryDelay,
                dropSchema: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.dropSchema,
                retryAttempts: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.retryAttempts,
                connectTimeout: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.connectTimeout,
                charset: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.charset,
                timezone: config_1.CONFIG_ENVIRONMENT.dbConnections.mysql.timezone,
            }),
            auth_module_1.AuthModule,
            cliente_module_1.ClienteModule,
            instructor__module_1.InstructorModule,
            usuario_module_1.UsuarioModule,
            medida_module_1.MedidaModule,
            producto_module_1.ProductoModule,
            musculo_module_1.MusculoModule,
            ejercicio_module_1.EjercicioModule,
            ejercicio_musculo_module_1.EjercicioMusculoModule,
            rutina_module_1.RutinaModule,
            ejercicio_rutina_module_1.EjercicioRutinaModule,
            plan_entrenamiento_module_1.PlanEntrenamientoModule,
            plan_entrenamiento_cliente_instructor_module_1.PlanEntrenamientoClienteInstructorModule,
            rutina_plan_entrenamiento_module_1.RutinaPlanEntrenamientoModule,
            comida_module_1.ComidaModule,
            dieta_module_1.DietaModule,
            comida_dieta_module_1.ComidaDietaModule,
            tipo_comida_module_1.TipoComidaModule,
            tipo_dieta_module_1.TipoDietaModule,
            dieta_dia_module_1.DietaDiaModule,
            plan_dieta_module_1.PlanDietaModule,
            plan_dieta_cliente_module_1.PlanDietaClienteModule,
            cabecera_factura_module_1.CabeceraFacturaModule,
            detalle_factura_module_1.DetalleFacturaModule,
            clase_module_1.ClaseModule,
            clase_hora_module_1.ClaseHoraModule,
            dia_module_1.DiaModule,
            clase_dia_hora_module_1.ClaseDiaHoraModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
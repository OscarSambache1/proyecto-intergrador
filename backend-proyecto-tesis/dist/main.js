"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const session = require("express-session");
const config_1 = require("./environment/config");
const init_1 = require("./environment/init");
const passport = require("passport");
init_1.init();
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        const RedisStore = require('connect-redis')(session);
        app.use(session({
            secret: config_1.CONFIG_ENVIRONMENT.expressSession.secret,
            cookie: config_1.CONFIG_ENVIRONMENT.expressSession.cookie,
            resave: config_1.CONFIG_ENVIRONMENT.expressSession.resave,
            saveUninitialized: config_1.CONFIG_ENVIRONMENT.expressSession.saveUninitialized,
        }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.useStaticAssets(__dirname + '/../public');
        app.enableCors();
        yield app.listen(config_1.CONFIG_ENVIRONMENT.port);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map
export interface Paginacion {
    skip: number;
    limite: number;
    consulta: object;
}

import { Paginacion } from 'interfaces/paginacion';
import { PrincipalService } from './servicion-principal.service';
export declare class PrincipakController<Entidad, CrearDto, EditarDto> {
    private readonly _principalService;
    constructor(_principalService: PrincipalService<Entidad, CrearDto, EditarDto>);
    findAll(objetosPaginacion: Paginacion): Promise<Entidad[]>;
    findOne(id: any): Promise<Entidad>;
    crear(cliente: CrearDto): Promise<Entidad>;
    editar(id: number, cliente: EditarDto): Promise<Entidad>;
    eliminar(id: number): Promise<String>;
}

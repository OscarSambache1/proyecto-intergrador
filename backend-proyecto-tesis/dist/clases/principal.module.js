"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const servicion_principal_service_1 = require("./servicion-principal.service");
const typeorm_2 = require("typeorm");
let PrincipalModule = class PrincipalModule {
};
PrincipalModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([typeorm_2.Entity], 'default')],
        controllers: [],
        providers: [servicion_principal_service_1.PrincipalService],
        exports: [servicion_principal_service_1.PrincipalService],
    })
], PrincipalModule);
exports.PrincipalModule = PrincipalModule;
//# sourceMappingURL=principal.module.js.map
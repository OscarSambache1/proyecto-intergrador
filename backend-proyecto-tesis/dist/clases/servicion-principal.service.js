"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
let PrincipalService = class PrincipalService {
    constructor(_entidadRepository) {
        this._entidadRepository = _entidadRepository;
    }
    findAll(skip, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._entidadRepository.find({
                order: {
                    id: 'DESC',
                },
                skip,
                take: limit,
            });
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._entidadRepository.findOne(id);
        });
    }
    createOne(elemento) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._entidadRepository.save(this._entidadRepository.create(elemento));
        });
    }
    update(id, elemento) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._entidadRepository.update(id, elemento);
            return yield this.findById(id);
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const elementoAEliminar = yield this.findById(id);
            yield this._entidadRepository.remove(elementoAEliminar);
            return `Se ha eliminado el elemento ${id}`;
        });
    }
    contar(consulta) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._entidadRepository.count({
                where: consulta,
            });
        });
    }
};
PrincipalService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(typeorm_2.Entity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], PrincipalService);
exports.PrincipalService = PrincipalService;
//# sourceMappingURL=servicion-principal.service.js.map
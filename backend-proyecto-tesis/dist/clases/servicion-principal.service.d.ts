import { Repository } from 'typeorm';
export declare class PrincipalService<Entidad, CrearDto, EditarDto> {
    private readonly _entidadRepository;
    constructor(_entidadRepository: Repository<any>);
    findAll(skip: number, limit: number): Promise<Entidad[]>;
    findById(id: number): Promise<Entidad>;
    createOne(elemento: CrearDto): Promise<Entidad>;
    update(id: number, elemento: EditarDto): Promise<Entidad>;
    delete(id: number): Promise<string>;
    contar(consulta: object): Promise<number>;
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function crearElemento(cliente) {
    const clienteACrear = {};
    Object.keys(cliente).map(atributo => {
        clienteACrear[atributo] = cliente[atributo];
    });
    return clienteACrear;
}
exports.crearElemento = crearElemento;
//# sourceMappingURL=crear-elemento.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function editarElemento(cliente) {
    const elementoAEditar = {};
    Object.keys(cliente).map(atributo => {
        elementoAEditar[atributo] = cliente[atributo];
    });
    return elementoAEditar;
}
exports.editarElemento = editarElemento;
//# sourceMappingURL=editar-elemento.js.map